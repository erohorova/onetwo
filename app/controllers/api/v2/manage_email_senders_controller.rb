# frozen_string_literal: true

class Api::V2::ManageEmailSendersController < Api::V2::BaseController
  include ManageSenderConcern

  has_limit_offset_constraints only: [:index]

  before_action :check_user_permissions
  before_action :set_email_sender, only: %i[
    destroy update show verify_domain
  ]

  resource_description do
    api_version 'v2'
    formats ['json']
  end

  api :GET, 'api/v2/manage_email_senders', 'Return list of organization email senders'
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  def index
    @email_senders = current_org.mailer_configs.from_lxp_admin

    @total = @email_senders.count
    @email_senders = @email_senders.limit(limit).offset(offset)
  end

  api :GET, '/api/v2/manage_email_senders/:id', 'Get the email sender details'
  param :id, Integer, desc: 'Mailer config ID', required: true
  def show
    # return details for the mailer config
  end

  api :POST, 'api/v2/manage_email_senders', 'Create an email sender'
  param :mailer_config, Hash, required: true do
    param :from_name, String, desc: 'From name of sender', required: true
    param :from_address, String, desc: 'From email of sender', required: true
  end
  def create
    @email_sender = current_org.mailer_configs.build(mailer_config_params)
    @email_sender.attach_default_values
    if @email_sender.save
      render 'show'
    else
      render_unprocessable_entity(
        @email_sender.errors.full_messages.join(', ')
      )
    end
  end

  api :PUT, 'api/v2/manage_email_senders/:id', 'Update the organization email sender'
  param :id, Integer, desc: 'Email sender ID', required: true
  param :mailer_config, Hash, required: true do
    param :from_name, String, desc: 'From name of sender', required: true
    param :is_active, String, desc: 'Should use for sending organization emails', required: false
  end
  def update
    if !(@email_sender.verification.present? && @email_sender.verification[:domain])
      params[:mailer_config][:is_active] = false
    end

    if (mailer_config_params[:is_active]&.to_bool && !@email_sender.is_active)
      MailerConfig.disable_active_sender_email(current_org)
    end

    @email_sender.invalidate_cache = true
    if @email_sender.update_attributes(mailer_config_params)
      render 'show'
    else
      render_unprocessable_entity(
        @email_sender.errors.full_messages.join(', ')
      )
    end
  end

  api :DELETE, 'api/v2/manage_email_senders/:id', 'Delete the organization email sender'
  param :id, Integer, desc: 'Email sender ID', required: true
  def destroy
    if @email_sender.destroy
      head :no_content
    else
      render_unprocessable_entity(
        @email_sender.errors.full_messages.join(', ')
      )
    end
  end

  api :GET, 'api/v2/manage_email_senders/:id/verify_domain', 'verify email address'
  param :id, Integer, desc: 'Email sender ID', required: true
  def verify_domain
    @email_sender.verify_domain unless @email_sender.verification[:domain]
    render json: {message: "Domain verification request has been sent, please wait for sometime"}, status: :ok
  end

end

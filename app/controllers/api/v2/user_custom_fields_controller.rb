class Api::V2::UserCustomFieldsController < Api::V2::BaseController
  before_action :require_organization_admin, only: [:update_all]

  has_limit_offset_constraints only: [:user_custom_fields_data], max_limit: 500

  api :GET, 'api/v2/user_custom_fields', 'Lists all user custom fields'
  formats ['json']
  api_version 'v2'
  param :user_ids, Array, desc: 'List of all user ids'
  param :send_array, String, desc: 'Send array as response'
  param :show_in_profile, [true,false]
  param :editable_by_users, [true,false]
  def index
    @users = current_org.users
    @users = @users.where(id: params[:user_ids]) if params[:user_ids].present?
    user_custom_fields = UserCustomField.where(user: @users).includes(:custom_field)

    filter_names = []
    filter_names.push('show_in_profile') if params['show_in_profile']&.to_bool
    filter_names.push('editable_by_users') if params['editable_by_users']&.to_bool
    user_custom_fields = user_custom_fields.filter_by_configs(filter_names) if filter_names.any?

    @custom_fields_hash = user_custom_fields.group_by(&:user_id)
      .map { |user_id, fields| [user_id, fields] }
      .to_h
  end

  # need pagination and delta support for request coming from workday group connector
  api :GET, 'api/v2/user_custom_fields_data', 'Lists paginated user custom fields'
  formats ['json']
  api_version 'v2'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10; max 500', required: false
  param :last_fetched_at, String, desc: 'Fetch custom fields only after last_fetched_at', required: false
  def user_custom_fields_data
    custom_field_users = User.select(:id)
      .where(organization_id: current_org.id, status: User::ACTIVE_STATUS)
      .joins("RIGHT JOIN user_custom_fields ON user_custom_fields.user_id = users.id")
      .includes(:assigned_custom_fields).distinct('users.id')

    if (@last_fetch_date = Date.parse(params[:last_fetched_at]) rescue nil ).present?
      custom_field_users = custom_field_users.where('user_custom_fields.updated_at > ?', @last_fetch_date.beginning_of_day)
    end

    custom_field_users = custom_field_users.limit(limit).offset(offset)

    @custom_fields_hash = custom_field_users
      .map { |user| [user.id, user.assigned_custom_fields] }
      .to_h
  end


  api :POST, 'api/v2/user_custom_fields/update_all', 'Update all user custom fields in an organization only by admin.'

  param :user_custom_fields, Hash, desc: 'user_custom_fields', required: true do
    param :user_id, Integer, desc: 'user id', required: true
    param :custom_fields, Array do
      param :value, String, desc: 'value', required: true
      param :custom_field_id, Integer, desc: 'user id', required: true
    end
  end

  formats ['json']
  api_version 'v2'

  # {
  #   "user_custom_fields":{
  #                           "user_id":438,
  #                           "custom_fields":[
  #                                              {
  #                                                "custom_field_id":1,
  #                                                "value":"1dd222ddd1"
  #                                              },
  #                                              {
  #                                                "custom_field_id":2,
  #                                                "value":"22"
  #                                              },
  #                                              {
  #                                                "custom_field_id":3,
  #                                                "value":"33"
  #                                              },
  #                                              {
  #                                                "custom_field_id":4,
  #                                                "value":"44"
  #                                              }
  #                                            ]
  #                         }
  # }

  # this is wrong way of updating user_custom_fields,
  # it should have been with user nested attributes
  # doing it because of less time,

  def update_all
    @user = current_org.users.find_by(id: user_custom_field_params[:user_id])

    return render_unprocessable_entity('user_id is not valid') if @user.blank?
    @message = {}
    @user_custom_fields = []
    initialize_custom_fields(user_custom_field_params)
  end

  api :POST, 'api/v2/user_custom_fields', 'Add custom field value for user'
  param :custom_field_id, String, desc: 'value', required: true
  param :value, String, desc: 'value', required: true
  formats ['json']
  api_version 'v2'
  def create
    return render_bad_request if (!params[:custom_field_id].present? || !params[:value].present?)
    @custom_field = current_org.custom_fields.find_by(id: params[:custom_field_id])
    return render_unprocessable_entity unless @custom_field.present?

    @user_custom_fields = current_user.assigned_custom_fields.where(custom_field_id: @custom_field.id).first_or_initialize
    @user_custom_fields.value = params[:value]
    if @user_custom_fields.save
      head :ok
    else
      render_unprocessable_entity(message: @user_custom_fields.full_errors)
    end
  end

  api :DELETE, 'api/v2/user_custom_fields/:id', 'Delete user custom field for user'
  formats ['json']
  api_version 'v2'
  def destroy
    @user_custom_fields = current_user.assigned_custom_fields.find_by(id: params[:id])
    return render_not_found('User custom field not found') unless @user_custom_fields.present?

    @user_custom_fields.destroy
    head :ok
  end


  private

  def user_custom_field_params
    params.require(:user_custom_fields).permit(:user_id, custom_fields: [:custom_field_id, :value])
  end

  def initialize_custom_fields(args = {})
    args[:custom_fields].each_with_index do |custom_field, index|
      user_custom_field = UserCustomField.find_or_initialize_by(
        user_id: args[:user_id], custom_field_id: custom_field[:custom_field_id]
      )

      if custom_field[:value].present?
        user_custom_field.attributes = { value: custom_field[:value] }
        @message[index] = user_custom_field.full_errors unless user_custom_field.save
        @user_custom_fields.push(user_custom_field)
      else
        @message[index] = user_custom_field.full_errors unless user_custom_field.destroy
      end
    end
  end
end

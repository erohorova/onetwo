class Api::V2::LeapsController < Api::V2::BaseController

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  before_action :load_card, only: [ :create, :index, :update, :destroy]
  before_action :load_leap, only: [:update, :destroy]

  api :POST, 'api/v2/cards/:card_id/leaps', "Create leap on a card"
  param :card_id, Integer, desc: "Card ID", required: true
  param :wrong_id, Integer, desc: "Card ID where leap will be implemented if answers is wrong", required: true
  param :correct_id, Integer, desc: "Card ID where leap will be implemented if answers is correct", required: true
  param :pathway_id, Integer, desc: "ID of pack card where quiz is present", required: false
  authorize :create, Permissions::CREATE_LEAP
  def create
    @leap = @card.leaps.build(leap_params)
    if @leap.save
      head :ok
    else
      render_unprocessable_entity(@leap.errors.full_messages.join(", "))
    end
  end

  api :PUT, 'api/v2/cards/:card_id/leaps/:id', "Update leap"
  param :id, Integer, desc: "Leap ID", required: true
  param :card_id, Integer, desc: "Card ID", required: true
  param :wrong_id, Integer, desc: "Card ID where leap will be implemented if answers is wrong", required: true
  param :correct_id, Integer, desc: "Card ID where leap will be implemented if answers is correct", required: true
  authorize :update, Permissions::UPDATE_LEAP
  def update
    if @leap.update_attributes(leap_params)
      head :ok
    else
      render_unprocessable_entity(@leap.errors.full_messages.join(", "))
    end
  end

  api :DELETE, 'api/v2/cards/:card_id/leaps/:id', 'Delete a leap on card'
  param :card_id, Integer, desc: "Card ID", required: true
  param :id, Integer, desc: "Leap ID", required: true
  authorize :destroy, Permissions::DELETE_LEAP
  def destroy
    if @leap.destroy
      head :ok
    else
      render_unprocessable_entity(@leap.errors.full_messages)
    end
  end

  private

  def load_leap
    @leap = @card.leaps.find_by(id: params[:id])
    return render_not_found('Leap not found') unless @leap.present?
  end

  def load_card
    @card = load_card_from_compound_id(params[:card_id])
  end

  def leap_params
    leap_params = params.require(:leap).permit(:card_id, :pathway_id, :correct_id, :wrong_id)
    leap_params.each do |k, v|
      next unless Card.is_ecl_id?(v.to_s)
      leap_params[k] = load_card_from_ecl_id(v)&.id
    end
  end
end

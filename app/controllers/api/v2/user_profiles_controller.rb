# DEPRECATED
class Api::V2::UserProfilesController < Api::V2::BaseController
  api :POST, 'api/v2/user_profile', 'insert/update profile details'
  param :profile_attributes, Hash, desc: 'Param description to save profile' do
    param :expert_topics, String, desc: "Collection of hashes of expert topics", required: false
    param :learning_topics, String, desc: 'Collection of hashes of learning topics', required: false
    param :dashboard_info, String, desc: 'Collection of dashboard template information', required: false
    param :time_zone, String, desc: "Time zone of user, example 'Asia/Tokyo'", required: false
    param :tac_accepted, String, desc: "Boolean, 'true' if user accepted terms & conditions ", required: false

  end
  formats ['json']
  api_version 'v2'
  def create
    profile = UserProfile.find_or_initialize_by(user_id: current_user.id)

    # refer: http://stackoverflow.com/questions/20164354/rails-strong-parameters-with-empty-arrays
    profile_params[:expert_topics] ||= [] if profile_params.has_key?(:expert_topics)

    if profile.update(profile_params)
      head :ok
    else
      head :unprocessable_entity
    end
  end

  private
    def profile_params
      params
        .require(:profile_attributes)
        .permit(
          :id, :time_zone, :tac_accepted,
          { expert_topics: [:topic_name, :topic_id, :topic_label, :domain_name, :domain_id, :domain_label] },
          { learning_topics: [:topic_name, :topic_id, :topic_label, :domain_name, :domain_id, :domain_label] },
          { dashboard_info: [:name, :visible] }
        )
    end
end

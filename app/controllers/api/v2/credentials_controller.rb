class Api::V2::CredentialsController < Api::V2::BaseController

  before_action :require_superadmin, only: :destroy
  before_action :require_organization_admin, only: :index

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized to access channel'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/v2/credentials', "Returns developer API credentials"
  description <<-EOS
    This will return dev api credentials
  EOS
  def index
    @credentials = current_org.developer_api_credentials
  end

  api :POST, 'api/v2/credentials', 'Create new developer api credential'
  description <<-EOS
    This will create new developer api credential and return details
  EOS
  formats ['json']
  def create
    developer = current_user.get_user_permissions.include?('DEVELOPER')
    super_admin = current_user.is_edcast_dot_com_user?
    if developer || super_admin
      @credential = current_org
        .developer_api_credentials
        .build(creator: current_user)
    else
      return render_unauthorized
    end

    unless @credential.save
      render_unprocessable_entity('Error while generating credentials') and return
    end
    render 'show'
  end

  api :DELETE, 'api/v2/credentials/:id', "Delete developer api credential"
  param :id, Integer, desc: 'Id of the credential', required: true
  description <<-EOS
    This will delete developer api credential
  EOS
  def destroy
    credential = current_org.developer_api_credentials.find(params[:id])

    if credential.destroy
      head :ok and return
    else
      render_unprocessable_entity(credential.errors.full_messages.join(",")) and return
    end

  end
end

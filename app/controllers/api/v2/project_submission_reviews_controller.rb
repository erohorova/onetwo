class Api::V2::ProjectSubmissionReviewsController < Api::V2::BaseController
  include TrackActions

  has_limit_offset_constraints only: [:index], limit_default: 15, offset_default: 0
  before_action :find_project_submission, only: [:create, :index]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized to access channel'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :POST, 'api/v2/project_submissions/:project_submission_id/project_submission_reviews',
      'create project submission review for a project submission.'
  description <<-EOS
      Create a project submission review for a project submission
  EOS
  param :project_submission_id, Integer, desc: 'Id of the project submission', required: true
  param :description, String, desc: 'description of the submission', required: true
  param :status, Integer, desc: 'Status of the review of the project submission can be approved or rejected', required: true
  def create
    @review = @project_submission.project_submission_reviews.build(project_submission_review_params)
    unless @review.save
      return render_unprocessable_entity(@review.full_errors)
    end
    if @review.reload.approved?
      card = @review.project_submission.project.card
      submitter_user = @review.project_submission.submitter
      card.mark_as_complete(user: submitter_user)
      track_mark_as_completion(card)
    end
  end

  api :GET, 'api/v2/project_submissions/:project_submission_id/project_submission_reviews',
      'list of all the project submission reviews of a project submission'
  description <<-EOS
    List of project submission reviews for a project_submission
  EOS
  param :project_submission_id, Integer, desc: 'Id of the project', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 15', required: false
  def index
    if params[:project_submission_id].present?
      reviews = @project_submission.project_submission_reviews
    else
      reviews = current_user.project_submission_reviews
    end
    @reviews_count = reviews.count
    @reviews = reviews.limit(limit).offset(offset)
    head :no_content if @reviews.blank?
  end

  private
  def find_project_submission
    @project_submission = ProjectSubmission.find_by(id: params[:project_submission_id])
    render_not_found('Content not found') and return if @project_submission.blank?
  end

  def project_submission_review_params
    review_params = params.require(:project_submission_review).permit(
      :description, :status, :project_submission_id, :reviewer_id)
    review_params.merge!(reviewer_id: current_user.id)
  end
end

class Api::V2::OrgSsosController < Api::V2::BaseController
  before_action :require_organization_admin

  before_action :set_sso, only: [:create]
  before_action :set_org_sso, only: [:toggle, :update]

  api :POST, 'api/v2/org_ssos', 'Add a SSO in an organization'
  param :sso_name, String, desc: 'Intended SSO name', required: true
  param :org_sso, Hash, desc: 'Org Sso' do
    param :idp, String, desc: 'IDP id provided', required: true
    param :label_name, String, desc: 'Configure label name', required: true
    param :oauth_authorize_url, String, desc: 'OAuth authorize URL', required: true
    param :oauth_client_id, String, desc: 'OAuth Client ID', required: true
    param :oauth_client_secret, String, desc: 'OAuth Client Secret', required: true
    param :oauth_login_url, String, desc: 'OAuth Login URL', required: true
    param :oauth_redirect_uri, String, desc: 'OAuth Redirect URI', required: true
    param :oauth_scopes, String, desc: 'OAuth Scopes', required: true
    param :oauth_token_url, String, desc: 'OAuth Token URL', required: true
    param :oauth_user_info_url, String, desc: 'OAuth User Info URL', required: true
    param :provider_name, String, desc: 'Provider name', required: true
  end
  formats ['json']
  api_version 'v2'
  def create
    @org_sso = @sso.org_ssos.build(org_sso_params)
    @org_sso.organization = current_org

    unless @org_sso.save
      render_unprocessable_entity(@org_sso.full_errors) and return
    end
  end

  api :PUT, 'api/v2/org_ssos/:id', 'Update a SSO in an organization'
  param :id, Integer, desc: 'Id of an Org Sso', required: true
  param :org_sso, Hash, desc: 'Org Sso' do
    param :idp, String, desc: 'IDP id provided', required: false
    param :label_name, String, desc: 'Configure label name', required: false
    param :oauth_authorize_url, String, desc: 'OAuth authorize URL', required: true
    param :oauth_client_id, String, desc: 'OAuth Client ID', required: true
    param :oauth_client_secret, String, desc: 'OAuth Client Secret', required: true
    param :oauth_login_url, String, desc: 'OAuth Login URL', required: true
    param :oauth_redirect_uri, String, desc: 'OAuth Redirect URI', required: true
    param :oauth_scopes, String, desc: 'OAuth Scopes', required: false
    param :oauth_token_url, String, desc: 'OAuth Token URL', required: true
    param :oauth_user_info_url, String, desc: 'OAuth User Info URL', required: true
    param :provider_name, String, desc: 'Provider name', required: false
  end
  formats ['json']
  api_version 'v2'
  def update
    unless @org_sso.update(org_sso_params)
      render_unprocessable_entity(@org_sso.full_errors) and return
    end
  end

  api :PUT, 'api/v2/org_ssos/:id/toggle', 'Enable/Disable Org Sso'
  param :id, Integer, desc: 'Id of an Org Sso', required: true
  param :org_sso, Hash, desc: 'Org Sso' do
    param :is_enabled, String, desc: 'True or False', required: true
  end
  def toggle
    switch = params[:org_sso][:is_enabled].try(:to_bool) || false

    @org_sso.update(is_enabled: switch)
  end

  api :PUT, 'api/v2/org_ssos/batch_update', 'Batch update or create org_ssos'
  param :update_org_ssos, Array, desc: 'Array of objects Org SSO' do
    param :id, Integer, desc: 'Id of an Org Sso', required: true
    param :is_enabled, String, desc: 'true or false', required: true
    param :position, Integer, desc: 'Org SSO position', requred: true
  end
  param :create_org_ssos, Array, desc: 'Array of objects Org SSO' do
    param :sso_name, Integer, desc: 'Name of Sso', required: true
    param :is_enabled, String, desc: 'true or false', required: true
    param :position, Integer, desc: 'Org SSO position', requred: true
  end
  def batch_update
    update_org_ssos = params['update_org_ssos']
    create_org_ssos = params['create_org_ssos']
    if update_org_ssos.present?
      unless update_org_ssos.is_a?(Array)
        render_unprocessable_entity('params update_org_ssos must be Array') and return
      end

      update_org_ssos = update_org_ssos.select { |org_sso| org_sso if org_sso['id'] }
      update_message_error = create_or_update_org_sso(objects: update_org_ssos, action: 'update')
    end

    if create_org_ssos.present?
      unless create_org_ssos.is_a?(Array)
        render_unprocessable_entity('params create_org_ssos must be Array') and return
      end

      create_org_ssos = create_org_ssos.select { |org_sso| org_sso if org_sso['sso_name'] }
      create_message_error = create_or_update_org_sso(objects: create_org_ssos, action: 'create')
    end

    render json: {
      create_org_ssos: create_message_error.blank? ? true : create_message_error,
      update_org_ssos: update_message_error.blank? ? true : update_message_error
    }
  end

  private
    def org_sso_params
      params
        .require(:org_sso)
        .permit(
          :idp,
          :label_name,
          :oauth_authorize_url,
          :oauth_client_id,
          :oauth_client_secret,
          :oauth_login_url,
          :oauth_redirect_uri,
          :oauth_scopes,
          :oauth_token_url,
          :oauth_user_info_url,
          :provider_name,
          :is_enabled,
          :saml_assertion_consumer_service_url,
          :saml_idp_cert,
          :saml_idp_sso_target_url,
          :saml_issuer,
          :is_visible
        )
    end

    def set_sso
      if params[:sso_name].blank?
        render_unauthorized('Set SSO Id') and return
      end

      @sso = Sso.find_by(name: params[:sso_name])

      if @sso.nil?
        render_unauthorized('No such SSO configured') and return
      end
    end

    def set_org_sso
      if params[:id].blank?
        render_unauthorized('No org sso id') and return
      end

      @org_sso = current_org.org_ssos.find_by(id: params[:id])

      if @org_sso.nil?
        render_unauthorized('No such SSO configured') and return
      end
    end

    def create_or_update_org_sso(objects:,  action:)
      message = []
      objects.each_with_index do |object, index|
        org_sso = if action == 'create'
          sso = Sso.find_by(name: object['sso_name'])
          unless sso
            message << [index, 'Sso record not found']
            next
          end
          current_org.org_ssos.joins(:sso).where(sso: sso).first_or_initialize
        else
          current_org.org_ssos.find_by(id: object['id'])
        end
        unless org_sso
          message << [index, 'OrgSso record not found']
          next
        end

        org_sso.is_enabled = object['is_enabled'] || false
        org_sso.position = object['position'] || current_org.org_ssos.count + 1

        unless org_sso.save
          message << [index, org_sso.errors.full_messages.join(", ")]
        end
      end
      message.to_h
    end
end

require 'jwt_auth'
class Api::V2::BaseController < ActionController::Base
  respond_to :json

  resource_description do
    api_version 'v2'
  end

  include NativeAppDetection
  include DetectPlatform
  include ApplicationConcern
  include LimitOffsetConcern
  include CompoundIdReader
  include EntityLoader
  include AuthenticationHelper

  skip_before_filter :verify_authenticity_token # No need to check for CSRF token for json apis
  protect_from_forgery with: :null_session

  before_filter :authenticate_user_from_token!
  before_filter :set_request_store
  before_filter :set_cache_headers
  before_action :set_paper_trail_whodunnit

  helper_method :is_native_app?

  def replica_database(&block)
    Octopus.using(:replica1, &block)
  end

  protected

  def verify_channel_curator
    if @channel && !@channel.is_curator?(current_user)
      render_unauthorized("You don't have access to this channel") and return
    end
  end

  def self.authorize(action_name, permission)
    before_action -> { authorize_permission(permission) }, only: [action_name]
  end

  def require_card_access
    # First check cover card's visibility for hidden cards, then check individual card's visibility
    if @card.hidden? && @card.cover_card.present? && !@card.cover_card.visible_to_user(current_user)
      return render_not_found("You are not authorized to access this card")
    end
    return render_not_found("You are not authorized to access this card") unless @card.visible_to_user(current_user)
  end

  def authorize_permission(permission)
    if current_org.enable_role_based_authorization #due to curators and collaborator roles we still need this
      render_unauthorized('unauthorized action') unless current_user.authorize?(permission)
    end
  end

  #ecl authentication for webhook
  def authenticate_ecl_with_jwt
    begin
      @jwt_payload,@jwt_header = JWT.decode(request.headers["X-Api-Token"], Settings.features.integrations.secret)
    rescue => e
      render_unauthorized('unauthorized action') and return
    end
  end

  protected
  def set_request_store
    RequestStore.store[:request_metadata] = get_metadata_payload
  end

  private

  def get_metadata_payload
    {
      user_id:        current_user&.id,
      platform:       get_platform,
      user_agent:     request.headers.env['HTTP_USER_AGENT'],
      platform_version_number: request.headers.env['HTTP_X_VERSION_NUMBER'],
      is_admin_request: ApplicationController.is_admin_request?(request)
    }
  end

  rescue_from(ActionController::ParameterMissing) do |exception|
    render_bad_request(exception.message)
  end

  rescue_from(ActiveRecord::RecordNotFound) do |exception|
    render_not_found('Record not found')
  end
end

class Api::V2::OrganizationsController < Api::V2::BaseController
  include AnalyticsPeriodOffsetConcern
  include SortOrderExtractor
  include UserLeaderboardV2Concern
  include OrganizationConcern

  has_limit_offset_constraints only: [
    :leaderboard,     :leaderboard_v2,
    :content_metrics, :assessments,
    :teams,           :channels,
    :badges,          :shareable_channels,
    :shared_channels, :cloned_channels
  ]
  extract_analytics_period_offsets only: [:leaderboard]

  skip_before_filter :authenticate_user_from_token!, only: [:show, :create_setting, :update_setting]
  skip_before_filter :set_request_store, only: [:create_setting, :update_setting]
  before_action :authenticate_superadmin_from_token!, only: [:create_setting, :update_setting]

  before_action :require_organization_admin, only: [
    :create_setting,          :update_setting,
    :content_metrics,         :list_ssos,
    :enable_sso,              :disable_sso,
    :get_notification_config, :update_notification_config,
    :update,                  :settings,
    :teams,                   :channel_performance,
    :shareable_channels,      :shared_channels,
    :cloned_channels
  ]

  before_action :require_org_or_team_admin, only: [:metrics]
  before_action :require_leaderboard_access, only: [:leaderboard, :leaderboard_v2]

  before_action -> { check_exclude_group_permission(Permissions::MANAGE_GROUP_ANALYTICS) }, only: [
    :assessments, :content_metrics, :metrics
  ]
  before_action :validate_quarter_dates, only: [:update]
  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  # See app/controllers/concerns/leaderboard_v2_concern.rb
  def leaderboard_v2
    show_leaderboard
  end

  api :GET, 'api/v2/organizations/details', 'Get organization details by host name. Host name is inferred from the subdomain of the URL. For example, if making an api call to ge.edcast.com/api/v2/organizations/details, this API responds with the details for the organization with host name \'ge\''
  def show
    set_current_org_using_host_name
    @organization = @current_org
    render_not_found and return if @organization.nil?

    if params.has_key?(:fields).present?
      fields = OrganizationBridge.new([current_org], fields: params[:fields]).attributes[0]
      render json: fields and return
    else
      @org_profile = @organization.organization_profile
      @org_subscriptions = @organization.org_subscriptions.includes(:prices)
    end
  end

  api :PUT, 'api/v2/organizations/details', 'Update organization details'
  param :organization, Hash, desc: 'Org parameters', required: true do
    param :name, String, desc: 'Name of new org', required: true
    param :description, String, desc: 'Description for new org', required: true
    param :is_registrable, String, desc: 'Allow open registration or invite only', required: false
    param :image, String, desc: 'Banner image', required: false
    param :mobile_image, String, desc: 'Banner image for mobile display', required: false
    param :co_branding_logo, String, desc: 'Co-Branding logo for an Org', required: false
    param :show_sub_brand_on_login, :bool, desc: 'Show or hide the Sub-Brand image on Login page', required: false
    param :configs_attributes, Hash, desc: 'Config settings for organization', required: false
    param :enable_role_based_authorization, String, desc: 'Boolean value. Configuration to enable role based authorization in organization. Default: false', required: false
    param :enable_analytics_reporting, String, desc: 'Boolean value. configuration to enable analytics reporting. Default: false', required: false
    param :custom_domain, String, desc: 'Custom domain Ex. learn.something.com', required: false
    param :quarters_attributes, Hash, desc: 'Custom Quarters for organization', required: false

  end
  param :send_analytics_reporting, String, desc: 'Send reporting email after configuration is saved', required: false
  def update
    @organization = @current_org
    org_attrs = prepare_organization_params(organization_params)

    unless @organization.update(org_attrs)
      render_unprocessable_entity(@organization.errors.full_messages.join(", ")) and return
    end

    if @organization.enable_analytics_reporting && params[:send_analytics_reporting].to_bool
      settings = @organization.get_settings_for('AnalyticsReportingConfig') || {}

      OrganizationAnalyticsJob.perform_later(@organization.id) if settings[:summary_feed]
      UserContentAnalyticsJob.perform_later(@organization.id) if settings[:content_activity_feed]
      SocialAnalyticsReportJob.perform_later(@organization.id) if settings[:social_activity_feed]
    end
  end

  api :GET, 'api/v2/organizations/teams', 'Return all teams of org'
  param :q, String, desc: 'Filter teams by this search term', required: false
  param :team_ids, Array, desc: 'Search teams within particular team ids only', required: false
  param :is_everyone, String, 'Attribute to get only everyone team of an org', required: false
  param :sort, String, 'Attribute name to sort by, ex title, created_at, default: created_at', required: false
  param :order, String, 'Sort order, asc or desc, default: desc', required: false
  param :limit, Integer, desc: 'limit for pagination; default 10', required: false
  param :offset, Integer, desc: 'offset for pagination; default 0', required: false
  param :exclude_team_ids, Array, desc: 'Team ids to exclude from teams search', required: false
  def teams
    order_attr, order = extract_sort_order(valid_order_attrs: Team.column_names , default_order_attr: "created_at",
      order: params[:order], order_attr: params[:sort])
    sort_query = "teams.#{order_attr} #{order}"
    query = current_org.teams

    # this condition is to be triggered only when the current user is just a group sub admin
    # not in case the current_user is the sub admin or the admin
    # in that case, they should be able to see the unfiltered set of teams.
    if current_user.is_group_sub_admin? && !current_user.is_org_admin? && !current_user.is_sub_admin?
      query = query.where(id: current_user.team_ids_for_group_type(['sub_admin']))
    end

    if params[:q].present?
      query = query.where("teams.name like ?", "%#{params[:q]}%")
    end

    if params[:is_everyone].present?
      query = query.where(is_everyone_team: params[:is_everyone].try(:to_bool))
    end

    if params[:team_ids].present?
      query = query.where(id: params[:team_ids])
    end

    if params[:exclude_team_ids].present?
      query = query.where.not(id: params[:exclude_team_ids])
    end

    if params[:full_response].present?
      query = query.includes(:dynamic_group_revision)
    end

    @skip_roles = true
    @total = query.count
    @teams = query.limit(limit).offset(offset).order(sort_query)

    render "api/v2/teams/index"
  end

  api :GET, 'api/v2/organizations/channels', 'Return all channels of org'
  param :q, String, desc: 'Filter channels by this search term', required: false
  param :sort, String, 'Attribute name to sort by, ex title, created_at, default: created_at', required: false
  param :only_public, String, 'Attribute to get only public channels of an org', required: false
  param :order, String, 'Sort order, asc or desc, default: desc', required: false
  param :limit, Integer, desc: 'limit for pagination; default 10', required: false
  param :offset, Integer, desc: 'offset for pagination; default 0', required: false
  def channels
    order_attr, order = extract_sort_order(valid_order_attrs: Channel.column_names , default_order_attr: "created_at",
      order: params[:order], order_attr: params[:sort])
    sort_query = "channels.#{order_attr} #{order}"
    query = current_org.channels
    if params[:only_public].try(:to_bool)
      query = query.where(is_private: false)
    end

    if params[:q].present?
      query = query.where("label like :search OR description like :search", search: params[:q])
    end
    @total = query.count
    @channels = query.limit(limit).offset(offset).order(sort_query)

    render "api/v2/channels/index"
  end

  api :GET, 'api/v2/organizations/shared_channels', 'Return all channels of org that are shared'
  param :sort, String, 'Attribute name to sort by, ex title, created_at, default: created_at', required: false
  param :order, String, 'Sort order, asc or desc, default: desc', required: false
  param :limit, Integer, desc: 'limit for pagination; default 10', required: false
  param :offset, Integer, desc: 'offset for pagination; default 0', required: false
  param :q, String, desc: 'Filter channels by this search term', required: false
  def shared_channels
    order_attr, order = extract_sort_order(valid_order_attrs: Channel.column_names , default_order_attr: "created_at",
      order: params[:order], order_attr: params[:sort])

    sort_query = "channels.#{order_attr} #{order}"

    channels = current_org.channels.shared

    if params[:q].present?
      channels = channels.where('label like ?', "%#{params[:q]}%")
    end
    @total = channels.count
    @channels = channels.limit(limit).offset(offset).order(sort_query)
    shared_channel_details
    render "api/v2/channels/shared_channels"

  end

  api :GET, 'api/v2/organizations/cloned_channels', 'Return all channels of org that were cloned'
  param :sort, String, 'Attribute name to sort by, ex title, created_at, default: created_at', required: false
  param :order, String, 'Sort order, asc or desc, default: desc', required: false
  param :limit, Integer, desc: 'limit for pagination; default 10', required: false
  param :offset, Integer, desc: 'offset for pagination; default 0', required: false
  def cloned_channels
    order_attr, order = extract_sort_order(valid_order_attrs: Channel.column_names , default_order_attr: "created_at",
      order: params[:order], order_attr: params[:sort])

    sort_query = "channels.#{order_attr} #{order}"

    channels = current_org.channels.cloned
    @total = channels.count
    @channels = channels.limit(limit).offset(offset).order(sort_query)
    shared_channel_details
    render "api/v2/channels/shared_channels"
  end

  api :GET, "api/v2/organizations/leaderboard", "Get Leaderboard for an organization"
  param :limit, Integer, desc: 'limit for pagination; default 10', required: false
  param :offset, Integer, desc: 'offset for pagination; default 0', required: false
  param :days, Integer, desc: 'get data for this many weeks in the past, default: 0, just today', required: false
  param :period, Integer, desc: 'one of \'day\', \'week\', \'month\', \'year\' or \'all_time\'. The \'days\' param is ignored if this is passed', required: false
  param :from_date, String, desc: "Starting date. Format: mm/dd/yyyy. Metrics are inclusive of this date", required: false
  param :to_date, String, desc: "End date. Format: mm/dd/yyyy. Metrics are inclusive of this date", required: false
  param :active, String, desc: "Boolean, whether to show only active users or not", required: false
  param :order_attr, String, desc: "Attribute to sort on. One of \"smartbites_consumed\", \"smartbites_created\", \"smartbites_completed\", \"smartbites_score\", \"smartbites_comments_count\", \"smartbites_liked\", \"time_spent\", default: \"smartbites_score\""
  param :order, String, desc: "Either \"asc\" or \"desc\", default: \"desc\"", required: false
  param :group_ids, String, desc: "Array of group ids. Show only users who are members of selected groups", required: false
  param :roles, String, desc: "Array of role names. Show only users with selected roles", required: false
  param :expertise_names, String, desc: "Array of expertise names. Show only users with selected expertises", required: false
  param :show_my_result, String, desc: "Boolean. Return current user metrics also with other metrics if true", required: false
  param :q, String, desc: "Show only users that match the search term", required: false
  def leaderboard
    # Filter on from and to date
    # Convert from_date and to_date to offsets
    metric_offset = if params[:from_date].present? && params[:to_date].present?
                      load_custom_offset_from_period(params[:from_date], params[:to_date])
                    else
                      analytics_offset
                    end
    result = LeaderboardService.new(
      organization: current_org,
      params: params,
      current_user: current_user,
      metric_period: analytics_period,
      metric_offset: metric_offset,
      limit: limit,
      offset: offset).call
    @total_count = result[:total_count]
    @metrics = result[:metrics]
    @current_user_metrics = result[:current_user_metrics]
    @users = User.where(id: @metrics.map(&:user_id)).index_by(&:id)
  end

  api :GET, 'api/v2/organizations/settings', 'Get org details for edit form'
  def settings
    @organization = @current_org
  end

  api :GET, "api/v2/organizations/metrics", "Get Metrics data for certian date range. Maximum timerange allowed is #{Settings.analytics_max_date_range} years."
  param :from_date, String, desc: "Starting date. Format: mm/dd/yyyy. Metrics are inclusive of this date", required: true
  param :to_date, String, desc: "End date. Format: mm/dd/yyyy. Metrics are inclusive of this date", required: true
  param :team_id, Integer, desc: "Micro-org id, which is team id", required: false
  param :period, String, desc: "Will be day, week, month or year", required: false
  def metrics
    render_unprocessable_entity("`from_date or `to date` is missing") and return if params[:from_date].blank? || params[:to_date].blank?

    if params[:period].present? && !PeriodOffsetCalculator::DRILL_DOWN_PERIOD.keys.include?(params[:period].to_sym)
      render_unprocessable_entity("Invalid value for period: #{params[:period]}") and return
    end

    from_date = Time.strptime(params[:from_date], "%m/%d/%Y").utc.beginning_of_day
    to_date   = Time.strptime(params[:to_date], "%m/%d/%Y").utc.end_of_day

    render_unprocessable_entity("Start date can not be greater than end date") and return if from_date > to_date
    render_unprocessable_entity("Exceeded maximum date range") and return if ((to_date - from_date) / 1.year) > Settings.analytics_max_date_range

    period = params[:period].present? ? params[:period].to_sym : :day
    time_period_offsets_from = PeriodOffsetCalculator.applicable_offsets(from_date)[period]
    time_period_offsets_to = PeriodOffsetCalculator.applicable_offsets(to_date)[period]

    timeranges = [[from_date, nil], [nil, to_date]]
    custom_period_data = OrganizationMetricsGatherer.metrics_over_span(organization: current_org, team_id: params[:team_id], period: period, offsets: [time_period_offsets_from, time_period_offsets_to], timeranges: timeranges)
    drill_down_data_for_custom_period = OrganizationMetricsGatherer.drill_down_data_for_custom_period(organization: current_org, team_id: params[:team_id], timerange: [from_date, to_date], period: period)
    result = Hash[OrganizationMetricsGatherer::METRICS.map{|metric|
      [metric, {
        value: custom_period_data[metric],
        drilldown: {
          values: drill_down_data_for_custom_period[metric],
        }
      }]
    }]

    clc_progress = if params[:team_id].present?
      ClcService.new.team_clc_progress(from_date.to_date, to_date.to_date, @current_org, team_id: params[:team_id])
    else
      ClcService.new.sumation_clc_progress(from_date.to_date, to_date.to_date, @current_org)
    end
    result[:clc_progress] = {value: clc_progress.count, drilldown: {values: clc_progress}}
    render json: result, status: :ok and return
  end

  api :GET, 'api/v2/organizations/content_metrics',
            'content level metrics for a date range'
  description <<-EOS
    Get content level metrics data for a certain date range.
    Maximum timerange allowed is #{Settings.analytics_max_date_range} years.
  EOS

  param :from_date, String, desc: "Starting date. Format: mm/dd/yyyy. Metrics are inclusive of this date", required: true
  param :to_date, String, desc: "End date. Format: mm/dd/yyyy. Metrics are inclusive of this date", required: true
  param :order_attr, String, :desc => "Attribute to sort on. One of \"views_count\", \"comments_count\", \"likes_count\""
  param :order, String, :desc => "Either \"asc\" or \"desc\", default: \"desc\"", required: false
  param :limit, Integer, desc: 'limit for pagination; default 10', required: false
  param :offset, Integer, desc: 'offset for pagination; default 0', required: false
  param :search_term, String, desc: "Show only team names that match the search term", required: false
  param :team_ids, Array, desc: "Show only metrics for content by teams", required: false
  def content_metrics
    render_unprocessable_entity("`from_date or `to date` is missing") and return if params[:from_date].blank? || params[:to_date].blank?

    from_date = Time.strptime(params[:from_date], "%m/%d/%Y").utc.beginning_of_day
    to_date   = Time.strptime(params[:to_date], "%m/%d/%Y").utc.end_of_day

    render_unprocessable_entity("Start date can not be greater than end date") and return if from_date > to_date
    render_unprocessable_entity("Exceeded maximum date range") and return if ((to_date - from_date) / 1.year) > Settings.analytics_max_date_range

    order_attr, order = extract_sort_order(valid_order_attrs: ["views_count", "comments_count", "likes_count", "completes_count"], default_order_attr: "views_count")

    team_ids = params[:team_ids] || []
    card_ids = if current_user.sub_admin_authorize?(Permissions::MANAGE_ANALYTICS)
      current_user.card_ids_for_team_users(['sub_admin'], group_ids: team_ids, sub_admin: true)
    elsif current_user.group_sub_admin_authorize?(Permissions::MANAGE_GROUP_ANALYTICS)
      current_user.card_ids_for_team_users(['sub_admin'], group_ids: team_ids, sub_admin: false)
    end
    @metrics = current_org.content_level_metrics_for(
      q: params[:search_term],
      from_date: from_date,
      to_date: to_date,
      card_ids: card_ids
    ).limit(limit).offset(offset).order("#{order_attr} #{order}")
  end

  api :GET, "api/v2/organizations/assessments", 'Get details of all pathway assessments of an org'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :q, String, desc: 'Filter teams by this search term', required: false
  def assessments
    set_current_org_using_host_name
    @assessment_pathways = @current_org.assessment_pathways

    card_ids = if current_user.sub_admin_authorize?(Permissions::MANAGE_ANALYTICS)
      current_user.card_ids_for_team_users(['sub_admin'], sub_admin: true)
    elsif current_user.group_sub_admin_authorize?(Permissions::MANAGE_GROUP_ANALYTICS)
      current_user.card_ids_for_team_users(['sub_admin'], sub_admin: false)
    end

    @assessment_pathways = @assessment_pathways.where(id: card_ids) if card_ids.present?
    if params[:q]
      @assessment_pathways = @assessment_pathways.search_by(params[:q])
    end
    @assessment_pathways = @assessment_pathways.limit(limit).offset(offset)
  end

  api :PUT, 'api/v2/organizations/settings/:id', 'Update organization setting'
  param :id, String, desc: 'Id of the config.'
  param :setting, Hash, desc: 'Org settings hash', required: true do
    param :name, String, desc: 'Name of the config.', required: true
    param :value, String, desc: 'Setting value, boolean, string or matching setting value data type', required: true
    param :description, String, desc: 'Detailed description of the setting', required: true
    param :category, String, desc: 'Setting category. default to "settings" if not provided', required: false
  end
  def update_setting
    config = Config.where(configable: current_org, id: params[:id]).first!
    _settings_params = setting_params
    _settings_params.delete :data_type
    _settings_params[:category] ||= 'settings' #default category to settings

    if config.update_attributes(_settings_params)
      head :ok
    else
      render_unprocessable_entity(config.errors.full_messages.join(", ")) and return
    end
  end

  api :POST, 'api/v2/organizations/settings', 'Create organization setting'
  param :setting, Hash, desc: "update org settings", required: true do
    param :name, String, desc: "name of the config", required: true
    param :value, String, desc: "setting value, boolean, string or matching setting value data type", required: true
    param :category, String, desc: 'setting category. default to "settings" if not provided', required: true
    param :data_type, String, desc: 'Data type for value of the setting. Default: string. Supported data types: json, boolean, integer, string', required: true
    param :description, String, desc: 'Detailed description of the setting', required: true
  end
  def create_setting
    _settings_params = setting_params
    _settings_params[:category] ||= 'settings'

    config = Config.where(configable: current_org, name: _settings_params[:name], category: _settings_params[:category]).first_or_initialize do |_config|
      _config.value       = _settings_params[:value]
      _config.description = _settings_params[:description]
      _config.data_type   = _settings_params[:data_type]
    end

    if config.save
      render json: config.to_json(only: [:id, :name, :description, :value, :data_type, :category])
    else
      render_unprocessable_entity(config.errors.full_messages.join(", ")) and return
    end
  end

  api :GET, "api/v2/organizations/ssos", "Get list of SSO's for an org"
  description <<-EOS
    List of all SSO options for authentication.
    SSO options enabled for current organization followed by other non-enabled SSO options.
  EOS
  def list_ssos
    @enabled_ssos = current_org.org_ssos.includes(:sso).order(:position).all
    available_ssos = Sso.all
    @disabled_ssos = available_ssos.select do |sso|
      !@enabled_ssos.any? {|org_sso| org_sso.sso_id == sso.id}
    end
    @ssos = @enabled_ssos + @disabled_ssos
  end

  api :PUT, "api/v2/organizations/ssos/:sso_name/enable", "Enable org sso"
  param :sso_name, String, desc: 'Name of available sso', required: true
  def enable_sso
    enable_or_disable_sso(is_enabled: true)
    save_and_return_sso
  end

  api :PUT, "api/v2/organizations/ssos/:sso_name/disable", "Disable org sso"
  param :sso_name, String, desc: 'Name of available sso', required: true
  def disable_sso
    enable_or_disable_sso(is_enabled: false)
    save_and_return_sso
  end

  api :PUT, 'api/v2/organizations/add_lms_users', 'Sync all users within  organization with LMS Integration'
  def add_lms_users
    set_current_org_using_host_name
    render_not_found and return if @current_org.nil?
    LmsAddOrganizationUsersJob.perform_later(@current_org.id)
    head :ok and return
  end

  api :POST, 'api/v2/organizations/push_ecl_content_to_channels', 'Push ecl content items to oraganization channels'
  param :ecl_ids, Array, desc: 'Array of ecl card ids', required: true
  param :channel_ids, Array, desc: 'Array of channel ids', required: true
  def push_ecl_content_to_channels
    PushECLContentToChannelsJob.perform_later(
      organization_id: @current_org.id,
      channel_ids: params[:channel_ids],
      ecl_ids: params[:ecl_ids]
    )
    head :ok and return
  end

  api :GET, 'api/v2/organizations/notification_config', 'Get notification config for organization'
  formats ['json']
  api_version 'v2'
  def get_notification_config
    render json: EDCAST_NOTIFY.settings.get_settings(@current_org.id)
  end

  api :PUT, 'api/v2/organizations/notification_config', 'Update notification config for organization'
  param :options, Hash, desc: "The options hash returned by GET", required: true
  formats ['json']
  api_version 'v2'
  def update_notification_config
    config = NotificationConfig.find_or_create_by!(organization: @current_org)
    EDCAST_NOTIFY.settings.save_settings(config, params[:options])

    render json: EDCAST_NOTIFY.settings.get_settings(@current_org.id)
  end

  api :GET, "api/v2/organizations/channel_performance"
  description <<-EOS
    Get channel performance data of organization for certain date range.
    Default will return last 30 days data
  EOS
  param :from_date, String, desc: "Starting date. Format: mm/dd/yyyy. Metrics are inclusive of this date", required: false
  param :to_date, String, desc: "End date. Format: mm/dd/yyyy. Metrics are inclusive of this date", required: false
  param :q, String, desc: "Show only channels that match the search term", required: false
  def channel_performance
    if params[:from_date].present? && params[:to_date].present?
      from_date = Date.strptime_with_format(params[:from_date])
      to_date   = Date.strptime_with_format(params[:to_date])

      render_unprocessable_entity("Incorrect from Date") and return if from_date.nil?
      render_unprocessable_entity("Incorrect to Date") and return if to_date.nil?
      render_unprocessable_entity("From date can not be greater than end date") and return if from_date > to_date
    else
      from_date = 30.days.ago
      to_date   = Date.today
    end
    search_term = params[:q]

    @channel_performance_data = current_org.get_channel_performance(from_date, to_date, search_term: search_term)
  end

  api :POST, '/api/v2/organizations/badges', 'Create Badge'
  param :type, String, desc: 'Type of badges for Card CardBadge and for Clc ClcBadge', required: true
  param :image_url, String, desc: 'Image of badge', required: true
  def create_badges
    if ['CardBadge', 'ClcBadge'].exclude? params['type']
      render_unprocessable_entity('Invalid type') and return
    end

    @badge = find_or_initialize_badge(params['type'], params['image_url'])

    if @badge.save
      render 'show_badge'
    else
      render_unprocessable_entity(@badge.full_errors)
    end
  end

  api :GET, '/api/v2/organizations/badges'
  param :type, String, desc: 'Type of badges for Card CardBadge and for Clc ClcBadge', required: true
  param :is_default, [true, false], desc: 'is_default of badge', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  def badges
    case params['type']
    when 'CardBadge', 'ClcBadge'
      @badges = Badge.where(organization: current_org, type: params['type'])
      if !params['is_default'].nil?
        @badges = @badges.where(is_default: params['is_default'].to_bool)
      end
    else
      render_unprocessable_entity('Invalid type')
    end
    @total = @badges.count
    @badges = @badges.limit(limit).offset(offset)
  end

  api :GET, '/api/v2/organizations/restrictable_users_or_teams'
  param :q, String, desc: 'search query string entered by user', required: true
  param :team_ids, Array, desc: 'array of team_ids selected to share card with', required: false
  param :channel_ids, Array, desc: 'array of channel_ids selected to share with', required: false
  param :card_id, Integer, desc: "card id whose author to be excluded from restriction", required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  def restrictable_users_or_teams
    if params[:channel_ids].blank? && params[:team_ids].blank?
      render_unprocessable_entity(
        "Cannot restrict to user/group when both channel_ids and group_ids is empty") and return
    end

    user_search_filter = {}
    user_search_filter[:channel_ids] = params[:channel_ids] if params[:channel_ids].present?
    user_search_filter[:team_ids] = params[:team_ids] if params[:team_ids].present?

    user_query = Search::UserSearch.new.search({viewer: current_user,
      q: params[:q], filter: user_search_filter})
    @user_matches = user_query.results

    #card_author and current_user should not be restricted
    card_author_id = Card.find(params[:card_id]).author_id if params[:card_id].present?
    exclude_user_ids = [current_user.id, card_author_id]
    @user_matches.reject! {|u| exclude_user_ids.compact.include? u["id"]}

    @total = @user_matches.count

    if params[:channel_ids].present?
      group_query = Search::TeamSearch.new.search({viewer: current_user, q: params[:q],
        filter: {channel_ids: params[:channel_ids]}})
      @group_matches = group_query.results
      @total +=  @group_matches.count
    end
  end

  api :GET, 'api/v2/organizations/shareable_channels'
  param :sort, String, 'Attribute name to sort by, ex title, created_at, default: created_at', required: false
  param :order, String, 'Sort order, asc or desc, default: desc', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :q, String, desc: 'Filter channels by this search term', required: false
  def shareable_channels
    order_attr, order = extract_sort_order(
      valid_order_attrs: Channel.column_names , default_order_attr: "created_at",
      order: params[:order], order_attr: params[:sort]
    )

    sort_query = "channels.#{order_attr} #{order}"

    channels = current_org.shareable_channels
    if params[:q].present?
      channels = channels.where('label like ?', "%#{params[:q]}%")
    end
    @total = channels.count
    @channels = channels.limit(limit).offset(offset).order(sort_query)
    shared_channel_details
    render "api/v2/channels/shared_channels"
  end

  private

  def validate_quarter_dates
    quarters = organization_params[:quarters_attributes]
    return true unless quarters
    quarters.each do |quarter|
      if quarter[:end_date].blank? || quarter[:start_date].blank?
        render_unprocessable_entity("#{quarter[:name]}: dates cannot be blank") and return
      end

      if  quarter[:end_date].to_date < quarter[:start_date].to_date
        render_unprocessable_entity("#{quarter[:name]}: end date must be after the start date") and return
      end

      if quarter[:name] == "quarter-4"
        quarters_end_date = (quarters[0][:start_date].to_date + 1.year) - 1
        if quarters_end_date != quarter[:end_date].to_date
          render_unprocessable_entity("Quarter 4: Invalid end date") and return
        end
      end

      if is_quarter_overlapping(quarters, quarter)
        render_unprocessable_entity("#{quarter[:name]}: dates are overlapping") and return
      end
    end
  end

  def is_quarter_overlapping(quarters, quarter)
    quarters.each do |q|
      if q[:name] != quarter[:name]
        return true if (q[:start_date].to_date..q[:end_date].to_date).overlaps?(quarter[:start_date].to_date..quarter[:end_date].to_date)
      end
    end
    false
  end

  def shared_channel_details
    @all_authors_count = ChannelsUser.where(channel_id: @channels.ids)
      .all_authors_types
      .group(:channel_id)
      .count
    @shared_with = Channel.shared_with_orgs(@channels.map(&:id))
    @cloned_channel_ids = Channel.cloned_channel_id(@channels.map(&:id), current_org.id)
    @organization_name = Channel.organization_name(@channels.map(&:id))
  end

  def organization_params
    if params[:organization][:enable_analytics_reporting].present?
      params[:organization][:enable_analytics_reporting] = params[:organization][:enable_analytics_reporting].to_bool
    end

    params.require(:organization).permit(:is_open, :is_registrable, :name, :description, :show_onboarding,
      :image, :mobile_image, :savannah_app_id, :redirect_uri, :co_branding_logo, :favicon, :show_sub_brand_on_login,
      :send_weekly_activity, :enable_personalized_email, :enable_personalized_email_reporting, :enable_role_based_authorization,
      :enable_analytics_reporting, :custom_domain,
      configs_attributes: [:id, :name, :value, :data_type],
      quarters_attributes: [:id, :name, :start_date, :end_date])
  end

  def setting_params
    params.require(:setting).permit(:category, :name, :value, :description, :data_type)
  end

  def enable_or_disable_sso(is_enabled: false)
    sso = Sso.where(name: params[:sso_name]).first!

    @org_sso = current_org.org_ssos.joins(:sso).where(sso: sso).first_or_initialize
    @org_sso.is_enabled = is_enabled
    @org_sso.position = current_org.org_ssos.count + 1 if @org_sso.new_record?
  end

  def save_and_return_sso
    unless @org_sso.save
      render_unprocessable_entity(@org_sso.errors.full_messages.join(", ")) and return
    end
    render partial: 'api/v2/org_ssos/details', locals: {org_sso: @org_sso} and return
  end

  def require_leaderboard_access
    unless current_org.leaderboard_enabled?
      render_not_found("You don't have permissions to filter leaderboard information")
    end
    if %i{roles group_ids expertise_names}.any? { |key| params[key].present? }
      unless current_user.authorize?(Permissions::GET_LEADERBOARD_INFORMATION) || current_org.has_admin_access?(current_user)
        render_unauthorized("You don't have permissions to filter leaderboard information")
      end
    end
  end

  def load_custom_offset_from_period(from_date, to_date)
    from_date = Time.strptime(from_date, "%m/%d/%Y").utc.beginning_of_day
    to_date = Time.strptime(to_date, "%m/%d/%Y").utc.end_of_day
    PeriodOffsetCalculator.drill_down_period_and_offsets_for_timerange(period: analytics_period, timerange: [from_date, to_date])[1]
  end

  def find_or_initialize_badge(type, image)
    @badge = type.constantize.find_or_initialize_by(organization: current_org, image_original_url: image) do |badge|
      badge.image = open(image)
    end
  end
end

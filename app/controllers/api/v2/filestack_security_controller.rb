class Api::V2::FilestackSecurityController < Api::V2::BaseController
  include Api::V2::CardsHelper

  api :GET, 'api/v2/filestack/signed_url', 'Get expirable filestack url'
  description <<-EOS
    This endpoint returns expirable filestack url
  EOS
  param :url, String, desc: "filestack url", required: true
  def signed_url
    if params[:url].present?
      uri = Filestack::Security.read_filestack_url(params[:url])
      uri = URI.parse(uri)

      # STEP 1: generate the signed url
      signed_url = Filestack::Security.new(call: ["convert"], org: current_org).signed_url(uri.path)

      # STEP 2: encrypt the filestack URL and user id.
      authenticated_url = secured_url(signed_url, current_user.id, current_org)

      render json: { signed_url: authenticated_url }
    else
      render_bad_request("url param is missing in the request")
    end
  end

  api :GET, 'api/v2/filestack/upload_policy_and_signature', 'Get upload policy and signature'
  description <<-EOS
    This endpoint generates policy and signature for uploading files to filestack
  EOS
  def upload_policy_and_signature
    # Kept expiry time after 1 hour considering slow clients
    # call "pick" is for Uploading files
    # call "store" is for Saving to custom storage
    # call "read" is for reading uploaded files
    # Both "store" and "read" are needed while selecting files from other sources
    obj = Filestack::Security.new(
      call: ["pick", "store", "read"],
      expire_after_seconds: 1.hour
    )
    render json: {
      policy: obj.policy,
      signature: obj.signature
    }
  end
end


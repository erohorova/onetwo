# frozen_string_literal: true

class Api::V2::EmailTemplatesController < Api::V2::BaseController

  has_limit_offset_constraints only: [:index]

  before_action :check_user_permissions
  before_action :set_email_template, only: %i[
    show destroy activate update disable
  ]

  resource_description do
    api_version 'v2'
    formats ['json']
  end

  api :GET, 'api/v2/email_templates', 'Return list of organization templates'
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :skip_default, String, desc: 'Boolean(true/false). To return or not default templates', required: false
  param :state, String, desc: 'Return templates filtered by state', required: false
  param :is_active, String, desc: 'Boolean. Return templates filtered by is_active field', required: false
  param :q, String, desc: 'Query string; search by title', required: false
  def index
    filter_state = params[:state].presence || %w[draft published]
    skip_default = params[:skip_default]&.to_bool
    #filter by state or all
    @email_templates = current_org.email_templates.where(state: filter_state)
    #skip default if params present
    @email_templates = @email_templates.customs if skip_default
    #return only activated templates
    if params[:is_active].present?
      @email_templates = @email_templates.customs
        .where(is_active: params[:is_active]&.to_bool)
    end
    if params[:q].is_a?(String) && params[:q].present?
      @email_templates = @email_templates.where('LOWER(title) LIKE ?', "%#{params[:q].downcase}%")
    end

    @total = @email_templates.count
    @email_templates = @email_templates.limit(limit).offset(offset)
  end

  api :GET, '/api/v2/email_templates/:id', 'Get the email template details'
  param :id, Integer, desc: 'Email template ID', required: true
  def show
    # return details for the custom template
  end

  api :POST, 'api/v2/email_templates', 'Create an email template'
  param :email_template, Hash, required: true do
    param :title, String, desc: 'Title of an email template', required: true
    param :default_id, String, desc: 'ID of the default email template', required: true
    param :content, String, desc: 'HTML content of an email template', required: true
    param :design, String, desc: 'JSON content of an email template', required: true
    param :language, String, desc: 'Language of an email template', required: true
    param :subject, String, desc: 'Custom Subject of an email template', required: false
    param :state, String, desc: 'Set state of the email template. Default: draft. Options: draft/published', required: false
  end
  def create
    org = current_user.organization
    clean_params = sanitize_template_params(email_template_params)
    @email_template = org.email_templates.build(clean_params)
    if @email_template.save
      render 'show'
    else
      render_unprocessable_entity(
        @email_template.errors.full_messages.join(', ')
      )
    end
  end

  api :PUT, 'api/v2/email_templates/:id', 'Update the email template'
  param :id, Integer, desc: 'Email template ID', required: true
  param :email_template, Hash, required: true do
    param :title, String, desc: 'Title of an email template', required: false
    param :content, String, desc: 'HTML content of an email template', required: false
    param :design, String, desc: 'JSON content of an email template', required: false
    param :language, String, desc: 'Language of an email template', required: false
    param :subject, String, desc: 'Custom Subject of an email template', required: false
    param :state, String, desc: 'Set state of the email template. Default: draft. Options: draft/published', required: false
    param :organization_id, Integer, desc: 'ID of the organization', required: false
    param :creator_id, Integer, desc: 'ID of the user', required: false
  end
  def update
    if @email_template.update_attributes(email_template_params)
      render 'show'
    else
      render_unprocessable_entity(
        @email_template.errors.full_messages.join(', ')
      )
    end
  end

  api :DELETE, 'api/v2/email_templates/:id', 'Delete the email template'
  param :id, Integer, desc: 'Email template ID', required: true
  def destroy
    if @email_template.destroy
      head :no_content
    else
      render_unprocessable_entity(
        @email_template.errors.full_messages.join(', ')
      )
    end
  end

  api :GET, 'api/v2/email_templates/:id/activate', 'Activate the email template'
  param :id, Integer, desc: 'Email template ID', required: true
  def activate
    @info = @email_template.activate_template
    return if @info[:is_activated]

    render_unprocessable_entity(
      @email_template.errors.full_messages.join(', ')
    )
  end

  api :GET, 'api/v2/email_templates/:id/disable', 'Deactivate the email template'
  param :id, Integer, desc: 'Email template ID', required: true
  def disable
    return render 'show' if @email_template.disable_template

    render_unprocessable_entity(
      @email_template.errors.full_messages.join(', ')
    )
  end

  api :POST, 'api/v2/email_templates/send_custom_template', 'Send a template to current user email'
  param :content, String, desc: 'Content of template to be send', required: true
  param :title, String, decs: 'Title of the template', required: false
  def send_custom_template
    TestTemplateMailer.send_custom_template(
      template_content: params[:content],
      title: params[:title],
      user_id: current_user.id
    ).deliver_later

    head :ok
  end

  private

  def email_template_params
    params.require(:email_template).permit(
      :content, :creator_id, :organization_id,
      :state, :language, :title, :default_id, :design, :subject
    )
  end

  def set_email_template
    org = current_user.organization
    @email_template = org.email_templates.find_by(id: params[:id])
    return render_not_found('Email template does not exist') unless @email_template
    check_template_attrs(@email_template, action_name)
  end

  def sanitize_template_params(attrs)
    attrs[:creator_id] = current_user.id
    attrs[:organization_id] = current_user.organization.id
    attrs
  end

  def check_user_permissions
    is_admin = current_user.authorize?(Permissions::ADMIN_ONLY)
    render_unauthorized('Permission denied') unless is_admin
  end

  def check_template_attrs(template, action)
    case action
    when 'update', 'destroy'
      if template.default?
        msg = "Can not #{action} default template"
        render_unprocessable_entity(msg)
      end
    when 'activate'
      if template.state == 'draft'
        render_unprocessable_entity('Can not activate draft template')
      end
    end
  end

end

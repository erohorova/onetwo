class Api::V2::CommentsController < Api::V2::BaseController
  include CommentConcern
  include EmbargoChecker

  before_action :embargo_content_check, only: :create
  before_action :require_organization_admin, only: [:unreport, :trash]
  before_action :load_comment, only: :unreport

  has_limit_offset_constraints only: [:index], max_limit: 50

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/v2/cards/:card_id/comments', 'Returns list of comments for a card'
  api :GET, 'api/v2/activity_streams/:activity_stream_id/comments', 'Returns list of comments for activity stream'
  api_version 'v2'
  description <<-EOS
    This will return comments from a card. Comments are always returned in the order desc or asc
  EOS
  param :card_id, String, desc: 'Retrieve comments for card with this id (required if use api/v2/cards/:card_id/comments)',
        required: true
  param :activity_stream_id, String,
        desc: 'Retrieve comments for activity stream with this id (required if use api/v2/activity_streams/:activity_stream_id/comments)',
        required: true
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10; max value 50', required: false
  param :order, String, desc: 'ordering based on created_at, desc or asc', required: false
  param :load_voters, String, desc: "Load first 10 upvoters for a comment. Value: true/false"
  def index
    order = params[:order].blank? ? :asc : params[:order]
    @comments = find_commentable.comments.includes(:tags, :mentioned_users, :file_resources, :user, :resource, :upvoted_by)
                    .order(created_at: order).limit(limit).offset(offset)
    @reported_comment_ids = CommentReporting.where(comment_id: @comments.map(&:id), user: current_user.id).pluck(:comment_id)
  end

  api :GET, 'api/v2/cards/:card_id/comments/:id', 'Returns data on a particular comment for a card'
  api :GET, 'api/v2/activity_streams/:activity_stream_id/comments/:id', 'Returns data on a particular comment for activity stream'
  api_version 'v2'
  description <<-EOS
    This will return data on a particular comment for a card
  EOS
  param :card_id, String,
        desc: 'Retrieve comment for card with this ids (required if use api/v2/cards/:card_id/comments/:id)',
        required: true
  param :activity_stream_id, String,
        desc: 'Retrieve comment for activity stream with this ids (required if use api/v2/activity_streams/:activity_stream_id/comments/:id)',
        required: true
  param :id, Integer, desc: "Retrieve data for comment with this id", required: true
  def show
    show_comment
  end

  api :POST, 'api/v2/cards/:card_id/comments', 'Post a comment on a card'
  api :POST, 'api/v2/activity_streams/:activity_stream_id/comments', 'Post a comment on activity stream'
  api :POST, 'api/v2/organizations/:organization_id/comments', 'Post a comment on a organization'
  api :POST, 'api/v2/teams/:team_id/comments', 'Post a comment on a team'
  api_version 'v2'
  description <<-EOS
    This is the API to add a comment to a card. Returns the created comment object on success
  EOS
  param :card_id, String,
        desc: 'Post a comment for the card with this id (required if use api/v2/cards/:card_id/comments)',
        required: true
  param :activity_stream_id, String,
        desc: 'Post a comment for the activity stream with this id (required if use api/v2/activity_streams/:activity_stream_id/comments)',
        required: true
  param :organization_id, String,
        desc: 'Post a comment for the organization with this id (required if use api/v2/organizations/:organization_id/comments)',
        required: true
  param :team_id, String,
        desc: 'Post a comment for the team with this id (required if use api/v2/teams/:team_id/comments)',
        required: true
  param :comment, Hash, desc: 'Comment object' do
    param :message, String, :desc => 'The comment text', required: true
    param :file_ids, Array, :desc => 'File Ids to be attached with the comment. For example, for uploading an image', required: false
  end
  authorize :create, Permissions::CREATE_COMMENT
  def create
    if create_comment
      @comment.trigger_event unless @comment.non_triggerable_comment?
      track_comment
      render 'show' and return
    else
      render_unprocessable_entity(@comment.errors.full_messages.join(",")) and return
    end
  end

  api :DELETE, 'api/v2/cards/:card_id/comments/:id', 'Delete a comment on card'
  api :DELETE, 'api/v2/activity_streams/:activity_stream_id/comments/:id', 'Delete a comment on activity stream'
  api :DELETE, 'api/v2/organizations/:organization_id/comments/:id', 'Delete a comment on organization'
  api :DELETE, 'api/v2/teams/:team_id/comments/:id', 'Delete a comment on team'
  param :card_id, String,
        desc: 'Delete a comment on the card with this id (required if use api/v2/cards/:card_id/comments/:id)',
        required: true
  param :activity_stream_id, String,
        desc: 'Delete a comment on activity stream with this id (required if use api/v2/activity_streams/:activity_stream_id/comments/:id)',
        required: true
  param :organization_id, String,
        desc: 'Delete a comment on organization with this id (required if use api/v2/organizations/:organization_id/comments/:id)',
        required: true
  param :team_id, String,
        desc: 'Delete a comment on team with this id (required if use api/v2/teams/:team_id/comments/:id)',
        required: true
  param :id, Integer, desc: "Delete the comment with this id", required: true
  authorize :destroy, Permissions::CREATE_COMMENT
  def destroy
    if delete_comment
      track_comment(action: "uncomment")
      head :ok and return
    else
      render_unprocessable_entity(@comment.errors.full_messages.join(",")) and return
    end
  end

  api :PUT, 'api/v2/comments/:id/unreport', 'Unreport a reported comment'
  param :id, Integer, desc: "Unreport comment for particular id", required: true
  authorize :unreport, Permissions::REPORT_COMMENT
  def unreport
    if @comment
      @comment.comment_reportings.destroy_all
      head :ok
    else
      render_unprocessable_entity(@comment.full_errors)
    end
  end

  api :DELETE, '/api/v2/comments/:id/trash', 'Trash a comment'
  param :id, Integer, desc: 'Trash the comment with this id', required: true
  authorize :trash, Permissions::REPORT_COMMENT
  def trash
    if trash_comment
      track_comment(action: "uncomment")
      head :ok
    else
      render_unprocessable_entity(@comment.full_errors)
    end
  end

  private

  def load_comment
    @comment = Comment.find_by(id: params[:id])
    return render_not_found('Reporting not found') unless @comment&.comment_reportings&.exists?
  end

  def comment_params
    #ids that are string are failed to load in associate files in models/comment
    params[:comment][:file_ids] = params[:comment][:file_ids].map(&:to_i) unless params[:comment][:file_ids].blank?

    params.require(:comment).permit(:message, :file_ids => [])
  end

end

class Api::V2::FeedController < Api::V2::BaseController
  include CardsFilter
  before_action :set_filter_by_language, :set_last_access_opts, only: [:index]
  has_limit_offset_constraints only: [:index]

  api :GET, 'api/v2/feed', "Returns a user's feed"
  api_version 'v2'
  description <<-EOS
    This will return cards on a user's feed
  EOS
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :filter_by_language, [true, false], desc: "Filter cards based on the user's language", required: false
  param :fields, String, desc: 'Gives On-demand response', required: false
  param :last_access_at, String, desc: 'Time of last access, format: 1505832150', required: true
  error 500, 'Internal server error'
  formats ['json']
  def index
    # Merge to detect if the requests originated from mobile or web.
    @options.merge!(is_native_app: is_native_app?)

    items = UserContentsQueue.feed_content_for_v2(
      user: current_user, limit: limit, offset: offset,
      filter_params: { filter_by_language: @filter_by_language }
    )
    @cards = items.map { |record| record['item'] }
  end

  api :GET, 'api/v2/feed/items_count', 'Get number of new items since timestamp'
  param :from_time, Integer, desc: 'Epoch timestamp', required: true
  api_version 'v2'
  formats ['json']
  def feed_items_count
    render_unprocessable_entity("from_time is required") and return if params[:from_time].blank?

    from_time = Time.at(params[:from_time].to_i).to_datetime.utc
    count = UserContentsQueue.get_cards_queue_for_user(
      user_id: current_user.id,
      filter_params: {from_time: from_time},
      method: :count
    )

    render json: {items_count: count}
  end
end

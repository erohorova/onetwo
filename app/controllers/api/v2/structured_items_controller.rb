class Api::V2::StructuredItemsController < Api::V2::BaseController
  include CardsFilter, StructureItemConcern
  before_action :set_filter_by_language, only: [:index, :entities_data]
  before_action :load_structure, except: [:entities_data]
  before_action :load_entity, only: [:create, :remove]
  before_action :check_entities, only: [:bulk_create, :bulk_remove]

  helper_method :current_structure

  api :GET, 'api/v2/structures/:structure_id/all_items', 'Get all items for a structure'
  param :structure_id, Integer, desc: 'A structure (carousel) ID', required: true
  formats ['json']
  api_version 'v2'
  def all_items
    render_not_found("Structure ID: #{params[:structure_id]} not found.") and return unless current_structure.present?

    @structured_items = current_structure.structured_items
  end

  api :GET, 'api/v2/structures/:structure_id/structured_items', 'Get all items for a structure'
  param :structure_id, Integer, desc: 'A structure (carousel) ID', required: true
  param :filter_by_language, [true, false], desc: "Filter cards based on the user's language", required: false
  param :is_cms, String, desc: 'Boolean. Do not apply exclude card conditions if request is from cms. Default: false', required: false
  param :user_fields, String, desc: 'Gives On-demand response for user', required: false
  formats ['json']
  api_version 'v2'
  def index
    render_not_found("Structure ID: #{params[:structure_id]} not found.") and return unless current_structure.present?
    @entities = fetch_viewable_items(filter_by_language: @filter_by_language, is_cms: params[:is_cms]&.to_bool)
  end

  api :GET, 'api/v2/structures_items/entities_data', 'Get all entities data of given structures'
  param :structure_ids, Array, desc: 'Array of structure (carousel) IDs', required: true
  param :filter_by_language, [true, false], desc: "Filter cards based on the user's language", required: false
  param :user_fields, String, desc: 'Gives On-demand response for user', required: false
  param :channel_fields, String, desc: 'Gives On-demand response for channel', required: false
  formats ['json']
  api_version 'v2'
  def entities_data
    fetch_viewable_structure_items(filter_by_language: @filter_by_language)

    render_not_found("Active Custom Carousels not found.") and return unless @structures.present?

    render json: generate_response, status: :ok
  end

  api :POST, 'api/v2/structures/:structure_id/structured_items', 'Add the item to the structure'
  param :structure_id, Integer, desc: 'A structure (carousel) ID', required: true
  param :entity_id, Integer, desc: 'Element ID', required: true
  param :entity_type, Integer, desc: 'Element type', required: true
  formats ['json']
  api_version 'v2'
  def create
    current_structure.structured_items.find_or_create_by(entity: @entity)
    @structured_items = current_structure.structured_items
  end

  api :POST, 'api/v2/structures/:structure_id/bulk_create', 'Add the set of items to the structure'
  param :structure_id, Integer, desc: 'A structure (carousel) ID', required: true
  param :entity_ids, Array, desc: 'Element IDs', required: true
  param :entity_type, Integer, desc: 'Element type', required: true
  formats ['json']
  api_version 'v2'
  def bulk_create
    items = current_structure.structured_items
    @entities.uniq.each { |entity| items.find_or_create_by(entity: entity) }
  end

  api :DELETE, 'api/v2/structures/:structure_id/remove_item', 'Delete a set of the entities from the structure in the organization'
  param :structure_id, Integer, desc: 'A structure (carousel) ID', required: true
  param :entity_id, Integer, desc: 'Element ID', required: true
  param :entity_type, Integer, desc: 'Element type', required: true
  def remove
    removable_item = StructuredItem.fetch_by_entity(params[:entity_id], params[:entity_type], @structure)
    removable_item.remove_from_list
    removable_item.destroy
    current_structure.update_entity_and_config(org: @current_org) if current_structure.structured_items.empty?
    @structured_items = current_structure.structured_items
  end

  api :DELETE, 'api/v2/structures/:structure_id/bulk_remove', 'Deletes an entity from the structure in an organization'
  param :structure_id, Integer, desc: 'A structure (carousel) ID', required: true
  param :entity_ids, Array, desc: 'Element IDs', required: true
  param :entity_type, Integer, desc: 'Element type', required: true
  def bulk_remove
    @entities.uniq.each do |entity|
      removable_item = StructuredItem.fetch_by_entity(entity, params[:entity_type], @structure)
      removable_item.remove_from_list
      removable_item.destroy
    end
    current_structure.update_entity_and_config(org: @current_org) if current_structure.structured_items.empty?
  end

  private

  def current_structure
    @structure
  end

  def load_structure
    @structure = current_org.structures.find_by(id: params[:structure_id])
  end

  def load_entity
    @entity = load_entity_from_type_and_id(params[:entity_type], params[:entity_id])

    if @entity.nil?
      render_unauthorized('Entity is not present')
    end
  end

  def fetch_viewable_items(filter_by_language: false, is_cms: false)
    entities = current_structure.structured_items
    @current_slug = current_structure.slug
    case @current_slug
    when 'discover-channels'
      current_user.viewable_channels(entities.pluck(:entity_id))
    when 'discover-cards', 'channel-cards'
      # To not call the visibility filter in case the logged in user is an org admin
      # or when there are no cards to filter.
      cards = Card.where(id: entities.pluck(:entity_id))
      cards = cards.filtered_by_language(current_user.language) if filter_by_language
      if !is_cms && exclude_ids = current_user.dismissed_card_ids
        cards = cards.where.not(id: exclude_ids)
      end
      unless (current_user.is_org_admin? || cards.empty?)
        cards = cards.visible_to_userV2(current_user)
      end
      cards.present? ? cards.order("field(cards.id, #{entities.pluck(:entity_id).join(',')})") : []
    when 'discover-users', 'channel-users'
      current_structure.users.order('structured_items.position asc')
    end
  end

  def check_entities
    @entities = []
    @entity_errors_ids = []
    entity_ids = params[:entity_ids]
    if entity_ids.blank? || !entity_ids.is_a?(Array)
      return render_unprocessable_entity('entity_ids should be present')
    end
    @entities = load_entities_from_type_and_ids(params[:entity_type], params[:entity_ids])

    @entity_errors_ids = params[:entity_ids] - @entities.pluck(:id)
    @entity_errors_ids -= @entities.pluck(:ecl_id).map { |i| "ECL-#{i}" } if params[:entity_type] == 'Card'
    render_not_found('Entity does not exist') if @entities.empty?
  end
end

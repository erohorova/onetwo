class Api::V2::CommentReportingsController < Api::V2::BaseController
  has_limit_offset_constraints only: [:index]
  before_action :require_organization_admin, only: [:index]
  before_action :check_trashed_comment, only: :create

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :POST, '/api/v2/comment_reportings', 'Record the information when a comment is reported'
  param :comment_id, Integer, desc: 'Comment which is reported', required: true
  param :reason, String, desc: 'Reason for reporting the comment', required: true
  def create
    @comment_reporting = CommentReporting.new(comment_reporting_params)
    @comment_reporting.user = current_user
    unless @comment_reporting.save
      return render_unprocessable_entity(@comment_reporting.full_errors)
    end
    head :ok
  end

  api :GET, '/api/v2/comment_reportings', 'List comments which are reported/trashed'
  param :state, String, desc: 'State of a comment viz. reported or trashed', required: true
  param :comment_id, Integer, desc: 'Id of a reported or trashed comment', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :q, String, desc: 'Search for Comment Message', required: false
  authorize :index, Permissions::ADMIN_ONLY
  def index
    reportings = CommentReporting.filter_content(
      state: params[:state],
      comment_id: params[:comment_id],
      organization: current_org
    )
    reportings = reportings.where("comments.message like ?","%#{params[:q]}%") if params[:q].present?
    @total = reportings.length
    @results = reportings.order('comment_reportings.created_at DESC').limit(limit).offset(offset)
    @comment_id = params[:comment_id]
  end

  private

  def comment_reporting_params
    params.permit(:comment_id, :reason)
  end

  def check_trashed_comment
    @comment = Comment.only_deleted.find_by(id: comment_reporting_params[:comment_id])
    return render_unprocessable_entity('Comment has been trashed') if @comment.present?
  end
end

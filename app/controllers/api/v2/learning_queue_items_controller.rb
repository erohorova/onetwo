class Api::V2::LearningQueueItemsController < Api::V2::BaseController
  has_limit_offset_constraints only: [:index]

  api :GET, "api/v2/learning_queue_items", "Get Learning Queue for a user"
  param :limit, Integer, desc: "page limit, default to 10"
  param :offset, Integer, desc: "Page offset, default 0"
  param :include_queueable_details, String, desc: "Set to 'true' if you need the underlying card object to be \
  included in the response, default: 'false'", required: false
  param :source, Array, desc: "Filter learning queue items by assignments or bookmarks. Ex. ['assignments', 'bookmarks']", required: false
  param :content_type, Array, desc: "Filter learning queue items by content type, ex: ['card', 'collection', 'video_streams']", required: false
  param :state, Array, desc: "Filter learning queue items by state, ex: ['active', 'completed']"
  param :query, String, desc: 'Search query', required: false
  formats ['json']
  api_version 'v2'
  def index
    @include_queueable_details = params[:include_queueable_details].present? ? params[:include_queueable_details].to_bool : false
    query = current_user.learning_queue_items

    # source filter
    if params[:source].present?
      sources = LearningQueueItem::SOURCES & params[:source]
      query = query.where(source: sources) if sources.present?
    end

    @pinnable_card_id = nil
    # if bookmarks source is selected, the following code will ensure that only learning_queue_item
    # for existing bookmarks is returned
    if params[:source]&.include?('bookmarks')
      bookmarks = current_user.bookmarks
      @pinnable_card_id = bookmarks.joins(:pin).first&.bookmarkable_id

      card_ids = bookmarks.select(:bookmarkable_id)
      query = query.where(queueable_id: card_ids, source: 'bookmarks')
    end

    # content type filter
    if params[:content_type].present?
      content_types = LearningQueueItem::CONTENT_TYPES & params[:content_type]
      query = query.where(deep_link_type: content_types) if content_types.present?
    end

    # state filter
    if params[:state].present?
      states = LearningQueueItem::STATES & params[:state]
      query = query.where(state: states) if states.present?
    end

    query = query.visible_learning_items

    if params[:query].present?
      card_ids = query.pluck(:queueable_id).uniq
      cards = Search::CardSearch.new.search(
        params[:query],
        current_org,
        viewer: current_user,
        filter_params: { id: card_ids },
        load: false
      ).results.map(&:id)
      cards = (cards | [@pinnable_card_id]) if @pinnable_card_id.present?
      query = query.where(queueable_id: cards)
    end

    if @pinnable_card_id.present?
      query = query.order("queueable_id = #{@pinnable_card_id} DESC")
    end

    @learning_queue_items = query.order(id: :desc).limit(limit).offset(offset)


    # Executing this only for mobile originated requests.
    # For web based request, this is hand led on LearningQueueItemBridge
    if is_native_app?
      if @include_queueable_details
        queueable_ids = @learning_queue_items.map(&:queueable_id)
        @queueables = current_org.cards.where(id: queueable_ids).visible_cards.index_by(&:id)

        @assignments = Hash[
          current_user.assignments.where(assignable_id: queueable_ids).compact
            .map { |assignment| [assignment.assignable_id, assignment] }
        ]

        @bookmarkable = if @learning_queue_items.map(&:source).uniq.include?('bookmarks')
          current_user.card_ids_grouped_by_bookmark_ids(queueable_ids)
        end
      end
    end
  end
end

# frozen_string_literal: true
class Api::V2::VersionsController < Api::V2::BaseController
  api :GET, '/api/v2/versions', 'List versions of entity passed as params'
  param :entity_type, String, desc: 'Possible value: card', required: true
  param :entity_id, Integer, desc: 'id to fetch versions of', required: true
  def index
    klass = VersioningService.get_klass(params[:entity_type])
    @results = klass.fetch_versions(id: params[:entity_id]).results
    get_author_names
    @users = User.where(
      id: @results.map(&:user_id).uniq
    ).each_with_object({}) { |a, h| h[a.id] = a.full_name }
  rescue => exception
    render_bad_request(exception.message)
  end

  private
  def get_author_names
    @results.map do |result|
      if result[:updated_fields][0].keys.include?("author_id")
        author_changes = result.updated_fields[0]["author_id"]
        authors = User.where(id: author_changes)
        result.updated_fields[0]["author"] = [authors.detect{|a| a.id == author_changes[0]}&.full_name, authors.detect{|a| a.id == author_changes[1]}&.full_name]
        result.updated_fields[0].delete "author_id"
      end
    end
  end
end

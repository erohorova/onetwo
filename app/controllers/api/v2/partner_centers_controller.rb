class Api::V2::PartnerCentersController < Api::V2::BaseController
  before_action :partner_center, only: [:destroy, :edit, :update]

  api :POST, 'api/v2/partner_centers', 'Link partner integration with EdCast'
  param :partner_center, Hash, desc: 'Partner Center' do
    param :description, String, desc: 'Readable description', required: true
    param :embed_link, String, desc: 'Deep link URL obtained from our IDP', required: true
    param :integration, String, desc: 'An integration added to an organization', required: true
  end
  formats ['json']
  api_version 'v2'
  def create
    @partner_center = current_org.partner_centers.build(partner_center_params)
    @partner_center.organization = current_org
    @partner_center.user = current_user

    unless @partner_center.save
      render_unprocessable_entity(@partner_center.full_errors) and return
    end
  end

  api :DELETE, 'api/v2/partner_center/:id', 'Delete a partner center'
  param :id, Integer, desc: 'Id of a partner center', required: true
  formats ['json']
  api_version 'v2'
  def destroy
    @partner_center.destroy

    head :ok
  end

  api :GET, 'api/v2/partner_center/:id', 'Get details of a partner center'
  param :id, Integer, desc: 'Id of a partner center', required: true
  formats ['json']
  api_version 'v2'
  def edit
  end

  api :GET, 'api/v2/partner_centers', 'Get all partner centers for an organization'
  formats ['json']
  api_version 'v2'
  def index
    @partner_centers = current_org.partner_centers
  end

  api :PATCH, 'api/v2/partner_center/:id', 'Update a partner center'
  param :id, Integer, desc: 'Id of a partner center', required: true
  param :partner_center, Hash, desc: 'Partner Center' do
    param :description, String, desc: 'Readable description', required: true
    param :embed_link, String, desc: 'Deep link URL obtained from our IDP', required: true
    param :integration, String, desc: 'An integration added to an organization', required: true
  end
  formats ['json']
  api_version 'v2'
  def update
    unless @partner_center.update(partner_center_params)
      render_unprocessable_entity(@partner_center.full_errors) and return
    end
  end

  private

    def partner_center
      @partner_center = current_org.partner_centers.find_by(id: params[:id])
    end

    def partner_center_params
      params.require(:partner_center).permit(:description, :embed_link, :enabled, :integration)
    end
end

class Api::V2::BookmarksController < Api::V2::BaseController
  include TrackActions

  before_action :find_bookmarkable, only: [:create, :destroy]
  has_limit_offset_constraints only: [:index]

  api :GET, "api/v2/bookmarks", "Get Card bookmarks for a user"
  param :limit, String, desc: 'Result limit; default 10', required: false
  param :offset, String, desc: 'Result offset; default 0', required: false
  formats ['json']
  api_version 'v2'
  def index
    @bookmark_cards = current_user.get_bookmarks
      .visible_to_userV2(current_user).limit(limit).offset(offset)
  end

  api :POST, 'api/v2/bookmarks', 'Create bookmark for the current logged in user'
  param :bookmark, Hash, desc: 'Bookmark hash' do
    param :content_id, Integer, desc: "content id", required: true
    param :content_type, String, desc: "Content type. Allowed content types: Card", required: true
  end
  formats ['json']
  api_version 'v2'
  authorize :create, Permissions::BOOKMARK_CONTENT
  def create
    bookmark = current_user.bookmarks.build(bookmarkable: @bookmarkable)
    if bookmark.save
      track_action(@bookmarkable, "bookmark")
      head :ok and return
    else
      render_unprocessable_entity(bookmark.errors.full_messages.join(",")) and return
    end
  end

  api :DELETE, 'api/v2/bookmarks', 'Delete a particular bookmark of the current logged in user'
  param :bookmark, Hash, desc: 'Bookmark hash' do
    param :content_id, Integer, desc: "content id", required: true
    param :content_type, String, desc: "Content type. Allowed content types: Card", required: true
  end
  formats ['json']
  api_version 'v2'
  authorize :destroy, Permissions::BOOKMARK_CONTENT
  def destroy
    bookmark = current_user.bookmarks.where(bookmarkable: @bookmarkable).first!
    if bookmark.destroy
      head :ok and return
    else
      render_unprocessable_entity(bookmark.errors.full_messages.join(",")) and return
    end
  end

  private

  def bookmark_params
    params.require(:bookmark).permit(:content_type, :content_id)
  end

  def find_bookmarkable
    @bookmarkable = load_entity_from_type_and_id(bookmark_params[:content_type], bookmark_params[:content_id])
  end
end

class Api::V2::JourneysController < Api::V2::BaseController
  include CardsFilter
  before_action :set_last_access_opts, only: [:show, :pathway_cards]
  before_action :load_card, :check_journey, :require_card_access

  has_limit_offset_constraints only: [:pathway_cards], limit_default: 10, offset_default: 0

  api :GET, 'api/v2/journeys/:id', 'Get detailed info about journey'
  description <<-EOS
    Render pathway details. This will return info required by journey and the cards in it.
  EOS
  param :id, Integer, 'Card id', required: true
  param :is_standalone_page, String, 'Boolean with `true` or `false` as values', required: false
  param :last_access_at, String, desc: 'Time of last access, format: 1505832150', required: false
  def show
    # Merge to detect if the requests originated from mobile or web.
    @options.merge!(is_native_app: is_native_app?)

    json_response = {}

    fields = params[:is_standalone_page].to_bool ?
      'views_count,channel_ids,voters,channels(id,label),teams(id,label),tags(id,name),tags,channels,teams,language' :
      'auto_complete,tags(id,name),channels(id,label),channels,tags,leaps,language'

    # Generate response for journeySection only, which contain pathway and pack_cards information.
    journey_section  = JourneyBridge.new([@card], user: current_user).attributes[0]
    # Generate minified response for cover card for default fields.
    default_cover_data = CardBridge.new([@card], user: current_user, options: @options).attributes[0]
    # Generate on demand response for cover card
    journey_details = CardBridge.new([@card], user: current_user, fields: fields, options: @options).attributes[0]

    json_response.merge!(default_cover_data)
    json_response.merge!(journey_details)
    json_response.merge!(journey_section)

    render json: json_response, status: :ok
  end

  api :GET, 'api/v2/journeys/:id/pathway_cards', 'Get detailed info about pathway cards'
  description <<-EOS
    This will return the details of cards in a pathway
  EOS
  param :id, Integer, 'Card id of journey', required: true
  param :pathway_id, Integer, 'Card id of pathway', required: true
  param :limit, Integer, 'Maximum number of pathway cards to be returned. Default: 10', required: false
  param :offset, Integer, 'Offset for the pathway cards to be returned. Default: 0', required: false
  param :is_standalone_page, String, 'Boolean with `true` or `false` as values', required: false
  def pathway_cards
    @pathway = load_card_from_compound_id(params[:pathway_id])
    json_response = {}

    # TO DO
    # Find out which keys of pack cards is used in web and then pass `fields`
    # Include `is_standalone_page` attribute for selecting fields
    children_cards, total_count = generate_pathway_children_response(
      pathway_author_id: @pathway.author_id,
      include_default_fields: true,
      fields: params[:fields],
      limit: limit, offset: offset
    )
    json_response.merge!(cards: children_cards)
    json_response.merge!(totalCount: total_count)

    render json: json_response, status: :ok
  end

  private

  def check_journey
    head :not_found and return unless @card.card_type == 'journey'
  end

  def load_card
    @card = load_card_from_compound_id(params[:id])
  end

  # --------------------------- PATHWAY METHODS --------------------------------
  # fields - response keys.
  # include_default_fields - will include default fields if true and vice-versa.
  # returns child cards of a pathway.
  # returns [] when no child cards.
  def generate_pathway_children_response(pathway_author_id:, include_default_fields: false, fields: nil, limit:, offset:)
    # Merge to detect if the requests originated from mobile or web.
    @options.merge!(is_native_app: is_native_app?)

    children_cards = []

    # Only children cards
    ordered_card_pack_rel = CardPackRelation.pack_relations(cover_id: @pathway.id)
      .reject { |cp_rel| cp_rel.from_id == @pathway.id }
    
    # Skipping deleted card pack relation.
    total_count = ordered_card_pack_rel.count {|cp_rel| !cp_rel.deleted }

    # This check is added because we can create draft pathway without any children cards
    if ordered_card_pack_rel.present?
      # Established an array that says if a card in a pathway is locked.
      # FORMAT: { `card_id`: `true/false` }
      card_pack_relations = Hash[
        ordered_card_pack_rel.map do |cp_rel|
          [cp_rel.from_id, cp_rel.locked || false]
        end
      ]
      relations = Card.where(id: card_pack_relations.keys, organization_id: current_org.id)
        .order("FIELD(id, #{card_pack_relations.keys.join(',')})")
        .limit(limit).offset(offset)

      # Generate minified response for cards in a pathway.
      children_cards = CardBridge.new(relations, user: current_user, fields: fields, options: @options).attributes

      if include_default_fields && fields.present?
        minified_children_cards = CardBridge.new(relations, user: current_user, options: @options).attributes
        children_cards.zip(minified_children_cards).each do |children_card, minified_children_card|
          children_card.merge!(**minified_children_card, **{isLocked: card_pack_relations[minified_children_card[:id].to_i]})
        end
      else
        children_cards.each do |children_card|
          children_card.merge!(isLocked: card_pack_relations[children_card[:id].to_i])
        end
      end

      unless (current_user.id == pathway_author_id) || current_user.is_org_admin?
        children_cards = filter_children_cards(children: children_cards)
      end
    end

    [children_cards, total_count]
  end

  def filter_children_cards(children: [])
    return [] unless children.is_a? Array
    
    private_children_cards = children.map { |child_card| OpenStruct.new(child_card) if !child_card[:isPublic] }.compact
    selected_private_children_cards = current_user.private_accessible_cards(private_children_cards.map(&:id))
    rejected_private_card_ids = private_children_cards.map{|private_child_card|private_child_card[:id].to_i} - selected_private_children_cards.map(&:id)
    children.each do |child_card|
      child_card.clear[:message] = 'You are not authorized to access this card' if rejected_private_card_ids.include?(child_card[:id].to_i)
    end unless rejected_private_card_ids.empty?
    children
  end
  # --------------------------- PATHWAY METHODS --------------------------------
end

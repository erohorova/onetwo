class Api::V2::MlpController < Api::V2::BaseController
  api :GET, "api/v2/mlp/latest_assignments", "Get latest learning assignments for user"
  param :limit, Integer, desc: 'Limit, default to 5', required: false
  param :card_types, Array, desc: "An array of card_types. possible values: journey, pack, course", required: false
  formats ['json']
  api_version 'v2'

  def latest_assignments
    @assignments = MlpService.new(
      user_id: current_user.id,
      limit:   params[:limit].presence,
      card_types: params[:card_types]
    ).latest_assignments
  end
end

class Api::V2::TeamsController < Api::V2::BaseController
  include TeamsConcern, TrackActions, SortOrderExtractor
  include TeamControllerLeaderboardConcern
  include CardsFilter

  around_filter :replica_database, only: [:cards, :index, :show]

  has_limit_offset_constraints only: [
    :index,                 :metrics,
    :channels,              :cards,
    :users,                 :top_contributors,
    :top_content,           :activity_streams,
    :search,                :addable_channels,
    :metrics_v2,
  ]
  before_action :set_team, only: [
    :destroy,               :change_user_role,
    :update,                :remove_users,
    :show,                  :channels,
    :cards,                 :remove_shared_card,
    :users,                 :invite,
    :add_users,             :activity_streams,
    :top_contributors,      :top_content,
    :analytics,             :remove_teams_type,
    :addable_channels,      :leave,
    :announcement,          :analytics_v2,
    :carousels
  ]
  before_action :require_org_or_team_admin!, only: [
    :destroy,               :change_user_role,
    :update,                :remove_users,
    :remove_shared_card,    :invite,
    :add_users,             :top_contributors,
    :top_content,           :analytics,
    :announcement,          :analytics_v2,
    :carousels
  ]
  before_action :set_filter_by_language, only: [:cards]
  before_action :set_team_user, only: [:change_user_role]
  before_action :require_organization_admin, only: [:metrics, :metrics_v2]
  before_action -> {check_exclude_group_permission(Permissions::MANAGE_GROUP_ANALYTICS)}, only: [:metrics]
  before_action :perspective_admins, only: [:create, :update]
  before_action :set_invitation, only: [:accept_invite, :decline_invite]
  before_action :check_for_mandatory, only: [:leave]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/v2/teams', 'Return array of teams that user is part of'
  param :all, [true, false], desc: 'All open, my private, and pending teams', required: false
  param :pending, [true, false], desc: 'Teams pending, user', required: false
  param :is_private, [true, false], desc: 'Open or private teams', required: false
  param :role, Array, desc: 'filter by type of teams: admin, sub_admin or member', required: false
  param :assign_id, Integer, desc: 'Content id (card id, course id)', required: false
  param :assign_type, Integer, desc: 'Content type (card, course)', required: false
  param :search_term, String, desc: 'Filter teams by this search term', required: false
  param :sort, String, "Attribute name to sort by, ex name, created_at and team_joined_at, default: created_at"
  param :order, String, "Sort order, asc or desc, default: desc"
  param :writables, String, "Restrict sharing to member & allow sub_admin, admin to post, default: false"
  param :fields, String, desc: 'Required fields for minimal response', required: false
  def index
    sort_query = build_sort_query_for_index
    
    teams_query = if params[:all]&.to_bool
      get_all_teams_for_user(current_user)
    elsif params[:pending]&.to_bool
      current_user.pending_teams
    else
      current_user.teams
    end

    @filtered_query = teams_query

    if params[:is_private].present?
      @filtered_query = @filtered_query.where(is_private: params[:is_private].to_bool)
    end

    if params[:role].present?
      get_teams_for_role(params[:role], current_user)
    end

    if params[:writables].present? && params[:writables].to_bool
      get_writable_teams(current_user)
    end

    if params[:search_term].present?
      @filtered_query = @filtered_query.where("teams.name like ?","%#{params[:search_term]}%")
    end

    @total = @filtered_query.uniq.count
    @teams = @filtered_query.limit(limit).offset(offset).uniq.order(sort_query)

    load_assignable(params)

    if @assignable
      fetch_assigned_groups
      fetch_unassigned_members_count teams: @teams
    end

    # TeamBridge now minimises the response. Only for web.
    # Bypass response for mobile to maintain backward-compatibility.
    if is_native_app?
      teams_ids = @teams.map(&:id)
      @existing_roles = current_user.teams_users.where(team_id: teams_ids).group_by(&:team_id).map do |id, teams_users|
        [id, teams_users.map(&:as_type)]
      end.to_h
    end
  end

  api :GET, 'api/v2/teams/search', 'Search current organization teams'
  param :q, String, desc: 'Query string; default matches all', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :is_private, String, desc: 'Boolean. Show only private or only open teams.', required: false
  param :current_user_teams, String, desc: "Boolean. Show only current user's teams", required: false
  param :current_user_pending_teams, String, desc: "Boolean. Show only current user's pending requests", required: false
  param :admins_names, Array, "Collection of team's admins' names", required: false
  param :leaders_names, Array, "Collection of team's leaders' names", required: false
  param :members_names, Array, "Collection of team's members' names", required: false
  param :skip_aggs, String, desc: 'Boolean. Show aggregation results', required: false
  def search
    filter_params = {}

    # We are skipping aggregation by default as this not requried on org-groups/my and /org-groups/all page.
    # We need aggregation only for this `org-groups/search?q=` i.e when q is present.
    # For above we are sending skip_aggs=false.
    filter_params[:skip_aggs] = params[:skip_aggs].nil? ? true : params[:skip_aggs]&.to_bool

    if params[:is_private].present?
      filter_params[:is_private] = params[:is_private]
    end

    if params[:admins_names].present?
      filter_params[:admins_names] = params[:admins_names]
    end

    if params[:leaders_names].present?
      filter_params[:leaders_names] = params[:leaders_names]
    end

    if params[:members_names].present?
      filter_params[:members_names] = params[:members_names]
    end

    if params[:current_user_teams]&.to_bool
      filter_params[:current_user_teams] = params[:current_user_teams]
    end

    if params[:current_user_pending_teams]&.to_bool
      filter_params[:current_user_pending_teams] = params[:current_user_pending_teams]
    end

    @query = params[:q]

    res = Search::TeamSearch.new.search(q: @query, viewer: current_user,
                                           filter: filter_params,
                                           offset: offset, limit: limit,
                                           load: true)
    @teams = res.results

    @total = res.total
    if !params[:skip_aggs]&.to_bool && @total > 0
      @aggs = res.aggs
    end

    # TeamBridge now minimises the response. Only for web.
    # Bypass response for mobile to maintain backward-compatibility.
    if is_native_app?
      teams_ids = @teams.map(&:id)
      @existing_roles = current_user.teams_users.where(team_id: teams_ids).group_by(&:team_id).map do |id, teams_users|
        [id, teams_users.map(&:as_type)]
      end.to_h
    end
  end

  api :GET, 'api/v2/teams/:id', 'Get team details'
  def show
    # TODO: Integrate with TeamBridge.
    @existing_roles = { @team.id => current_user.teams_users.where(team_id: @team).pluck(:as_type) }
  end

  api :GET, 'api/v2/teams/:id/channels', 'Get list of all channels followed by team'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :fields, String, desc: 'Required fields for minimal response', required: false
  def channels
    @channels = @team.following_channels
    @total_channels = @channels.count
    @channels = @channels.limit(limit).offset(offset)
    if params[:fields]
      # Generate on demand response
      channels = ChannelBridge.new(@channels, user: current_user, fields: params[:fields]).attributes
      render json: {channels: channels, total: @total_channels}, status: :ok
    end

  end

  api :GET, 'api/v2/teams/:id/addable_channels', 'Returns list of channels that can be added to this team exept already added'
  param :id, Integer, desc: 'Id of the card', required: true
  param :q, String, desc: 'Query string; default matches all', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  def addable_channels
    channels_ids = @team.following_channels.pluck(:id)
    @channels = current_org.channels.where.not(id: channels_ids)
    if params[:q].present?
      @channels = @channels.where('label like ?', "%#{params[:q]}%")
    end
    @all_authors_count = ChannelsUser.where(channel_id: @channels.ids)
      .all_authors_types
      .group(:channel_id)
      .count
    @total_channels = @channels.count
    @channels = @channels.limit(limit).offset(offset)
    render 'api/v2/teams/channels'
  end

  api :GET, 'api/v2/teams/:id/cards', 'Get cards of a team depending on type'
  param :type, String, desc: 'Type of team card(assigned/shared)', required: true
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :filter_by_language, [true, false], desc: "Filter cards based on the user's language", required: false
  def cards
    unless ['shared','assigned'].include?(params[:type])
      render_unprocessable_entity("Invalid card type #{params[:type]} ") and return
    end

    @cards = case params[:type]
    when 'shared'
      @team.shared_cards.published.order("teams_cards.id DESC")
    when 'assigned'
      @team.assignment_cards
    end

    if @filter_by_language
      @cards = @cards.filtered_by_language(current_user.language)
    end

    @cards = @cards.visible_to_userV2(current_user) unless current_user.is_org_admin? || @cards.empty?
    if params[:type] == 'assigned'
      @total = Assignment.count_by_sql("SELECT COUNT(*) FROM (#{@cards.to_sql}) assigments")
    else
      @total = @cards.count
    end

    @cards = @cards.limit(limit).offset(offset)

    render 'api/v2/teams/team_cards'
  end

  api :DELETE, 'api/v2/teams/:id/remove_shared', 'Remove shared card from team'
  param :card_id, Integer, desc: 'Card ID to be removed from shared content of team', required: :true
  def remove_shared_card
    shared_card = SharedCard.find_by(team_id: @team.id, card_id: params[:card_id])
    unless shared_card && shared_card.delete
      render_unprocessable_entity("Could not delete card #{params[:card_id]} from team #{params[:id]}") and return
    end
    head :ok
  end

  api :GET, 'api/v2/teams/:id/users', 'List/search group members'
  param :id, Integer, desc: 'Team id', required: true
  param :q, String, desc: 'Query string; default matches all', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :user_type, String, desc: 'Filter results by Pending, admin, member', required: false
  formats ['json']
  def users
    @query = params[:q]

    if params[:user_type].present? && !['member', 'admin', 'pending'].include?(params[:user_type])
      render_not_found("The user role #{params[:role]} is invalid") and return
    end

    unless params[:user_type].blank?
      @invitations = search_pending_team_members(q: params[:q]).limit(limit).offset(offset) if params[:user_type] == 'pending'
      search_team_members(q: params[:q], offset: offset, limit: limit, filter: {role: params[:user_type]}) if ['member', 'sub_admin', 'admin'].include?(params[:user_type])
    else
      # Search across both pending and active members
      @invitations = search_pending_team_members(q: params[:q]).limit(limit).offset(offset)
      search_team_members(q: params[:q], offset: offset, limit: limit)
    end
  end

  api :POST, 'api/v2/teams', 'Creates a team'
  param :is_cms, String, desc: 'Flag for creating team from admin panel', required: false
  param :admins_ids, Array, desc: 'Array. Ids of users who should be added as admins for team', required: false
  param :team, Hash, desc: 'A team hash with params' do
    param :is_dynamic, [true, false], desc: 'Dynamic or static team, default: false', required: false
    param :is_private, [true, false], desc: 'Private or open team, default: false', required: false
    param :is_mandatory, [true, false], desc: 'Mandatory team. Only private team can be mandatory', required: false
    param :name, String, desc: 'Name of the team', required: true
    param :description, String, desc: 'Description of the team', required: true
    param :image, String, desc: 'Image url received from s3 image upload', required: false
    param :auto_assign_content, String, desc: 'Auto assign old and new content to newly added users to group (true/false)(default: true)', required: false
    param :domains, String, desc: 'Auto assign teams to the users which have these domains in their emails, format should be comma seperated domains', required: false
    param :only_admin_can_post, [true, false], desc: 'Restrict groups to allow only sub-admin/admin OR member/admin to post', required: false
  end
  authorize :create, Permissions::CREATE_GROUP
  def create
    create_params = team_params
    create_params["image"] = Filestack::Security.new.signed_url(create_params["image"]) if create_params["image"]
    @team = current_org.teams.build(create_params)
    if @team.save
      if params[:is_cms].try(:to_bool)
        @team.add_to_team(@perspective_admins, as_type: 'admin') if @perspective_admins.present?
      else
        @team.add_admin current_user
      end
      track_action(@team, 'create')
      # TODO: Integrate with TeamBridge.
      @existing_roles = { @team.id => current_user.teams_users.where(team_id: @team).pluck(:as_type) }
    else
      render_unprocessable_entity(@team.full_errors)
    end
  end

  api :PUT, 'api/v2/teams/:id', 'Update team'
  param :id, String, desc: 'Id of team', required: true
  param :is_cms, String, desc: 'Flag for creating team from admin panel', required: false
  param :admins_ids, Array, desc: 'Array. Ids of users who should be added as admins for team', required: false
  param :team, Hash, desc: 'Team hash with params' do
    param :is_dynamic, [true, false], desc: 'Dynamic or static team, default: false', required: false
    param :is_private, [true, false], desc: 'Private or open team, default: false', required: false
    param :is_mandatory, [true, false], desc: 'Mandatory team. Only private team can be mandatory', required: false
    param :name, String, desc: 'Title of the group', required: true
    param :description, String, desc: 'Description for the group', required: true
    param :image, String, desc: 'Banner image for the group', required: false
    param :auto_assign_content, String, desc: 'Auto assign old and new content to newly added users to group (true/false)(default: true)', required: false
    param :domains, String, desc: 'Auto assign teams to the users which have these domains in their emails, format should be comma seperated domains', required: false
    param :only_admin_can_post, [true, false], desc: 'Restrict groups to allow only sub-admin/admin OR member/admin to post', required: false
  end
  def update
    update_params = @team.is_dynamic ? team_params.permit(:image, :description) : team_params
    update_params["image"] = Filestack::Security.new.signed_url(update_params["image"]) if update_params["image"]
    if @team.update_attributes(update_params)
      # TODO: Integrate with TeamBridge.
      @existing_roles = { @team.id => current_user.teams_users.where(team_id: @team).pluck(:as_type) }
      if params[:is_cms].try(:to_bool) && @perspective_admins.present?
        @team.add_to_team(@perspective_admins, as_type: 'admin', actor_name: current_user.name)
      end
      render partial: 'api/v2/teams/details', locals: { team: @team }
    else
      render_unprocessable_entity(@team.full_errors)
    end
  end

  api :POST, 'api/v2/teams/:id/invite', 'Invite users to team'
  param :emails, Array, desc: 'Array of recipient emails to invite', required: true
  param :roles, String, desc: 'role of the user in the invited team, example: member, admin or sub_admin', required: true
  def invite
    unless TeamsUser::GROUP_ROLES.include?(params[:roles])
      render_unprocessable_entity("Invalid role for team user") and return
    end

    payload = {invitable: @team, roles: params.require(:roles)}

    if params[:emails].blank?
      render_unprocessable_entity("Something went wrong. Please try again, Missing email") and return
    end

    if params[:emails].is_a?(Array)
      payload[:recipient_emails] = params[:emails]
      payload[:recipient_emails].reject! {|email| email.match(Devise::email_regexp).nil? }
    else
      render_unprocessable_entity("Invalid format for param emails") and return
    end
    payload[:sender_id] = current_user.id
    process_multi_invites(payload) and return
  end

  api :POST, 'api/v2/teams/:id/add', 'Add users directly to team'
  param :user_ids, Array, desc: 'Array of user ids to be added to the team', required: true
  param :role, String, desc: 'Role of user to be added to the team, example: member or admin', required: true
  def add_users
    # check if valid role
    unless TeamsUser::GROUP_ROLES.include?(params[:role])
      render_unprocessable_entity('Invalid role for team user') and return
    end
    # user_ids must be an array
    if !params[:user_ids].is_a?(Array) || params[:user_ids].blank?
      render_unprocessable_entity('Missing required params') and return
    end
    user_ids = params[:user_ids].reject(&:blank?)
    TeamAddUsersJob.perform_later(
      team_id:  @team.id,
      user_ids: user_ids,
      as_type:  params[:role],
      actor_name: current_user.name
    )

    head :ok
  end

  api :DELETE, 'api/v2/teams/:id', 'Destroy a team'
  param :id, String, desc: 'Id of team', required: true
  def destroy
    if @team.destroy
      head :no_content
    else
      render_unprocessable_entity(@team.errors.full_messages.join(", ")) and return
    end
  end

  api :DELETE, 'api/v2/teams/:id/remove_teams_type', 'remove teams_users type for users in team'
  param :user_ids, Array, desc: 'Array of user ids to be remove role in team', required: true
  param :role, String, desc: 'Role of user to be remove for user in team, example: admin or sub_admin', required: true
  def remove_teams_type
    user_ids = current_org.users.where(id: params[:user_ids]).pluck(:id)
    if user_ids.blank? || params[:role].blank?
      return render_unprocessable_entity('Missing required params')
    end
    @team.remove_users_type(user_ids, params[:role])
    head :ok
  end

  api :GET, 'api/v2/teams/:id/top_content', 'To Fetch top content of a team'
  param :id, Integer, desc: 'Id of team', required: true
  def top_content
    @cards = @team.top_content.limit(limit).offset(offset)
  end

  api :DELETE, 'api/v2/teams/:id/users', 'Delete users from team'
  param :id, String, desc: 'Id of team', required: true
  param :user_ids, Array, desc: 'Array of user ids', required: true
  param :is_pending, [true, false], desc: 'Invited users', required: false
  def remove_users
    if params[:user_ids].reject!(&:blank?) || params[:user_ids].blank?
      return render_unprocessable_entity('Missing required params')
    end
    # use destroy_all if user was admin and sub_admin
    team_user = if params[:is_pending]&.to_bool
      # search for invites and delete pending users
      @team.invitations.where(recipient_id: params[:user_ids])
    else
      @team.teams_users.where(user_id: params[:user_ids])
    end
    team_user.destroy_all

    head :no_content
  end

  api :GET, 'api/v2/teams/:id/top_contributors', 'Get top contributors of the team'
  param :id, Integer, desc: 'Id of team', required: true
  def top_contributors
    @top_contributors = @team.top_contributors(limit, offset)
  end

  api :PUT, 'api/v2/teams/:id/users/:user_id', 'Toggle role between member and admin'
  param :id, String, desc: 'Id of team', required: true
  param :user_id, String, desc: 'Id of user', required: true
  def change_user_role
    @team.modify_user(@user, op: :switch_role)
    @role = TeamsUser.find_by(team_id: params[:id], user_id: params[:user_id]).as_type
    # TODO: Integrate with TeamBridge.
    @existing_roles = { @team.id => current_user.teams_users.where(team_id: @team).pluck(:as_type) }
  end

  api :GET, 'api/v2/teams/metrics', 'Get team level metrics'
  param :from_date, String, desc: "Starting date. Format: mm/dd/yyyy. Metrics are inclusive of this date", required: false
  param :to_date, String, desc: "End date. Format: mm/dd/yyyy. Metrics are inclusive of this date", required: false
  param :order_attr, String, desc: "Attribute to sort on. One of \"smartbites_consumed\", \"smartbites_created\", \"smartbites_comments_count\", \"smartbites_liked\", \"time_spent\""
  param :order, String, desc: "Either \"asc\" or \"desc\", default: \"desc\"", required: false
  param :search_term, String, desc: "Show only team names that match the search term", required: false
  # =========================================
  # NOTE: Would like to deprecate this in favor of metrics_v2
  # =========================================
  def metrics
    render_unprocessable_entity("`from_date or `to date` is missing") and return if params[:from_date].blank? || params[:to_date].blank?

    from_date = Time.strptime(params[:from_date], "%m/%d/%Y").utc.beginning_of_day
    to_date   = Time.strptime(params[:to_date], "%m/%d/%Y").utc.end_of_day

    render_unprocessable_entity("Start date can not be greater than end date") and return if from_date > to_date
    render_unprocessable_entity("Exceeded maximum date range") and return if ((to_date - from_date) / 1.year) > Settings.analytics_max_date_range
    order_attr, order = extract_sort_order(valid_order_attrs: ["smartbites_consumed", "smartbites_comments_count", "smartbites_liked", "smartbites_created", "time_spent"], default_order_attr: "smartbites_consumed")
    team_ids = if current_user.group_sub_admin_authorize?(Permissions::MANAGE_GROUP_ANALYTICS)
      current_user.team_ids_for_group_type(['sub_admin'])
    end
    @metrics = current_org.team_level_metrics_for(
      q: params[:search_term],
      from_date: from_date,
      to_date: to_date,
      team_ids: team_ids
    ).limit(limit).offset(offset).order("#{order_attr} #{order}")
    @teams = current_org.teams.where(id: @metrics.map(&:team_id)).index_by(&:id)
    @metrics = @metrics.to_a.reject{|metric| !@teams.has_key?(metric.team_id)}
  end

  api :GET, 'api/v2/teams/:id/analytics', 'Analytics specific to the team'
  param :id, Integer, desc: "ID of team for which analytics is to be fetched"
  param :from_date, String, desc: "Starting date. Format: mm/dd/yyyy. Metrics are inclusive of this date", required: true
  param :to_date, String, desc: "End date. Format: mm/dd/yyyy. Metrics are inclusive of this date", required: true
  # param :period, String, desc: "Will be day, week, month or year", required: false
  # =========================================
  # NOTE: Would like to deprecate this in favor of analytics_v2
  # =========================================
  def analytics

    from_to_date = from_to_date_params(params[:from_date], params[:to_date])
    return unless from_to_date

    period = :year
    offsets_an_timeranges = offsets_and_timeranges(from_to_date[:from_date], from_to_date[:to_date], period)

    custom_period_data = @team.metrics_over_span(
      organization: current_org,
      team_id: params[:id],
      period: period,
      offsets: [offsets_an_timeranges[:time_period_offsets_from], offsets_an_timeranges[:time_period_offsets_to]],
      timeranges: offsets_an_timeranges[:timeranges])

    drill_down_data_for_custom_period = @team.drill_down_data_for_custom_period(
      organization: current_org,
      team_id: params[:id],
      timerange: [from_to_date[:from_date], from_to_date[:to_date]],
      period: period)

    result = Hash[Team::METRICS.map{|metric|
      [metric, {
        value: custom_period_data[metric],
        drilldown: {
          values: drill_down_data_for_custom_period[metric],
        }
      }]
    }]

    clc_progress = ClcService.new
      .team_clc_progress(from_to_date[:from_date].to_date, from_to_date[:to_date].to_date, @current_org, team_id: params[:id])
    result[:clc_progress] = {value: clc_progress.count, drilldown: {values: clc_progress}}

    render json: result, status: :ok and return
  end

  api :POST, 'api/v2/teams/:id/accept_invite', 'Accept join invite'
  param :id, Integer, desc: 'Id of team', required: true
  def accept_invite
    # accept the invitation and add the user to the team
    @team = @invite.invitable
    unless @invite.destroy
      return render_unprocessable_entity(@invite.full_errors)
    end
    if @team.respond_to?("add_#{@invite.roles}".to_sym)
      @team.send("add_#{@invite.roles}".to_sym, current_user)
    end

    track_action(@team, 'join', current_user)
    NotificationGeneratorJob.perform_later(
      { item_id: @team.id, item_type: 'accept_group_invite', team_user_id: current_user.id }
    )
    # TODO: Integrate with TeamBridge.
    @existing_roles = { @team.id => current_user.teams_users.where(team_id: @team).pluck(:as_type) }
    render 'show'
  end

  api :GET, 'api/v2/teams/:id/activity_streams', 'List of team activity streams'
  param :id, Integer, desc: 'Team id', required: true
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :only_conversations, String, desc: 'Boolean: true/false show only conversation', required: false
  formats ['json']
  api_version 'v2'
  def activity_streams
    @team_streams = FetchActivitiesService.new(
      user: current_user,
      team_id: @team.id,
      filter: {
        only_conversations: params[:only_conversations]&.to_bool,
        limit: limit,
        offset: offset
      }
    ).fetch_activities(filter_by_team: true)
  end

  api :POST, 'api/v2/teams/:id/decline_invite', 'Decline invite for a team'
  param :id, Integer, desc: 'Team id', required: true
  def decline_invite
    # to refuse the invitation to join the team
    # delete the invitation
    @team = @invite.invitable
    if @invite.destroy
      NotificationGeneratorJob.perform_later(
        { item_id: @team.id, item_type: 'decline_group_invite', team_user_id: current_user.id }
      )
      # TODO: Integrate with TeamBridge.
      @existing_roles = { @team.id => current_user.teams_users.where(team_id: @team).pluck(:as_type) }
      render 'show'
    else
      render_unprocessable_entity(@invite.full_errors)
    end
  end

  api :POST, 'api/v2/teams/:id/join', 'User joins in team'
  param :id, Integer, desc: 'Team ID', requred: true
  def join
    @team = current_org.teams.friendly.find(params[:id])
    return render_not_found('Team not found') if @team.blank?
    return render_unprocessable_entity('Team is not open') if @team.is_private?
    if @team.user_ids.include?(current_user.id)
      return render_unprocessable_entity('User is already exist in this team')
    end
    if @team.add_member(current_user)
      NotificationGeneratorJob.perform_later(
        { item_id: @team.id, item_type: 'join_group', team_user_id: current_user.id }
      )
    end
    # TODO: Integrate with TeamBridge.
    @existing_roles = { @team.id => current_user.teams_users.where(team_id: @team).pluck(:as_type) }
    render 'show'
  end

  api :DELETE, 'api/v2/teams/:id/leave', 'User leave team'
  param :id, Integer, desc: 'Team ID', requred: true
  def leave
    teams_users = @team.teams_users.where(user: current_user)
    if teams_users.present?
      teams_users.destroy_all
      NotificationGeneratorJob.perform_later(
        { item_id: @team.id, item_type: 'leave_group', team_user_id: current_user.id }
      )
      invite = @team.invitations.find_by(recipient_id: current_user.id)
      if invite.present?
        @messages = [invite.full_errors] unless invite.destroy
      end
      # TODO: Integrate with TeamBridge.
      @existing_roles = { @team.id => current_user.teams_users.where(team_id: @team).pluck(:as_type) }
      render 'show'
    else
      render_unprocessable_entity('User does not exist in this team')
    end
  end

  api :POST, '/api/v2/teams/:id/announcement', 'Announcements for team'
  param :team_id, Integer, desc: 'Team ID', requred: true
  param :mobile_notification, String, desc: 'Boolean, true if mobile_notification is to be sent'
  param :email_notification, String, desc: 'Boolean, true if email_notification is to be sent'
  param :notification, Hash, desc: 'Notification hash with params' do
    param :content, String, desc: 'Announcement text'
  end
  def announcement
    render_unprocessable_entity('Cannot announce to Everyone team') and return if @team.is_everyone_team?
    mobile_notification if params[:mobile_notification]
    email_notification if params[:email_notification]
    head :ok
  end

  api :PUT, 'api/v2/teams/:id/carousels', 'Update team carousels config'
  param :id, Integer,  desc: 'Id of the team', required: true
  param :carousels, Array, desc: 'Array of team carousels.', required: true
  def carousels
    carousels = carousels_params[:carousels].delete_if(&:blank?)
    if carousels.present?
      carousels_config = @team.set_carousels_config(carousels.to_json)
      render json: {carousels: carousels_config&.parsed_array_value}, status: :ok
    else
      render_unprocessable_entity('Missed required params')
    end
  end

  private

  def team_params
    params.require(:team).permit(
      :description, :image, :name, :email, :roles, :auto_assign_content,
      :is_private, :is_mandatory, :is_dynamic, :domains, :only_admin_can_post
    )
  end

  def set_member_role
    @users.each do |user|
      user.role = user.teams.find{|t| t.id.to_i == @team.id}.try(:role)
      # set whether user's invitation is pending
      user.pending = false
    end
  end

  # Check whether user is following team user or not
  def set_followed_user_info
    followed_user_ids = current_user.followed_user_ids

    @users.each do |u|
      u['is_followed'] = followed_user_ids.include?(u.id)
      u['is_current_user'] = current_user.id == u.id
    end
  end

  def from_to_date_params(from_date, to_date)
    render_unprocessable_entity("`from_date or `to date` is missing") and return if from_date.blank? || to_date.blank?

    from_date = Time.strptime(from_date, "%m/%d/%Y").utc.beginning_of_day
    to_date   = Time.strptime(to_date, "%m/%d/%Y").utc.end_of_day

    render_unprocessable_entity("Start date can not be greater than end date") and return if from_date > to_date
    render_unprocessable_entity("Exceeded maximum date range") and return if ((to_date - from_date) / 1.year) > Settings.analytics_max_date_range

    {from_date: from_date, to_date: to_date}
  end

  def offsets_and_timeranges(from_date, to_date, period)
    time_period_offsets_from = PeriodOffsetCalculator.applicable_offsets(from_date)[period]
    time_period_offsets_to = PeriodOffsetCalculator.applicable_offsets(to_date)[period]
    timeranges = [[from_date, nil], [nil, to_date]]
    {time_period_offsets_from: time_period_offsets_from, time_period_offsets_to: time_period_offsets_to, timeranges: timeranges}
  end

  def search_team_members(q: nil, offset: , limit: , filter: {})
    filter.merge! scope: @team, uncomplete: true
    @response = Search::UserSearch.new.search(q: q, viewer: current_user, offset: offset, limit: limit, allow_blank_q: true,
      sort: :created_at, order: :desc, filter: filter)
    @users = @response.results
    @total = @response.total
    set_member_role
    set_followed_user_info
  end

  def search_pending_team_members(q: )
    query = Invitation.where(invitable: @team).pending.where('recipient_email like ?', "%#{q}%")
    @invitations_total = query.count
    query.order(created_at: :desc)
  end

  def require_org_or_team_admin!
    render_unauthorized unless @team.is_admin?(current_user) || current_user.is_admin_user?
  end

  def check_for_mandatory
    render_unprocessable_entity('You can not leave mandatory team') if @team.is_mandatory
  end

  def process_multi_invites(payload)
    new_invite_emails   = []
    sent_invite_emails = []
    recipient_ids = []
    payload[:recipient_emails].each do |email|

      existing_invitation = Invitation.where(
        invitable: @team,
        roles: payload[:roles],
        recipient_email: email
      ).first

      existing_user = @team.users.where(email: email).first
      recipient_id = current_org.users.find_by(email: email)&.id
      if existing_invitation || existing_user
        sent_invite_emails << email
      else
        new_invite_emails << email
        recipient_ids << recipient_id
      end
    end

    if new_invite_emails.present?
      MultiInvitationsJob.perform_later(
        emails: new_invite_emails,
        sender_id: current_user.id,
        recipient_ids: recipient_ids,
        invitable_type: @team.class.name,
        invitable_id: @team.id,
        roles: payload[:roles]
      )
    end

    if sent_invite_emails.present?
      render(
        json: {"message": "Some users were already invited: #{sent_invite_emails.join(', ')}" },
        status: :ok
      )
    else
      head :ok
    end
  end

  def build_sort_query_for_index
    sort_keys = %w(name created_at team_joined_at)
    sort_by = params.fetch(:sort, :created_at)
    sort_order = params.fetch(:order, :desc)

    unless sort_keys.include? sort_by.to_s
      render_unprocessable_entity('Sort key is invalid') and return
    end

    if sort_by == 'team_joined_at'
      "teams_users.created_at #{sort_order}"
    else
      "teams.#{sort_by} #{sort_order}"
    end
  end

  def perspective_admins
    @perspective_admins = current_org.users.where(id: params[:admins_ids])
  end

  def set_invitation
    # find invite to the team for current_user
    @invite = Invitation.find_by(
      invitable_type: 'Team',
      invitable_id: params[:id],
      recipient_email: current_user.email
    )
    if @invite.blank? || !@invite.pending?
      return render_not_found("Invite for team #{params[:id]} not found")
    end
  end

  def notification_params
    params.require(:notification).permit(:content)
  end

  def mobile_notification
    TeamPushNotificationJob.perform_later({
      content: notification_params[:content],
      team_id: @team.id
    })
  end

  def email_notification
    TeamUsersMailerJob.perform_later({
      message: notification_params[:content],
      team_id: @team.id,
      sender_id: current_user.id
    })
  end

  def get_all_teams_for_user(user)
    if user.is_org_admin?
      user.organization.teams
    else
      user.all_user_teams
    end
  end

  def get_teams_for_role(role, user)
    roles = role.is_a?(Array) ? role : [role]
    
    if (TeamsUser::GROUP_ROLES & roles).present?
      @filtered_query = @filtered_query.joins("LEFT JOIN teams_users tu ON teams.id = tu.team_id")
        .where('teams_users.user_id = ?', current_user.id)
        .where('teams_users.as_type in (?)', roles)
    end
  end

  def get_writable_teams(user)
    @filtered_query = @filtered_query.joins("LEFT JOIN teams_users tu ON teams.id = tu.team_id")
    @filtered_query = @filtered_query
      .where('teams_users.user_id = ?', user.id)
      .where("teams.only_admin_can_post = false or
      (teams_users.as_type in ('sub_admin','admin') AND teams.only_admin_can_post = true)")
  end

  def carousels_params
    params.permit(carousels: [:default_label, :index, :visible])
  end
end

class Api::V2::AnalyticsController < Api::V2::BaseController

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized to access channel'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :POST, 'api/v2/analytics/record_event', 'Record a generic analytics event to InfluxDB'
  param :event,       String, desc: "Event name", required: true
  param :card_id,     String, desc: "Card id. Only used if measurement = cards"
  param :channel_id,  String, desc: "Card id. Only used if measurement = channels"
  param :group_id,    String, desc: "Group id. Only used if measurement = groups"
  param :attributes,  Hash,   desc: "Any additional attributes to record with the event"
  def record_event
    recorder_params = params.slice(*%i{
      event measurement card_id channel_id group_id attributes
    })
    event, err = Analytics::GenericEventRecorder.new(
      user_id: current_user.id.to_s,
      org_id:  current_org.id.to_s,
      params:  recorder_params
    ).enqueue_recording_job

    if err then render_unprocessable_entity(err)
    else render(json: event, status: :ok)
    end
  end  

  # =========================================
  # DEPRECATED. Use 'api/v2/record_event' instead
  # =========================================
  api :POST, 'api/v2/analytics', 'Post Analytics Events on Content'
  param :event, Hash, desc: 'Event hash' do
    param :content_id, Integer, desc: "content id", required: true
    param :content_type, String, desc: "Content type. Allowed content types: Card", required: true
    param :type, String, desc: "Event type. Allowed types: \"impression\"", required: true
    param :properties, Hash, desc: "Free form attributes associated with the event. Currently supported - a key called \"view\" with the values: \"summary\" or \"full\", corresponding to summary impression and full impression for a piece of content. For example - If a user sees a card on the feed, pass in {\"type\": \"impression\", \"properties\": {\"view\": \"summary\"}. If a user visits the standalong page for the card, pass the same thing, except set \"view\" key to \"full\". Both event types and supported properties can be expanded, with attention paid to how downstream aggregators handle such new events", required: false
    param :referer, String, desc: "Request referer source"
  end
  def create
    render_with_status_and_message(
      'API has been removed', status: :gone
    ) and return
  end

  api :POST, 'api/v2/analytics/visit', 'Record view event on entity'
  param :entity_type, String, desc: 'entity type. possible values: Card, Channel', required: true
  param :entity_id, String, desc: 'entity id. ex, card id, channel id', required: true
  def visit
    entity = load_entity_from_type_and_id(params[:entity_type], params[:entity_id])
    record_card_viewed_metric(entity, current_user)
    render nothing: true
  end

  api :GET, 'api/v2/analytics/group_performance', 'Get user performance report for a given group'
  param :group_id, String, desc: 'Id of a given group', required: true
  def group_performance
    # =========================================
    # The following is DEPRECATED!
    # =========================================
    render_unprocessable_entity("Group id cannot be blank") and return if params[:group_id].blank?
    unless current_org.teams.pluck(:id).include?(params[:group_id].to_i)
      render_not_found("Group id not found") and return
    end

    group_query = "select * from \"groups\" where org_id = '%{org_id}' AND group_id = %{group_id} AND event = 'group_user_added'"
    group_users = INFLUXDB_CLIENT.query(group_query, params: {
      group_id: params[:group_id],
      org_id: current_org.id
    })

    if group_users.blank?
      render json: group_users
    else
      group_user_ids = group_users[0]['values'].collect {|v| v['user_id']}
      #TODO: update schema to have card_id in field_set
      group_performance = INFLUXDB_CLIENT.query("select count(card_id) from cards where _user_id =~ /^(#{group_user_ids.map(&:to_i).join('|')})$/ group by event")
      render json: group_performance
    end
  end

  private

  def event_params
    params.require(:event).permit(
      :content_id, :content_type, :type, :referer, :properties => [:view]
    )
  end

  def record_card_viewed_metric(entity, user)
    return unless entity && user
    Analytics::ViewEventMetricRecorder.record_view_event(
      entity: entity,
      viewer: user
    )
  end

end

class Api::V2::TopicRequestsController < Api::V2::BaseController

  has_limit_offset_constraints only: [:index]
  before_action :require_organization_admin, only: [:approve, :reject, :publish]
  before_action :find_topic_request, only: [:approve, :reject, :publish]
  before_action :check_batch_values, only: [:batch_create]
  before_action :check_publish_values, only: [:publish]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :POST, '/api/v2/topic_requests', 'Create topic request '
  param :topic, Hash, desc: 'Topic Request' do
    param :label, String, desc: 'label of the topic', required: true
  end
  def create
    return render_unprocessable_entity('Similar topic is already created') if TopicRequest.find_by(label: topic_request_params[:label], organization: current_org)

    @topic_request = TopicRequest.new(topic_request_params)
    @topic_request.requestor = current_user
    @topic_request.organization = current_org

    unless @topic_request.save
      return render_unprocessable_entity(@topic_request.errors.full_messages.join(','))
    end
    head :ok
  end

  api :POST, 'api/v2/topic_requests/batch_create', 'Create topic requests'
  param :topic, Array, desc: 'Array of topics to be added', required: true
  def batch_create
    errors = []
    @topics.each do |topic|
      topic_request = TopicRequest.new(label: topic)
      topic_request.requestor = current_user
      topic_request.organization = current_org
      unless topic_request.save
        errors.push("#{topic} #{topic_request.errors.full_messages.join(', ')}")
      end
    end
    if errors.any?
      message = "There were errors when adding the following content: #{errors.join(", ")}"
      render_with_status_and_message(message, status: :multi_status)
    else
      head :ok
    end
  end

  api :PUT, 'api/v2/topic_requests/:id/approve', 'Approve topic_request'
  description <<-EOS
    This endpoint is to approve topic_request.
  EOS
  def approve
    @topic_request.approver = current_user
    if @topic_request.approve!
      head :ok
    else
      render_unprocessable_entity(@topic_request.errors.full_messages.join(', '))
    end
  end

  api :PUT, 'api/v2/topic_requests/:id/reject', 'Reject topic_request'
  description <<-EOS
    This endpoint is to reject topic_request.
  EOS
  def reject
    @topic_request.approver = current_user
    if @topic_request.reject!
      head :ok
    else
      render_unprocessable_entity(@topic_request.errors.full_messages.join(', '))
    end
  end

  api :PUT, 'api/v2/topic_requests/:id/publish', 'Publishes the topic_request'
  param :taxonomy_topic, Hash, desc: 'Hash of learning topics', required: true do
    param :topic_id, String, desc: "Topic id from taxonomies", required: true
    param :topic_name, String, desc: "Name from taxonomies example: 'software.mobile'", required: true
    param :topic_label, String, desc: "Topic label example 'C#'", required: true
    param :domain_id, String, desc: "domain id from taxonomies", required: true
    param :domain_name, String, desc: "Name from taxonomies engineering.technology", required: true
    param :domain_label, String, desc: "Domain label from taxonomies Example Technology", required: true
  end
  def publish
    @topic_request.publisher = current_user
    if @topic_request.publish!
      @topic_request.update_requestor_learning_topic(taxonomy_topic_params)
      head :ok
    else
      render_unprocessable_entity(@topic_request.errors.full_messages.join(', '))
    end
  end

  api :GET, '/api/v2/topic_requests', 'Returns the list of topic request of the org'
  param :state, Array, desc: 'Array of topic states can be pending, approved, rejected, published ', required: true
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :order, String, desc: "Either asc or desc, default: desc", required: false
  param :sort, String, "Attribute name to sort by param. Default: created_at date desc"
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :requestor_id, Integer, desc: 'Id of the user, who requested the topic', required: false
  param :q, String, 'Search term, this will returm only exact match', required: false
  def index
    return render_bad_request('State param is missing') if params[:state].nil?
    @query = TopicRequest.where(organization: current_org, state: params[:state]).includes(:requestor)

    if params[:q].present?
      @query = @query.where(label: params[:q])
    end

    if params[:requestor_id].present?
      @query = @query.where(requestor_id: params[:requestor_id])
    end

    if params[:sort].present?
      order = %w[asc desc].include?(params[:order]&.downcase) ? params[:order] : 'desc'
      @query = @query.order(params[:sort] => order)
    else
      @query = @query.order('created_at desc')
    end

    @total = @query.count
    @topic_requests = @query.offset(offset).limit(limit)
  end

  api :GET, '/api/v2/topic_requests/aggregate_data', 'Returns the summary of topics states count'
  def aggregate_data
    @results = TopicRequest.where(organization: current_org).group(:state).count('state')
  end

  private


  def topic_request_params
    params.require(:topic).permit(:label)
  end

  def taxonomy_topic_params
    params.require(:taxonomy_topic).permit(:topic_name, :topic_id, :topic_label, :domain_name, :domain_id, :domain_label )
  end

  def find_topic_request
    @topic_request = current_org.topic_requests.find_by(id: params[:id])
    unless @topic_request.present?
      render_not_found('Topic Request not found') and return
    end
  end

  def check_publish_values
    if params[:taxonomy_topic].is_a?(Hash)
      @taxonomy_topic = params[:taxonomy_topic]
      unless @taxonomy_topic.values_at(*%i( topic_id topic_name topic_label)).all?(&:present?)
        render_unprocessable_entity('Missing required topic_id, topic_name, topic_label or domain_id param') and return
      end
    else
      render_unprocessable_entity('Invalid data type, taxonomy_topic should be hash')
    end
  end

  def check_batch_values
    if params[:topics].is_a?(Array)
      @topics = params[:topics].reject(&:blank?)
      render_unprocessable_entity('Missing required params Or topics is empty') if @topics.empty?
    else
      render_unprocessable_entity('Invalid data type, topics should be array')
    end
  end
end

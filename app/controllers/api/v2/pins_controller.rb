class Api::V2::PinsController < Api::V2::BaseController
  include CardsFilter
  has_limit_offset_constraints only: [:index]

  before_action :find_object, only: [:pin, :unpin]
  before_action :require_creator_or_curator
  before_action :set_filter_by_language, only: [:index]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/v2/pins', 'Get all pins of Pinnable'
  description <<-EOS
    Currently, Bookmark, Channel and Team are Pinnable with Objectable being
    only Card.
  EOS
  param :pin, Hash, desc: 'Get all pinned objects for pinnable' do
    param :pinnable_id, Integer, desc: 'Pinnable id. Bookmark ID/Channel ID/Team ID', required: true
    param :pinnable_type, String, desc: 'Pinnable type. Bookmark/Channel/Team', required: true
  end
  param :filter_by_language, [true, false], desc: "Filter cards based on the user's language", required: false
  param :limit, Integer, desc: 'Limit, default 10', required: false
  param :offset, Integer, desc: 'Offset, default 0', required: false
  param :object, Hash, 'Sort and order options for object of a pin' do
    param :sort, String, desc: <<-TXT, required: false
      Sort criteria for object. Currently, object is only Card. The sorting options
      for Card are created_at, likes_count, views_count
    TXT
    param :order, String, desc: 'Sort order(asc or desc)', required: false
  end
  def index
    find_pinnable
    if @pinnable.try(:auto_pin_cards?)
      @pins = Pin.fetch_featured_pins(@pinnable, count: limit)
    else
      @pins = @pinnable.pins.includes(:object)
    end
    @pin_objects = sort_and_order_object(params).limit(limit).offset(offset) if @pins.present?
  end

  api :POST, 'api/v2/pin', 'Create a pin'
  description <<-EOS
    Create pin for a channel (with pinnable_id and pinnable_type) based on object_id and object_type("Card").
    pinnable_id, pinnable_type, object_id and object_type are required.
  EOS
  param :pin, Hash, desc: 'Pin hash' do
    param :pinnable_id, Integer, desc: "Pinnable id. Channel id", required: true
    param :pinnable_type, String, desc: "Pinnable type. Channel", required: true
    param :object_id, Integer, desc: "id of the object wants to pin. Card id", required: true
    param :object_type, String, desc: "object type. Allowed object types: Card", required: true
  end
  formats ['json']
  api_version 'v2'
  def pin
    @pin = Pin.new(pinnable: @pinnable, object: @object, user: current_user)
    if @pin.save
      head :ok and return
    else
      render_unprocessable_entity(@pin.full_errors) and return
    end
  end

  api :DELETE, 'api/v2/unpin', 'Delete a pin for pinnable'
  description <<-EOS
    Deletes a pin from a channel (with pinnable_id and pinnable_type) based on object_id and object_type("Card").
    pinnable_id, pinnable_type, object_id and object_type are required.
  EOS
  param :pin, Hash, desc: 'Pin hash' do
    param :pinnable_id, Integer, desc: "Pinnable id. Channel id", required: true
    param :pinnable_type, String, desc: "Pinnable type. Channel", required: true
    param :object_id, Integer, desc: "id of the object wants to unpin. Card id", required: true
    param :object_type, String, desc: "object type. Allowed object types: Card", required: true
  end
  formats ['json']
  api_version 'v2'
  def unpin
    pin = Pin.find_by(pinnable: @pinnable, object: @object)

    render_not_found and return unless pin

    if pin.destroy
      head :ok and return
    else
      render_unprocessable_entity(pin.full_errors) and return
    end
  end

  protected

  def pinnable_params
    params.require(:pin).permit(:pinnable_id, :pinnable_type, :object_type, :object_id)
  end

  def require_creator_or_curator
    if @pinnable.class == Channel
      creator_or_curator = @pinnable.is_creator?(current_user) || @pinnable.is_curator?(current_user)
      render_unauthorized("Creator or curator can only pin.") unless creator_or_curator
    end
  end

  def find_pinnable
    @pinnable = load_entity_from_type_and_id(pinnable_params[:pinnable_type], pinnable_params[:pinnable_id])
  end

  def find_object
    find_pinnable
    @object = load_entity_from_type_and_id(pinnable_params[:object_type], pinnable_params[:object_id])
  end

  # Currently, only 'Card' is objectable.
  # TO DO: As and when we add new objectables, this method will have to be scaled.
  def sort_and_order_object(params)
    cards = Card.includes(:pins).where(id: @pins.map(&:object_id))
    cards = cards.filtered_by_language(current_user.language) if @filter_by_language

    # Apply default sorting and order if sort-order not passed
    unless params[:object].present? && params[:object][:sort].present? && params[:object][:order].present?
      return cards.order('pins.id desc')
    end

    sort_by = params[:object][:sort].to_sym
    order = params[:object][:order]

    case sort_by
    when :created_at
      cards.order("cards.created_at #{order}")
    when :likes_count
      cards.joins("LEFT JOIN content_level_metrics on cards.id = content_level_metrics.content_id")
            .where(content_level_metrics: { period: ['all_time', nil], offset: [0, nil] })
            .order("content_level_metrics.likes_count #{order}")
    when :views_count
      cards.joins("LEFT JOIN content_level_metrics on cards.id = content_level_metrics.content_id")
            .where(content_level_metrics: { period: ['all_time', nil], offset: [0, nil] })
            .order("content_level_metrics.views_count #{order}")
    end
  end
end

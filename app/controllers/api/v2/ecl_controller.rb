class Api::V2::EclController < Api::V2::BaseController

  before_filter :authenticate_ecl_with_jwt
  skip_before_filter :authenticate_user_from_token!

  api :POST, 'api/v2/ecl/sync_card', 'Sync the card from external client i.e. ECL'
  param :ecl_id, Integer, desc: 'Card ecl id', required: true
  param :organization_id, Integer, desc: 'Card organization id', required: true
  formats ['json']
  def sync_card
    if params['ecl_id'].present? && params['organization_id'].present? &&
      (
        card = Card.find_by(
          organization_id: params['organization_id'],
          ecl_id: params['ecl_id']
        )
      )

      ecl_card_connector = EclApi::CardConnector.new(ecl_id: card.ecl_id)
      ecl_card_connector.update_card_change_from_ecl(card, params)
      render nothing: true
    else
      render_bad_request("Card not found")
    end
  end

  api :POST, 'api/v2/ecl/sync_team_sources', 'Update team sources'
  param :del_team_ids, Array, desc: 'removed team ids from source', required: false
  param :new_team_ids, Array, desc: 'newly added team ids to source', required: false
  param :source_id, Integer, desc: 'Source id which was updated with team_ids in ecl', required: true
  param :organization_id, Integer, desc: 'Source Id of organization', required: true

  def sync_team_sources
    if params[:source_id].present? && params[:organization_id].present?
      if params[:del_team_ids].present?
        Team.where(organization_id: params[:organization_id], id: params[:del_team_ids]).find_each do |team|
          team.source_ids.delete(params[:source_id])
          team.save
        end
      end
      if params[:new_team_ids].present?
        Team.where(organization_id: params[:organization_id], id: params[:new_team_ids]).find_each do |team|
          team.source_ids = (team.source_ids << params[:source_id]).flatten.uniq
          team.save
        end
      end
      Rails.cache.delete("organization_#{params[:organization_id]}_team_private_source_ids")
      render nothing: true
    else
      render_bad_request("Not able to delete team cards, for params: #{params}")
    end
  end

  api :DELETE, 'api/v2/ecl/delete_cards_of_source', 'Delete the cards that were created from the source id'
  param :source_id, Integer, desc: 'Source id which was deleted from ecl', required: true
  param :organization_id, Integer, desc: 'Source organization id', required: true

  def delete_cards_of_source
    if params['source_id'].present? && params['organization_id'].present?
      CardDeletionByEclSourceJob.perform_later(params['source_id'], params['organization_id'], params['file_url']) if params['file_url'].present?
      EclSource.where(ecl_source_id: params['source_id']).delete_all
      render nothing: true
    else
      render_bad_request("Source not found")
    end
  end

  api :DELETE, 'api/v2/ecl/:id', 'Delete the card using ecl_id'
  param :id, Integer, desc: 'Card ecl id', required: true
  def destroy
    if params[:id].present? && (card = Card.find_by(ecl_id: params[:id]))
      card.update_request_from_ecl = true
      card.delete_card
      render nothing: true
    else
      render_not_found("Card not found")
    end
  end

  api :POST, '/api/v2/ecl/sync_source_cards', 'Sync cards when source gets updated.'
  param :source_id, Integer, desc: 'Source id', required: true
  param :organization_id, Integer, desc: 'Source organization id', required: true
  param :payload, Hash, desc: "payload to update for cards.", required: true

  def sync_source_cards
    if params[:source_id].present? && params[:organization_id].present?
      EclCardUpdateBySource.perform_later(params)
      render nothing: true
    else
      render_bad_request("Source not found")
    end
  end

end

class Api::V2::NotificationsController < Api::V2::BaseController
  include FollowableConcern

  has_limit_offset_constraints only: [:index]
  before_action :require_organization_admin, only: [:push_to_org]

  resource_description do
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
    api_version 'v2'
    formats ['json']
  end

  api :GET, 'api/v2/notifications', 'Get notifications for logged in user. Ordered by created at'
  param :limit, Integer, desc: 'Number of notifications to retrieve', required: false
  param :offset, Integer, desc: 'Number of records to skip before the next record', required: false
  def index
    user_notif_read = UserNotificationRead.where(user: current_user).first
    query = current_user.notifications.where(widget_deep_link_id: nil).where.not(app_deep_link_id: nil).unseen
    @unseen_count       = user_notif_read ? query.where("created_at >= (:seen_upto)", seen_upto: user_notif_read.seen_at).count : query.count
    @notifications      = fetch_notifications.includes(:notifications_causal_users, :causal_users)
                          .where(widget_deep_link_id: nil)
                          .where.not(app_deep_link_id: nil)
                          .limit(limit).offset(offset)
                          .order(created_at: :desc)
    @preloaded_snippets = Notification.preload_app_deep_link_snippets @notifications
    user_ids            = @notifications.map(&:causal_users).flatten.compact.map(&:id).uniq
    load_is_following_for(user_ids, 'User', current_user)
  end

  api :POST, "api/v2/notifications/seen_at", "To update the time for when a user clicks on the notification bubble"
  param :timestamp, DateTime, :desc => 'The timestamp of last fetch before the user clicked on the notification bubbble', :required => true
  def seen_at
    user_notification_read = UserNotificationRead.find_or_initialize_by(user_id: current_user.id)
    user_notification_read.seen_at = params.require(:timestamp)
    if user_notification_read.save
      head :ok and return
    else
      return render_unprocessable_entity('Unable to update the seen at')
    end
  end

  api :POST, "api/v2/notifications/:id/read", "Mark a particular notification as read"
  param :id, Integer, desc: 'Id of the notification', required: true
  def read
    @notification = Notification.find(params[:id])
    unless @notification.user_id == current_user.id
      render_unauthorized and return
    end
    @notification.mark_as_seen
    head :ok and return
  end

  # push notification through Admin UI, first iteration
  api :POST, "api/v2/notifications/push_to_org", "Send push notifications to entire organization"
  param :content, String, desc: 'Push notification message content', required: true
  param :payload, String, desc: 'Push notification json payload', required: true
  def push_to_org
    begin
      payload = JSON.parse(params[:payload])
      payload.symbolize_keys!
      payload[:content] = params[:content]

      push_notification = PushNotificationService.new(current_org, payload)

      if push_notification.payload_valid?
        push_notification.notify
        push_notification.record_custom_notification_event
        head :ok and return
      else
        log.info("Invalid payload: #{payload}")
        return render_unprocessable_entity("Invalid payload: #{payload}")
      end

    rescue JSON::ParserError => e
      log.info("JSON Parse Error: #{params[:payload]}")
      return render_unprocessable_entity("JSON Parse Error:: #{params[:payload]}")
    end
  end

  private

  def fetch_notifications
    notification_limit_days = current_org.notification_limit_days.to_i

    if (notification_limit_days != 0)
      current_user.notifications.where('created_at BETWEEN ? and ?', (Date.today - notification_limit_days.days), Time.now)
    else
      current_user.notifications
    end
  end

end

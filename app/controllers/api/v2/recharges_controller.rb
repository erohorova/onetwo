class Api::V2::RechargesController < Api::V2::BaseController

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, '/api/v2/wallet/recharges', 'Get recharges list for wallet.'
  description <<-EOS
    Get recharges list for wallet
  EOS

  # list all the active recharges for wallet
  def index
    @recharges = Recharge.active.includes(:prices)
    render_not_found('Content not found') and return if @recharges.blank?
  end
end

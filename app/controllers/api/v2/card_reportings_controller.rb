class Api::V2::CardReportingsController < Api::V2::BaseController
  has_limit_offset_constraints only: [:index, :search]
  before_action :require_organization_admin, only: [:index, :search]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :POST, '/api/v2/card_reportings', 'Record the information when a content is reported'
  param :card_id, Integer, desc: 'Content which is reported', required: true
  param :reason, String, desc: 'Reason for reporting the content', required: true
  def create
    @card = load_card_from_compound_id(card_reporting_params[:card_id])
    @card_reporting = CardReporting.new(card_id: @card.id, reason: card_reporting_params[:reason], user_id: current_user.id)
    unless @card_reporting.save
      return render_unprocessable_entity(@card_reporting.full_errors)
    end
    CardReportingIndexingJob.perform_later(card_id: @card.id)
    head :ok
  end

  api :GET, '/api/v2/card_reportings', 'List the information when a content is reported or trashed'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :state, String, desc: 'Get type of flagged content by passing type param e.g.type=reported, type=trashed'
  authorize :index, Permissions::ADMIN_ONLY
  def index
    results = CardReporting.fetch_content(organization: current_org, state: params[:state])
    @total = results.length
    @flagged = results.limit(limit).offset(offset)
  end

  api :GET, '/api/v2/card_reportings/search', 'Search reported contents based on title/message of card'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :q, String, desc: 'Query String'
  param :state, String, desc: 'State of flagged content e.g.state=reported, state=trashed'
  authorize :search, Permissions::ADMIN_ONLY
  def search
    @results = FlagContent::CardReportingSearchService.search_for_card(organization: current_org,
      query: params[:q],
      offset: offset,
      limit: limit,
      state: params[:state])
    @users = FlagContent::CardReportingSearchService.extract_user_ids(results: @results)
    @total = @results.count
  end

  private

  def card_reporting_params
    params.permit(:card_id, :reason)
  end
end

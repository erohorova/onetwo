class Api::V2::CareerAdvisorsController < Api::V2::BaseController
  include CompoundIdReader
  before_action :require_organization_admin

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized to access'
    error 404, 'Not found'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/v2/career_advisor', 'Returns last updated career advisor'
  description <<-EOS
    Render career advisor details. This will return all the career advisor information available
  EOS
  param :skill, String, desc: 'skill career advisor, values: data_scientist, data_architect, dev_ops_engineer', required: false
  def show
    @career_advisor = if params[:skill] && CareerAdvisor::SKILLS.include?(params[:skill])
      current_org.career_advisors.find_by(skill: params[:skill])
    else
      current_org.career_advisors.order(:updated_at).last
    end
  end

  api :PUT, 'api/v2/career_advisor', 'update career advisor and career advisor cards'
  description <<-EOS
    This endpoint is to update career advisor.
  EOS
  param :career_advisor, Hash, required: true do
    param :skill, String, desc: 'skill career advisor, values: data_scientist, data_architect, dev_ops_engineer', required: true
    param :links, String, desc: 'links career advisor', required: false
    param :career_advisor_cards_attributes, Hash, required: false do
      param :id, Integer, desc: 'id career advisor card, required: true for destroy', required: false
      param :card_id, Integer, desc: 'id course card, required: false for destroy', required: true
      param :level, String, desc: 'level career advisor card, required: false for destroy', required: true
      param :_destroy, String, desc: 'Boolean : 1 or true,required: true for destroy', required: false
    end
  end
  def update
    @career_advisor = current_org.career_advisors.find_by(skill: career_advisor_params[:skill])
    return render_not_found('CareerAdvisor not found') if @career_advisor.blank?

    career_advisor_cards = params[:career_advisor][:career_advisor_cards_attributes]
    unless career_advisor_cards.blank?
      career_advisor_cards.map do |ca_card|
        ca_card['card_id'] = load_card_from_compound_id(ca_card['card_id'])&.id unless ca_card[:_destroy]
      end
    end

    if @career_advisor.update(career_advisor_params)
      render 'show'
    else
      return render_unprocessable_entity(@career_advisor.errors.full_messages.join(", "))
    end
  end

  private

  def career_advisor_params
    params.require(:career_advisor).permit(:skill,:links,
      career_advisor_cards_attributes: [
        :id, :card_id, :level, :career_advisor_id, :_destroy
      ]
    )
  end
end

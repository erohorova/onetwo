# frozen_string_literal: true

class Api::V2::ActivityStreamsController < Api::V2::BaseController

  before_action :check_team_feed_entity, only: :destroy

  has_limit_offset_constraints only: [:index]

  resource_description do
    api_version 'v2'
    formats ['json']
  end

  api :GET, 'api/v2/activity_streams', 'Get all ActivityStreams in current Organization
                                        filtered by visibility to Current User'
  param :limit, Integer, desc: 'Limit, default 10', required: false
  param :offset, Integer, desc: 'Offset, default 0', required: false
  param :only_conversations, String, desc: 'Boolean: true/false show only conversation', required: false
  param :user_id, String, desc: 'Presence of User id will show only user activities'
  param :activity_actions, Array, desc: 'Array of activity actions : Possible values 
                                 created_card, created_pathway, created_livestream, upvote, smartbite_uncompleted, comment, smartbite_completed, created_journey, changed_author, assignment_completed'
  param :linkable_card_required, String, desc: 'Boolean: true/false show only user post activities', required: false
  def index
    if params[:user_id].present?
      @user = User.find_by(id: params[:user_id], organization: current_org)
      render_not_found if !@user.present?
    else
      @user = current_user
    end

    @activity_streams = FetchActivitiesService.new(
      user: @user,
      filter: {
        only_conversations: params[:only_conversations]&.to_bool,
        only_user_activities: params[:user_id].present?,
        activity_actions: params[:activity_actions],
        current_user: current_user,
        limit: limit,
        offset: offset
      }
    ).fetch_org_activities
  end

  api :DELETE, 'api/v2/activity_streams/:id', 'Remove particular conversational activity stream'
  param :id, Integer, desc: 'ID of the activity stream', required: true
  def destroy
    if @team_feed_entry.remove_team_feed_entry
      head :no_content
    else
      render_unprocessable_entity(
        @team_feed_entry.errors.full_messages.join(',')
      )
    end
  end

  private

  def check_team_feed_entity
    @team_feed_entry = current_org.activity_streams.find_by(id: params[:id])
    return render_not_found('TeamFeed activity does not exist') unless @team_feed_entry
    @streamable = @team_feed_entry.streamable
    # could remove only comments (Team and Organization)
    unless @streamable.try(:commentable).class.in?([Team, Organization])
      return render_unprocessable_entity('Incorrect activity type')
    end
    user_id = @team_feed_entry.streamable.user_id
    # return 422 is user not author or admin
    return if user_id == current_user.id || current_user.is_admin_user?
    render_with_status_and_message(
      'You are not authorized for this action', status: :forbidden
    )
  end
end

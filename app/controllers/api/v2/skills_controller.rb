class Api::V2::SkillsController < Api::V2::BaseController
  has_limit_offset_constraints only: [:index]

  before_action :load_user_skill,:load_skill, only: [:update]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, '/api/v2/skills', 'Get skills'
  description <<-EOS
    Get skills for search
  EOS
  param :q, String, 'Search term', required: true
  param :limit, Integer, desc: 'Limit, default 10', required: false
  param :offset, Integer, desc: 'Offset, default 0', required: false

  def index
    if params[:q].present?
      @skills = Skill.where("name like ?","%#{params[:q]}%").limit(limit).offset(offset)
    else
      render_bad_request('Search term is nil.')
    end
  end

  api :POST, '/api/v2/skills', 'Set skills'
  description <<-EOS
    Create skill for user
  EOS
  param :name, String, 'Label of skill', required: true
  param :description, String, 'Summery of Skill'
  param :expiry_date, String, 'Expiry date of user skill'
  param :credential, Array, 'Any valid certification'
  param :credential_name, String, 'Certification name'
  param :credential_url, String, 'Certification url'
  param :experience, String, 'Expirence in skill EX 5y 10m'
  param :skill_level, String

  def create
    @skill = Skill.where(name: skill_params[:name]).first_or_create
    if @current_user.skills.exists?(@skill.id)
      render_unprocessable_entity("User skill with name #{@skill.name} already exists")
    else
      @current_user.add_skill(@skill, skill_user_params)
      head :ok
    end
  end

  api :PUT, '/api/v2/skills/:id', 'Update user skill'
  param :name, String, 'Label of skill', required: true
  param :id, Integer, desc: "Id of the user skill", required: true
  param :description, String, 'Summery of Skill', required: false
  param :credential, Array, 'Any valid certification', required: false
  param :credential_name, String, 'Certification name', required: false
  param :credential_url, String, 'Certification url', required: false
  param :experience, String, 'Expirence in skill EX 5y 10m', required: false
  param :skill_level, String, required: false
  param :expiry_date, String, 'Expiry date of user skill'
  def update
    begin
      ActiveRecord::Base.transaction do
        @skill.update_attributes!(skill_params)
        @skill_user.update_attributes!(skill_user_params)
      end
      head :ok
    rescue => e
      render_unprocessable_entity(e.message)
    end
  end

  private

  def skill_params
    params.required(:skill).permit(:name)
  end
  def skill_user_params
    params.require(:skill).permit(:description, :experience, :credential_name, :credential_url, :skill_level, :expiry_date,
      credential: [:name, :mimetype, :size, :source, :url, :handle, :status])
  end

  def load_user_skill
    @skill_user = current_user.skills_users.find_by(id: params[:id])
    render_not_found("Skill_user with id: #{params[:id]} not found") unless @skill_user
  end

  def load_skill
    @skill = @skill_user.skill
    render_not_found("skill with Skill_user id: #{params[:id]} not found") unless @skill
  end
end

class Api::V2::WidgetsController < Api::V2::BaseController
  # Inclusions
  include WidgetConcern

  # Filters
  before_filter :set_parent, only: [:index, :create]
  before_filter :set_widget, only: [:update, :show, :destroy]
  before_filter :authorize_viewer, only: [:create, :update, :destroy]
  around_filter :replica_database, only: [:index, :show]

  has_limit_offset_constraints only: :index

  api :GET, 'api/v2/widgets', 'Get all the widgets for the organization'
  param :widget, Hash, required: true do
    param :parent_id, Integer, desc: 'ID of the parent where the widget will be shown', required: true
    param :parent_type, String, desc: 'Type of the parent where the widget will be shown', required: true
    param :context, String, desc: 'Component where the widget will be shown', required: false
    param :enabled, String, desc: 'Boolean. If present, then return accordingly. Else return ALL widgets', required: false
  end
  def index
    @widgets = current_org.widgets.where(parent: @parent)
    @widgets = @widgets.where(context: widget_params[:context]) if widget_params[:context].present?
    @widgets = @widgets.where(enabled: widget_params[:enabled].to_bool) if widget_params[:enabled].present?
    render json: { widgets: WidgetBridge.new(@widgets, user: current_user).attributes }, status: :ok
  end

  api :POST, 'api/v2/widgets', 'Create a widget'
  param :widget, Hash, required: true do
    param :code, String, desc: 'HTML text for the widget', required: true
    param :parent_id, Integer, desc: 'ID of the parent where the widget will be shown', required: true
    param :parent_type, String, desc: 'Type of the parent where the widget will be shown', required: true
    param :context, String, desc: 'Component where the widget will be shown', required: true
    param :enabled, String, desc: 'Boolean. If true, return only active widgets else all', required: false
  end
  def create
    @widget = current_org.widgets.build(widget_params)
    @widget.creator = current_user

    unless @widget.save
      render_unprocessable_entity(@widget.full_errors) and return
    end
    render json: WidgetBridge.new([@widget], user: current_user).attributes.first, status: :ok
  end

  api :GET, 'api/v2/widgets/:id', 'Get details of a widget'
  param :id, Integer, desc: 'ID of the widget to be updated', required: true
  def show
    render json: WidgetBridge.new([@widget], user: current_user).attributes.first, status: :ok
  end

  api :PUT, 'api/v2/widgets/:id', 'Update a widget'
  param :id, Integer, desc: 'ID of the widget to be updated', required: true
  param :widget, Hash, required: true do
    param :code, String, desc: 'HTML text for the widget', required: false
    param :parent_id, Integer, desc: 'ID of the parent where the widget will be shown', required: false
    param :parent_type, String, desc: 'Type of the parent where the widget will be shown', required: false
    param :context, String, desc: 'Component where the widget will be shown', required: false
    param :enabled, String, desc: 'Boolean. If true, return only active widgets else all', required: false
  end
  def update
    unless @widget.update(widget_params)
      render_unprocessable_entity(@widget.full_errors) and return
    end
    render json: WidgetBridge.new([@widget], user: current_user).attributes.first, status: :ok
  end

  api :DELETE, 'api/v2/widgets', 'Delete a widget'
  param :id, Integer, desc: 'ID of the widget to be updated', required: true
  def destroy
    unless @widget.destroy
      return render_unprocessable_entity(@widget.full_errors)
    end

    head :ok and return
  end

  private

  def widget_params
    params.require(:widget).permit(:code, :parent_id, :parent_type, :context, :enabled)
  end

  def set_parent
    if widget_params[:parent_type].blank? || widget_params[:parent_id].blank?
      return render_unprocessable_entity('Blank parent_type or parent_id')
    end
    @parent = load_entity_from_type_and_id(widget_params[:parent_type], widget_params[:parent_id])
    render_unprocessable_entity('Invalid parent') if @parent.nil?
  end

  def set_widget
    @widget = current_org.widgets.find_by(id: params[:id])
    render_not_found("Widget not found with id #{params[:id]}") unless @widget
  end
end

# frozen_string_literal: true

class Api::V2::WorkflowsController < Api::V2::BaseController
  include WorkflowConcern

  has_limit_offset_constraints only: %i[list index]

  skip_before_filter :authenticate_user_from_token!, except: :dynamic_selections
  before_action :auth_workflows_action, except: :dynamic_selections
  before_action :validate_patch_versions, only: :handle_patches
  before_action :validate_dynamic_actions, only: :dynamic_selections
  before_action :find_workflow, only: [:toggle, :run, :show, :destroy]
  before_action :set_valid_sort_order, only: :index
  before_action :require_organization_admin, only: [:create, :show, :index, :destroy]

  api :GET, 'api/v2/workflows/sources/:id', 'Get list of Users Profiles'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  def list
    case params[:id]
    when 'profile'
      profiles = @current_org.profiles.joins(:user)
        .where(
          users: {
            is_suspended: false,
            status: %w[active inactive],
            is_anonymized: [nil, false]
          }
        )
      @total = profiles.count
      u_profiles = profiles.order(id: :desc).limit(limit).offset(offset)
      @cols, @rows = response_mapper(u_profiles)
    else
      render json: {message: 'Not valid ID'}, status: :unprocessable_entity
    end
  end

  api :GET, 'api/v2/workflows/sources/profile/columns', 'Get columns of Users Profiles'
  def profile_columns
    render json: columns_mapper('profile'), status: :ok
  end

  api :POST, 'api/v2/workflows/handle/patches', 'Execute patch data'
  def handle_patches
    job_id = WorkflowPatchRevisionsJob.perform_later(
      patches: params[:_json],
      version: @version
    ).job_id

    render json: { job_id: job_id }, status: :ok
  end

  api :POST, 'api/v2/workflows/dynamic-selections', 'Create/remove dynamic selection group using workflow'
  def dynamic_selections
    body, status = send_dynamic_selection(
      payload: @prepared_params[:payload],
      type: @prepared_params[:additions][:action]
    )
    progress_info = dynamic_group_revision(
      res: body,
      addition: @prepared_params[:additions],
      status: status.to_s,
      payload: @prepared_params[:payload]
    )

    render json: {
      message: progress_info[:message],
      external_status: status.to_s,
      external_body: body
    }, status: progress_info[:status].to_sym
  end

  api :GET, 'api/v2/workflows', 'List all the active or inactive workflows, If `is_enabled` is not given then by
    default it will provide active workflows only'
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :sort, String, "Attribute name to sort by, supported values: created_at"
  param :order, String, "Sort order, asc or desc"
  param :is_enabled, String, desc: 'Used to get only active or inactive workflows. If `is_enabled=true` return active workflows and vice-versa', required: false
  def index
    sort = if @valid_sort_order.member?(params[:sort]&.downcase)
             params[:sort].downcase
           else
             'created_at'
           end
    order = %w[asc desc].include?(params[:order]&.downcase) ? params[:order] : 'desc'
    query = current_org.workflows
    query = query.where(is_enabled: params[:is_enabled]&.to_bool) unless params[:is_enabled].nil?
    total = query.length
    workflows = query.limit(limit).offset(offset).order(sort => order)

    render json: { workflows: WorkflowBridge.new(workflows, user: current_user).attributes, total: total }
  end

  api :GET, 'api/v2/workflows/input_source', 'list User, channel, Groups input fields'
  param :type, String, 'Gives input fields according to the type. Valid source types user,team,channel'
  def input_source
    return render_unprocessable_entity('Missing required parameter') if params[:type].blank?

    return render_unprocessable_entity('Invalid source type') if %w[user team channel].exclude?(params[:type])

    source_service = WorkflowService::Source.new(type: params[:type],
      input_source: nil,
      organization: current_user.organization)
    columns = source_service.fields
    preview_data = source_service.preview_data

    render json: { columns: columns, preview_data: preview_data }, status: :ok
  end

  api :POST, 'api/v2/workflows', 'create workflows'
  def create
    @workflow = current_org.workflows.build(workflow_params)
    @workflow.author = current_user
    @workflow.wf_rules = params[:workflow][:wf_rules]

    unless @workflow.save
      render_unprocessable_entity("Error while creating Workflow Record #{@workflow.errors.full_messages.join(",")}") and return
    end

    head :ok
  end

  api :PUT, 'api/v2/workflows/:id/toggle', 'Make workflow enable or disable'
  param :is_enabled, String, 'Used for enable or disable workflow', required: true
  def toggle
    return render_unprocessable_entity('Missing required parameter') unless params.include?('is_enabled')
    return render_unprocessable_entity('You can not enable or disable CSV workflows') if @workflow.wf_input_source[0][:type] === 'CSV'

    @workflow.update(is_enabled: params[:is_enabled])

    render json: {
      id: @workflow.id,
      isEnabled: @workflow.is_enabled
    }
  end

  api :POST, 'api/v2/workflows/:id/run', 'Runs the workflow if it is successfully created'
  def run
    return render_unprocessable_entity("You can not process #{@workflow.status} workflows") if @workflow.status != 'new'
    WorkflowProcessorJob.perform_later(
      workflow_id: @workflow.id,
      model_name: @workflow.wf_type
    )
    head :ok
  end

  api :GET, 'api/v2/workflows/:id', 'Gives workflow infromation'
  param :fields, String, 'Gives on-demand fields', required: false
  def show
    render json: { workflows: WorkflowBridge.new([@workflow], user: current_user, fields: params[:fields]).attributes[0]}
  end

  api :DELETE, 'api/v2/workflows/:id', 'Delete the give workflow'
  def destroy
    if @workflow.destroy
      head :ok and return
    else
      return render_unprocessable_entity(@workflow.errors.full_messages.join(", "))
    end
  end

  private

  def workflow_params
    params.require(:workflow).permit(:name, :status, :is_enabled, :wf_type, :wf_rules,
      wf_action: [:entity, :type,
        value:[:column_name, :prefix, :suffix, :custom_column_name, :id, wf_column_names: []],
        metadata: [:is_private, :description, :is_mandatory]
      ],
      wf_input_source: [:type, :source],
      wf_actionable_columns: [:column_name, :csv_name, :type, :is_dynamic, :formula, :is_identifier]
    )
  end

  def validate_dynamic_actions
    # TODO: write JSON schema for validation both actions
    unless server_available?
      return render_unprocessable_entity('Server is not available')
    end
    unless extract_specific_data(%i[workflow payload], params).present?
      return render_unprocessable_entity('Params validation failed')
    end
    @prepared_params = dynamic_data_formatter(params[:workflow])
    return render_unprocessable_entity('Params validation failed') unless @prepared_params
    if @prepared_params[:errors].present?
      return render_unprocessable_entity(@prepared_params[:errors])
    end
    group_exist = DynamicGroupRevision.exists?(
      title: @prepared_params[:payload][:groupTitle],
      organization_id: current_org.id
    )
    render_unprocessable_entity('Group already exists') if group_exist
  end

  def auth_workflows_action
    begin
      request_token = request.headers['x-api-token']
      env_token = Settings.workflows.edcast_master_token
      is_valid = request_token&.eql?(env_token)
    rescue Settingslogic::MissingSetting => e
      log.warn("EDCAST_MASTER_TOKEN env key not specified. #{e.message}")
      is_valid = false
    end
    if is_valid
      @current_org = Organization.find_by_request_host(request.host)
      unless @current_org
        return render_not_found('Organization not found')
      end
    else
      authenticate_user_from_token!
    end
  end

  def validate_patch_versions
    valid_versions = WorkflowPatchRevision::AVAILABLE_VERSIONS
    # TODO: remove fallback for version 0 in next sprints (.52, .53)
    @version = request.headers['x-api-version']&.to_i || 0
    unless @version.in?(valid_versions)
      render json: {
        error: "X-API-VERSION not valid. Available versions: #{valid_versions.join(', ')}"
      }, status: :unprocessable_entity
    end
  end

  def find_workflow
    @workflow = Workflow.find_by(id: params[:id])
    return render_not_found('Record not found') unless @workflow.present?
  end

  def set_valid_sort_order
    @valid_sort_order = Set.new(%w{created_at})
  end
end

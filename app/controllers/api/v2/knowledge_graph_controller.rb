class Api::V2::KnowledgeGraphController < Api::V2::BaseController

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized to access channel'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/v2/knowledge_graph', 'Get data used to build knowledge graph'
  param :start_time,
        String,
        desc: "lower time bounds of user activity to consider. DD/MM/YYYY format",
        required: true
  param :end_time,
        String,
        desc: "upper time bounds of user activity to consider. DD/MM/YYYY format",
        required: true
  param :user_ids,
        String,
        desc: "user ids to filter by (comma separated)",
        required: false
  param :team_ids,
        String,
        desc: "team ids to filter by (comma separated)",
        required: false
  param :num_top_topics,
        Integer,
        desc: "number of top topics to return per user (default: 3, max: 100)",
        required: false
  param :num_top_users,
        Integer,
        desc: "number of top users to return (default: 500, max: 2000)",
        required: false

  def knowledge_graph
    query_service_params = {
      org_id:          current_org.id,
      current_user_id: current_user.id,
      is_admin:        current_user.is_admin_user?,
      num_top_topics:  params.fetch(:num_top_topics, 3).to_i.clamp(0, 100),
      num_top_users:   params.fetch(:num_top_users, 50).to_i.clamp(0, 2000),
    }

    query_service_params.merge!(
      params.
        slice(*%i{user_ids team_ids}).
        symbolize_keys.
        transform_values do |ids_str|
          ids_str.split(",").map(&:to_i)
        end
    )

    start_time, start_time_err = parse_time_param(:start_time)
    end_time, end_time_err     = parse_time_param(:end_time)

    return render_unprocessable_entity(start_time_err) if start_time_err
    return render_unprocessable_entity(end_time_err) if end_time_err

    if start_time >= end_time
      return render_unprocessable_entity("end_time must be greater than start_time")
    end

    query_service_params[:start_time] = start_time.utc.beginning_of_day.to_i
    query_service_params[:end_time]   = end_time.utc.end_of_day.to_i

    results = KnowledgeGraphQueryService.run(query_service_params)

    render json: results
  end

  private

  # Parses time param at given key
  # Returns [timestamp, error]
  def parse_time_param(key)
    date_str = params[key].to_s
    begin
      date_obj = DateTime.parse(date_str)
      return [date_obj, nil]
    rescue ArgumentError
      return [nil, "#{key} is missing or not dd/mm/yyyy format"]
    end
  end

end

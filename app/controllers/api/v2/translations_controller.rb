class Api::V2::TranslationsController < Api::V2::BaseController

  api :POST, 'api/v2/translations', 'Saves untranslated strings in redis'
  param :strings, String, desc: 'untranslated string', required: true
  param :language, String, desc: 'language', required: true
  formats ['json']
  def create
    date_str = Time.now.utc.strftime("%Y%m%d")
    params[:strings].each do |a|
      RedisHashKey.new(key: date_str, field: params[:language], value: a).set_as_array
    end
    head :ok
  end
end

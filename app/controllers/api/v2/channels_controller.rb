class Api::V2::ChannelsController < Api::V2::BaseController
  include TrackActions
  include FollowableConcern, ChannelConcern
  include SortOrderExtractor
  include CardsFilter
  # This defines the /aggregated_metrics, /top_cards, and /top_users endpoints
  include ChannelControllerLeaderboardConcern

  around_filter :replica_database, only: [:cards, :index, :show]
  before_action :set_filter_by_language, only: [:cards]

  before_action :require_organization_admin, only: [
    :promote,       :demote,
    :destroy,       :remove_followers,
    :batch_promote, :batch_demote,
    :carousels
  ]

  before_action :current_channel, only: [
    :show,                :update,
    :add_collaborators,   :remove_collaborators,
    :list_ecl_sources,    :add_ecl_source,
    :remove_ecl_source,   :remove_ecl_source_list,
    :list_topics,         :add_topic,
    :remove_topic,        :remove_topic_list,
    :add_curators,        :remove_curators,
    :cards,               :follow,
    :unfollow,            :promote,
    :demote,              :destroy,
    :remove_followers,    :remove_cards,
    :versions,            :carousels,
    :reorder_card,        :aggregated_metrics,
    :top_cards,           :top_users,
    :add_topic_list,      :add_ecl_source_list,
    :search_cards,        :programming
  ]

  before_action :require_org_admin_or_channel_collaborator, only: [
    :add_collaborators, :remove_collaborators,
  ]

  before_action :require_org_admin_or_channel_curator, only: [
    :aggregated_metrics, :top_cards, :top_users
  ]

  before_action :require_channel_read_access, only: [:show, :update, :search_cards]
  before_action :verify_batch_params, only: [:batch_promote, :batch_demote]
  before_action :verify_channel_curator_or_creator, only: [:remove_cards, :reorder_card]
  before_action :set_last_access_opts, only: [:cards]
  has_limit_offset_constraints only: [
    :followers,     :contributors,
    :index,         :curators,
    :cards,         :as_curator,
    :suggestions,   :versions,
    :not_curators,  :not_contributors,
    :search,        :trusted_collaborators,
    :search_cards
  ]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized to access channel'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  def_param_group :channel do
    param :channel, Hash, required: true do
      param :label, String, desc: 'Name of channel.'
      param :content_type, Array, desc: 'Content type for channel programming'
      param :language, Array, desc: 'language preference for channel programming'
      param :description, String, desc: 'Description of channel.'
      param :keywords, String, desc: 'Comma separated list of keywords associated with the channel'
      param :topics, String, desc: 'Array of taxonomy topics for the channel'
      param :auto_follow, [true, false]
      param :image, String, desc: 'Image (aka banner image), supports image url or image file itself'
      param :mobile_image, String, desc: 'Profile image for channel.'
      param :is_private, [true, false]
      param :curate_only, [true, false]
      param :only_authors_can_post, [true, false], desc: 'When true collaborators can post content to channel else its open for everyone.'
      param :follower_ids, Array, desc: 'Array of user ids to add as followers.'
      param :followers, Hash, required: false do
        param :group_ids, Array, desc: 'Array of group ids whose users to add as followers.'
      end
      param :ecl_enabled, [true, false]
      param :ecl_sources_attributes, Hash, required: false do
        param :id, Integer, 'Id of ECL source, requred while updating resource', required: false
        param :ecl_source_id, String, 'ECL source UUID'
        param :rate_interval, Integer, 'Rate measurement', required: false
        param :rate_unit, String, 'Rate unit of time. one of hours, minutes, days', required: false
        param :number_of_items, Integer, 'Number of items to fetch', required: false
      end
      param :provider, String, desc: 'Channel provider', required: false
      param :provider_image, String, desc: 'Channel provider`s image url', required: false
      param :allow_follow, String, desc: 'Boolean values, Set this false if you want to disable follow button on UI.', required: false
      param :show_lock_icon, String, desc: 'Boolean values, Show lock icon', required: false
      param :auto_pin_cards, [true, false], desc: "when true trending cards will be pinned", required: false
    end
  end

  api :GET, 'api/v2/channels/suggestions', 'Get channels for autosuggest'
  param :q, String, 'Search term', required: false
  param :limit, Integer, 'Limit for pagination, default 10', required: false
  formats ['json']
  def suggestions
    filter = current_user.is_admin_user? ? {include_private: true} : {}
    @channels = Search::ChannelSearch.new.autosuggest_for_cms(params[:q], current_org.id, filter: filter).results
  end

  api :GET, 'api/v2/channels', 'Get channels with user following info. Default order- promoted DESC and Label ASC. Returns all channels that the signed in user has access to'
  param :order_label, String, desc: 'order channels by label ASC or DESC. Default: ASC'
  param :q, String, desc: 'Channel search term', required: false
  param :limit, Integer, desc: 'Limit, default: 10', required: false
  param :offset, Integer, desc: 'Limit, default: 0', required: false
  param :is_following, Integer, desc: 'filter to get only channels a user is following, or not following, use true/t/yes/y/1 or false/f/no/n/0', required: false
  param :is_curator, Integer, desc: 'filter to get only channels a user is curating, or not curating, use true/t/yes/y/1 or false/f/no/n/0', required: false
  param :is_author, Integer, desc: 'filter to get only channels a user is an author on, or not an author on, use true/t/yes/y/1 or false/f/no/n/0', required: false
  param :is_trusted, String, desc: 'filter to get only channels a user is an trusted author on, or not an trusted author on, use true/t/yes/y/1 or false/f/no/n/0', required: false
  param :writables, String, desc: 'Boolean to get writable channels (authored, curated & followed postable channels) for user', required: false
  param :sort, String, desc: 'set name param for order. Example: created_at', required: false
  param :is_cms, String, desc: 'Flag set for admin to show all the channels from the organization', required: false
  param :fields, String, desc: 'Required fields for minimal response', required: false
  def index
    order_by = %w[asc desc].include?(params[:order_label]&.downcase)? params[:order_label]&.downcase : 'asc'
    sort_and_order_by = if params[:sort]
                          if params[:sort] != 'creator' and Channel.column_names.exclude?(params[:sort])
                            return render_unprocessable_entity("#{params[:sort]} is not a valid value for channel")
                          end
                          params[:sort] == 'creator' ? "users.first_name #{order_by}" : { params[:sort] => order_by }
                        else
                          # Default options
                          { is_promoted: :desc, label: order_by }
                        end

    filter_params = {}
    [:is_following, :is_curator, :is_author, :is_trusted, :writables].each do |k|
      filter_params[k] = params[k].to_bool if params.has_key?(k)
    end

    channel_query = if params[:is_cms].try(:to_bool)
                      # Admin should be available to see all channels in org context
                      query = @current_org.channels
                      if params[:q].present?
                        order_attr, order_option = extract_sort_order(
                                                    valid_order_attrs: Channel.column_names,
                                                    default_order_attr: "id",
                                                    order: params[:order_label],
                                                    order_attr: params[:sort]
                                                  )
                        query = query.search_by(params[:q]).order("#{order_attr} #{order_option}")
                      end
                      query
                    else
                      Channel.fetch_eligible_channels_for_user(
                        user: current_user,
                        q: params[:q],
                        filter_params: filter_params
                      )
                    end
    @channels = channel_query.joins("LEFT JOIN users on channels.user_id = users.id").includes(:followed_teams).order(sort_and_order_by).limit(limit).offset(offset)
    channel_ids = @channels.map(&:id)
    @all_authors_count = ChannelsUser.where(channel_id: channel_ids)
      .all_authors_types
      .group(:channel_id)
      .count
    @total = channel_query.count
  end

  api :GET, 'api/v2/channels/search', 'Search current organization channels'
  param :q, String, desc: 'Query string; default matches all', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :is_private, String, desc: 'Boolean. Show only private/open channels.', required: false
  param :only_my_channels, String, desc: 'Boolean. Show only channels a user is following.', required: false
  param :curators_names, Array, "Collection of channel's curators' names", required: false
  param :collaborators_names, Array, "Collection of channel's collaborators' names", required: false
  param :followers_names, Array, "Collection of channel's followers' names", required: false
  param :skip_aggs, String, desc: 'Boolean. Show aggregation results', required: false
  def search
    @query = params[:q]
    res = channel_search(@query, offset, limit)
    @channels = res.results

    if !params[:skip_aggs]&.to_bool && res.total > 0
      @aggs = res.aggs
    end
  end

  api :GET, 'api/v2/channels/:id/search_cards', 'Search cards within channel'
  param :q, String, desc: 'Query string. Default matches all', required: false
  param :offset, Integer, desc: 'Result offset. Default 0', required: false
  param :limit, Integer, desc: 'Result limi. Default 10', required: false
  param :content_type, Array, desc: <<-TXT, required: false
    Filters search results to include these card types and card sub types only.
    It is an array of hashes. Each hash is of format { card_type: 'media', card_subtype: 'link' }.
  TXT
  param :source_display_name, Array, desc: <<-TXT, required: false
    Filters search results to include content from these sources only.
  TXT
  param :author_id, Array, desc: <<-TXT, required: false
    Filters search results to include content posted by these users' only.
  TXT
  param :date_range, Array, 'Collection of hashes of date ranges', required: false do
    param :from_date, String, desc: 'Filter to get cards created after a date, format: DD-MM-YYYY', required: false
    param :till_date, String, desc: 'Filter to get cards create before date, format: DD-MM-YYYY', required: false
  end
  param :skip_aggs, String, desc: 'Boolean. Skip aggregation results. Default is false', required: false
  param :is_cms, String, desc: 'Boolean. Do not apply exclude card conditions if request is from cms. Default: false', required: false
  def search_cards
    @query = params[:q]
    res = card_search_in_channel(@query, offset, limit)
    @cards = res[:results]
    @total = res[:total]
    @aggs = res[:aggs] || []

    render json: { cards: CardBridge.new(@cards, user: current_user, options: { is_native_app: is_native_app? }).attributes, total: @total, aggs: @aggs }, status: :ok
  end

  api :GET, 'api/v2/channels/as_curator', 'Get list of channels that a user is a curator for with curate_only true'
  param :limit, Integer, desc: 'Limit, default: 10', required: false
  param :offset, Integer, desc: 'Limit, default: 0', required: false
  def as_curator
    @channels = Channel.joins(:channels_curators).where({channels: {curate_only: true}, channels_curators: {user_id: current_user.id}})
    @total = @channels.count
    render :index
  end

  api :POST, 'api/v2/channels', 'create channel'
  param_group :channel
  authorize :create, Permissions::CREATE_CHANNEL
  def create
    create_params = channel_params
    create_params["mobile_image"] = Filestack::Security.new.signed_url(create_params["mobile_image"]) if is_filestack?(create_params["mobile_image"])

    @channel = current_org.channels.new create_params
    @channel.creator = current_user
    set_follow_team_ids

    if @channel.save
      process_followers unless @channel.auto_follow
      track_action(@channel, 'create')
    else
      return render_unprocessable_entity(@channel.errors.full_messages.join(", "))
    end
    @all_authors_count = ChannelsUser.where(channel_id: @channel.id)
      .all_authors_types
      .group(:channel_id)
      .count
    render 'show'
  end

  api :GET, 'api/v2/channels/:id', 'Show channel'
  def show
    @all_authors_count = ChannelsUser.where(channel_id: @channel.id)
      .all_authors_types
      .group(:channel_id)
      .count
    load_is_following_for([@channel.id], 'Channel', current_user)
  end

  api :DELETE, 'api/v2/channels/:id/remove_cards', 'remove cards from channel'
  param :card_ids, Array, 'Array of card ids', required: true

  def remove_cards
    channels_cards = @channel.channels_cards.where(card_id: params[:card_ids])
    if channels_cards.present?
      channels_cards.destroy_all
      head :ok
    else
      render_not_found and return
    end
  end

  api :PUT, 'api/v2/channels/:id', 'update channel'
  param_group :channel
  def update
    set_follow_team_ids
    params[:channel][:show_lock_icon] = nil unless params[:channel][:is_private] == true
    update_params = prepare_channel_params(channel_params)
    if @channel.update_attributes(update_params)
      process_followers unless @channel.auto_follow
    else
      return render_unprocessable_entity(@channel.errors.full_messages.join(", "))
    end
    @all_authors_count = ChannelsUser.where(channel_id: @channel.id)
      .all_authors_types
      .group(:channel_id)
      .count
    render 'show'
  end

  api :PUT, 'api/v2/channels/:id/carousels', 'Update channel carousels config'
  param :id, Integer,  desc: 'Id of the channel', required: true
  param :carousels, Array, desc: 'Array of channel carousels.', required: true
  def carousels
    carousels = carousels_params[:carousels].delete_if(&:blank?)
    if carousels.present?
      carousels_config = @channel.set_carousels_config(carousels.to_json)
      render json: {carousels: carousels_config&.parsed_array_value}, status: :ok
    else
      render_unprocessable_entity('Missed required params')
    end
  end

  api :PUT, 'api/v2/channels/:id/add_collaborators', 'Add collaborators to channel'
  param :id, Integer, 'Id of the record to be updated', required: true
  param :user_ids, Array, 'Array of user ids', required: false
  param :role_ids, Array, 'The array of role the users will be added to', required: false
  param :trusted, String, 'Boolean. Add collaborators as trusted collaborators. Default: false', required: false
  def add_collaborators
    @as_trusted = !!params[:trusted]&.to_bool
    if params[:user_ids].present?
      users = User.where(id: params[:user_ids])
      @channel.add_collaborators(from: current_user, users: users, as_trusted: @as_trusted)
    end
    if params[:role_ids].present?
      ChannelCollaboratorsJob.perform_later(
        channel_id: @channel.id,
        role_ids: params[:role_ids],
        from_id: current_user.id,
        as_trusted: @as_trusted
      )
    end
    render json: {}, status: :ok
  end

  api :PUT, 'api/v2/channels/:id/remove_collaborators', 'remove collaborators from channel'
  param :id, Integer, 'Id of channel to be updated', required: true
  param :user_ids, Array, 'Array of collaborator ids', required: true
  param :trusted, String, 'Boolean. Remove trusted collaborators. Default: false', required: false
  def remove_collaborators
    remove_trusted = !!params[:trusted]&.to_bool
    users = current_org.users.where(id: params[:user_ids])
    if users.present?
      @channel.remove_authors(users) unless remove_trusted
      @channel.remove_trusted_authors(users) if remove_trusted
    end
    # don't do anything with users roles if destroying trusted curators
    if current_org.has_role?(Role::TYPE_COLLABORATOR) && !remove_trusted
      users.each { |user| user.remove_collaborator_role(Role::TYPE_COLLABORATOR) }
    end
    render json: {}
  end

  api :GET, 'api/v2/channels/:id/trusted_collaborators', 'Get trusted authors of a channel'
  param :id, Integer, desc: 'Id of the channel', required: true
  param :limit, Integer, desc: 'Limit, default: 10', required: false
  param :offset, Integer, desc: 'Offset, default: 0', required: false
  param :search_term, String, desc: 'Filter channel contributors by this search term', required: false
  def trusted_collaborators
    @authors = current_channel.trusted_authors
    @authors = @authors.search_by_name(params[:search_term]) if params[:search_term].present?
    @total = @authors.count
    @authors = @authors.order('channels_users.created_at DESC').limit(limit).offset(offset)
  end

  api :GET, 'api/v2/channels/:id/followers', 'Get Followers of a channel'
  param :id, Integer, desc: 'Id of the channel', required: true
  param :limit, Integer, desc: 'Limit, default: 10', required: false
  param :offset, Integer, desc: 'Offset, default: 0', required: false
  param :search_term, String, desc: 'Filter channel followers by this search term', required: false
  param :type, String, desc: 'Get type of followers by passing type param e.g. type=individual'
  def followers
    @channel = current_channel
    @followers = current_channel.all_followers(type: params[:type]).visible_members
    @followers = @followers.search_by_name(params[:search_term]) if params[:search_term].present?
    @total = @followers.uniq.count
    @followers = @followers.uniq.order('created_at DESC').limit(limit).offset(offset)
  end

  api :PUT, 'api/v2/channels/:id/remove_followers', 'Remove followers from channel'
  param :id, Integer, desc: 'Id of the channel', required: true
  param :user_ids, Array, desc: 'Array of follower ids', required: true
  authorize :remove_followers, Permissions::ADMIN_ONLY
  def remove_followers
    Follow.where(followable_type: 'Channel', followable_id: @channel.id, user_id: params[:user_ids]).destroy_all
    head :ok
  end

  api :GET, 'api/v2/channels/:id/contributors', 'Get authors of a channel'
  param :id, Integer, desc: 'Id of the channel', required: true
  param :limit, Integer, desc: 'Limit, default: 10', required: false
  param :offset, Integer, desc: 'Offset, default: 0', required: false
  param :search_term, String, desc: 'Filter channel contributors by this search term', required: false
  param :trusted, String, 'Boolean. Return only trusted collaborators. Default: false', required: false
  def contributors
    trusted_authors = !!params[:trusted]&.to_bool
    @authors = current_channel.authors
    # to return only trusted collaborators
    @authors = current_channel.trusted_authors if trusted_authors
    @authors = @authors.search_by_name(params[:search_term]) if params[:search_term].present?
    @total = @authors.count
    @authors = @authors.order('channels_users.created_at DESC').limit(limit).offset(offset)
  end

  api :GET, 'api/v2/channels/:id/not_contributors', 'Get users who have not posted in a channel yet'
  param :id, Integer, desc: 'Id of the channel', required: true
  param :limit, Integer, desc: 'Limit, default: 10', required: false
  param :offset, Integer, desc: 'Offset, default: 0', required: false
  param :search_term, String, desc: 'Filter users by this search term', required: false
  param :trusted, String, 'Boolean. Exclude collaborators from search output. Default: false', required: false
  def not_contributors
    author_ids = if !!params[:trusted]&.to_bool
                   current_channel.trusted_authors.pluck(:id)
                 else
                   current_channel.authors.pluck(:id)
                 end

    res = Search::UserSearch.new.search(q: params[:search_term],
                                        viewer: current_user,
                                        offset: offset,
                                        limit: limit,
                                        allow_blank_q: true,
                                        filter: { exclude_ids: author_ids },
                                        load: true)
    @total = res.total
    @not_authors = res.results
  end

  api :GET, 'api/v2/channels/:id/topics', 'List channel topics'
  param :id, Integer, desc: 'Id of the channel', required: true
  def list_topics
    @channel_topics = @channel.topics

    render json: {topics: @channel_topics}, status: :ok
  end

  api :PUT, 'api/v2/channels/:id/topics', 'Add channel topic'
  param :id, Integer, desc: 'Id of the channel', required: true
  def add_topic
    unless @channel.topics.any?{|x| x[:id] == channel_topic_params[:id]}
      @channel.topics << channel_topic_params
      @channel.save
    end

    @channel_topic = @channel.topics.last

    render json: @channel_topic, status: :ok
  end

  api :PUT, 'api/v2/channels/:id/add_topic_list', 'Add channel topic list'
  param :id, Integer, desc: 'Id of the channel', required: true
  param :topic, Array, desc: 'Topic list', required: true
  def add_topic_list
    if params[:topic].blank?
      render_unprocessable_entity('Unable to add channel topic list') and return
    end
    params[:topic].each do |topic|
      if topic.present? && @channel.topics.all? { |x| x[:id] != topic[:id] }
        @channel.topics.push(topic)
        @channel.save
      end
    end

    render json: @channel.topics, status: :ok
  end

  api :DELETE, 'api/v2/channels/:id/topics', 'Remove channel topic'
  param :id, Integer, desc: 'Id of the channel', required: true
  param :topic_id, Integer, desc: 'Id of the topic', required: false
  def remove_topic
    @channel.topics.delete_if { |x| x[:id] == channel_topic_params[:id] }
    @channel.save

    render json: {}, status: :no_content
  end

  api :DELETE, 'api/v2/channels/:id/remove_topic_list', 'Remove channel topic list'
  param :id, Integer, desc: 'Id of the channel', required: true
  param :topic, Hash, desc: 'Hash with array of Ids of the topic list', required: true do
    param :topic_ids, Array, desc: 'Ids of the topic list', required: true
  end
  def remove_topic_list
    topic_ids = params[:topic][:topic_ids]
    unless topic_ids.present? && topic_ids.is_a?(Array)
      return render_unprocessable_entity('Params is blank or format is incorrect')
    end
    topic_ids.each do |topic_id|
      @channel.topics.delete_if { |x| x[:id] == topic_id }
      @channel.save
    end
    render json: {}, status: :no_content
  end

  api :GET, 'api/v2/channels/:id/ecl_sources', 'List ecl channel sources'
  param :id, Integer, desc: 'Id of the channel', required: true
  def list_ecl_sources
    @ecl_sources = EclSource.where(channel: @channel)
    if @ecl_sources.present?

      server_ecl_sources = get_server_ecl_sources

      ecl_source_ids = @ecl_sources.map(&:ecl_source_id)
      selected_ecl_sources = server_ecl_sources.values.select{|source| ecl_source_ids.include?(source['id'])}

      restricted_user_ids = selected_ecl_sources.map{|x| x['restricted_users']}.flatten.uniq
      restricted_team_ids = selected_ecl_sources.map{|x| x['restricted_teams']}.flatten.uniq

      restricted_teams = Team.where(id: restricted_team_ids).pluck(:id,:name).map{|id,name| {id:id,name:name} }
      restricted_users = User.where(id: restricted_user_ids).pluck(:id,:first_name,:last_name).map{|id,first_name,last_name| {id:id,first_name:first_name,last_name:last_name}}

      @ecl_sources.each do |s|
        s.display_name = get_ecl_display_name(server_ecl_sources, s.ecl_source_id)
        s.restricted_users = get_restricted_users_or_teams(server_ecl_sources,s.ecl_source_id,'restricted_users',restricted_users)
        s.restricted_teams = get_restricted_users_or_teams(server_ecl_sources,s.ecl_source_id,'restricted_teams',restricted_teams)
      end
    end
    render 'api/v2/ecl_sources/index'
  end

  api :PUT, 'api/v2/channels/:id/ecl_sources', 'Add ecl channel source'
  param :id, Integer, desc: 'Id of the channel', required: true
  param :ecl_source_id, String, desc: 'Channel UUID', required: true
  param :private_content, [true, false], desc: 'Cards from source will be made private if true', required: false
  def add_ecl_source
    @ecl_source = EclSource.find_or_create_by(ecl_source_params.merge(channel: @channel))
    if @ecl_source.save
      server_ecl_sources = get_server_ecl_sources(source_id: @ecl_source.ecl_source_id)
      @ecl_source.display_name = get_ecl_display_name(server_ecl_sources, @ecl_source.ecl_source_id)

      render 'api/v2/ecl_sources/show', status: :ok
    else
      render_unprocessable_entity(@ecl_source.full_errors)
    end
  end

  api :PUT, 'api/v2/channels/:id/add_ecl_source_list', 'Add ecl channel source list'
  param :id, Integer, desc: 'Id of the channel', required: true
  param :ecl_source, Hash, desc: 'Hash with array of Channel UUIDs', required: true do
    param :ecl_source_ids, Array, desc: 'Channel UUIDs', required: true
  end
  def add_ecl_source_list
    ecl_source_ids = params[:ecl_source][:ecl_source_ids]
    return render_unprocessable_entity('Params is missing') if ecl_source_ids.blank?

    @ecl_sources = []

    ecl_source_ids.each do |ecl_id|
      source = { ecl_source_id: ecl_id }
      source[:channel] = @channel
      @ecl_sources.push(EclSource.find_or_create_by(source))
    end

    @ecl_sources.each do |source|
      if source.save
        server_ecl_sources = get_server_ecl_sources(source_id: source.ecl_source_id)
        source.display_name = get_ecl_display_name(server_ecl_sources, source.ecl_source_id)
      end
    end

    render 'api/v2/ecl_sources/index', status: :ok
  end

  api :PUT, 'api/v2/channels/:id/programming', 'Set-up a channel for content fetching from sociative'
  param :channel, Hash, desc: 'parameter for programming', required: true do
    param :ecl_sources, Array, desc: 'Array of source details to be configured' do
      param :source_id, String, desc: 'Source Id', required: true
      param :private_content, [true, false], desc: 'Is private', required: true
      param :display_name, String, desc: 'Display Name of Source', required: true
    end
    param :language, Array, desc: 'Filter content by language via channel programming'
    param :content_type, Array, desc: 'Filter content by content_type via channel programming'
    param :topic, Array, desc: 'Filter content by topics via channel programming'
    param :card_fetch_limit, Integer, desc: 'Limit to number of cards that gets pulled into channel in one fetch'
  end
  def programming
    return render_unprocessable_entity('Missing Channel Parameters') if params[:channel].blank?
    return render_unprocessable_entity('Card Fetch limit cannot be greater than 500') if params[:channel][:card_fetch_limit].to_i > 500
    @channel.program(params[:channel])
    render json: {}, status: :ok
  end

  api :DELETE, 'api/v2/channels/:id/ecl_sources', 'Remove ecl channel source'
  param :id, Integer, desc: 'Id of the channel', required: true
  param :ecl_source_id, String, 'Channel UUID', required: true
  def remove_ecl_source
    @ecl_source = EclSource.find_by(
      channel: @channel, ecl_source_id: params[:ecl_source][:ecl_source_id]
    )

    @ecl_source.destroy if @ecl_source.present?

    render json: {}, status: :no_content
  end

  api :DELETE, 'api/v2/channels/:id/remove_ecl_source_list', 'Remove ecl channel source list'
  param :id, Integer, desc: 'ID of the channel', required: true
  param :ecl_source, Hash, desc: 'Hash with array of Channel UUIDs', required: true do
    param :ecl_source_ids, Array, desc: 'Channel UUIDs', required: true
  end
  def remove_ecl_source_list
    ecl_source_ids = params[:ecl_source][:ecl_source_ids]
    unless ecl_source_ids.present? && ecl_source_ids.is_a?(Array)
      return render_unprocessable_entity('Params is blank or format is incorrect')
    end
    @ecl_sources = @channel.ecl_sources.where(ecl_source_id: ecl_source_ids)
    if @ecl_sources.destroy_all
      head :no_content
    else
      render_unprocessable_entity(@ecl_sources.full_errors)
    end
  end

  api :POST, 'api/v2/channels/:id/follow', 'Follow a given channel in an organization'
  param :id, Integer, desc: 'Channel Id of the following channel', required: true
  def follow
    follow = current_user.follows.create(followable: @channel)
    if !follow.errors.empty?
      render_unprocessable_entity(follow.errors.full_messages.join(", ")) and return
    else
      head :ok and return
    end
  end

  api :POST, 'api/v2/channels/:id/unfollow', 'Follow a given channel in an organization'
  param :id, Integer, desc: 'Following channel id', required: true
  def unfollow
    if @channel.follows_via_team?(current_user)
      render_unprocessable_entity('Unable to Unfollow because you are a member of the group that is following this channel') and return
    end
    follow = current_user.follows.where(followable: @channel).first!
    if !follow.destroy
      render_unprocessable_entity(follow.errors.full_messages.join(", ")) and return
    else
      head :ok and return
    end
  end

  api :PUT, 'api/v2/channels/:id/add_curators', 'Add curators to channel'
  param :id, Integer, 'Id of the record to be updated', required: true
  param :user_ids, Array, 'Array of user ids', required: true
  def add_curators
    users = current_org.users.where(id: params[:user_ids])
    if users.present?
      added = @channel.add_curators(users)
      @channel.send_curator_invites tos: added, from: current_user
      users.each { |user| user.add_role(Role::TYPE_CURATOR) } if current_org.has_role?(Role::TYPE_CURATOR)
    end
    render json: {} and return
  end

  api :PUT, 'api/v2/channels/:id/remove_curators', 'Remove curators from channel'
  param :id, Integer, 'Id of channel to be updated', required: true
  param :user_ids, Array, 'Array of curator ids', required: true
  def remove_curators
    users = current_org.users.where(id: params[:user_ids])
    @channel.remove_curators(users) if users.present?
    users.each { |user| user.remove_curator_role(Role::TYPE_CURATOR) } if current_org.has_role?(Role::TYPE_CURATOR)
    render json: {} and return
  end

  api :GET, 'api/v2/channels/:id/curators', 'List of curators for channel'
  param :id, Integer, desc: 'Id of the channel', required: true
  param :limit, Integer, desc: 'Limit, default: 10', required: false
  param :offset, Integer, desc: 'Limit, default: 0', required: false
  param :search_term, String, desc: 'Filter channel curators by this search term', required: false
  def curators
    @curators = current_channel.curators
    @curators = @curators.search_by_name(params[:search_term]) if params[:search_term].present?
    @total = @curators.count
    @curators = @curators.order('channels_curators.created_at DESC').limit(limit).offset(offset)
  end


  api :GET, 'api/v2/channels/:id/not_curators', 'List of users who is not curator for channel'
  param :id, Integer, desc: 'Id of the channel', required: true
  param :limit, Integer, desc: 'Limit, default: 10', required: false
  param :offset, Integer, desc: 'Offset, default: 0', required: false
  param :search_term, String, desc: 'Filter users by this search term', required: false
  def not_curators
    curators_ids = current_channel.curators.pluck(:id)
    res = Search::UserSearch.new.search(q: params[:search_term],
                                        viewer: current_user,
                                        offset: offset,
                                        limit: limit,
                                        allow_blank_q: true,
                                        filter: {exclude_ids: curators_ids},
                                        load: true)

    @total = res.total
    @not_curators = res.results
  end

  api :GET, 'api/v2/channels/:id/cards', 'Get cards of a channel'
  param :id, Integer, desc: 'Id of the channel', required: true
  param :channel_card_state, String, desc: <<-TXT, required: false
    State of channel cards i.e curated, new, skipped or archived. Default: curated
  TXT
  param :card_type, Array, desc: <<-TXT, required: false
    Filter channel cards based on card types
  TXT
  param :card_subtype, Array, desc: <<-TXT, required: false
    Filter channel cards based on card subtypes. This will only filter cards when
    card_type is media.
  TXT
  param :ugc, String, desc: 'Get only UGC cards', required: false
  param :filter_by_language, String, desc: 'Boolean. Filter cards based on the users language', required: false
  param :from_date, String, desc: 'Filter to get cards created after a date, format: DD-MM-YYYY', required: false
  param :to_date, String, desc: 'Filter to get cards create before date, format: DD-MM-YYYY', required: false
  param :last_access_at, String, desc: 'Time of last access, format: 1505832150', required: false
  param :sort, String, desc: <<-TXT, required: false
    Sort criteria for channel cards viz. created_at, likes_count, views_count
    Default: rank of channel_card
  TXT
  param :order, String, 'Sort order(asc or desc). Default: desc', required: false
  param :limit, Integer, desc: 'Limit. Default: 10', required: false
  param :offset, Integer, desc: 'Offset. Default: 0', required: false
  def cards
    # Merge to detect if the requests originated from mobile or web.
    @options.merge!(is_native_app: is_native_app?)

    # Authorize current user for curation if requested for non curated cards
    verify_channel_curator if params[:channel_card_state] == 'new'
    params[:language_filter] = @filter_by_language
    service_response = ChannelCardService.new(channel: @channel,
                                              params: params,
                                              viewer: current_user,
                                              limit: limit,
                                              offset: offset).call
    @cards = service_response[:cards]
    @total = service_response[:total]
    @new_cards_total = service_response[:new_cards_total]
  end

  api :PUT, '/api/v2/channels/:id/cards/:card_id/reorder_card',
    'Update sort order for card, this is only accessible to Channel Curators'
  param :id, Integer, desc: 'Id of the channel', required: true
  param :card_id, String, desc: "Id of the card to update sort order", required: true
  param :rank, Integer, desc: 'rank number to move', required: true
  def reorder_card
    channels_card = @channel.channels_cards.where(card_id: params[:card_id]).first
    if channels_card.present? && params[:rank].present?
      channels_card.update_attribute(:rank, params[:rank])
      head :ok and return
    else
      render_unprocessable_entity("channel card not founds.") and return
    end
  end

  api :PUT, 'api/v2/channels/:id/promote', 'Promote a channel'
  param :id, Integer, 'Id of channel to be promoted', required: true
  def promote
    if @channel.promote
      head :ok and return
    else
      render_unprocessable_entity(@channel.errors.full_messages.join(', ')) and return
    end
  end

  api :POST, 'api/v2/channels/batch_promote', 'Promote a channels'
  param :channel_ids, Array, 'Array of channel to be promoted', required: true
  def batch_promote
    channels_actions('promote')
  end

  api :PUT, 'api/v2/channels/:id/demote', 'Demote a channel'
  param :id, Integer, 'Id of channel to be demoted', required: true
  def demote
    if @channel.demote
      head :ok and return
    else
      render_unprocessable_entity(@channel.errors.full_messages.join(', ')) and return
    end
  end

  api :POST, 'api/v2/channels/batch_demote', 'Demote a channels'
  param :channel_ids, Array, 'Array of channel ids to be demoted', required: true
  def batch_demote
    channels_actions('demote')
  end

  api :DELETE, 'api/v2/channels/:id', 'Delete a channel from organization
    This will delete all channel data including authors, collaborators, followers and cards posted to this channel
    This will also remove groups, interests, ecl sources associated this with channel'
  param :id, Integer, 'Id of channel to be deleted', required: true
  def destroy
    if @channel.destroy
      head :ok and return
    else
      render_unprocessable_entity(@channel.errors.full_messages.join(", ")) and return
    end
  end

  api :GET, 'api/v2/channels/:id/versions', 'Get version of the channels'
  param :id, Integer, desc: 'Channel id', required: true
  param :limit, Integer, desc: 'Limit, default 10', required: false
  param :offset, Integer, desc: 'Offset, default 0', required: false
  formats ['json']
  api_version 'v2'
  def versions
    @versions = @channel.version_logs.order('versions.id DESC').limit(limit).offset(offset)
    @versions_count = @channel.version_logs.length
  end

  private

  def channel_params
=begin
  Rails converts empty arrays to nil.
  In case of deleting all associated objects, set value to empty array
=end

    params.require(:channel).permit(
      :label, :description, :keywords, :auto_follow, :image, :mobile_image,
      :is_private, :only_authors_can_post, :ecl_enabled, :curate_only,
      :curate_ugc, :add_as_curator, :provider, :provider_image, :is_open, :allow_follow,
      :shareable, :parent_id, :auto_pin_cards,
      ecl_sources_attributes: [
        :id, :ecl_source_id, :rate_interval, :rate_unit, :number_of_items,
        :_destroy
      ], language: [], content_type: []
    )
  end

  def carousels_params
    params.permit(carousels: [:id, :custom_carousel, :default_label, :index, :visible])
  end

  def process_followers
    followers_ids = (params[:channel][:follower_ids] || [])
    if followers_ids.present? || @team_ids.present?
      TeamsChannelsAutofollowJob.perform_later(team_ids: @team_ids, followers_ids: followers_ids, channel_id: @channel.id, sender_id: current_user.id)
    end
  end

  def ecl_source_params
    params.require(:ecl_source).permit(:ecl_source_id, :private_content)
  end

  def channel_topic_params
    params.require(:topic).permit(:id, :name, :label, domain: [:id, :name, :label])
  end

  def get_server_ecl_sources(ecl_params={})
    ecl_sources = EclApi::EclSource.new(@channel.organization_id).get_sources(ecl_params)
    ecl_sources_default = EclApi::EclSource.new(@channel.organization_id).get_sources(ecl_params.merge({is_default: true}))

    ecl_sources_indexed = {}
    ecl_sources.each do |s|
      ecl_sources_indexed[s['id']] = s
    end

    ecl_sources_default.each do |s|
      ecl_sources_indexed[s['id']] = s
    end

    ecl_sources_indexed
  end

  def get_ecl_display_name(sources, ecl_source_id)
    (sources[ecl_source_id] && sources[ecl_source_id]['display_name']) || ''
  end

  def get_restricted_users_or_teams(sources, ecl_source_id, type, restricted_data)
   ecl_restricted_ids = (sources[ecl_source_id] && sources[ecl_source_id][type]) || []
   restricted_data.select{|data| ecl_restricted_ids.include?(data[:id])} || []
  end

  def set_follow_team_ids
    @team_ids = []
    if params[:channel][:followers] && new_team_ids = params[:channel][:followers]&.fetch(:group_ids)
      #process only for new teams exlude existing one
      @team_ids = new_team_ids - @channel.followed_team_ids
      @channel.followed_team_ids += @team_ids
    end
  end

  # Do not check curate permission for open channels
  def authorize_permission(permission)
    super unless (permission == Permissions::CURATE_CONTENT && @channel&.is_open?)
  end

  def verify_channel_curator_or_creator
    if @channel && (!@channel.is_curator?(current_user) and !@channel.is_creator?(current_user))
      render_unauthorized("You don't have access to this channel") and return
    end
  end
end

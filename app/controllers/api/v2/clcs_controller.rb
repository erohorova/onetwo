class Api::V2::ClcsController < Api::V2::BaseController

  before_action :require_organization_admin, only: [:create]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized to access CLC'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  def_param_group :clc do
    param :clc, Hash, required: true do
      param :name, String, desc: "Name of the clc", required: true
      param :entity_id, Integer, desc: 'Id of Entity for CLC - Organization, Channel, Group', required: true
      param :entity_type, String, desc: 'Type Entity. Allowed options -  Organization, Channel, Group', required: true
      param :from_date, String, desc: 'CLC from date: mm/dd/yyyy', required: true
      param :to_date, String, desc: 'CLC to date: mm/dd/yyyy', required: true
      param :target_score, Integer, desc: 'Target score in minutes for CLC', required: true
      param :target_steps, Integer, desc: 'Target score in percentage for CLC. Example: 25, 50 etc'
    end
  end

  api :POST, 'api/v2/clcs', 'create new CLC'
  description <<-EOS
    It is used to create clc for any specific Organization/Channel/Group.
    EOS
  param_group :clc
  param :clc_badgings_attributes, Hash, required: true do
    param :title, String, desc: 'title of clc badge', required: true
    param :badge_id, Integer, desc: 'id of clc badge', required: true
    param :target_steps, Integer, desc: 'percentage of clc progress for badge allocation', required: true
  end
  def create
    begin
      set_date
    rescue
      return render_unprocessable_entity("Invalid from_date/to_date")
    end
    @clc = current_org.all_clcs.build(clc_params)
    unless @clc.save
      return render_unprocessable_entity(@clc.errors.full_messages.join(", "))
    end
    @user = current_user
    render 'show'
  end

  api :GET, 'api/v2/clcs', 'Get active clcs information'
  description <<-EOS
    It is used to get any active Clc for any particular Organization/Channel/Group at current instance
  EOS
  param :clc, Hash, desc: 'CLC hash' do
    param :entity_id, Integer, desc: 'Id of Entity for CLC - Organization, Channel, Group', required: true
    param :entity_type, String, desc: 'Type Entity. Allowed options -  Organization, Channel, Group', required: true
  end
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 25', required: false

  def index
    @clcs = Clc.unscoped.get_clc_for_entity(clc_params).offset(params[:offset]).limit(params[:limit])
    @clcs_count = Clc.unscoped.get_clc_for_entity(clc_params).count
  end

  private
  def clc_params
    params.require(:clc).permit(:name, :entity_id, :entity_type, :from_date, :to_date, :target_score, :target_steps,
                                clc_badgings_attributes: [:title, :badge_id, :target_steps])
  end

  def set_date
    params[:clc][:from_date] = Date.strptime(params[:clc][:from_date], "%m/%d/%Y")
    params[:clc][:to_date] = Date.strptime(params[:clc][:to_date], "%m/%d/%Y")
  end
end
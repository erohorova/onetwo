class Api::V2::AssignmentsController < Api::V2::BaseController
  include SortOrderExtractor
  include CardsFilter
  before_action :require_organization_admin, only: [:notify]
  before_action :set_filter_by_language, :set_last_access_opts, only: [:index]
  before_action :require_org_or_team_admin, only: [:metrics, :metric_users]
  before_action -> {current_user.is_org_admin? || check_exclude_group_permission(Permissions::MANAGE_GROUP_ANALYTICS)},
    only: [:metrics]

  around_filter :replica_database, only: [:index]

  has_limit_offset_constraints only: [:index, :metrics, :metric_users]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/v2/assignments', 'Get current user assignments'
  param :state, Array, desc: "An array of states. possible values: assigned, started, completed"
  param :card_type, Array, desc: "An array of types. possible values: media poll pack course video_stream"
  param :limit, Integer, desc: "page limit, default to 10"
  param :offset, Integer, desc: "Page offset, default 0"
  param :mlp_period, String, desc: "pass any of one value for mlp data 'quarter-1' or 'quarter-2' or 'quarter-3' or 'quarter-4' or 'untimed' or 'range' or 'skill-wise' or 'other' "
  param :start_date, String, desc: "start due date for quater"
  param :end_date, String, desc: "end due date for quater"
  param :learning_topics_name, String, desc: "learning topics name to get result skill wise"
  param :sort, String, "Attribute name to sort by param"
  param :order, String, "Sort order, asc or desc"
  param :query, String, desc: 'Search param, all or part of the title of assignment', required: false
  param :last_access_at, String, desc: 'Time of last access, format: 1505832150', required: false
  param :filter_by_language, [true, false], desc: "Filter cards based on the user's language", required: false
  param :user_id, Integer, desc: 'Presence of User id will return respective assignments of that user'
  param :year_filter, Integer, desc: 'Presence of year_filter  will return respective assignments of particular year.'

  def index
    if params[:user_id].present?
      @user = current_org.users.not_suspended.find_by(id: params[:user_id])
      render_not_found if @user.blank?
    else
      @user = current_user
    end

    @learning_topics_name = @user.learning_topics_name if (params[:mlp_period].present? && params[:mlp_period] == 'other')
    assignment_query = Assignment.sort_by_due_date(
      @user,
      opts: {
        state: params[:state],
        card_type: params[:card_type],
        mlp_period: params[:mlp_period],
        start_date: params[:start_date],
        year_filter: params[:year_filter] || Date.current.year,
        end_date: params[:end_date],
        learning_topics_name: params[:learning_topics_name],
        learning_topics_names: @learning_topics_name,
        filter_by_language: @filter_by_language,
        viewer: current_user
      }
    )

    if params[:sort].present?
      order_attr, order = extract_sort_order(
        valid_order_attrs: Assignment.column_names + ["-due_at"],
        default_order_attr: "id",
        order_attr: params[:sort],
        order: params[:order]
      )
      assignment_query = assignment_query + " ORDER BY #{order_attr} #{order}"
    end

    if params[:query].present?
      @assignments = Assignment.find_by_sql(assignment_query)
      card_ids = @assignments.map(&:assignable_id).uniq
      cards = Search::CardSearch.new.search(
        params[:query],
        current_org,
        viewer: current_user,
        filter_params: { id: card_ids },
        limit: limit,
        offset:offset,
        load: true
      ).results.map(&:id)
      @assignments = @assignments.select { |a| cards.include? a.assignable_id }
    else
      final_query = assignment_query + " limit #{limit} offset #{offset}"
      @assignments = Assignment.find_by_sql(final_query)
    end

    fields = 'id,state,completed_at,due_at,start_date,started_at,card(id,completed_percentage,card_subtype,
              is_new,card_type,slug,badging,skill_level,title,message,ecl_duration_metadata,readable_card_type),
              assignor,assignable,created_at'
    @fields = params[:fields].present? ? params[:fields] : fields
    # assignments is alias used to reference the derived table i.e. assignment_query
    @assignments_count = Assignment.count_by_sql("SELECT COUNT(*) from ( #{assignment_query} ) assignments")
  end

  api :POST, 'api/v2/assignments', 'Create assignment for group or an individual'
  param :assignment, Hash, desc: 'Assignment hash' do
    param :content_id, Integer, desc: "content id", required: true
    param :content_type, String, desc: "Content type. Allowed content types: Card", required: true
    param :assignment_type, String, desc: "Assignment type. Group or Individual(case insensitive)", required: true
    param :due_at, String, desc: "Due date with format mm/dd/yyyy", required: false
    param :start_date, String, desc: "Assignment Start Date with format mm/dd/yyyy", required: false
    param :message, String, desc: "Custom message for an assignment", required: false
    param :team_ids, Array, desc: "An array of team ids. Required if it's a group assignment", required: true
    param :assignee_ids, Array, desc: "An array of member ids, Required if it's an individual assignment", required: true
    param :self_assign, Array, desc: 'assign to admin as well', required: false
    param :exclude_user_ids, Array, desc: "An array of excluded team member ids.
      This will create an individual assignment", required: false
  end
  authorize :create, Permissions::ASSIGN_CONTENT
  def create
    if assignment_params[:assignment_type].blank?
      render_unprocessable_entity("Assignment Type is missing") and return
    end

    @assignable = load_card_from_compound_id(assignment_params[:content_id])

    unless @assignable
      render_unprocessable_entity("Content cannot be assigned") and return
    end

    opts = prepare_opts_for_assignment

    if assignment_params[:assignment_type].downcase == 'group' &&
      !assignment_params[:team_ids].blank? && assignment_params[:exclude_user_ids].blank?
      @assignable.create_assignment_for_teams(
        team_ids: assignment_params[:team_ids],
        assignor: current_user, opts: opts
      )
      head :ok and return
      # if assignee_ids or exclude_user_ids present
      # create individual assignment.
    elsif !assignment_params[:assignee_ids].blank? ||
      !assignment_params[:exclude_user_ids].blank?
      @assignable.create_assignment_for_individuals(
        assignee_ids: get_individual_assignee_ids,
        assignor: current_user, opts: opts
      )
      head :ok and return
    else
      render_unprocessable_entity("Invalid assignment type") and return
    end
  end

  api :POST, 'api/v2/assignments/dismiss', 'Dismiss Assignment'
  param :content_id, Integer, desc: "content id will be considered as assignable id ", required: true
  formats ['json']
  def dismiss
    assignment = Assignment.where(assignable_id: params[:content_id], user_id: current_user).first
    render_unprocessable_entity("Assignment not found") and return if assignment.nil?

    render_unprocessable_entity("Assignment already dismissed") and return if assignment.dismissed?

    if assignment.dismiss!
      head :ok and return
    else
      render_unprocessable_entity("Error while dismissing assignment") and return
    end
  end

  api :GET, 'api/v2/assignments/metrics', 'Get assignment metrics data for an organization'
  param :q, String, desc: "", required: false
  param :limit, Integer, desc: "page limit, default to 10"
  param :offset, Integer, desc: "Page offset, default 0"
  param :team_id, Integer, desc: "Metrics for this Team id"
  def metrics
    @metrics_hash = []

    query = TeamAssignmentMetric.joins(:card, :assignor, :team).where('users.organization_id = ?', current_user.organization)
    if current_user.group_sub_admin_authorize?(Permissions::MANAGE_GROUP_ANALYTICS)
      query = query.where("teams.id IN (?)", current_user.team_ids_for_group_type(['sub_admin']))
    end
    query = query.where("teams.id = ?",params[:team_id]) if params[:team_id].present?
    query = query.select("team_assignment_metrics.*, teams.name as team_name")
    query = query.where("cards.message like ? OR cards.title like ?", "%#{params[:q]}%", "%#{params[:q]}%") if params[:q].present? && params[:search_type] == 'card'
    query = query.where("users.first_name like ? OR users.last_name like ? OR CONCAT(users.first_name,' ',users.last_name) like ?", "%#{params[:q]}%", "%#{params[:q]}%", "%#{params[:q]}%") if params[:q].present? && params[:search_type] == 'assignor'

    metrics = query.limit(limit).offset(offset).order("team_assignment_metrics.assigned_at DESC")
    metrics.each do |metric|
      metric_hash ={}
      due_at = TeamAssignment.joins(:assignment).where('team_id is not null and team_id = ? and assignable_id = ?', metric.team_id, metric.card_id).group('team_assignments.team_id, assignments.assignable_id').select('MAX(assignments.due_at) as due_at, assignable_id, team_id')
      metric_hash['due_at'] = due_at.present? ? due_at.first['due_at'] : nil
      metric_hash['metric'] = metric
      @metrics_hash << metric_hash
    end

    @total_count = query.length
    @metrics_hash
  end

  api :GET, 'api/v2/assignments/metric_users', 'Get users of an assigment metric data'
  param :content_id, Integer, desc: "Assignable Id or content Id.", required: true
  param :assignor_id, Integer, desc: "Content assignor Id. Default is current logged in user", required: false
  param :team_id, Integer, desc: 'Assigned team id', required: false
  param :q, String, desc: "search term to search users", required: false
  param :state, Array, desc: "An array states. possible values: assigned, started, completed", required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  def metric_users
    render_unprocessable_entity("Content Id is missing") and return if (params[:content_id].nil?)

    if (params[:assignor_id])
      assignor = current_org.users.where(id: params[:assignor_id]).first
      render_unprocessable_entity("Assignor does not exists") and return unless assignor
    end
    if (params[:team_id])
      team = current_org.teams.where(id: params[:team_id]).first
      render_unprocessable_entity("Team does not exists") and return unless team
    end

    @metric_users = Assignment.assigned_users(
      q:               params[:q],
      assignor_id:     params[:assignor_id],
      team_id:         params[:team_id],
      content_id:      params[:content_id],
      state:           params[:state]
    )
    @total = @metric_users.count
    @metric_users = @metric_users.limit(limit).offset(offset)
  end

  api :GET, 'api/v2/assignments/assigned', 'Fetch teams and users the content has been assigned to'
  param :content_id, Integer, desc: "Assignable Id or content Id.", required: true
  param :q, String, desc: "search term to search users/teams", required: false
  param :search_type, String, desc: "search by users/teams", required: false
  param :assignor_id, Integer, desc: 'Currently assigned by specific assignor', required: false
  param :state, Array, desc: "An array states. possible values: assigned, started, completed", required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  def assigned

    render_unprocessable_entity("Content Id is missing") and return if (params[:content_id].nil?)

    if (params[:q] && params[:search_type] == 'user')
      user_search_query = params[:q]
    end

    if (params[:q] && params[:search_type] == 'team')
      team_search_query = params[:q]
    end

    @assigned_users = Assignment.assigned_users(
      q:               user_search_query,
      assignor_id:     params[:assignor_id],
      team_id:         params[:team_id],
      content_id:      params[:content_id],
      state:           params[:state]
    )

    @assigned_teams = TeamAssignment.assigned_teams(
      q:               team_search_query,
      assignor_id:     params[:assignor_id],
      content_id:      params[:content_id]
    )
        
    @total_teams = @assigned_teams.count
    @assigned_teams = @assigned_teams.limit(limit).offset(offset)
    
    @total_users = @assigned_users.count
    @assigned_users = @assigned_users.limit(limit).offset(offset)
  end

  api :GET, 'api/v2/assignments/notice', 'Notice on New Assignments for Banner'
  formats ['json']
  def notice
    render json: {new_assignments_count: current_user.new_assignments.count}, status: :ok and return
  end

  api :POST, 'api/v2/assignments/dismiss_notice', 'Dismiss Assignment Notice'
  formats ['json']
  def dismiss_notice
    assignment_notice_status = AssignmentNoticeStatus.where(user: current_user).first_or_initialize
    assignment_notice_status.notice_dismissed_at = Time.now.utc
    if assignment_notice_status.save
      head :ok and return
    else
      render_unprocessable_entity("Error while dismissing notice") and return
    end
  end

  api :POST, 'api/v2/assignments/notify', "send notifications to assignees"
  description <<-EOS
    Send notifications to assignees. This will notify through push and in-app notifications.
    One of team_id, assignor_id, content_id, state or assignment_ids is required
  EOS

  param :assignment_ids, Array, desc: 'An array of assignment ids', required: false
  param :team_id, Integer, desc: 'send notification to all the members of the team', required: false
  param :assignor_id, Integer, desc: 'send notification to assignments of this assignor id', required: false
  param :content_id, Integer, desc: 'send notification to assignments of this content id.', required: false
  param :state, Array, desc: "An array of assignment states. values: assigned, started, completed", required: false
  param :message, String, desc: 'Payload message for notification', required: true
  authorize :notify, Permissions::ASSIGN_CONTENT
  def notify
    render_unprocessable_entity("Message can not be blank") and return if params[:message].blank?
    if (params[:team_id] || params[:assignor_id] || params[:state] || params[:assignment_ids] || params[:content_id]).blank?
      render_unprocessable_entity("One of team_id, assignor_id, content_id, state,assignment_ids is required") and return
    end

    team_assignments = TeamAssignment.joins(:assignment).where(team: current_org.teams)
    team_assignments = team_assignments.where(team_id: params[:team_id])                        unless params[:team_id].blank?
    team_assignments = team_assignments.where(assignor_id: params[:assignor_id])                unless params[:assignor_id].blank?
    team_assignments = team_assignments.where("assignments.id IN (?)", params[:assignment_ids]) unless params[:assignment_ids].blank?
    team_assignments = team_assignments.where("assignments.state IN (?)", params[:state])       unless params[:state].blank?
    team_assignments = team_assignments.where("assignments.assignable_id IN (?) AND assignments.assignable_type = ?", params[:content_id], 'Card') unless params[:content_id].blank?

    if team_assignments.blank?
      render_unprocessable_entity("No assignments found") and return
    else
      opts = {causal_user_id: current_user.id, notification_type: Notification::TYPE_ASSIGNMENT_NOTICE, message: params[:message]}
      team_assignments.each do |team_assignment|
        opts.merge!(team_id: team_assignment.team_id, team_assignment_id: team_assignment.id)
        UserTeamAssignmentNotificationJob.perform_later(opts)
      end
      render json: {message: "#{team_assignments.size} were notified"}
    end
  end

  private

  def assignment_params
    params.require(:assignment).permit(
      :content_id, :content_type,
      :message, :due_at,
      :start_date, :assignment_type,
      :self_assign, team_ids: [],
      assignee_ids: [], exclude_user_ids: [])
  end

  # assign to self not needed authorize_permission (RBAC) check
  def authorize_permission(permission)
    super unless ( permission == Permissions::ASSIGN_CONTENT && action_name == 'create' && assignment_params[:self_assign] == true )
  end

  def prepare_opts_for_assignment
    opts = {}
    opts[:due_at] = assignment_params[:due_at] if assignment_params[:due_at].present?
    opts[:start_date] = assignment_params[:start_date] if assignment_params[:start_date].present?
    opts[:self_assign] = assignment_params[:self_assign].present? ? assignment_params[:self_assign] : false
    opts[:message] = assignment_params[:message] if assignment_params[:message].present?
    opts
  end

  def get_individual_assignee_ids
    assignee_ids = []
    if assignment_params[:exclude_user_ids]
      if assignment_params[:team_ids].blank?
        render_unprocessable_entity("Team id is required") and return
      else
        assignee_ids = TeamsUser.joins(team: :organization)
          .where("teams.organization_id = ?", current_org.id)
          .where(team_id: assignment_params[:team_ids])
          .where.not(user_id: assignment_params[:exclude_user_ids]).pluck(:user_id)
      end
    end
    assignment_params[:assignee_ids] || assignee_ids
  end
end


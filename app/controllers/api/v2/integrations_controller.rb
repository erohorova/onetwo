class Api::V2::IntegrationsController < Api::V2::BaseController
  include CoursesConcern

  api :PUT, 'api/v2/integrations/:id/toggle', 'get integrations of a user'
  param :connect, String, desc: 'true/false', required: true
  param :provider, String, desc: "External provider name", required: true
  param :fields, Hash, desc: "Required if connected = true", required: false
  formats ['json']
  api_version 'v2'
  def toggle_integration
    @users_integration = current_user.users_integrations.where(integration_id: params[:id]).first
    @users_integration.update_attribute(:connected, params[:connect]) if @users_integration
    if !params[:connect] || ( (params[:fields] && params[:fields].all? {|k,v| v.present?}) && scrape_courses )
      render json: {} and return
    else
      render_unprocessable_entity("Unable to fetch courses") and return
    end
  end
end

class Api::V2::UsersController < Api::V2::BaseController
  include FollowableConcern, DetectPlatform, UsersConcern, SortOrderExtractor

  before_action :require_organization_admin, only: [
    :unlock,               :suspend,
    :user_status,          :exclude_from_leaderboard_status,
    :admin_delete_account, :unsuspend,
    :bulk_suspend,         :dev_api_token,
    :anonymize
  ]
  before_action :load_user, only: [
    :expert_topics,        :integrations,
    :learning_topics,      :pwc_records,
    :courses,              :unlock,
    :suspend,              :unsuspend,
    :follow,               :unfollow,
    :show,                 :update,
    :channels,             :teams,
    :terms_and_conditions, :badges,
    :skills,               :remove_skill,
    :user_status,          :exclude_from_leaderboard_status,
    :admin_delete_account, :anonymize
  ]
  before_filter :set_current_org_using_host_name, only: [
    :magic_link, :email_signup, :public_profile
  ]
  skip_before_filter :authenticate_user_from_token!, only: [
    :magic_link,                    :verify_badge,
    :email_signup,                  :public_profile,
    :validate_password_reset_token, :login_landing,
    :providers_assignment,          :join_url_mobile,
    :magic_link_login
  ]
  before_filter :authenticate_user_from_token_for_lms!,  only: [:providers_assignment, :magic_link_login]
  prepend_before_action :authenticate_user_from_welcome_email, only: [:login_landing, :join_url_mobile]
  before_action :allow_public_profile_page, only: :public_profile
  before_action :validate_status, only: [:index, :user_status]
  before_action :set_valid_sort_order, only: [:index, :paid_users]
  before_action :validate_default_team, only: [:update]
  before_action :check_org_subscription_enabled, only: [:paid_users]
  before_action :decrypt_password_payload, only: [:update, :update_password], unless: :is_native_app?
  before_action :onboarding_password_decryption, only: [:onboarding], unlesss: :is_native_app?

  has_limit_offset_constraints only: [
    :following,               :followers,
    :suggested,               :index,
    :recommended_users,       :channels,
    :teams,                   :badges,
    :assignments_by_assignor, :activity_streams,
    :managed_cards,           :transactions,
    :shared_cards,            :paid_users,
    :search,                  :field_values,
    :peer_learning 
  ]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end
  api :GET, 'api/v2/users/accessible_candidates', 'Search current organization members'
  def accessible_candidates
    candidates = {}
    candidates[:groups_with_access] = TeamsUser.where(user: current_user).pluck(:team_id)
    candidates[:channels_with_access] = current_user.followed_channel_ids

    encrypted_key = Aes.encrypt(candidates.to_json, Settings.features.integrations.secret)

    render json: { candidates: encrypted_key }
  end

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/v2/users', 'Search current organization members'
  param :q, String, desc: 'Query string; default matches all', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :role, Array, desc: "Array, show only users with roles, ex. role=['sme']", required: false
  param :exclude_team_members, String, desc: "Boolean, filter users who are not part of teams from param team_ids", required: false
  param :exclude_user_ids, String, desc: "Return users except users with the following ids", required: false
  param :team_ids, Array, desc: "Search users within particular team ids only", required: false
  param :user_ids, Array, desc: "Search users within particular user ids only", require: false
  param :status, String, desc: "Filter by user status, can be active, inactive or suspended", required: false
  param :sort, String, "Attribute name to sort by, supported values: created_at, current_sign_in_at, first_name, last_name, email"
  param :order, String, "Sort order, asc or desc"
  param :show_payment_info, String, desc: "Boolean, Flag to show/hide payment information in the response, default: false", required: false
  param :include_onboarded_only, String, "include onboarding completed users, default: false", required: false
  param :only_from_my_teams, String, "include only users from teams where current_user is leader, default: false", required: false
  param :fields, String, desc: 'Required fields for minimal response', required: false
  def index
    sort = if @valid_sort_order.member?(params[:sort]&.downcase)
             params[:sort].downcase
           else
             'created_at'
           end
    order = %w[asc desc].include?(params[:order]&.downcase) ? params[:order] : 'desc'
    # Dont get users with no email(system generated admin) and current user
    query = current_org.users.where.not(email: nil).not_anonymized

    query = query.completed_profile if params[:include_onboarded_only]&.to_bool

    query = get_users_by_status(query, params[:status])

    if params[:user_ids].present?
      query = query.where(id: params[:user_ids])
    end

    team_ids = params[:team_ids]

    if params[:only_from_my_teams]&.to_bool
      team_ids = current_user.team_ids_for_group_type(['admin'])
    end

    # Will ensure that the group_sub_admin permitted users check is only applied for admin
    # is_cms passed for this API from admin app only NOT from web
    if params[:is_cms]
      group_sub_admin = current_user.group_sub_admin_authorize?(Permissions::MANAGE_GROUP_USERS)
      if group_sub_admin
        team_ids = current_user.team_ids_for_group_type(['sub_admin'])
        if params[:team_ids].present?
          team_ids = team_ids & params[:team_ids].map(&:to_i)
        end
      end
    end

    if team_ids.present?
      if params[:exclude_team_members]
        query = query.eager_load(:teams_users).where("NOT(teams_users.team_id IN (?)) OR teams_users.team_id IS NULL", team_ids)
      else
        query = query.joins(:teams).where("teams.id IN (?)", team_ids)
      end
    end

    if params[:exclude_user_ids].present?
      query = query.where.not(id: params[:exclude_user_ids])
    end

    if params[:role].present?
      query = query.joins(:roles).where("roles.default_name IN (:user_roles)", user_roles: params[:role])
    end
    query = search_by_fields(query) if params[:q].present?
    @total = query.uniq.count
    query = query.limit(limit).offset(offset).order(sort => order).uniq

    @show_payment_info = current_org.org_subscription_enabled? && params[:show_payment_info]&.to_bool
    if @show_payment_info
      @users = query.includes(:invitations, :active_org_subscriptions)
    else
      @users = query.includes(:invitations, :teams, :profile)
    end
    load_is_following_for(@users.map(&:id), 'User', current_user) # this will load @following
    @is_admin_user = current_user.is_admin_user?
  end

  api :GET, 'api/v2/users/search', 'Search current organization members'
  param :q, String, desc: 'Query string; default matches all', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :followers, String, desc: 'Boolean. Show only followers, default: false', required: false
  param :following, String, desc: 'Boolean. Show only following, default: false', required: false
  param :include_suspended, String, desc: 'Boolean, whether to include suspended users or not', required: false
  param :mention_user, String, desc: 'Boolean, will render only required details when this api used for user tagging in comments', required: false
  param :only_suspended, String, desc: 'Boolean. Show only suspended users. Use with include_suspended = true', required: false
  param :role, Array, desc: "Array, show only users with roles, ex. role=['sme'] or role='sme'", required: false
  param :search_by, String, desc: "One of ['name', 'skills']. Search users only by skills or by user's information(first_name, last_name, handle, email)", required: false
  param :expertises_names, Array, desc: "Array of expertise names. Show only users with selected expertises", required: false
  param :names_for_filtering, Array, desc: "Array of users' names. Show only users with these names", required: false
  param :emails_for_filtering, Array, desc: "Array of users' emails. Show only users with these emails", required: false
  param :is_dynamic_selection, String, desc: 'Boolean. Key for dynamic selection search, default: false', required: false
  param :custom_fields, Array, "Collection of hashes of custom fields to filter by name and value", required: false do
    param :name, String, desc: "Custom field name"
    param :values, Array, desc: "Collection of custom field's values"
  end
  param :from_date, String, desc: "Filter by date when user's joined to organization. Start of period. Format: yyyy-MM-dd.", required: false
  param :to_date, String, desc: "Filter by date when user's joined to organization. End of period. Format: yyyy-MM-dd.", required: false
  param :only_user_ids, [true, false], desc: 'return all user ids by filter', required: false
  param :sort, String, 'Attribute name to sort by, supported values: first_name, last_name'
  param :order, String, 'Sort order, asc or desc'
  param :skip_aggs, String, desc: 'Boolean. Show aggregation results', required: false
  def search
    @query = params[:q]
    if params[:only_user_ids]&.to_bool
      users = load_all_users_by_filter(@query, offset, limit: limit)
      user_ids = users.map(&:id)
      return render json: { user_ids: user_ids, total: users.count }
    end

    res = users_search(@query, offset, limit)

    if params[:mention_user]&.to_bool
      users = res.results
      users = UserBridge.new(users, user: current_user, fields: 'id,handle,avatarimages,name').attributes
      return render json: { users: users }
    end
    users = res.results
    aggs = res.aggs if res.total.positive?
    fields = params[:fields].present? ? params[:fields] : 'id,handle,avatarimages,name'

    users = UserBridge.new(users, user: current_user, fields: fields ).attributes
    render json: { users: users, total: users.count, query: @query, aggregations: aggs}
  end

  api :GET, 'api/v2/users/:id', 'Get info about user'
  description <<-EOS
    Render user details.This will return user information available
  EOS
  param :id, Integer, "User id", required: true
  def show
    load_is_following_for([@user.id], 'User', current_user) # this will load @following
  end

  api :GET, 'api/v2/users/projects', 'get list of all the projects for a user'
  description <<-EOS
    Get list of all projects for a user.
  EOS

  def projects
    @projects = current_user.projects.includes(:card).limit(limit).offset(offset)
  end

  api :GET, '/api/v2/users/wallet_balance', 'Get skillcoin balance for a user wallet.'
  description <<-EOS
    Get skillcoin balance for a user wallet
  EOS

  def skillcoin
    @balance = current_user.wallet_balance
  end

  api :GET, 'api/v2/users/following', 'Return list of users that current user is following to'
  param :q, String, desc: 'Query string; default matches first name, last name and handle', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false

  def following
    query = current_user.following

    if params[:q].present?
      query = search_by_fields(query)
    end

    @users = query.limit(limit).offset(offset)
    load_is_following_for(@users.map(&:id), 'User', current_user) # this will load @following
    @following_count = query.count
  end

  api :GET, "api/v2/users/dev_api_token", "Get the Dev api token for a user"
  param :api_key, String, desc: 'API key for developer API', required: true
  def dev_api_token
    @dev_api_token = current_user.dev_api_token(params[:api_key])
  end

  api :GET, "api/v2/users/wallet_api_token", "Get the Wallet api token for a user"
  def wallet_api_token
    @wallet_api_token = current_user.wallet_api_token
  end

  api :GET, 'api/v2/users/followers', 'Return list of users that are following current user'
  param :q, String, desc: 'Query string; default matches first name, last name and handle', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false

  def followers
    query = current_user.my_following_users.not_suspended.completed_profile.uniq

    if params[:q].present?
      query = search_by_fields(query)
    end

    @users = query.limit(limit).offset(offset)
    load_is_following_for(@users.map(&:id), 'User', current_user)

    @followers_count = query.count
  end

  api :GET, 'api/v2/users/pubnub_channels', 'Return list of pubnub_channels for current user'
  def pubnub_channels
    @pubnub_channels =  User.pubnub_channels_to_subscribe_to_for_id(@current_user.id)
  end

  api :POST, 'api/v2/users/:id/suspend', 'Suspend a user'
  param :id, Integer, desc: 'Id of the user to suspend', required: true

  def suspend
    if @user == current_user
      render_unprocessable_entity("Admins can't suspend themselves")
      return
    end
    if @user.is_suspended?
      render_unprocessable_entity('User is already suspended')
      return
    else
      @user.suspend!
      @user.update_attribute(:status, User::SUSPENDED_STATUS)
      render(json: {}, status: :ok) and return
    end
  end

  api :POST, 'api/v2/users/:id/admin_delete_account', (
    "suspends the user and schedules their data to be deleted immediately. " +
    "This is the route for admins to hit on behalf of users. "
  )
  param :id, Integer, desc: 'Id of the user to suspend', required: true
  def admin_delete_account
    if @user == current_user
      render_unprocessable_entity("Admins can't suspend themselves")
      return
    end

    @user.update(
      is_active: false,
      is_suspended: true,
      status: User::SUSPENDED_STATUS
    )

    Analytics::UserDataDeletionJob.perform_later(user: @user)

    head :ok
  end

  api :POST, 'api/v2/users/delete_account', (
    "suspends the user and schedules their data to be deleted. " +
    "This is the route for users to hit on behalf of themselves. " +
    "Logs out the user."
  )
  def delete_account

    # Make a reference to the user.
    # After signing them out, current_user will be nil
    user = current_user

    # set the scope for devise and log out the user
    sign_out :user
    sign_out user

    # Update the user's attributes in order to suspend them.
    user.update(
      is_active: false,
      is_suspended: true,
      status: User::SUSPENDED_STATUS
    )

    # Schedule the job to delete the user's data
    Analytics::UserDataDeletionJob.perform_later(user: user)

    head :ok
  end

  api :POST, 'api/v2/users/clear_search_history', (
    "Deletes the current user's search history"
  )
  def clear_search_history
    Analytics::DeleteUserSearchHistoryJob.perform_later(
      user_id: current_user.id,
      org_id: current_org.id
    )
    head :ok
  end

  api :POST, 'api/v2/users/:id/anonymize', 'Anonymize a user'
  param :id, Integer, desc: 'Id of the user to anonymize', required: true
  def anonymize
    render_unprocessable_entity('User is already anonymized.') and return if @user.is_anonymized

    if !@user.is_suspended?
      @user.suspend!
      @user.status = User::SUSPENDED_STATUS
    end

    if @user.anonymize
      render_with_status_and_message('User is successfully anonymized.', status: :ok) and return
    else
      render_unprocessable_entity('Failed to anonymize user.')  and return
    end
  end

  api :POST, 'api/v2/users/:id/user_status', 'Change user status'
  param :id, Integer, desc: 'Id of the user to change status', required: true
  param :status, String, desc: 'Set user status, can be active, inactive or suspended', required: true
  def user_status
    case params[:status]
    when 'suspended'
      if !@user.suspended?
        @user.suspend!
      else
        render_unprocessable_entity('User is already suspended') and return
      end
    when 'inactive'
      if !@user.inactive?
        @user.inactivate!
      else
        render_unprocessable_entity('User is already inactive') and return
      end
    when 'active'
      if !@user.active?
        @user.unsuspend!
      else
        render_unprocessable_entity('User is already active') and return
      end
    else
      render_with_status_and_message('Unknown state', status: :unprocessable_entity) and return
    end

    render_with_status_and_message('Status was successfully updated', status: :ok)
  end

  api :POST, 'api/v2/users/:id/exclude_from_leaderboard_status', 'show/exclude from leaderboard'
  param :id, Integer, desc: 'Id of the user', required: true
  param :exclude_from_leaderboard, String, desc: 'Boolean, exclude_from_leaderboard status', required: true
  def exclude_from_leaderboard_status
    if @user.update_attribute(:exclude_from_leaderboard, params[:exclude_from_leaderboard])
      render json: {} and return
    else
      render_unprocessable_entity('Exclude/Show from leaderboard failed')
    end
  end


  api :POST, 'api/v2/users/:id/unlock', 'Unlock a user'
  param :id, Integer, desc: 'Id of the user to suspend', required: true
  def unlock
    @user.unlock_account!
    render json: {} and return
  end

  api :POST, 'api/v2/users/bulk_suspend', 'Bulk suspend multiple users'
  param :user_ids, Array, desc: 'Ids of the users to suspend', required: true

  def bulk_suspend
    user_ids = params[:user_ids]
    user_ids.each do |user_id|
      SuspendUserJob.perform_later(user_id, current_org.id)
    end
    render_with_status_and_message('Request for users suspension is queued and will be executed in sometime.', status: :ok)
  end

  api :POST, 'api/v2/users/:id/unsuspend', 'Unsuspend a user'
  param :id, Integer, desc: 'Id of the user to unsuspend', required: true
  def unsuspend
    render_unprocessable_entity('User is anonymized, anonymized users cannot convert to active') and return if @user.is_anonymized

    if @user.is_suspended?
      @user.unsuspend!
      @user.update_attribute(:status, User::ACTIVE_STATUS)
      render(json: {}, status: :ok) and return
    else
      render_unprocessable_entity('User is already unsuspended')
    end
  end

  api :GET, '/api/v2/users/suggested', 'return suggested users for current user to follow'
  param :offset, Integer, desc: 'current offset, default: 0', required: false
  param :limit, Integer, desc: 'number of results, default: 10', required: false
  def suggested
    @users = User.suggested_users_for(user: current_user).limit(limit).offset(offset)
  end

  api :GET, 'api/v2/users/:user_id/integrations', 'get integrations of a user'
  def integrations
    return render_unprocessable_entity unless @user

    @integrations = Integration.enabled
    @user_integrations = @user.users_integrations.index_by(&:integration_id)
  end

  api :GET, 'api/v2/users/:id/courses', 'get courses for a user'
  def courses
    @external_courses = @user.load_external_courses
    @internal_courses = @user.courses
  end

  api :GET, 'api/v2/users/enrolled_courses', 'Get user enrolled courses'
  def enrolled_courses
  end

  api :GET, 'api/v2/users/:id/skills', 'Get user skill'
  param :id, Integer, desc: 'Id of the user to get skills', required: true
  param :sort, String, "Attribute name to sort by param, default:created_at", required: false
  param :order, String, "Sort order asc or desc , default: asc", required: false
  def skills
    order = params[:order] ? params[:order] : 'asc'
    sort, order = extract_sort_order(valid_order_attrs: ["created_at", "updated_at"],
                                      default_order_attr: "created_at",
                                      order: order,
                                      order_attr: params[:sort])

    @user_skills = SkillsUser.where(user_id: @user.id).includes(:skill).order(sort => order)
  end

  api :DELETE, 'api/v2/users/:id/remove_skill', 'Delete/Remove user skill'
  param :id, Integer, desc: 'Id of the user for whom skill is to be removed', required: true
  param :skills_user_id, String, "Id of skills_user object which is to be removed", required: true
  def remove_skill
    skills_user = SkillsUser.find_by(id: params[:skills_user_id])
    if skills_user
      skills_user.destroy
      head :ok
    else
      render_not_found
    end
  end

  api :PUT, 'api/v2/users/:id', 'Update user information'
  param :user, Hash, desc: "Hash with user parameters", required: true do
    param :first_name, String, desc: "First name of user"
    param :last_name, String, desc: "Last name of user"
    param :bio, String, desc: "Brief bio about user"
    param :handle, String, desc: "User handle"
    param :avatar, String, desc: "Avatar image (aka profile image), supports image url or file itself"
    param :picture_url, String, desc: "new picture_url"
    param :coverimage, String, desc: "Coverimage (aka banner image), supports image url or image file itself"
    param :default_team_id, Integer, desc: "Default team id", required: false
    param :pref_notification_follow, String, desc: "Boolean, enable/disable follow notification, default: true"
    param :pref_newsletter_opt_in, String, desc: "Boolean, disable edcast newsletter, default: false"
    param :pref_notification_weekly_activity, String, desc: "Boolean, enable/disable weekly activity notifications, default: true"
    param :hide_from_leaderboard, String, desc: "Boolean, Hide user from leaderboard, default: false"
    param :profile_attributes, Hash, desc: 'Param description to save profile' do
      param :expert_topics, Array, desc: "Collection of hashes of expert topics" do
        param :topic_id ,String, desc: "Topic id from taxonomies"
        param :topic_name, String, desc: "Name from taxonomies example: 'software.mobile'"
        param :topic_label, String, desc: "Topic label example 'C#'"
        param :domain_id, String, desc: "domain id from taxonomies"
        param :domain_name, String, desc: "Name from taxonomies engineering.technology"
        param :domain_label, String, desc: "Domain label from taxonomies Example Technology"
      end
      param :learning_topics, Array, desc: 'Collection of hashes of learning topics', required: false do
        param :topic_id, String, desc: "Topic id from taxonomies"
        param :topic_name, String, desc: "Name from taxonomies example: 'software.mobile'"
        param :topic_label, String, desc: "Topic label example 'C#'"
        param :domain_id, String, desc: "domain id from taxonomies"
        param :domain_name, String, desc: "Name from taxonomies engineering.technology"
        param :domain_label, String, desc: "Domain label from taxonomies Example Technology"
        param :level, Integer, desc: "Level of understanding of selected topic: 1, 2, 3"
      end
      param :dashboard_info, Array, desc: 'Collection of hashes Dashboard overview blocks', required: false do
        param :name, String, desc: "Name of dashboard block"
        param :sort_order, String, desc: "sort_order of dashboard block"
        param :visible, String, desc: "Boolean true/false visibility of sort order"
      end
      param :time_zone, String, desc: "Time zone of user, example 'Asia/Tokyo'", required: false
      param :tac_accepted, String, desc: "Boolean, 'true' if user accepted terms & conditions", required: false
      param :language, String, desc: "Preferred Language selected by user", required: false
      param :job_title, String, desc: "Job title from user profile", required: false
    end
  end

  def update
    params[:user][:password] = @decrypt_password if @decrypt_password.present?
    permitted_user_params = user_params
    if !current_user.is_admin_user? && @user.first_name.present? && @user.identity_providers.present?
      permitted_user_params = permitted_user_params.except("first_name")
    end
    if !current_user.is_admin_user? && @user.last_name.present? && @user.identity_providers.present?
      permitted_user_params = permitted_user_params.except("last_name")
    end
    if can_manage? && @user.update_attributes(permitted_user_params)
      render 'show'
    else
      return render_unprocessable_entity(@user.full_errors)
    end
  end

  api :PUT, 'api/v2/users/update_password', 'Update user password'
  param :user, Hash, :desc => "Hash with user parameters", :required => true do
    param :password, String, :desc => "New password"
    param :password_confirmation, String, :desc => "Re-type new password"
    param :current_password, String, :desc => "User current password"
  end

  def update_password
    params[:user] = @decrypt_password if @decrypt_password.present?
    if current_user.update_with_password(user_password_params)
      render json: {} and return
    else
      return render_unprocessable_entity(current_user.errors.full_messages.join(", "))
    end
  end

  api :GET, 'api/v2/users/validate_password_reset_token', 'Validate reset passsword token'
  param :token, String, desc: "Reset password token", required: true

  def validate_password_reset_token
    if params[:token].present?
      user = User.devise_find_by_reset_password_token(params[:token])
      unless user.new_record?
        head :ok
      else
        render_unprocessable_entity('Invalid Token.')
      end
    else
      render_unprocessable_entity('Reset password token missing.')
    end
  end

  api :GET, 'api/v2/users/magic_link', 'Send email containing a magic link to the user'
  param :email, String, desc: 'Email of the user'

  def magic_link
    user = current_org.users.find_by_email(params[:email])
    if user
      token = user.jwt_token({sent_from_native_app: is_native_app?})
      user.allow_sign_in!
      UserMailer.magic_link(user, token).deliver_later
    end
    render json: { message: 'Email sent if it exists'}
  end

  api :GET, 'api/users/:id/learning_topics', 'get list of learning_interests for a user'
  def learning_topics
    if @user
      topics = @user.learning_topics
      render json: topics, status: :ok and return
    else
      render json: { errors: 'Failed to get list of interests or competencies' }, status: :unprocessable_entity
    end
  end

  api :GET, 'api/v2/users/:id/pwc_records', 'get list of pwc_records for a user'
  def pwc_records
    if @user
      @pwc_records = @user.pwc_records
    else
      render_unprocessable_entity("Failed to get list of pwc records")
    end
  end

  api :GET, 'api/v2/users/:user_id/expert_topics', 'get list of expertise for a user'
  def expert_topics
    if @user
      topics = @user.expert_topics
      render json: topics, status: :ok and return
    else
      render json: { errors: 'Failed to get list of expertise' }, status: :unprocessable_entity
    end
  end

  api :GET, 'api/v2/users/info', 'Get logged in user information'
  def info
    @user = current_user
  end

  api :POST, 'api/v2/users/:id/follow', "Follow a given user in an organization"
  param :id, Integer, desc: 'User Id of the following user', required: true
  def follow
    follow = current_user.follows.create(followable: @user)
    if !follow.errors.empty?
      return render_unprocessable_entity(follow.errors.full_messages.join(", "))
    else
      EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_FOLLOW, follow)
      head :ok and return
    end
  end

  api :POST, 'api/v2/users/:id/unfollow', "Follow a given user in an organization"
  param :id, Integer, desc: 'Following user id', required: true
  def unfollow
    follow = current_user.follows.where(followable: @user).first!
    if !follow.destroy
      return render_unprocessable_entity(follow.errors.full_messages.join(", "))
    else
      head :ok and return
    end
  end

  api :GET, 'api/v2/users/onboarding_status', "Get current user onboarding status"
  param :camel_case_format, String, desc: 'Camel case formatting for the response', required: false
  def onboarding_status
    # WORKAROUND:
    #   To handle revised custom onboarding.
    #   current_step may exceeds the last step configured by admin for onboarding.
    #   In this case, mark user onboarding as complete.
    #   This is a temporary change.
    user_onboarding = current_user.try(:user_onboarding)
    current_step    = user_onboarding.try(:current_step)
    onboarding_options = current_user.onboarding_options.values
    if params[:custom_onboarding].to_s.to_bool && !current_user.onboarding_completed?
      last_step = onboarding_options.last
      # Use case 1: If last step is disabled by org-admin, complete the user's onboarding status
      current_step > last_step && user_onboarding.complete!
      # Use case 2: If intermediate is disabled by org-admin, shift the current step to next step of onboarding options
      !onboarding_options.include?(current_step) && user_onboarding.update(current_step: onboarding_options.last)
    end

    # NOTE: Render json is not converting response keys to camel case.
    if params[:camel_case_format].present? && params[:camel_case_format].to_bool
      render 'api/v2/users/onboarding_status' and return
    else
      render json: { jwt_token: current_user.jwt_token, step: user_onboarding.try(:current_step), user: current_user.onboarding_data, onboarding_completed: current_user.onboarding_completed? }
    end
  end

  api :POST, "api/v2/users/reset_onboarding", "reset user onboarding state"
  def reset_onboarding
    onboarding = current_user.user_onboarding
    onboarding.reset! if onboarding.may_reset?
    head :ok
  end

  api :PUT, 'api/v2/users/onboarding', 'Continue user through onboarding'
  # STEP 1: First name, last name, password, profile pic
  # STEP 2: Submit profile attributes. learning and expert topics
  # STEP 3: Finish
  def onboarding
    if params[:step].blank? || !UserOnboarding::STEPS.include?(params[:step].to_i)
      render_unprocessable_entity("Invalid step") and return
    end

    attempted_step  = params[:step].to_i
    user_onboarding  = current_user.user_onboarding
    password_required = current_user.onboarding_data[:password_required]

    if current_user.onboarded?
      head :ok and return
    end

    if !user_onboarding.started?
      user_onboarding.start!
    end

    # WORKAROUND:
    #   Web development is 3 weeks ahead of mobile development.
    #   As per new implementation, `profile_setup` is a mandatory step irrespective of onboarding steps configured or not.
    #   This will break onboarding flow in mobiles. Keeping backward-compatibility in mind for initial stages.
    #   Hence, customised onboarding is flexible with query parameter (`custom_onboarding` = TRUE)
      user_onboarding.custom_onboarding = true
      onboarding_steps = current_user.onboarding_options.values
      current_step = onboarding_steps.at(onboarding_steps.find_index(attempted_step).to_i + 1) || params[:step].to_i

    # step 3. No params posted
    if params[:step].to_i >= onboarding_steps.last
      user_onboarding.complete!
    end

    #
    # Password is not applicable for users logging in with SSO
    # Poor condition but for the sake of hotfix
    if params[:step].to_i == 1 && (password_required && user_params[:password].blank?)
      render_unprocessable_entity("Password can not be blank") and return
    end

    if params[:step].to_i == 2 && user_params[:profile_attributes].blank?
      render_unprocessable_entity("Topics cannot be blank") and return
    end

    #
    # Deletes `password` from parameters when user is logged in via SSO
    # Poor condition but for the sake of hotfix
    cloned_attributes = user_params
    cloned_attributes.delete('password') unless password_required

    if current_user.update(cloned_attributes) && user_onboarding.update(current_step: current_step)
      head :ok and return
    else
      log.warn user_onboarding.errors.full_messages.join(", ")
      render_unprocessable_entity(user_onboarding.errors.full_messages.join(", ")) and return
    end
  end

  api :GET, 'api/v2/users/recommended_users', 'Return recommended users for current user to follow'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :my_teams_users, Integer, desc: 'Show only users who are members of groups that the current user belongs to', required: false
  def recommended_users
    my_teams_users = params[:my_teams_users]&.to_bool || false
    @users = DiscoveryService::Users.new(current_user.id, limit, offset, my_teams_users).recommendations
  end

  def login_landing
    head :ok and return if request.user_agent.include?('Branch Metrics API')

    cookies[:first_sign_in] = {value: true, path: "/"} if current_user.sign_in_count == 0

    if params[:invitation_link].present?
      if should_login?
        sign_in(current_user)
        current_user.update(invitation_accepted: true)
      end
      return redirect_to '/'
    end
    sign_in(current_user)
    redirect_to '/'
  end

  def join_url_mobile
    cookies[:first_sign_in] = {value: true, path: "/"} if current_user.sign_in_count == 0

    sign_in(current_user)
    response.headers['X_API_TOKEN'] = current_user.jwt_token(expire_after: 5.minutes)
    render(json: {}, status: :ok) and return
  end

  api :PUT, 'api/v2/users/:id/invitation', "Accept the invitation"
  param :id, Integer, desc: 'Id of the user', required: true
  param :invitation_id, Integer, desc: 'invitation Id', required: true
  def invitation
    current_user.accept_invitation(invitation_id: params[:invitation_id])

    head :ok
  end

  #TODO #TECHDEBT
  def magic_link_login
    token = current_user.jwt_token
    current_user.allow_sign_in!
    if is_mobile_browser? && (claims['sent_from_native_app'] || params[:sent_from_forgot_password].to_s.to_bool)
      redirect_to "#{current_org.mobile_uri_scheme}://auth/magic_link_login?token=#{token}&host_name=#{current_org.host_name}&org_url=#{current_org.edcast_host}" and return
    else
      cookies[:first_sign_in] = {value: true, path: "/"} if current_user.sign_in_count == 0

      sign_in(current_user)
      redirect_to '/'
    end
  end

  api :GET, 'api/v2/users/notification_config', 'Get notification config for organization'
  def get_notification_config
    render json: {user_config: EDCAST_NOTIFY.settings.get_settings(current_user.organization_id, current_user.id),
     org_config: EDCAST_NOTIFY.settings.get_settings(current_user.organization_id)}
  end

  api :PUT, 'api/v2/users/notification_config', 'Update notification config for organization'
  param :options, Hash, desc: "The options hash returned by GET", required: true
  def update_notification_config
    config = NotificationConfig.find_or_create_by!(user: current_user)
    EDCAST_NOTIFY.settings.save_settings(config, params[:options])

    render json: EDCAST_NOTIFY.settings.get_settings(current_user.organization_id, current_user.id)
  end

  api :GET, 'api/v2/users/profile', 'Get logged in users profile information'
  def user_profile
  end

  api :GET, 'api/v2/public_profile/:handle', 'Get users public profile information'
  def public_profile
    @user = User.find_by(handle: "#{params[:handle]}", organization: current_org)
    if @user.present?
      @user_card_badges = @user.user_badges.joins(:card_badging).where("type = ?", "CardBadging").includes("card_badging")
      @user_card_badges_count = @user_card_badges.count if @user_card_badges.present?
      @user_clc_badges = @user.user_badges.joins(:clc_badging).where("type = ?", "ClcBadging").includes("clc_badging").order("badgings.badgeable_id DESC")
      @user_clc_badges_count = @user_clc_badges.count if @user_clc_badges.present?
      @user_skills = @user.skills_users.includes(:skill)
      @external_courses = @user.load_external_courses.order(:created_at).limit(10)
    else
      render_not_found
    end
  end

  api :GET, 'api/v2/users/clc', 'Get clc information for the user'
  param :user_id, String, desc: 'Presence of User id will return respective clc info'
  def clc
    if params[:user_id].present?
      @user = User.find_by(id: params[:user_id], organization: current_org)
      render_not_found if !@user.present?
    else
      @user = current_user
    end

    @clc = Clc.active.get_clc_for_entity({entity_id: @user.organization_id, entity_type: "Organization"}).first
    render_not_found and return unless @clc.present?
    @clc_progress = ClcProgress.get_progress_for(@clc.id, @user.id)
    render 'api/v2/clcs/show'
  end

  api :GET, '/api/v2/users/:id/badges',  'Fetch list of badges for a certain user'
  param :type, String, desc: 'Type of badges for {Card => CardBadge, Clc => ClcBadge and all => for all badges}', required: true
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false

  def badges
    @user = User.find_by(id: params[:id], organization: current_org)
    if @user.present?
      case params['type']
      when 'CardBadge'
        user_badges = @user.user_badges.joins(:card_badging).where("type = ?", "CardBadging")
        @user_badges = user_badges.includes("card_badging").order("badgings.badgeable_id DESC").limit(limit).offset(offset)
        @user_badges_count = user_badges.count
      when 'ClcBadge'
        user_badges = @user.user_badges.joins(:clc_badging).where("type = ?", "ClcBadging")
        @user_badges = user_badges.includes("clc_badging").order("badgings.badgeable_id DESC").limit(limit).offset(offset)
        @user_badges_count = user_badges.count
      when 'all'
        user_badges = UserBadge.where(user_id: @user.id)
        @user_badges = user_badges.includes(:card_badging, :clc_badging).order("badgings.badgeable_id DESC").limit(limit).offset(offset)
        @user_badges_count = user_badges.count
      else
        render_unprocessable_entity("Incorrect type") and return
      end
    else
      render_not_found
    end
  end

  api :GET, '/api/v2/verify_badge/:identifier', 'Verify and return badge info with identifier for badge'
  description <<-EOS
    Verify and return badge info with identifier for badge
  EOS
  param :identifier, String, desc: 'Identifier of badge', required: true
  def verify_badge
    if params[:identifier].present?
      @user_badge = UserBadge.find_by(identifier: params[:identifier])
      render_not_found if @user_badge.nil?
    else
      render_bad_request
    end
  end

  api :GET, '/api/v2/users/assignments_by_assignor', <<-EOS
    Fetch assignments created by user with assignor_id for current user.
  EOS
  param :assignor_id, Integer, desc: 'User id for which assignments to be fetched', required: true
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false

  def assignments_by_assignor
    query = Assignment.assignments_per_assignor(user_id: current_user.id, assignor_id: params[:assignor_id])

    @assignments_count = query.count(:id)
    @assignments = query.order("assignments.due_at desc").limit(limit).offset(offset).select("assignments.*, team_assignments.assignor_id")
    
    render 'api/v2/assignments/index'
  end

  api :POST, '/api/v2/users/providers_assignment', <<-EOS
    Create or update assignment for the course from content providers for current user.
  EOS
  param :ecl_id, String, desc: 'Ecl id of the course from content provider', required: true
  param :status, String, desc: 'Status of the course: enrolled, started, completed', required: true
  param :assignment_id, Integer, desc: 'Id of assignment', required: false
  param :assignor_id, String, desc: 'email of assignor user', required: false
  param :skip_notifications, String, desc: 'To decide if assignment notification should be sent', required: false

  def providers_assignment
    if params[:ecl_id].present? || params[:assignment_id].present?
      @assignment = LrsAssignmentService.new(user_id: current_user.id,
                                          ecl_id: params[:ecl_id],
                                          status: params[:status],
                                          assignment_id: params[:assignment_id],
                                          start_date: params[:start_date],
                                          complete_date: params[:complete_date],
                                          assignor_id: params[:assignor_id],
                                          skip_notifications: params[:skip_notifications]&.to_bool
                                          ).run
    else
      render_bad_request
    end
  end

  api :POST, '/api/v2/users/email_signup', <<-EOS
    Allow User to sign up using first name, last name, email, password
  EOS
  param :user, Hash, desc: "Hash with user parameters", required: true do
    param :first_name, String, :desc => "First name of user"
    param :last_name, String, :desc => "Last name of user"
    param :email, String, :desc => "Email of user"
    param :password, String, :desc => "Password of user"
    param :profile_attributes, Hash, desc: 'Param description to save profile' do
      param :tac_accepted, String, desc: "Boolean, 'true' if user accepted terms & conditions ", required: false
      param :dob, String, desc: "Date, users date of birth", required: false
    end
  end

  api :GET, 'api/v2/users/activity_streams', 'Get all Activity Streams for groups where current user is participate'
  param :id, Integer, desc: 'ID of the user', required: false
  param :limit, Integer, desc: 'Limit, default 10', required: false
  param :offset, Integer, desc: 'Offset, default 0', required: false
  param :only_conversations, String, desc: 'Boolean: true/false show only conversation', required: false
  def activity_streams
    @activity_streams = FetchActivitiesService.new(
      user: current_user,
      filter: {
        only_conversations: params[:only_conversations]&.to_bool,
        limit: limit,
        offset: offset
      }
    ).fetch_activities

    @activity_streams = @activity_streams.select do |activity_stream|
      activity_stream.activity_card.try(:visible_to_user, current_user)
    end
  end

  api :GET, 'api/v2/users/wallet_transactions', 'Get all wallet transactions for a user'
  param :limit, Integer, desc: 'Result limit, default 10', required: false
  param :offset, Integer, desc: 'Result offset, default 0', required: false
  def transactions
    query = current_user.processed_transactions.includes(order: :wallet_transaction)
    @total_transactions_count = query.count(:id)
    @transactions = query.order(updated_at: :desc).limit(limit).offset(offset)
  end

  def email_signup
    @signup_params = user_email_signup_params
    if @signup_params[:first_name].blank?
      render_unprocessable_entity("First name can't be blank") and return
    elsif @signup_params[:last_name].blank?
      render_unprocessable_entity("Last name can't be blank") and return
    elsif @signup_params[:email].blank?
      render_unprocessable_entity("Email can't be blank") and return
    elsif !current_org.email_registrable?(@signup_params[:email])
      render_unprocessable_entity("Email domain is invalid for signup") and return
    end

    klass = UserInfo::Alliance.new(organization: current_org, user_params: @signup_params, opts: { platform: get_platform })
    klass.connect

    if klass.errors.any?
      render_unprocessable_entity(klass.message)
    else
      head :ok
    end
  end

  api :GET, 'api/v2/users/managed_cards', 'Return list of users managed cards'
  def managed_cards
    includes_list = %i[votes comments channels leaps quiz_question_stats quiz_question_options
                       users_with_access shared_with_teams file_resources tags
                       card_badging cards_ratings prices mentions]

    @cards_by_action = current_user.users_cards_managements.group_by(&:action)

    @cards = current_user.managed_cards.includes(includes_list).limit(limit).offset(offset)

    @assignments = current_user.assignments.where(
      assignable_type: 'Card',
      assignable_id: @cards.map(&:id)
    ).index_by(&:assignable_id)
  end

  api :GET, 'api/v2/users/shared_cards', 'return list of shared cards for current user'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 15', required: false
  param :shared_type, String, desc: 'String value for content shared type i.e.,user_share, team_share, user_team_share', required: false
  param :sort, String, "Attribute name to sort by param, default:created_at", required: false
  param :order, String, "Sort order for shared cards, asc or desc , default: desc", required: false
  description <<-EOS
    This endpoint is to get shared cards with the current_user.
  EOS

  def shared_cards
    shared_type = params[:shared_type].present? ? params[:shared_type].downcase : "user_team_share"
    order = %w[asc desc].include?(params[:order]&.downcase) ? params[:order] : 'desc'
    sort = if %w[created_at updated_at].include?(params[:sort]&.downcase)
             params[:sort].downcase
           else
             'created_at'
           end
    unless ['user_share', 'team_share', 'user_team_share'].include?(shared_type)
      return render_unprocessable_entity('Invalid params')
    end
    query = get_shared_cards_query(shared_type, sort, order)
    @cards = query.limit(limit).offset(offset)
  end

  api :POST, 'api/v2/users/start_data_export', 'creates a data export and returns the url'
  def start_data_export
    # we return early and start the export in a background job
    current_user.update(data_export_status: "started")
    Analytics::UserDataExportJob.perform_later(user: current_user)
    head :no_content
  end

  api :GET, 'api/v2/users/data_export', 'returns the url for the export if it is ready'
  def data_export
    status = current_user.data_export_status
    last_data_export_time = current_user.last_data_export_time
    # Url will be null unless status is "done"
    url = if status == "done"
      Analytics::UserDataExport.generate_link_for(
        Analytics::UserDataExport.build_s3_zipfile_key(current_user)
      )
    end
    response = {
      "data_export_status" => status,
      "data_export_url" => url,
      "last_data_export_time" => last_data_export_time&.to_i
    }
    render json: response, status: :ok
  end

  api :GET, 'api/v2/users/paid_users', 'returns the list of users who have paid for Org subscription'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 15', required: false
  param :sort, String, "Attribute name to sort by, supported values: created_at, current_sign_in_at, first_name, last_name, email"
  param :order, String, "Sort order, asc or desc"
  def paid_users
    sort = if @valid_sort_order.member?(params[:sort]&.downcase)
             params[:sort].downcase
           else
             'created_at'
           end
    order = %w[asc desc].include?(params[:order]&.downcase) ? params[:order] : 'desc'

    query = current_org.users.where.not(email: nil).joins(:active_org_subscriptions)

    @show_payment_info = true
    @total = query.count
    @users = query.limit(limit).offset(offset).order(sort => order)
  end

  api :GET, 'api/v2/users/fields', 'returns list of names fields for users'
  #get all names of fields(custom and default) for user filter
  def users_fields
    @fields = User.users_fields(current_org)
    render json: { fields: @fields }
  end

  api :GET, 'api/v2/users/fields/:name/values', 'returns list of values for field users'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :name, String, desc: 'Name of field users', required: true
  #get values for users field
  def field_values
    field = params[:name]
    unless field.present?
      return render_unprocessable_entity('Missing required params')
    end

    if User.users_fields(current_org).exclude?(field)
      return render_unprocessable_entity("#{field} is not field of users")
    end

    @values = if User::DEFAULT_FIELDS.include?(field)
      current_org.users.where.not(field => nil).select(field + ' as value')
    else
      UserCustomField.joins(:custom_field)
        .where('custom_fields.display_name = ? and custom_fields.organization_id = ?', field, current_org.id)
        .where.not(value: nil)
        .select(:value)
    end

    @values = @values.uniq
    @total = @values.length
    @values = @values.limit(limit).offset(offset)
    render json: { values: @values.map(&:value), total: @total }
  end

  api :GET, 'api/v2/users/transcript', 'returns one pager transcript data of the user'
  def user_transcript

    @user = current_user
    @badges= @user.earned_bagdes.limit(9).order("user_badges.id desc")
    @content_stats = @user.user_stats
    @quarter_wise_content = @user.quarter_wise_content
    render  pdf: "Transcript",
            template: "api/v2/users/transcript.html.slim",
            page_size:  'A4',
            title: "Transcript",
            disposition: 'attachment',
            margin: { left: 0,right: 0,bottom:6},
            footer: { html: { template: 'api/v2/users/transcript_footer.html.slim'}}
  end

  api :POST, 'api/v2/users/data_download_request'
  def data_download_request
    current_user.request_personal_data
    head :ok and return
  end

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end
  api :GET, 'api/v2/users/profiles', 'Returns information of intended users'
  param :user_ids, Array, desc: 'Collection of user IDs', required: true
  param :fields, String, desc: 'Comma-separated field names', required: false
  def profiles
    users = @current_org.users.where(id: params[:user_ids])

    minified_users = UserBridge.new(users, user: current_user, fields: params[:fields]).attributes

    render json: { users: minified_users }
  end

  api :GET, 'api/v2/users/last_viewed'
  description <<-EOS
    Returns information of last viewed card/channel/user_profile  of the user
  EOS
  def last_viewed
    render json: get_last_viewed
  end

  api :GET, 'api/v2/users/:id/peer_learning'
  description <<-EOS
    Returns the following information
    1) requested user_id's sum of time spent month wise
    2) avg time spent by all user of the orgs month wise
  EOS
  param :id, Integer, "User id of the current user or any other user", required: true
  param :period, String, desc: 'Period can be year, month, week', required: true
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10. Max 1000', required: false
  def peer_learning
    if params["period"].nil?
     return render_unprocessable_entity("Period cant be blank")
    end

    if is_limit_exceeds(max_limit: 1000)
      return render_unprocessable_entity("Limit can't be greater than 1000")
    end
    render json: get_peer_learning_data
  end

  private

  def load_user
    @user = current_org.users.where(id: params[:id]).first!
  end

  def user_email_signup_params
    format_dob
    params
      .require(:user)
      .permit(
        :first_name,
        :last_name,
        :password,
        :email,
        profile_attributes: [
          :tac_accepted,
          :dob,
          :tac_accepted_at
        ]
      )
  end

  def format_dob
    if params[:user][:profile_attributes].try(:[], :dob)
      params[:user][:profile_attributes][:dob] = Date.strptime(params[:user][:profile_attributes][:dob], "%m/%d/%Y")
    end
  end

  def user_params
    params[:user][:profile_attributes][:expert_topics] ||= [] if params[:user][:profile_attributes]&.has_key?(:expert_topics)
    params[:user][:profile_attributes][:learning_topics] ||= [] if params[:user][:profile_attributes]&.has_key?(:learning_topics)
    params
      .require(:user)
      .permit(
        :first_name,
        :last_name,
        :password,
        :bio,
        :picture_url,
        :coverimage,
        :avatar,
        :handle,
        :default_team_id,
        :pref_notification_follow,
        :pref_newsletter_opt_in,
        :pref_notification_weekly_activity,
        :hide_from_leaderboard,
        profile_attributes: [
          :time_zone,
          :tac_accepted,
          :language,
          :job_title,
          :company,
          :job_role,
          { expert_topics: [:topic_name, :topic_id, :topic_label, :domain_name, :domain_id, :domain_label] },
          { learning_topics: [:topic_name, :topic_id, :topic_label, :domain_name, :domain_id, :domain_label, :level] },
          { dashboard_info: [:name, :visible] }
        ]
      )
  end

  def user_password_params
    params.require(:user).permit(:password, :password_confirmation, :current_password)
  end

  def can_manage?
    current_user == @user || current_user.is_admin_user?
  end

  def search_by_fields(query)
    search_criteria = "concat_ws(' ', users.first_name, users.last_name) like :search_term OR users.first_name like :search_term OR users.last_name like :search_term OR users.handle like :search_term"
    search_criteria << " OR users.email like :search_term" if current_user.authorize? Permissions::ADMIN_ONLY
    query.where(search_criteria, search_term: "%#{params[:q]}%")
  end

  def allow_public_profile_page
    unless current_org.public_profile_enabled?
      return authenticate_user_from_token! && @current_user.present?
    end
  end

  def validate_status
    if params[:status].present? && User::PERMITED_STATUSES.exclude?(params[:status])
      render_unprocessable_entity("Supported statuses are: #{User::PERMITED_STATUSES}")
    end
  end

  def validate_default_team
    team_id = params[:user][:default_team_id]
    if team_id.present? && !@user.teams.exists?(id: team_id)
      render_unprocessable_entity("User is not member of group with id: #{team_id}")
    end
  end

  def check_org_subscription_enabled
    unless current_org.org_subscription_enabled?
      render_unprocessable_entity("Org Subscription - member pay is not enabled for the organization")
    end
  end

  def set_valid_sort_order
    @valid_sort_order = Set.new(%w{created_at current_sign_in_at first_name last_name email})
  end

  def check_allow_user_search
    render_unprocessable_entity('User search is disabled for current org') if current_org.disable_user_search?
  end

  def is_json?(json_string)
    begin
      !!ActiveSupport::JSON.decode(json_string)
    rescue
      false
    end
  end

  def get_users_by_status(query, status)
    case status
      when User::ACTIVE_STATUS, User::INACTIVE_STATUS
        query.where(status: status, is_suspended: false)
      when User::SUSPENDED_STATUS
        query.where(status: status, is_suspended: true)
    else
      query.where(status: 'active')
    end
  end

  def get_shared_cards_query(shared_type, sort, order)
    if shared_type == "user_team_share"
      query = current_user.shared_cards
    elsif shared_type == "user_share"
      query = current_user.cards_shared_with.get_public_cards.order("card_user_shares.#{sort} #{order}")
    elsif shared_type == "team_share"
      query = current_user.team_shared_cards(sort,order)
    end

    return query
  end

  def authenticate_user_from_welcome_email
    if params[:welcome_token]
      authenticate_user_using_welcome_token!
    else
      # need to remove authenticating users using JWT token
      # for users coming from welcome email
      authenticate_user_from_token!
    end
  end

  def should_login?
    if params[:welcome_token]
      !current_user.invitation_accepted && current_user.created_at > 24.hours.ago
    else
      timestamp = JwtAuth.decode(token)["timestamp"]&.to_time&.utc
      !current_user.invitation_accepted && timestamp && timestamp > 24.hours.ago
    end
  end

  def decrypt_password_payload
    #skip encryption if is_cms is present
    return if params[:user][:is_cms]

    password_params  = params[:action] == 'update_password' ? params[:user] : params[:user][:password]
    if current_user.organization.encryption_payload? && password_params.present? && ENV['JS_PRIVATE_KEY'].present?
      begin
        private_key = OpenSSL::PKey::RSA.new(ENV['JS_PRIVATE_KEY'].gsub("\\n", "\n"))
        @decrypt_password = password_params.inject({}){ |hash, (k, v)| hash.merge( k.to_sym => private_key.private_decrypt(Base64.decode64(v)) ) }
      rescue OpenSSL::PKey::RSAError
        @decrypt_password = password_params
      end
    end
  end

  def onboarding_password_decryption
    if current_user.organization.encryption_payload? && user_params[:password].present? && ENV['JS_PRIVATE_KEY'].present?
      begin
        private_key = OpenSSL::PKey::RSA.new(ENV['JS_PRIVATE_KEY'].gsub("\\n", "\n"))
        params[:user].merge!(password: private_key.private_decrypt(Base64.decode64(params[:user][:password])))
      rescue OpenSSL::PKey::RSAError
        params
      end
    end
  end

end

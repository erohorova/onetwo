class Api::V2::ResourcesController < Api::V2::BaseController
  include EmbargoChecker

  before_action :embargo_content_check, only: [:create, :update]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized to access resource'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :POST, 'api/v2/resources', 'Create new resource from link'
  param :resource, Hash, required: true do
    param :link, String, desc: 'Valid resource link. It can be video, image, article link', required: true
    param :s3_bucket, String, desc: 'Boolean, LD(Custom default images) flag configuration', required: false
  end
  def create
    @resource = Resource.extract_resource(
      resource_create_params[:link],
      user: current_user,
      organization: current_org,
      s3_bucket: resource_create_params[:s3_bucket]&.to_bool
    )

    if @resource.nil?
      message = "Error while saving resource"
      render_unprocessable_entity(message)
    elsif !@resource.errors.full_messages.empty?
      message = @resource.errors.full_messages.join(", ") || "Error while saving resource"
      render_unprocessable_entity(message)
    else
      render "api/v2/resources/_details", locals: {resource: @resource, object_type: @resource}
    end
  end

  api :PUT, 'api/v2/resources/:id', 'Update resource details'
  param :id, Integer, desc: 'Id of the resource', required: true
  param :resource, Hash, required: true do
    param :title, String, :desc => "Title of the resource", :required => false
    param :description, String, :desc => "Description of the resource", :required => false
    param :image_url, String, :desc => "New image url obtained from AWS", :required => false
    param :video_url, String, :desc => "New video url for this resource", :required => false
  end

  def update
    # Allow admin to update resource
    # Resource doesnt hold organization reference
    # Check if organization card exists for related resource
    if current_user.is_admin_user? && Card.find_by(resource_id: params[:id], organization_id: current_org.id)
      @resource = Resource.where(id: params[:id]).first!
    else
      @resource = Resource.where(user_id: current_user.id, id: params[:id]).first!
    end

    attrs = update_image_url
    if @resource.update_attributes(attrs)
      # log resource update so that it can be merged with card update
      # when update button is clicked
      LogHistoryJob.perform_later(
        "entity_type" => 'resource',
        "entity_id" => @resource.id,
        "changes" => @resource.previous_changes.as_json,
        "user_id" => current_user.id
      )
      head :ok and return
    else
      return render_unprocessable_entity(@resource.errors.full_messages.join(", "))
    end
  end

  private
  def resource_params
    params.require(:resource).permit(:title, :description, :image_url, :video_url)
  end

  def resource_create_params
    params.require(:resource).permit(:link, :s3_bucket)
  end

  # DESC:
  #   Pick filestack's raw URL before updating.
  def update_image_url
    attrs = resource_params

    if attrs["image_url"].present?
      attrs["image_url"] = Filestack::Security.read_filestack_url(attrs["image_url"])
    end

    attrs
  end
end

class Api::V2::UserImportStatusesController < Api::V2::BaseController
  before_action :require_organization_admin

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end
  api :GET, 'api/v2/user_import_statuses', 'List all status of all the user imports'
  def index
    imports = current_user.invitation_files.select(:id, :created_at, :send_invite_email)

    render json: { imports: InvitationFileBridge.new(imports, user: current_user).attributes }, status: :ok
  end

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end
  api :GET, 'api/v2/user_import_statuses/:id', 'Give status of perticular user import'
  param :id, String, desc: 'invitation file id', required: true
  def show
    head :not_found and return if params[:id].blank?

    import = InvitationFile.find_by(id: params[:id])

    parameters = import.parameter

    old_users_count = parameters.try(:[], 'old_users_count') || import.user_import_statuses.old_users.count
    new_users_count = parameters.try(:[], 'new_users_count') || import.user_import_statuses.new_users.count
    failed_users_count = parameters.try(:[], 'failed_users_count') || import.user_import_statuses.failed_users.count
    total_users_count = parameters.try(:[], 'total_users_count') || import.user_import_statuses.count
    new_groups_count = import.parameter.try(:[], 'new_groups_count')
    old_groups_count = import.parameter.try(:[], 'old_groups_count')
    resent_emails_count = parameters.try(:[], 'resent_emails_count') || import.user_import_statuses.resent_emails.count

    user_import_statuses = {
      oldUsersCount: old_users_count,
      newUsersCount: new_users_count,
      failedUsersCount: failed_users_count,
      totalUsersCount: total_users_count,
      newGroupsCount: new_groups_count,
      oldGroupsCount: old_groups_count,
      resentEmailsCount: resent_emails_count
    }
    render json: { status: user_import_statuses } , status: :ok
  end
end

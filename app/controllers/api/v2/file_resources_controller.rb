class Api::V2::FileResourcesController < Api::V2::BaseController
  include Api::V2::BaseHelper

  api :POST, 'api/v2/file_resources', 'Create a file_resources with the ID of the current user'
  param :file, String, desc: 'Attachment as file or an URL', required: true
  param :filestack, Array, desc: 'Array of objects received from filestack', required: false
  def create
    @file_resource = FileResource.new(user: current_user)
    data = params[:file]
    if data.nil? || ( data.is_a?(String) && check_status_external_link(data) != 200 )
      render_unprocessable_entity('File parameter absent') and return
    end
    @file_resource.attachment = data
    @file_resource.filestack = params[:filestack]
    unless @file_resource.save
      render_unprocessable_entity(@file_resource.errors.join(", ")) and return
    end
  end

  api :DELETE, 'api/v2/file_resources/:id', 'Delete file_resource'
  param :id, Integer, desc: 'Id of the file_resource', required: true
  def destroy
    @file_resource = FileResource.where(id: params[:id], user_id: current_user.id).first!
    if @file_resource.destroy
      head :ok
    else
      render_unprocessable_entity(@file_resource.errors.join(", ")) and return
    end
  end

  api :GET, 'api/v2/file_resources/:id', 'Get info about file_resource'
  param :id, Integer, desc: 'Id of the file_resource', required: true
  def show
    @file_resource = FileResource.where(id: params[:id], user_id: current_user.id).first!
  end
end
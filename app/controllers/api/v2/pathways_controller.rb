class Api::V2::PathwaysController < Api::V2::BaseController
  include PathwayConcern
  include CardsFilter
  before_action :set_last_access_opts, only: [:show]
  before_action :load_card, :check_pathway, :require_card_access, except: [:add_cards]

  api :GET, 'api/v2/pathways/:id', 'Display contents of a pathway.'
  param :id, Integer, "Card id", required: true
  param :last_access_at, String, desc: 'Time of last access, format: 1505832150', required: false
  param :is_standalone_page, String, 'Boolean with `true` or `false` as values', required: false
  def show
    # Merge to detect if the requests originated from mobile or web.
    @options.merge!(is_native_app: is_native_app?)

    json_response = {}

    fields = params[:is_standalone_page].to_bool ?
      'views_count,channel_ids,voters,channels(id,label),due_at,assigner,teams(id,name),tags(id,name),tags,channels,teams,language,auto_complete' :
      'auto_complete,tags(id,name),channels(id,label),channels,tags,leaps,language,filestack,provider_image'
    # Generate minified response for cover card for default fields.
    minified_cover = CardBridge.new([@card], user: current_user, options: @options).attributes[0]
    pathway_details = CardBridge.new(
      [@card],
      user: current_user,
      fields: fields,
      options: @options
    ).attributes[0]
    # push cover card details.
    json_response.merge!(minified_cover)
    json_response.merge!(pathway_details)

    children_cards = generate_pathway_children_response(pathway_author_id: @card.author_id)
    # push children cards to key `packCards`.
    json_response.merge!(packCards: children_cards)

    render json: json_response, status: :ok
  end

  api :GET, 'api/v2/pathways/:id/edit', 'Fetch data required for editing a pathway.'
  param :id, Integer, "Card id", required: true
  def edit
    json_response = {}
    if params[:fields]
      @fields = params[:fields]
    else
      default_fields = 'id,title,message,card_type,card_subtype,slug,state,is_official,provider,provider_image,readable_card_type,share_url,average_rating,skill_level,filestack,votes_count,comments_count,published_at,prices,is_assigned,is_bookmarked,hidden,is_public,'
      default_fields += 'is_paid,mark_feature_disabled_for_source,completion_state,is_upvoted,all_ratings,is_reported,card_metadatum,ecl_metadata,ecl_duration_metadata,author,rank,badging,quiz,resource,video_stream,project_id,is_clone,language,leaps'
      direct_fields = 'views_count,auto_complete,channel_ids,voters,users_with_access,teams_permitted,users_permitted,badging,show_restrict,tags,channels,teams,channels(id,label,is_private),teams(id,label),tags(id,name)'
      children_fields = 'auto_complete,duration,teams_permitted,users_permitted,completed_percentage,file_resources,channel_ids,non_curated_channel_ids,paid_by_user,locked,show_restrict,users_with_access,teams(id,label),tags(id,name),channels(id,label,is_private),channels,tags,teams'
      @fields = "#{default_fields},#{direct_fields},pack_cards(#{default_fields},#{children_fields}),pack_cards"
    end
    json_response =  PathwayBridge.new([@card], user: current_user, fields: @fields, options: { is_native_app: is_native_app? }).attributes[0]
    render json: json_response, status: :ok
  end

  api :POST, 'api/v2/pathways/add_cards', 'This API is NOT FOR MOBILE. Add contents to respective sections(pathway).'
  param :data, Array, of: Hash, desc: 'This will contain all the data. It will be an array of hash', required: true do
    param :section_id, Integer, desc: 'Section id i.e. pathway id'
    param :content_ids, Array, of: Integer, desc: 'Content ids i.e. cards which are to be added in the pathway'
  end
  authorize :add_cards, Permissions::ADD_TO_PATHWAY
  def add_cards
    sections = params[:data]
    return render_unprocessable_entity('Data should be of type array') unless sections.is_a?(Array)
    @errors = []

    # fetch all the sections
    section_ids = sections.map { |section| section[:section_id] }&.reject(&:blank?)
    @section_data = current_org.cards.where(id: section_ids).index_by(&:id)

    sections.each do |section|
      cover = @section_data[section[:section_id]&.to_i]
      process_section(cover: cover, section: section)
    end

    if @errors.any?
      message = "There were errors when adding cards with the following ids: #{@errors}"
      render_with_status_and_message(message, status: :ok)
    else
      head :ok
    end
  end

  private

  def filter_children_cards(children: [])
    return [] unless children.is_a? Array

    private_children_cards = children.map { |child_card| OpenStruct.new(child_card) if !child_card[:isPublic] }.compact
    selected_private_children_cards = current_user.private_accessible_cards(private_children_cards.map(&:id))
    rejected_private_card_ids = private_children_cards.map{|private_child_card|private_child_card[:id]} - selected_private_children_cards.map{|child_card|child_card[:id].to_s}
    children.each do |child_card|
      child_card.clear[:message] = 'You are not authorized to access this card' if rejected_private_card_ids.include?(child_card[:id])
    end unless rejected_private_card_ids.empty?
    children
  end

  # fields - response keys.
  # include_default_fields - will include default fields if true and vice-versa.
  # returns child cards of a pathway.
  # returns [] when no child cards.
  # TODO
  # remove the generate_pathway_children_response method, since it has been handled in pathway bridge
  def generate_pathway_children_response(pathway_author_id:, include_default_fields: false, fields: nil)
    # Merge to detect if the requests originated from mobile or web.
    @options.merge!(is_native_app: is_native_app?)

    children_cards = []

    # Only children cards
    ordered_card_pack_rel = CardPackRelation.pack_relations(cover_id: @card.id).reject { |cp_rel| cp_rel.from_id == @card.id }
    # This check is added because we can create draft pathway without any children cards
    if ordered_card_pack_rel.present?
      # Established an array that says if a card in a pathway is locked.
      # FORMAT: { `card_id`: `true/false` }
      card_pack_relations = ordered_card_pack_rel.map { |cp_rel| [cp_rel.from_id, cp_rel.locked] }.to_h
      relations = Card.where(id: card_pack_relations.keys, organization_id: current_org.id).order("FIELD(id, #{card_pack_relations.keys.join(',')})")
      # Generate minified response for cards in a pathway.
      children_cards = CardBridge.new(relations, user: current_user, fields: fields, options: @options).attributes
      if include_default_fields && fields.present?
        minified_children_cards = CardBridge.new(relations, user: current_user, options: @options).attributes
        children_cards.zip(minified_children_cards).each do |children_card, minified_children_card|
          children_card.merge!(**minified_children_card, **{locked: card_pack_relations[minified_children_card[:id].to_i]})
        end
      else
        children_cards.each { |children_card| children_card.merge!(locked: card_pack_relations[children_card[:id].to_i]) }
      end

      children_cards = filter_children_cards(children: children_cards) unless (current_user.id == pathway_author_id) || current_user.is_org_admin?
    end

    children_cards
  end

  def check_pathway
    head :not_found and return unless @card.is_pack?
  end

  def load_card
    @card = load_card_from_compound_id(params[:id])
  end
end

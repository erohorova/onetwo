class Api::V2::UserDailyLearningsController < Api::V2::BaseController
  include SearchConcern
  include LimitOffsetConcern
  include CardsFilter
  has_limit_offset_constraints only: [:search_user_daily_learnings]

  before_action :check_availablity_and_respond, only:[:index]
  before_action :set_filter_by_language, :set_last_access_opts, only: [:index, :search_user_daily_learnings]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, '/api/v2/user_daily_learnings',
            "Get current user's daily learnings. This API returns state of the daily learning process and cards.
             Success value for state is 'completed'"
  param :filter_by_language, [true, false], desc: "Filter cards based on the user's language", required: false
  param :last_access_at, String, desc: 'Time of last access, format: 1505832150', required: false
  param :fields, String, desc: 'Gives On-demand response', required: false
  def index
    @cards = []
    if @user_daily_learning
      exclude_ids = (current_user.dismissed_card_ids + current_user.bookmarked_card_ids + current_user.completed_card_ids).uniq
      @cards = Card.where(id: @user_daily_learning.card_ids).where.not(id: exclude_ids)
      if @filter_by_language
        @cards = @cards.filtered_by_language(current_user.language)
      end
      # To call on the visibility filter only if user is not an org admin and if cards are not empty
      @cards = @cards.visible_to_userV2(current_user) unless (current_user.is_org_admin? || @cards.empty?)
    end
  end

  api :GET, '/api/v2/user_daily_learnings/available',
            "Get the status of current user's daily learnings. This API returns state of the daily learning process.
             Success value for state is 'completed'"
  def available
  end

  api :GET, '/api/v2/user_daily_learnings/search_user_daily_learnings',
            "Get current user's daily learnings. This API returns state of the daily learning process and cards.
             Success value for state is 'completed'"
  param :filter_by_language, [true, false], desc: "Filter cards based on the user's language", required: false
  param :last_access_at, String, desc: 'Time of last access, format: 1505832150', required: false
  param :fields, String, desc: 'Gives On-demand response', required: false

  def search_user_daily_learnings
    # Merge to detect if the requests originated from mobile or web.
    @options.merge!(is_native_app: is_native_app?)

    @_limit = 100
    todays_learning_version = current_org.current_todays_learning

    @state = DailyLearningProcessingStates::COMPLETED
    current_params = params
    current_params.merge!(offset: offset, limit: limit)
    case todays_learning_version
    when 'v2'
      @cards = TodaysLearning::V2.new(user: current_user, opts: current_params).process
    when 'v3'
      @cards = TodaysLearning::V3.new(user: current_user, opts: current_params).process
    when 'v4'
      @cards = TodaysLearning::V4.new(user: current_user, opts: current_params).process
    else
      @cards = []
    end
    if @filter_by_language
      cards_ids = @cards.map(&:id)
      @cards = Card.where(id: cards_ids).filtered_by_language(current_user.language)
    end
  end

  private
    def check_availablity_and_respond
      # Cut-off for today's learning 6 AM Users time zone / 6 AM PST default
      date = Time.now.in_time_zone(current_user.time_zone || "Pacific Time (US & Canada)")
      date = date - 1.day if date.hour < 6
      date = date.strftime('%F') # yyyy-mm-dd

      @user_daily_learning = UserDailyLearning.daily_learning(user_id: current_user.id , date: date)
      if @user_daily_learning.nil?
        UserDailyLearning.initialize_and_start(user_id: current_user.id, date: date)
        @state = DailyLearningProcessingStates::INITIALIZED
      else
        @state = @user_daily_learning.processing_state
      end
    end
end

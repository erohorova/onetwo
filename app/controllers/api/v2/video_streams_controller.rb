class Api::V2::VideoStreamsController < Api::V2::BaseController
  include FollowableConcern
  include EventTracker

  before_action :load_video_stream, only: [:create_recording, :recordings, :start, :ping,
    :stop, :join, :leave, :viewers, :retranscode, :presigned_post_url, :show,
    :record_download_metric, :record_preview_metric]
  before_action :ensure_creator_only_access, only: [:create_recording, :start, :ping, :stop, :retranscode, :presigned_post_url]
  before_action :restrict_video_stream_access_based_on_state, only: [:recordings, :join, :leave, :viewers]
  has_limit_offset_constraints only: [:viewers]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET,'api/v2/video_streams/uuid', 'Generate new uuid for new video stream'
  def uuid
    render json: {uuid: VideoStream.new.assign_uuid}
  end

  api :POST, "api/v2/video_streams/:id/recordings", "Upload recorded video from AWS s3"
  param :id, Integer, desc: "Id of the video stream", required: true
  param :bucket, String, desc: "s3 bucket name", required: true
  param :key, String, desc: "s3 file key", required: true
  param :location, String, desc: "Full s3 file location", required: true
  param :checksum, String, desc: "s3 file checksum", required: true
  param :sequence_number, String, desc: "Sequence number of the video stream if it was splitted up into multiple file. Default will be 0", required: false
  def create_recording
    sequence_number = params[:sequence_number] || 0

    recording = @video_stream.recordings.where(sequence_number: sequence_number, source: 'client').first_or_initialize

    recording.bucket    = params[:bucket]
    recording.key       = params[:key]
    recording.location  = params[:location]
    recording.checksum  = params[:checksum]

    if recording.save
      render json: {id: recording.id, url: recording.location}, status: :created
    else
      render_unprocessable_entity(recording.errors.full_messages.join(", ")) and return
    end
  end

  api :GET, "api/v2/video_streams/:id/recordings", "Get past recordings of a given video streams"
  param :id, Integer, desc: "Id of the video stream", required: true
  def recordings
    unless @video_stream.can_be_accessed_by?(current_user)
      render_unauthorized and return
    end
    @recordings = @video_stream.recordings
  end

  api :POST, "api/v2/video_streams/:id/start", "Start recording a video stream"
  param :id, Integer, desc: "Id of the video stream", required: true
  def start
    if !@video_stream.start!
      render_unprocessable_entity(@video_stream.errors.full_messages.join(", ")) and return
    end
    head :ok
  end

  api :POST, "api/v2/video_streams/:id/ping", "Tell the server that the app is still streaming"
  param :id, Integer, desc: "Id of the video stream", required: true
  def ping
    unless @video_stream.live? && @video_stream.touch(:last_stream_at)
      render_unprocessable_entity(@video_stream.errors.full_messages.join(", ")) and return
    end

    head :ok
  end

  api :POST, "api/v2/video_streams/:id/stop", "Stop recording a video stream"
  param :id, Integer, desc: "Id of the video stream", required: true
  def stop
    if @video_stream.live? && @video_stream.stop!
      head :ok
    else
      render_unprocessable_entity('Can not stop video stream with not live state') and return
    end
  end

  api :POST, "api/v2/video_streams/:id/join", "join a video stream"
  param :id, Integer, desc: "Id of the video stream", required: true
  def join
    if @video_stream.live? && @video_stream.add_viewer(current_user)
      head :ok
    else
      render_unprocessable_entity("Error while joining the video stream")
    end
  end

  api :POST, "api/v2/video_streams/:id/leave", "leave a video stream"
  param :id, Integer, desc: "Id of the video stream", required: true
  def leave
    if @video_stream.live? && @video_stream.watchers.destroy(current_user)
      head :ok
    else
      render_unprocessable_entity("Error while leaving the video stream")
    end
  end

  api :PUT, 'api/v2/video_streams/:id/retranscode', 'Retranscode recording'
  param :recording_id, Integer, desc: "Id of a particular recording to transcode to", required: false
  def retranscode
    find_recording
    if !@recording.nil?
      TranscoderJob.perform_later(recording_id: @recording.id, overwrite: true)
      head :ok
    else
      render_not_found and return
    end
  end

  api :GET, 'api/v2/video_streams/:id/viewers', 'Get viewers for the live streaming video'
  param :id, Integer, desc: 'Id of the live streaming video', required: true
  param :limit, Integer, decs: 'Number of viewers to fetch. Default: 10', required: false
  param :offset, Integer, desc: 'skip specified number of records. Use in combination with limit. Default: 0'
  def viewers
    unless @video_stream.can_be_accessed_by?(current_user) && @video_stream.live?
      render_unauthorized and return
    end
    query = @video_stream.watchers
    @total = query.count
    @viewers = query.limit(limit).offset(offset)
    load_is_following_for(@viewers.map(&:id), 'User', current_user)
  end

  api :GET, 'api/v2/video_streams/:id/presigned_post_url', 'Get s3 information for video upload'
  def presigned_post_url
    @recording, @recording_upload = @video_stream.get_presigned_post_object
    render json: { presigned_post: @recording_upload.fields, recording_id: @recording.id, url: @recording_upload.url.to_s } and return
  end

  api :GET, 'api/v2/video_streams/:id', "Get video streams details"
  param :id, Integer, desc: 'Id of the live streaming video', required: true
  def show
    unless @video_stream.can_be_accessed_by?(current_user)
      render_unauthorized and return
    end
  end

  api :POST, 'api/v2/video_streams/:id/record_download_metric',
             'tell the server when a video stream is downloaded'
  param :id, Integer, desc: 'Id of the live streaming video', required: true
  def record_download_metric
    record_custom_event(
      event: 'card_video_stream_downloaded',
      actor: :current_user,
      job_args: [{card: @video_stream.card}]
    )
    head :ok
  end

  api :POST, 'api/v2/video_streams/:id/record_preview_metric',
             'tell the server when a video stream is previewed'
  param :id, Integer, desc: 'Id of the live streaming video', required: true
  def record_preview_metric
    record_custom_event(
      event: 'card_video_stream_previewed',
      actor: :current_user,
      job_args: [{card: @video_stream.card}]
    )
    head :ok
  end

  private

  def load_video_stream
    # TODO: we will me removing columns VS that are on VS as well as on card
    @video_stream = VideoStream.joins(:card).where('cards.organization_id = ?', current_org.id).friendly.find(params[:id])
  end

  def ensure_creator_only_access
    if !((@video_stream && @video_stream.card.author_id == current_user.id) || current_user.is_admin_user?)
      render_unauthorized and return
    end
  end

  def restrict_video_stream_access_based_on_state
    if @video_stream.card.deleted? || (@video_stream.card.draft? && @video_stream.card.author_id != current_user.id)
      render_not_found and return
    end
  end

  def find_recording
    return if @video_stream.nil?

    @recording = if params[:recording_id]
      @video_stream.recordings.find(params[:recording_id])
    else
      Recording.where(video_stream_id: @video_stream.id, source: 'client', transcoding_status: 'pending').first
    end
  end
end

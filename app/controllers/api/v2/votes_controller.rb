class Api::V2::VotesController < Api::V2::BaseController
  include VotesConcern

  before_filter :find_votable

  api :POST, 'api/v2/votes', 'Create an upvote for the currently logged in user'
  param :vote, Hash, desc: 'Vote hash' do
    param :content_id, Integer, desc: 'Content id', required: true
    param :content_type, String, desc: 'Content type. Allowed content types: Card, Comment, ActivityStream', required: true
  end
  formats ['json']
  api_version 'v2'
  authorize :create, Permissions::LIKE_CONTENT
  def create
    if create_vote
      track_vote
      head :ok
    else
      render_unprocessable_entity(@vote.errors.full_messages.join(","))
    end
  end

  api :DELETE, 'api/v2/votes', 'Delete an upvote for the currently logged in user'
  param :vote, Hash, desc: 'Vote hash' do
    param :content_id, Integer, desc: 'Content id', required: true
    param :content_type, String, desc: 'Content type. Allowed content types: Card, Comment, ActivityStream', required: true
  end
  formats ['json']
  api_version 'v2'
  authorize :destroy, Permissions::LIKE_CONTENT
  def destroy
    if delete_vote
      track_vote(action: 'unlike')
      head :ok
    else
      @vote ? render_unprocessable_entity(@vote.errors.full_messages.join(",")) : render_not_found('Vote not found')
    end
  end
  
end

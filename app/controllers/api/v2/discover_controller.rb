class Api::V2::DiscoverController < Api::V2::BaseController
  include CardsFilter

  has_limit_offset_constraints only: [:carousel_content, :videos, :trending_cards, :trending_pathways,
    :trending_journeys, :channels, :interested_cards, :cards_by_channels ]

  before_action :set_filter_by_language, only: [
    :trending_cards,    :trending_pathways,
    :trending_journeys, :videos,
    :carousel_content
  ]
  before_action :set_last_access_opts, only: [
    :trending_cards,    :trending_pathways,
    :trending_journeys, :videos
  ]

  api :GET, 'api/v2/discover/items_count', 'Get total counts of discover tab items'
  api_version 'v2'
  formats ['json']
  def items_count
    @data = Rails.cache.fetch("discover-items-counts-for-#{current_user.id}", expires_in: 10.minutes) do
      exclude_user_ids = current_user.followed_user_ids + [current_user.id]
      collections_count = current_org.cards.published.where(card_type: 'pack').count
      video_streams_count = current_org.cards.published.where(card_type: 'video_stream', hidden: false).count

      {
        total_users_count: current_org.users.size,
        pathways_count: collections_count,
        video_streams_count: video_streams_count,
        courses_count: current_org.featured_courses.count,
        channels_count: Channel.fetch_eligible_channels_for_user(user: current_user).count,
        suggested_users_count: User.suggested_users_for(user: current_user).count,
        total_suggested_users_count: User.sme_and_influencers.where(organization: current_org).count
      }
    end

    render json: @data, status: :ok
  end

  api :GET, 'api/v2/discover/trending_cards', 'Get trending cards on discover tab'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 25', required: false
  param :filter_by_language, [true, false], desc: "Filter cards based on the user's language", required: false
  param :last_access_at, String, desc: 'Time of last access, format: 1505832150', required: false
  param :fields, String, desc: 'Required fields for minimal response', required: false
  api_version 'v2'
  formats ['json']
  def trending_cards
    # Merge to detect if the requests originated from mobile or web.
    @options.merge!(is_native_app: is_native_app?)

    @cards = DiscoveryService::UserCardDiscovery.new(
      user_id: current_user.id,
      limit: limit,
      offset: offset,
      filter_params: {
        filter_by_language: @filter_by_language
      }
    ).trending_cards
  end

  api :GET, 'api/v2/discover/interested_cards', 'Get interested pathways, journeys and courses based on user interests'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 25', required: false
  param :channel_ids, Integer, desc: "Array of channels_ids to fetch the content from", :required => false
  api_version 'v2'
  formats ['json']
  def interested_cards
    @cards = DiscoveryService::UserCardDiscovery.new(
      user_id: current_user.id,
      channel_ids: params[:channel_ids],
      limit: limit,
      offset: offset
    ).interested_cards

    if @cards.blank?
      head :no_content and return
    end

    render 'cards_by_channels'
  end

  api :GET, 'api/v2/discover/user_interested_cards', 'Get interested
             pathways, journeys and courses from user followed channels based on
             user interests.'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 25', required: false
  param :card_types, Array, desc: "Array of card types i.e media, journey, pack etc", :required => false
  api_version 'v2'
  formats ['json']
  def user_interested_cards
    channel_ids = current_user.followed_channel_ids
    @cards = DiscoveryService::UserCardDiscovery.new(
      user_id: current_user.id,
      channel_ids: channel_ids,
      card_types: params[:card_types],
      limit: current_org.number_of_learning_items
    ).relevent_interested_cards

    if @cards.blank?
      head :no_content and return
    end
    render 'cards_by_channels'
  end

  api :GET, 'api/v2/discover/cards_by_channels', 'Get pathways, journeys and courses based on user interests from user following channels'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 25', required: false
  api_version 'v2'
  formats ['json']
  def cards_by_channels
    channel_ids = current_user.followed_channel_ids
    @cards = Card.curated_cards_for_channel_id(channel_ids).published.where(card_type: ['pack', 'journey', 'course']).limit(limit).offset(offset)

    if @cards.blank?
      head :no_content and return
    end
  end

  api :GET, 'api/v2/discover/trending_pathways', 'Get trending pathways on discover tab'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 25', required: false
  param :filter_by_language, [true, false], desc: "Filter cards based on the user's language", required: false
  param :last_access_at, String, desc: 'Time of last access, format: 1505832150', required: false
  param :fields, String, desc: 'Required fields for minimal response', required: false
  api_version 'v2'
  formats ['json']
  def trending_pathways
    # Merge to detect if the requests originated from mobile or web.
    @options.merge!(is_native_app: is_native_app?)

    if @current_org.trending_org_pathway?
      @cards = DiscoveryService::UserCardDiscovery.new(
        user_id: current_user.id,
        limit: limit,
        offset: offset,
        duration: 30.days.ago,
        filter_params: {
          filter_by_language: @filter_by_language
        }
      ).trending_pathways
    else
      @cards = DiscoveryService::UserCardDiscovery.new(
        user_id: current_user.id,
        limit: limit,
        offset: offset,
        card_types: ["pack"],
        filter_params: {
          filter_by_language: @filter_by_language
        }
      ).personalized_trending_pathway
    end
    @fields = params[:fields] || 'title,id,slug,filestack,pack_cards_count,message,card_type,is_new'
  end

  api :GET, 'api/v2/discover/trending_journeys', 'Get trending journeys on discover tab'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 25', required: false
  param :filter_by_language, [true, false], desc: "Filter cards based on the user's language", required: false
  param :last_access_at, String, desc: 'Time of last access, format: 1505832150', required: false
  param :fields, String, desc: 'Required fields for minimal response', required: false
  api_version 'v2'
  formats ['json']
  def trending_journeys
    # Merge to detect if the requests originated from mobile or web.
    @options.merge!(is_native_app: is_native_app?)

    if @current_org.trending_org_journey?
      @cards = DiscoveryService::UserCardDiscovery.new(
        user_id: current_user.id,
        limit: limit,
        offset: offset,
        duration: 30.days.ago,
        filter_params: {
          filter_by_language: @filter_by_language
        }
      ).trending_journeys
    else
      @cards = DiscoveryService::UserCardDiscovery.new(
        user_id: current_user.id,
        limit: limit,
        offset: offset,
        card_types: ["journey"],
        filter_params: {
          filter_by_language: @filter_by_language
        }
      ).personalized_trending_journey
    end

    fields = 'title,id,slug,filestack,journey_packs_count,message,card_type,is_new'
    fields = params[:fields].present? ? params[:fields] : fields
    render json: {
      cards: CardBridge.new(
        @cards,
        user: current_user,
        fields: fields,
        options: @options.merge!(is_native_app: is_native_app?)
      ).attributes
    }
  end

  api :GET, 'api/v2/discover/channels', "Get channels as per user\'s learning topics"
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 25', required: false
  api_version 'v2'
  formats ['json']
  def channels
    @channels = DiscoveryService::Channels.new(
      user: current_user,
      params: { limit: limit, offset: offset }
    ).discover
  end

  api :GET, 'api/v2/discover/videos', 'Get videos on discover tab'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 25', required: false
  param :q, String, desc: 'Query string; default matches all', required: false
  param :filter_by_language, [true, false], desc: "Filter cards based on the user's language", required: false
  param :last_access_at, String, desc: 'Time of last access, format: 1505832150', required: false
  param :fields, String, desc: 'Required fields for minimal response', required: false
  api_version 'v2'
  formats ['json']
  def videos
    @videos = DiscoveryService::Videos.new(
      current_user.id, limit, offset, params[:q].presence,
      filter_params: {
        filter_by_language: @filter_by_language
      }
    ).video_cards
    # NOTE: Not maintaining backward-compatibility
    render json: {
      videos: CardBridge.new(@videos,
        user: current_user,
        fields: params[:fields],
        options: @options.merge!(is_native_app: is_native_app?)
      ).attributes
    }
  end

  api :GET, 'api/v2/discover/carousel_content', 'Get carousel_content on discover tab for uuid'
  api_version 'v2'
  param :uuid, String, desc: 'UUID of custom carousel', required: true
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 25', required: false
  param :filter_by_language, [true, false], desc: "Filter cards based on the user's language", required: false
  formats ['json']
  def carousel_content
    if params[:uuid]
      @cards = DiscoveryService::CarouselContent.new(
        current_user, params["uuid"], limit, offset,
        filter_params: {
          filter_by_language: @filter_by_language
        }
      ).get_content || []
      render('cards_by_channels') and return
    else
      render_unprocessable_entity("UUID not present") and return
    end
  end
end

class Api::V2::RolesController < Api::V2::BaseController

  has_limit_offset_constraints only: :index
  before_action :require_organization_admin
  before_action :set_role, only: %i[update show destroy]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized to access role'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/v2/roles/', 'Get all possible roles exist in an organization'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  def index
    @roles = current_org.roles.includes(:role_permissions)
      .order(id: :desc).limit(limit).offset(offset).to_a
    @total_count = current_org.roles.count
  end

  api :POST, 'api/v2/roles', 'Create Role'
  param :name, String, desc: 'role name', required: true
  param :enable_permissions, Array, desc: 'set of permissions to update the role', required: false
  def create
    @role = current_org.roles.new(role_params)
    if @role.save
      permissions = params[:enable_permissions].is_a?(Array) && params[:enable_permissions].presence
      if permissions
        permissions.reject(&:blank?).each do |role_permission|
          @role.role_permissions.find_or_create_by(name: role_permission).enable!
        end
      end
      render 'show'
    else
      render json: {errors: @role.errors.full_messages}, status: :unprocessable_entity
    end
  end

  api :PUT, 'api/v2/roles/:id', 'Update Role details.'
  description <<-EOS
    This endpoint is to update role details for ex. name, image_url, threshold.
  EOS
  param :name, String, desc: 'role name', required: false
  param :id, Integer, desc: 'role id', required: true
  param :image_url, String, desc: 'Image used to indicate role of org-member.', required: false
  param :threshold, String, desc: 'threshold', required: false
  def update
    if @role.update(role_params)
      render 'show'
    else
      render json: {errors: @role.errors.full_messages}, status: :unprocessable_entity
    end
  end

  api :DELETE, "api/v2/roles/:id", "Delete a Role"
  param :id, Integer, desc: "role id", required: true
  def destroy
    if @role.destroy
      render_with_status_and_message('Role was successfully deleted', status: :ok)
    else
      render_unprocessable_entity(@role.errors.full_messages.join(", "))
    end
  end

  api :GET, 'api/v2/roles/:id', 'Get detailed info about role.'
  def show; end

  api :PUT, 'api/v2/roles/permission', 'Update role permissions. Disabled permission saved at the last.'
  param :id, String, desc: 'id of the role', required: true
  param :role_name, String, desc: 'Name of the role', required: false
  param :enable_permissions, Array, desc: 'Permissions to be enabled for this role. Use permission slug', required: false
  param :disable_permissions, Array, desc: 'permissions to be disabled for this role. Use permission slug', required: false
  def role_permission
    enable_permissions = params.fetch(:enable_permissions, [])
    disable_permissions = params.fetch(:disable_permissions, [])
    if params[:id]
      role = current_org.roles.where(id: params[:id]).first!
    else
      render_unprocessable_entity('Role name is required') and return if params[:role_name].blank?
      role = current_org.roles.where(name: params[:role_name]).first!
    end

    # enable permissions
    if enable_permissions.present?
      enable_permissions.each do |role_permission|
        role.role_permissions.find_or_create_by(name: role_permission).enable!
      end
    end

    # disable permissions
    if disable_permissions.present?
      role.role_permissions.where(name: disable_permissions).map(&:disable!)
    end

    head :ok
  end

  protected

  def role_params
    params.permit(:name, :image_url, :threshold)
  end

  def set_role
    @role = current_org.roles.find(params[:id])
  end

end

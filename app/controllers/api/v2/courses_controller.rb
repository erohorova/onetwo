class Api::V2::CoursesController < Api::V2::BaseController
  include CoursesConcern

  before_filter :require_organization_admin, only: [:export]

  api :POST, 'api/v2/courses/refresh_external_courses', 'get external courses of a user from integration'
  param :fields, Hash, desc: 'input fields', required: true
  param :provider, String, desc: 'External provider name', required: true
  formats ['json']
  api_version 'v2'
  def refresh_external_courses
    if scrape_courses
      render json: {} and return
    else
      return render_unprocessable_entity("Error occurred while fetching external courses")
    end
  end

  api :GET, 'api/v2/courses/export', 'Export courses in an organization'
  param :course_ids, Array, "Array of group IDs", required: true
  formats ['json']
  def export
    @groups = ResourceGroup.where(client_resource_id: params[:course_ids], organization: current_org)
    ExportCoursesJob.perform_later(group_ids: @groups.pluck(:id), user_id: current_user.id)

    head :ok
  end
end

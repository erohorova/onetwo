class Api::V2::Superadmin::PlatformMetricsController < Api::V2::Superadmin::BaseController

  api :GET, 'api/v2/superadmin/platform_metrics', 'Returns Usage Metrics for the Edcast Platform'
  api_version 'v2'
  description <<-EOS
    This will return a set of usage metrics for the teams within Edcast. 
    Currently, the supported metrics are # Users, DAU, and MAU
    === Example:
    To get data for GE for January 2016, call:
    api/v2/superadmin/platform_metrics?team=ge&month=1&year=2016 \n
  EOS
  param :team, String, desc: 'Host name of the team for which metrics are needed', required: true
  param :month, Integer, desc: 'Get Data for this month, integer between 1 (January) to 12 (December)', required: true
  param :year, Integer, desc: 'Get Data for this year, integer in the YYYY format', required: true
  error 401, 'Logged in User is not authorized to perform this request'
  error 500, 'Probably bad month parameter, valid values are between 1 and 12'
  error 404, 'Organization with the host name does not exist'
  formats ['json']
  def index
    organization = Organization.where(host_name: params[:team]).first!
    data = OrganizationMetricsGatherer.platform_metrics_report_data(organization: organization, year: params[:year], month: params[:month])
    render json: data, status: :ok and return
  end
end

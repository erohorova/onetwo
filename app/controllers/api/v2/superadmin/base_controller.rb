class Api::V2::Superadmin::BaseController < Api::V2::BaseController
  before_filter :require_superadmin
  def require_superadmin
    return render_unauthorized unless current_user.is_admin?
  end
end

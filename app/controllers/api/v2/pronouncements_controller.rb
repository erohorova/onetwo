class Api::V2::PronouncementsController < Api::V2::BaseController
  has_limit_offset_constraints only: [:index]
  before_action :check_pronouncement_params, only: [:index, :create, :update]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/v2/pronouncements', 'Fetch announcements for given id and type'
  def_param_group :pronouncement do
    param :pronouncement, Hash, required: true do
      param :scope_id, Integer, desc: 'Id for which announcements to be fetched', required: true
      param :scope_type, String, desc: 'Type for which id is related. e.g. Organization, Team', required: true
      param :is_active, String, desc: 'To fetch active announcements', required: false
    end
  end
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  formats ['json']
  def index
    if params[:pronouncement][:scope_id].blank? || params[:pronouncement][:scope_type].blank?
      head :unprocessable_entity and return
    end

    find_pronounceable
    pronouncements = @pronounceable.pronouncements

    if params[:pronouncement][:is_active].try(:to_bool)
      pronouncements = pronouncements.live_pronouncement
    end

    @total = pronouncements.count
    @pronouncements = pronouncements.limit(limit).offset(offset).order('end_date DESC')
  end

  api :POST, 'api/v2/pronouncements', 'Create an announcement for given type and id'
  description <<-EOS
    Create announcement for a team / organization based on scope_id and scope_type.
    label, scope_id, scope_type, start_date is required
  EOS

  param :pronouncement, Hash, required: true do
    param :label, String, desc: 'Announcement text with html if given', required: true
    param :scope_id, Integer, desc: 'Id for which announcement to be created', required: true
    param :scope_type, String, desc: 'Type for which id is related. e.g. Organization, Team', required: true
    param :start_date, String, desc: 'Start date for announcement to show, format: mm/dd/yyyy hh:mm:ss in UTC', required: true
    param :end_date, String, desc: 'End date upto which announcement to show. Default is null. Format: , format: mm/dd/yyyy hh:mm:ss in UTC', required: false
    param :is_active, String, desc: 'Announcement should be enabled or not. Default is true', required: false
  end
  formats ['json']
  def create
    set_pronouncement_date_params
    @pronouncement = Pronouncement.new(pronouncements_params)
    if @pronouncement.save
      render 'show'
    else
      render_unprocessable_entity(@pronouncement.full_errors) and return
    end
  end

  api :PUT, 'api/v2/pronouncements/:id', 'Update an existing announcement for given type and id'
  description <<-EOS
    Update existing announcement for a team/organization based id parameter.
    One of label, start_date, end_date or is_active is required.
  EOS

  param :id, Integer, desc: 'Announcement id to be updated', required: true
  param :pronouncement, Hash, required: true do
    param :label, String, desc: 'Announcement text with html if given'
    param :start_date, String, desc: 'Start date for announcement to show, format: mm/dd/yyyy hh:mm:ss in UTC'
    param :end_date, String, desc: 'End date upto which announcement to show. Default is null. Format: mm/dd/yyyy hh:mm:ss in UTC'
    param :is_active, String, desc: 'Announcement should be enabled or not. Default is true'
  end
  formats ['json']
  def update
    set_pronouncement_date_params
    @pronouncement = Pronouncement.find(params[:id])

    if @pronouncement.update_attributes(pronouncements_params)
      render 'show'
    else
      render_unprocessable_entity(@pronouncement.full_errors) and return
    end
  end

  api :DELETE, 'api/v2/pronouncements/:id', 'Delete announcement'
  param :id, Integer, desc: 'Announcement id to be deleted', required: true
  formats ['json']
  def destroy
    @pronouncement = Pronouncement.find(params[:id])
    if @pronouncement.destroy
      head :ok and return
    else
      render_unprocessable_entity(@pronouncement.full_errors) and return
    end
  end

  private

  def pronouncements_params
    params.require(:pronouncement).permit(:scope_id, :scope_type, :is_active, :start_date, :label, :end_date)
  end

  def find_pronounceable
    case pronouncements_params[:scope_type]
      when "Organization"
        @pronounceable = Organization.find(pronouncements_params[:scope_id])
      when "Team"
        @pronounceable = Team.find(pronouncements_params[:scope_id])
    end
  end

  def check_pronouncement_params
    if params[:pronouncement].blank?
      head :bad_request and return
    end
  end

  def set_pronouncement_date_params
    if pronouncements_params[:start_date].present? and validate_date(pronouncements_params[:start_date])
      params[:pronouncement][:start_date] = set_dates(pronouncements_params[:start_date])
    end

    if pronouncements_params[:end_date].present? and validate_date(pronouncements_params[:end_date])
      params[:pronouncement][:end_date] = set_dates(pronouncements_params[:end_date])
    end
  end

  def validate_date(date)
    begin
      DateTime.strptime(date, '%m/%d/%Y %H:%M:%S')
    rescue InvalidDate => e
      return false
    end
    return true
  end

  def set_dates date
    DateTime.strptime(date, '%m/%d/%Y %H:%M:%S').try(:utc)
  end
end

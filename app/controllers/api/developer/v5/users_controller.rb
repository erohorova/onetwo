class Api::Developer::V5::UsersController < Api::Developer::V5::BaseController

  resource_description do
    api_version 'Developer'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/developer/v5/users/clc', 'Get clc information for the user'
  formats ['json']
  def clc
    clc = Clc.active.get_clc_for_entity({entity_id: @current_user.organization_id, entity_type: "Organization"}).first
    clc_progress = ClcProgress.get_progress_for(clc.id, @current_user.id) 
    render json: { data: clc_progress } and return 
  end
end
class Api::Developer::V5::ProfilesController < Api::Developer::V5::BaseController
  include ProfileConcern

  before_action :set_profile, only: [:show, :update, :destroy]
  before_action :can_update, only: [:update]

  has_limit_offset_constraints only: [:index, :index_custom_fields]

  resource_description do
    api_version 'Developer'
    formats ['json']
    error 401, 'Unauthorized to access profile'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end
  def_param_group :profile do
    param :profile, Hash, required: true do
      param :email, String, desc: 'Email of the user', require: true
      param :first_name, String, desc: 'First name of the user', require: false
      param :last_name, String, desc: 'Last name of the user', require: false
      param :status, String, desc: 'Status (active or suspended) of the user', require: false
      param :uid, String, desc: 'User identifier of the profile', require: false
      param :roles, Array, desc: 'Role names for add to the user, default member', require: false
      param :language, String, desc: 'Preferred Language selected by user', require: false
      param :avatar, String, desc: 'Avatar image (aka profile image), supports image url', required: false
    end
  end

  api :GET, 'api/developer/v5/profiles/custom_fields', 'Returns list of custom fields attributes from org'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  def index_custom_fields
    custom_fields = current_org.custom_fields
    @custom_fields = custom_fields.limit(limit).offset(offset)
    @total = custom_fields.count
  end

  api :POST, 'api/developer/v5/profiles/custom_fields', 'Create custom field from org'
  param :name_fields, Array, desc: 'Name for custom fields display_name', require: true
  def create_custom_fields
    name_fields = params['name_fields'].reject(&:blank?)
    return render_unprocessable_entity('Missed required params') if name_fields.blank?
    @messages = []
    @created_custom_fields = []
    name_fields.each  do |name|
      custom_field = current_org.custom_fields.build(
        display_name: name,
        enable_people_search: true
      )
      if custom_field.save
        @created_custom_fields.push(custom_field)
      else
        @messages.push(custom_field.full_errors)
      end
    end

    render_with_status_and_message(@messages, status: :ok) if @messages.present?
  end

  api :GET, 'api/developer/v5/profiles', 'Returns list of profiles from org'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :order_label, String, desc: 'ASC or DESC', required: false
  param :sort_by, String, desc: 'Sort results. Default: created_at date. Example: created_at, first_name', required: false
  def index
    order_label = params[:order_label].presence || :asc
    sort = params[:sort_by] || :created_at
    profiles = current_org.profiles
    @total = profiles.count
    @profiles = profiles.joins(:user).limit(limit).offset(offset).order(sort => order_label )
  end

  api :GET, 'api/developer/v5/profiles/:id', 'Returns profile from org'
  param :id, Integer, desc: 'external_id or uid profile', required: true
  def show

  end

  api :POST, 'api/developer/v5/profiles', 'Creating profiles of one or many'
  param :profiles, Array, of: Hash, require: true do
    param :external_id, String, desc: 'external_id profile', require: true
    param :email, String, desc: 'Email of the user', require: true
    param :first_name, String, desc: 'First name of the user', require: false
    param :last_name, String, desc: 'Last name of the user', require: false
    param :status, String, desc: 'Status (active or suspended) of the user', require: false
    param :uid, String, desc: 'User identifier of the profile', require: false
    param :roles, Array, desc: 'Role names for add to the user, default member', require: false
    param :avatar, String, desc: 'Avatar image (aka profile image), supports image url', required: false
    param :job_title, String, desc: 'Job title of the user', required: false
  end
  def create
    unless params[:profiles].is_a? Array
      render_unprocessable_entity('Invalid data type for params profiles') and return
    end
    @all_messages = {}
    @created_profiles = []
    batch_initialize(profiles: params[:profiles])
  end

  api :PUT, 'api/developer/v5/profiles/:id', 'Update profile'
  param :id, Integer, desc: 'external_id or uid profile', required: true
  param :profile, Hash, required: true do
    param :external_id, String, desc: 'external_id profile', require: false
    param :email, String, desc: 'Email of the user', require: false
    param :first_name, String, desc: 'First name of the user', require: false
    param :last_name, String, desc: 'Last name of the user', require: false
    param :status, String, desc: 'Status (active or suspended) of the user', require: false
    param :roles, Array, desc: 'Role names for add to the user, default member', require: false
    param :avatar, String, desc: 'Avatar image (aka profile image), supports image url', required: false
    param :anonymize, String, desc: 'Anonymize suspended user', require: false
    param :job_title, String, desc: 'Job title of the user', required: false
  end
  def update
    user_params, custom_fields_params, user_profile_params = set_params(profile_params)
    @messages = []

    update_value_custom_fields(@user, custom_fields_params&.except("anonymize"))

    if should_anonymize?
      (user_params ||= {}).merge!(@user.anonymize_attributes)
    elsif profile_params[:anonymize].present?
      @messages.push("Cannot anonymize user")
    end

    if user_params.present?
      render_unprocessable_entity(@user.full_errors) and return unless @user.update(user_params)
    end

    if profile_params[:external_id].present?
      unless @profile.update(external_id: profile_params[:external_id])
        @messages.push(*@profile.full_errors)
      end
    end

    if profile_params[:roles].present?
      valid_roles = get_valid_roles(roles: profile_params[:roles])
      user_roles = @user.roles.pluck(:name)
      add_roles = valid_roles - user_roles
      remove_roles = user_roles - valid_roles
      { add_user: add_roles, remove_user: remove_roles }.each do |action, roles|
        next if roles.blank? || action.blank?
        add_or_remove_role(roles: roles, action: action, user: @user)
      end
    end

    if user_profile_params&.values&.any?(&:present?)
      if @user.profile.nil?
        user_profile = @user.build_profile(user_profile_params)
        @messages.push(*user_profile.full_errors) unless user_profile.save
      else
        unless @user.profile.update(user_profile_params)
          @messages.push(*user_profile.full_errors)
        end
      end
    end

    render 'show'
  end

  api :DELETE, 'api/developer/v5/profiles/:id', 'Destroy profile'
  param :id, Integer, desc: 'external_id or uid profile', required: true
  def destroy
    if @profile.destroy
      head :ok
    else
      render_unprocessable_entity(@profile.full_errors)
    end
  end

  private

  def can_update
    render_unprocessable_entity('Cannot update anonymized user') if @user.is_anonymized
  end

  def set_profile
    profiles = current_org.profiles
    @profile = profiles.where('uid = :id or external_id = :id', id: params[:id]).first
    if @profile.present?
      @user = @profile.user
    elsif profile_params.present? && profile_params[:email].present?
      @user = current_org.users.find_by(email: profile_params[:email])
      @profile = @user&.external_profile
    end
    return render_not_found('Profile not found') if @profile.blank?
  end

  def profile_params
    params.require(:profile)
  end

  def should_anonymize?
    @user.status == "suspended" && profile_params[:anonymize].present? && profile_params[:anonymize].to_bool
  end
end

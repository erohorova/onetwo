class Api::Developer::V5::AssignmentsController < Api::Developer::V5::BaseController
  include LimitOffsetConcern

  has_limit_offset_constraints only: [:index], limit_default: 10, offset_default: 0

  resource_description do
    api_version 'Developer'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/developer/v5/assignments', 'Get learning assignments for user'
  param :state, String,  desc: 'State of assignments. Possible values: started, completed, assigned, dismissed', required: false
  param :limit, Integer, desc: 'Limit, default to 10', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  formats ['json']
  def index
    state = params[:state].presence || 'assigned'
    unless Assignment::VALID_STATES.include?(state)
      render json: {error: 'state is invalid, valid states: started, completed, assigned'} and return
    end

    assignments = @current_user.assignments.filter_by_state(state).limit(limit).offset(offset)
    render json: { assignments: assignments } and return 
  end
end
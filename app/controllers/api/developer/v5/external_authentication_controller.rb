class Api::Developer::V5::ExternalAuthenticationController < Api::Developer::V5::BaseController

  skip_filter :authenticate_api_request!, only: :auth

  api :GET, 'api/developer/v5/auth', 'Get JWT token for API access'
  formats ['json']
  def auth
    api_key = request.headers['X-API-KEY']
    auth_token = request.headers['X-AUTH-TOKEN']

    return render_bad_request("Missing required params") unless api_key && auth_token

    authenticator = DeveloperApiAuthentication.new(api_key: api_key, auth_token: auth_token)
    @current_user, @current_org, jwt_payload, jwt_header = authenticator.authenticate_external_api_request

    if @current_user && (@current_org == Organization.find_by_request_host(request.host))
      @jwt_token = authenticator.generate_jwt
    else
      return render_unauthorized("Could not authenticate with API KEY and AUTH TOKEN")
    end

    render json: { jwt_token: @jwt_token }
  end
end

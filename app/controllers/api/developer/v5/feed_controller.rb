class Api::Developer::V5::FeedController < Api::Developer::V5::BaseController
  include LimitOffsetConcern

  has_limit_offset_constraints only: [:index], limit_default: 10, offset_default: 0

  resource_description do
    api_version 'Developer'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/developer/v5/feed', "Returns a user's feed"
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  error 500, 'Internal server error'
  formats ['json']
  def index
    items = UserContentsQueue.feed_content_for_v2(user: @current_user, limit: limit, offset: offset)
    render json: { items: items } and return 
  end
end
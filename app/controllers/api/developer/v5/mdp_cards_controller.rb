class Api::Developer::V5::MdpCardsController < Api::Developer::V5::BaseController

  has_limit_offset_constraints only: [:index], limit_default: 50, offset_default: 0, max_limit: 100

  resource_description do
    api_version 'Developer'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/developer/v5/mdp_cards', 'Get MDP card details for user'
  param :limit, Integer, desc: 'Limit, default to 50, Max 100', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :last_fetched_at, String, desc: 'Fetch MDP cards created/updated after last_fetched_at', required: false 
  formats ['json']
  def index
    user_completions = UserContentCompletion.includes(:user)
                .joins("INNER JOIN mdp_user_cards
                  ON mdp_user_cards.user_id = user_content_completions.user_id
                  AND mdp_user_cards.card_id = user_content_completions.completable_id")
                .where("user_content_completions.completable_type = 'Card'")

    if (last_fetch_date = Date.parse(params[:last_fetched_at]) rescue nil ).present?
      user_completions = user_completions.where('user_content_completions.updated_at > ?', last_fetch_date.beginning_of_day)
    end

    @user_completions = user_completions.limit(limit).offset(offset)
    @mdp_cards = MdpUserCard.where(user_id: @user_completions.map(&:user_id).uniq, 
                  card_id:  @user_completions.map(&:completable_id).uniq)

    render json: { responseData: generate_response(limit, offset) } and return
  end


  private

  def generate_response(limit, offset)
    response = {}
    response[:prevPageOffset] = (offset.zero? || (offset < limit)) ? nil : (offset - limit)
    response[:nextPageOffset] = @user_completions.length < limit ? nil : (offset + limit)

    response[:catalogData] = get_catalog_data
    response
  end

  def get_catalog_data
    data = []
    @user_completions.each do |user_completion|
      mdp_card = @mdp_cards.find do |mdp_card| 
        (mdp_card.card_id == user_completion.completable_id) && (mdp_card.user_id == user_completion.user_id)
      end

      next if mdp_card.blank?
      data << {
        cardID:               mdp_card.card_id,
        userEmail:            user_completion.user.email,
        status:               user_completion.state,
        formDetails: {
          sourceOfMDP:        mdp_card.form_details['source_of_mdp'],
          learningActivity:   mdp_card.form_details['learning_activity'],
          completionDate:     user_completion.completed_at,
          subCompetencyValue: mdp_card.form_details['sub_competency_value'],
          comments:           mdp_card.form_details['additional_comments']
        }
      }
    end
    data
  end
end
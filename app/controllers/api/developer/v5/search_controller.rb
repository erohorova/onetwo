class Api::Developer::V5::SearchController < Api::Developer::V5::BaseController
  include FollowableConcern, SearchConcern

  has_limit_offset_constraints only: [:index]

  api :GET, 'api/developer/v5/search', 'Search for content from ECL'
  param :q, String, desc: 'Query string', required: true
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :content_type, Array, desc: 'Filters search results to include these content types only. Some examples: pathway, poll, article, video, insight, video_stream, course.', required: false
  param :query_type, String, desc: "Search query type; default 'and', 'or' - union of all matched results. 'and' - fetch results which satisfies all applied conditions.", required: false
  formats ['json']
  def index
    render_unprocessable_entity('Query parameter q cannot be empty') and return if params[:q].nil?
    query_type = params[:query_type].presence || 'and'
    set_limit_offset(params)
    cards, aggs = ecl_content_search(search_params, query_type, [])
    render json: { cards: cards, aggregations: aggs } and return
  end

  private
    def search_params
      params.permit(:q, :content_type => [])
    end
end

class Api::Developer::V5::BaseController < Api::Developer::BaseController

  protected

  def authenticate_api_request!
    api_key = request.headers['X-API-KEY']
    jwt_token = request.headers['X-ACCESS-TOKEN']

    return render_bad_request("Missing required params") unless api_key && jwt_token

    authenticator = DeveloperApiAuthentication.new(api_key: api_key, jwt_token: jwt_token)
    @current_user, @current_org, @jwt_payload = authenticator.authenticate_external_api_request

    unless @current_user && (@current_org == Organization.find_by_request_host(request.host))
      return render_unauthorized("Could not authenticate with API KEY and ACCESS TOKEN")
    end
  end
end

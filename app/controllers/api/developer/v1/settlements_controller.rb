# coding: utf-8
class Api::Developer::V1::SettlementsController < Api::Developer::V1::BaseController

  before_filter :authenticate_settlement_request
  skip_filter :authenticate_api_request!, only: :transactions # authentication will be done using static token.
  has_limit_offset_constraints only: [:transactions]

  resource_description do
    api_version 'Developer'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, '/api/developer/v1/transactions', 'Get list of all the transactions'
  formats ['json']
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  def transactions
    @total_count = Transaction.where(order_type:"Card", payment_state: 1).count
    @transactions = Transaction.where(order_type:"Card", payment_state: 1).includes(order: :orderable).order(updated_at: :desc).limit(limit).offset(offset)
  end

  protected
  def authenticate_settlement_request
    shared_secret = Settings.settlements['shared_secret']
    token = request.headers['AUTH-TOKEN']
    payload = {"url" => "https://integrations.edcast.com", "date" => Date.today.to_s}
    jwt_payload, jwt_header = JWT.decode(token, shared_secret)

    return render_unauthorized unless (payload == jwt_payload)
  end
end

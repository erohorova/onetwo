class Api::Developer::V1::ChannelsController < Api::Developer::V1::BaseController

  include ChannelConcern
  include LimitOffsetConcern
  before_action :current_channel, only:[:show]
  before_action :set_logs_attrs

  resource_description do
    api_version 'Developer'
    formats ['json']
    error 401, 'Unauthorized to access channel'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/developer/v1/channels', 'Get channels with user following info. Default order- promoted DESC and Label ASC'
  param :q, String, :desc => 'Channel search term', :required => false
  def index
    channel_list
  end

  api :GET, 'api/developer/v1/channels/:id', 'Show channel'
  def show
  end

  private

  def set_logs_attrs
    @api_name = "#{params[:action]}_channel"
    @entity = "Channel"
  end
end  
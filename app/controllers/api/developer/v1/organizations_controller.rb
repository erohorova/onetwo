class Api::Developer::V1::OrganizationsController < Api::Developer::V1::BaseController
  before_filter :require_organization_admin, only: :lrs_credentials
 
  api :GET, 'api/developer/v1/organizations/lrs_credentials', 'Get xapi_credentials of LRS with DEV API'
  formats ['json']
  api_version 'Developer'

  def lrs_credentials
    @xapi_credential = current_org.xapi_credential
  end
end

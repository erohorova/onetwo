class Api::Developer::V1::CardsController < Api::Developer::V1::BaseController

  include CardConcern
  include LimitOffsetConcern

  has_limit_offset_constraints only: [:index], limit_default: 15, offset_default: 0
  before_action :set_logs_attrs

  resource_description do
    api_version 'Developer'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  #we are allowing only four filters for public API's - state, author_id, channel_id, card_type
  api :GET, 'api/developer/v1/cards', 'Returns list of cards from org'
  param :q, String, desc: 'Query string; default matches all', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :state, Array, desc: 'Array of card states to filter by. Default: ["published"]. Allowed values: new, published, archived, deleted', required: false
  param :author_id, Array, desc: 'Fetch cards created by users with these ids. Most common use case is to get cards created by a single user, in which case set params["author_id"] = [user_id]', required: false
  param :channel_id, Array, desc: 'Fetch cards posted to at least one of the channels identified with these ids. Most common use case is to get cards for a channel, in which case set params["channel_id"] = [channel_id]', required: false
  param :card_type, Array, desc: 'Array of card types. possible values: poll, media, pack Ex. card_type=[poll, pack]'
  param :load_topics, Array, desc: 'Load topics with card data, Ex. card_type=true/false'
  param :sort, String, desc: 'Sort results. Default: published_at date. Values: promoted_first, like_count, created, updated'
  def index
    params.slice!(:q, :offset, :limit, :state, :author_id, :channel_id, :card_type, :load_topics, :sort)
    get_cards
  end


  api :POST, 'api/developer/v1/cards', 'create smartbite/insight/card'
  param :message, String, :desc => "card message - can include links and mentions", :required => true
  param :channel_ids, Integer, :desc => "Array of channels to post the card to", :required => false
  param :resource_url, String, "Resource url", required: false
  param :topics, Array, desc: 'Array of related topics to the card. Topics are strings', required: false
  def create
    options = {inline_indexing: true, action: :create}
    card_attrs = prepare_card_params(card_params, options)
    @card = current_org.cards.build(card_attrs.merge!(author_id: current_user.id))

    unless @card.save
      return render_unprocessable_entity(@card.errors.full_messages.join(", "))
    end
    render 'show'
  end

  api :GET, 'api/developer/v1/cards/:id', 'Get info about card'
  param :id, Integer, "Card id", required: true
  def show
    find_published_card
  end

  api :PUT, 'api/developer/v1/cards/:id', 'update smartbite/insight/card'
  param :id, Integer, :desc => "Card id", required: true
  param :message, String, :desc => "card message - can include links and mentions", :required => true
  param :channel_ids, Integer, :desc => "Array of channels to post the card to. set to [] or nil to remove all", :required => false
  param :resource_url, String, :desc => "Resource id. Set to nil if the user removes the link from the card", required: false
  param :topics, Array, desc: 'Array of related topics to the card. Topics are strings', required: false
  def update
    if update_card(params)
      render 'show'
    else
      return render_unprocessable_entity(@card.errors.full_messages.join(", "))
    end
  end

  api :DELETE, 'api/developer/v1/cards/:id', "delete a card"
  param :id, Integer, :desc => "Card id", required: true
  def destroy
    if delete_card(params)
      head :ok and return
    else
      return render_unprocessable_entity(@card.errors.full_messages.join(", "))
    end
  end

  private
  def card_params
    params.require(:card).permit(:message, :resource_url, topics:[], channel_ids: [])
  end

  def set_logs_attrs
    @api_name = "#{params[:action]}_card"
    @entity = "Card"
  end
end

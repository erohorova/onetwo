class Api::Developer::V1::CommentsController < Api::Developer::V1::BaseController
  include CommentConcern
  has_limit_offset_constraints only: [:index]

  resource_description do
    api_version 'Developer'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/developer/v1/cards/:card_id/comments', 'Returns list of comments for a card'
  param :card_id, String, desc: 'Retrieve comments for card with this id', required: true
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :order, String, desc: 'ordering based on created_at, desc or asc', required: false
  def index
    @comments = []

    unless Card.is_ecl_id?(params[:card_id])
      order = params[:order].blank? ? :asc : params[:order]
      @comments = find_commentable.comments.order(created_at: order).limit(limit).offset(offset)
    end
  end

  api :GET, 'api/developer/v1/cards/:card_id/comments/:id', 'Returns data on a particular comment for a card'
  param :card_id, String, desc: 'Retrieve comments for card with this id', required: true
  param :id, Integer, desc: "Retrieve data for comment with this id", required: true
  def show
    show_comment
  end

  api :POST, 'api/developer/v1/cards/:card_id/comments', 'Post a comment on a card'
  param :card_id, String, desc: 'Post a comment for the card with this id', required: true
  param :comment, Hash, desc: 'Comment object' do
    param :message, String, :desc => "The comment text", :required => true
  end
  def create
    if create_comment
      track_comment
      render 'show' and return
    else
      render_unprocessable_entity(@comment.errors.full_messages.join(",")) and return
    end
  end

  api :DELETE, "api/developer/v1/cards/:card_id/comments/:id", "Delete a comment on a card"
  param :card_id, String, desc: 'Delete a comment on the card with this id', required: true
  param :id, Integer, desc: "Delete the comment with this id", required: true
  def destroy
    if delete_comment
      head :ok and return
    else
      render_unprocessable_entity(@comment.errors.full_messages.join(",")) and return
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:message)
  end
end
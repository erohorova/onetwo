class Api::Developer::V1::EventsController < Api::Developer::V1::BaseController

  has_limit_offset_constraints only: [:index], limit_default: 200_000, offset_default: 0

  api :GET, 'api/developer/v1/events', 'Get raw event data from influx'
  param :entity, Array, desc: <<-TXT, required: true
    A valid entity. values:
      users,
      cards,
      groups,
      channels,
      searches,
      organizations,
      org_scores_daily,
      user_scores,
      user_scores_daily,
      group_scores_daily,
      channel_aggregation_daily,
      group_user_scores_daily,
      content_engagement_metrics,
      xapi_credentials,
      integrations
  TXT
  param :start_date, String, desc: "Start or from date for events to pull from.
    Format: epoch timestamp", required: true
  param :end_date, String, desc: "end or till date for events to pull. default: none", required: false
  param :group_by, String, desc: "comma separate attributes to group by", required: false
  param :limit, String, desc: "limit, defaults to 10,000", required: false
  param :offset, String, desc: "offset, defaults to 0", required: false
  param :card_id, String, desc: "Get events for matching card id.", required: false
  param :user_id, String, desc: "Get events for matching user id.", required: false
  param :channel_id, String, desc: "Get events for matching channel id.", required: false
  param :event_name, String, desc: "Get events for matching event.", required: false
  formats ['json']
  api_version 'Developer'
  def index
    error_status, error_msg = sanitize_params
    if error_status
      render_with_status_and_message(error_msg, status: error_status)
      return
    end
    entity = params[:entity]
    query, fetch_params = InfluxQuery.get_query_for_entity(
      entity: entity,
      organization_id: current_org.id,
      filters: permitted_params.merge(limit: limit, offset: offset)
    )
    results = InfluxQuery.fetch(query, fetch_params)
    results&.each do |result|
      # Remove nulls from tags and values
      result["tags"]&.compact!
      result["values"]&.each(&:compact!)
      # Return the resuls as JSON
    end
    render json: results || []
  rescue InfluxQuery::InfluxQueryError => e
    render_unprocessable_entity(e.message)
  end

  private

  def permitted_params
    params.permit(*%i{
      start_date end_date group_by card_id user_id channel_id group_id
      limit offset event_name
    }).tap do |params|
      params[:event] = params.delete(:event_name) if params[:event_name]
    end
  end

  def sanitize_params
    # Returns the first of the results that is truthy, or nil if all are valid.
    # If a truthy result is returned, there is a problem with the given params.
    [
      sanitize_dates,
      sanitize_group_by,
      sanitize_event
    ].find &:itself
  end

  def sanitize_dates
    # Dates are passed as Unix timestamps, so to_i can be used to sanitize.
    # If the original passed value is incorrect, no error will be raised,
    # and a numeric value of 0 will be used instead.
    %i{start_date end_date}.each do |key|
      if params.key? key
        params[key] = params[key].to_i
      end
    end
    nil
  end

  def sanitize_group_by
    # An expected group_by is a comma-separated list of values
    # which are each contained in a predetermined enum.
    # Unless this is the case, a 422 is returned
    if params[:group_by]
      invalids = params[:group_by]
                .gsub(" ", "")
                .split(",")
                .reject { |val| val.in? %w{event user_id} }
      unless invalids.empty?
        return :unprocessable_entity, "given group_by param is invalid"
      end
    end
    nil
  end

  def sanitize_event
    return unless params[:event_name]
    if !Analytics::MetricsRecorder.get_recorder_for_event(params[:event_name])
      return :unprocessable_entity, "given event_name doesn't exist"
    end
  end

end

class Api::Developer::V1::UsersController < Api::Developer::V1::BaseController

  skip_filter :authenticate_api_request!, only: :auth


  resource_description do
    api_version 'Developer'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/developer/v1/auth', 'Get user info and JWT token for API access'
  formats ['json']
  def auth
    @api_name = "authentication"
    @entity = "User"
    api_key = request.headers['X-DEVELOPER-API-KEY']
    auth_token = request.headers['X-DEVELOPER-AUTH-TOKEN']

    if api_key && auth_token
      authenticator = DeveloperApiAuthentication.new(api_key: api_key, auth_token: auth_token)
      @current_user, @current_org, jwt_payload, jwt_header = authenticator.authenticate_jwt_request
    end

    if @current_user && (@current_org == Organization.find_by_request_host(request.host))
      @jwt_token = authenticator.generate_jwt
    else
      return render_unauthorized("Request is invalid")
    end
  end


  api :POST, '/api/developer/v1/users', 'Bulk create users'
  param :payload, Hash, required: true do
    param :users, Array, required: true
  end
  formats ['json']

  def create
    render json: { message: "Invalid user data." }, status: :unprocessable_entity and return if params[:payload][:users].blank?
    csv_string = convert_to_csv(params[:payload][:users])
    invitation_file = InvitationFile.new(sender_id: @current_user.id,
                                         invitable: @current_org,
                                         data: csv_string,
                                         roles: "member",
                                         skip_processing_invitations: true)
    if invitation_file.save
      BulkImportUsersJob.perform_later(
      file_id: invitation_file.id,
      options: {},
      admin_id: @current_user.id)
      render json: { status: :ok, message: "Users data successfully uploaded" } and return
    end
  end

  api :POST, '/api/developer/v1/users/suspend', 'Suspend users'
  param :payload, Hash, required: true do
    param :users, Array, required: true
  end
  formats ['json']

  def suspend
    if params[:payload].blank? || params[:payload][:users].blank?
      render json: { message: "Invalid user data." }, status: :unprocessable_entity and return 
    end
    users_payload = params[:payload][:users]
    users_payload.each do |user_payload|
      user = User.find_by(email: user_payload[:email], organization_id: @current_org.id)
      if user && !user.is_suspended?
        user.suspend!
        user.update_attribute(:status, User::SUSPENDED_STATUS)
      end
    end  
    render json: { status: :ok, message: "User suspended successfully." } and return
  end

  protected

  def convert_to_csv(users)
    custom_fields = @current_org.custom_fields
    abbreviations = custom_fields.pluck(:abbreviation)
    csv_headers = %w(first_name last_name email groups picture_url password) + abbreviations
    csv_string = CSV.generate do |csv|
      csv << users.first.slice(*csv_headers).keys
      users.each do |user|
        csv << user.slice(*csv_headers).values
      end
    end
    csv_string
  end

end
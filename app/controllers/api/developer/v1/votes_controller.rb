class Api::Developer::V1::VotesController < Api::Developer::V1::BaseController
  include VotesConcern
  
  before_filter :find_votable

  resource_description do
    api_version 'Developer'
    formats ['json']
    error 401, 'Unauthorized to access channel'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :POST, 'api/developer/v1/votes', 'Create an upvote for the currently logged in user'
  param :vote, Hash, desc: 'Vote hash' do
    param :content_id, Integer, desc: "content id", required: true
    param :content_type, String, desc: "Content type. Allowed content types: Card, Comment", required: true
  end
  def create
    if create_vote
      track_vote
      head :ok and return
    else
      render_unprocessable_entity(@vote.errors.full_messages.join(",")) and return
    end
  end

  api :DELETE, 'api/developer/v1/votes', 'Delete an upvote for the currently logged in user'
  param :vote, Hash, desc: 'Vote hash' do
    param :content_id, Integer, desc: "content id", required: true
    param :content_type, String, desc: "Content type. Allowed content types: Card, Comment", required: true
  end
  def destroy
    if delete_vote
      head :ok and return
    else
      render_unprocessable_entity(vote.errors.full_messages.join(",")) and return
    end
  end
end
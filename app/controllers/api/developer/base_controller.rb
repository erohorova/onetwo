require 'developer_api_authentication'
class Api::Developer::BaseController < ActionController::Base
  respond_to :json

  resource_description do
    api_version 'Developer'
  end

  include NativeAppDetection
  include DetectPlatform
  include LimitOffsetConcern
  include CompoundIdReader
  include RequestHelper
  include ApplicationConcern


  skip_before_action :verify_authenticity_token # No need to check for CSRF token for json apis
  protect_from_forgery with: :null_session
  before_action :authenticate_api_request!
  before_action :set_request_store
  before_filter :set_cache_headers

  protected

  def current_org
    @current_org
  end

  def current_user
    @current_user
  end

  def authenticate_api_request!
    api_key = request.headers['X-DEVELOPER-API-KEY']
    jwt_token = request.headers['X-JWT-TOKEN']
    if(api_key && jwt_token)
      @current_user, @current_org, @jwt_payload = DeveloperApiAuthentication.new(
        api_key: api_key, jwt_token: jwt_token
      ).authenticate_api_request
    end

    unless @current_user && (@current_org == Organization.find_by_request_host(request.host))
      return render_unauthorized
    end
  end

  def set_request_store
    payload = {
        user_id:        @current_user&.id,
        platform:       get_platform,
        user_agent:     request.headers.env['HTTP_USER_AGENT'],
        platform_version_number: request.headers.env['HTTP_X_VERSION_NUMBER'],
        is_admin_request: ApplicationController.is_admin_request?(request)
    }

    RequestStore.store[:request_metadata] = payload
  end

  rescue_from(JWT::DecodeError) do |exception|
    message = exception.is_a?(JWT::ExpiredSignature) ? "JWT token has expired" : "Token is invalid"
    render_bad_request(message)
  end

  rescue_from(ActionController::ParameterMissing) do |exception|
    render_bad_request(exception.message)
  end

  rescue_from(ActiveRecord::RecordNotFound) do |exception|
    render_not_found("Record not found")
  end
end

class Api::TeamNotificationsController < ApiController
  include OrganizationContext

  def create
    if current_org.is_admin?(current_user)
      @team = current_org.teams.excluding_everyone_team.find(params[:team_id])
      mobile_notification if params[:mobile_notification]
      email_notification if params[:email_notification]
      head :ok
    else
      head :unauthorized
    end
  end

  private

  def mobile_notification
    @team.users.each do |user|
      opts = {
        host_name: current_org.host,
        user_id: user.id,
        notification_type: 'AdminGenerated'
      }.merge!(notification_params.symbolize_keys)

      PubnubNotifier.notify(opts)
    end
  end

  def email_notification
    TeamUsersMailerJob.perform_later({
      message: notification_params[:content],
      team_id: @team.id,
      sender_id: current_user.id
    })
  end

  def notification_params
    params.require(:notification).permit(:content, :deep_link_id, :deep_link_type)
  end

end

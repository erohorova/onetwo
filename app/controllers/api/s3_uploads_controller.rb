class Api::S3UploadsController < ApiController

  # API to get presigned post objects to be able to upload to an S3 bucket
  # Browsers use this API to get data needed for image upload forms

  api :GET, "api/s3_uploads/configuration", "Get Configuration for uploading a file to S3"
  formats ['json']
  def configuration
    # All static right now. Can be extended later if required for tighter 
    # control on access policy and content type
    image_upload = AWS_S3_TEMP_UPLOAD_BUCKET
      .presigned_post(
        key:                   "organization_#{current_user.organization.id}/temp_uploads/#{SecureRandom.uuid}/${filename}",
        success_action_status: 201,
        acl:                   :public_read )
      .where(:content_type)
      .starts_with('')
    render json: {fields: image_upload.fields, url: image_upload.url}, status: :ok and return
  end

  api :POST, 'api/s3_bucket/get_content'
  description <<-EOS
    Get public_url of image objects from S3 bucket
    by provided `link` or `bucket_info` parameters
  EOS
  formats ['json']
  param :link, String, desc: 'Link to the S3 bucket', required: false
  param :bucket_info, Hash, desc: 'Bucket information' do
    param :bucket_name, String, desc: 'Bucket name', required: true
    param :bucket_region, String, desc: 'Bucket region', required: false
  end
  def get_content
    if [params[:link], params[:bucket_info]].all?(&:blank?)
      return render json: {message: 'At least one parameter should be present'},
             status: :unprocessable_entity
    end
    bucket_info = params[:bucket_info]

    bucket_data = if bucket_info
      {bucket: bucket_info[:bucket_name], region: bucket_info[:bucket_region]}
    else
      S3ObjectExtractorService.extract_bucket_info(link: params[:link])
    end

    if bucket_data[:bucket].blank?
      return render json: {message: 'wrong bucket name'},
             status: :unprocessable_entity
    end

    obj = S3ObjectExtractorService.new(bucket_data)
    data = obj.extract_image_objects
    if obj.failures.has_key?(:aws_access)
      return render json: {
        message: obj.failures[:aws_access]
      }, status: :unprocessable_entity
    end
    data = data.map { |url| URI.escape(url)} if data.present?
    fails = obj.failures

    render json: {
      image_links: data,
      error_objects: fails[:object_errors]
    }, status: :ok
  end
end

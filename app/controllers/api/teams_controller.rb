#deprecated
class Api::TeamsController < ApiController
  include ContentTypeFilter
  include LimitOffsetConcern
  include SortOrderExtractor
  include OrganizationContext
  include CardSearchHelpers
  include TeamsConcern
  include TrackActions
  include CompoundIdReader # Till we start using api/v2/index, need to support ECL VCs.

  has_limit_offset_constraints only: [:team_users, :assignments, :index]

  before_action :set_team, except: [:index, :create]
  before_action :authenticate_admin!, only: %i[user_action update destroy]

  api :POST, 'api/teams', 'Creates a team'
  param :team, Hash, desc: 'Param description for all methods' do
    param :title, String, desc: 'Title', required: true
    param :description, String, desc: 'Description', required: true
    param :image, String, desc: 'Image', required: false
  end
  formats ['json']
  def create
    @team = Team.new(team_params.merge(organization: current_org))
    if @team.save
      @team.add_admin current_user
      track_action(@team, 'create')
      render 'show'
    else
      render json: { error: "Error creating team: #{@team.full_errors}" }, status: :unprocessable_entity and return
    end
  end

  api :DELETE, 'api/teams/:id', 'Destroys a team'
  param :id, String, desc: 'Id of team', required: true
  def destroy
    if @team.is_deletable? && @team.destroy
      head :no_content
    else
      render json: { error: "Error updating team: #{@team.full_errors}" }, status: :unprocessable_entity and return
    end
  end

  api :GET, 'api/teams', 'Return array of groups that user is part of'
  param :org, String, desc: 'true to search entire org', required: false
  param :role, String, desc: 'one of admin or member', required: false
  param :assign_id, Integer, desc: 'Content id (card id, course id)', required: false
  param :assign_type, Integer, desc: 'Content type (card, course)', required: false
  param :search_term, String, desc: 'Filter teams by this search term', required: false
  param :sort, String, "Attribute name to sort by, ex title, created_at, default: created_at"
  param :order, String, "Sort order, asc or desc, default: desc"
  def index
    filtered_query = nil
    order_attr, order = extract_sort_order(valid_order_attrs: Team.column_names, default_order_attr: "created_at",
      order: params[:order], order_attr: params[:sort])
    sort_query = "teams.#{order_attr} #{order}"
    # if org is passed as true, get all teams from the org
    if params[:org] && params[:org].to_bool
      if !current_user.is_admin_user?
        render json: {error: 'Unable to access teams'}, status: :unauthorized and return
      else
        filtered_query = current_org.teams.
          joins("LEFT OUTER JOIN teams_users ON teams_users.id = teams.id").
          order(sort_query)
      end
    else
      filtered_query = current_user.teams.joins(:teams_users)

      if params[:role].present? && User::ROLES.include?(params[:role])
        filtered_query = filtered_query.where('teams_users.as_type = ?', params[:role])
      end

      unless params[:search_term].blank?
        filtered_query = filtered_query.where("teams.name like ?", "%#{params[:search_term]}%")
      end
    end

    @teams = filtered_query.limit(limit).offset(offset).uniq.
      select('teams.*, teams_users.as_type').order(sort_query)
    load_assignable(params)

    if @assignable
      fetch_assigned_groups
      fetch_unassigned_members_count teams: @teams
    end
  end

  api :GET, 'api/teams/:id', 'Get team details'
  param :assign_id, Integer, desc: 'Content id (card id, course id)', required: false
  param :assign_type, Integer, desc: 'Content type (card, course)', required: false
  def show
    load_assignable(params)
    if @assignable
      @assigned_team_ids = @team.assignments.where('assignments.assignable_id = ? AND assignable_type = ?', @assignable.id, @assignable.class.name).count > 0 ? [@team.id] : []
      fetch_unassigned_members_count teams: [@team]
    end
  end

  api :PATCH, 'api/teams/:id/users/:user_id/action', 'Promote or demote a team user'
  api :DELETE, 'teams/:id/users/:user_id', 'Remove user from a team'
  param :id, Integer, desc: "Team id", required: true
  param :user_id, Integer, desc: "User id", required: true
  error 422, 'response {"error": "error message"}'
  formats ['json']
  def user_action
    operation = {
        PATCH: :switch_role,
        DELETE: :remove,
    }[request.method.to_sym]
    result, error = @team.modify_user(params[:user_id], op: operation)
    if result
      head :no_content
    else
      render json: {error: USER_ERRORS[error]}, status: :unprocessable_entity
    end
  end

  api :GET, 'api/teams/:id/users', 'List/search group members'
  param :id, Integer, desc: 'Team id', required: true
  param :q, String, desc: 'Query string; default matches all', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :role, Integer, desc: 'Filter results by Pending, admin, member', required: false
  formats ['json']
  def team_users
    @query = params[:q]
    unless params[:role].blank?
      search_pending_team_members(q: params[:q], offset: offset, limit: limit) if params[:role] == 'pending'
      search_team_members(q: params[:q], offset: offset, limit: limit, filter: {role: params[:role]}) if ['member', 'admin'].include?(params[:role])
    else
      # Search across both pending and active members
      search_pending_team_members(q: params[:q], offset: offset, limit: limit)
      search_team_members(q: params[:q], offset: offset, limit: limit, filter: {})
    end
  end

  api :POST, 'api/teams/:id/follow_all', 'Follow all users of a group'
  param :id, Integer, desc: 'Team id', required: true
  formats ['json']
  def follow_all
    opts = {platform: get_platform, device_id: cookies[:_d]}
    current_user.follow_all_team_users(@team, opts)
    head :ok
  end

  # Same API to get more assignments and search assignments in an Org/current org
  api :GET, 'api/teams/:id/assignments', 'search org assignments and get more assignments'
  param :id, Integer, desc: "Team id", required: true
  param :term, String, desc: "Search term", required: false
  param :limit, Integer, desc: "Page results count", required: false
  param :offset, Integer, desc: "Result offset", required: false
  def assignments
    pars = params.permit(:term, :limit, :offset)

    # search term length check
    if pars[:term].present?
      term = pars[:term].length > 256 ? pars[:term][0..255] : pars[:term]
    end

    params[:ct] ||= 'collections'
    filter_params = get_content_type_filters(params[:ct])
    fetch_cards_in_current_org(term, viewer: current_user, filter_params: filter_params, limit: limit, offset: offset)

    if @team && !@cards.blank?
      assignable_ids = @team.assignments.where(assignable_id: @cards.map {|c| c['id']}, assignable_type: 'Card').pluck(:assignable_id).uniq
      @cards.each do |card|
        card["is_assigned"] = assignable_ids.include?(card['id'])
      end
    end
  end

  api :PUT, 'api/teams/:id', 'Updates a team'
  param :id, String, desc: 'Id of team', required: true
  param :team, Hash, desc: 'Param description for all methods' do
    param :title, String, desc: 'Title', required: true
    param :description, String, desc: 'Description', required: true
    param :image, String, desc: 'Image', required: false
  end
  formats ['json']
  def update
    if @team.update_attributes(team_params)
      render 'show'
    else
      render json: { error: "Error updating team: #{@team.full_errors}" }, status: :unprocessable_entity
    end
  end

  private

  def search_team_members(q: , offset: , limit: , filter: {})
    filter.merge! scope: @team
    @response = Search::UserSearch.new.search(q: params[:q], viewer: current_user, offset: offset, limit: limit, allow_blank_q: true, filter: filter)

    @users = @response.results
    @total = @response.total

    set_member_role
    set_followed_user_info
  end

  def search_pending_team_members(q: , offset: , limit: )
    @invitations_total = Invitation.where(invitable: @team).pending.where('recipient_email like ?', "%#{q}%").count
    @invitations = Invitation.where(invitable: @team).pending.where('recipient_email like ?', "%#{q}%").limit(limit).offset(offset).order(id: :desc)
  end

  def set_member_role
    @users.each do |user|
      user.role = user.teams.find{|t| t.id.to_i == @team.id}.try(:role)
      # set whether user's invitation is pending
      user.pending = false
    end
  end

  def team_params
    params
      .require(:team)
      .permit(
        :description,
        :image,
        :name )
  end

  # Check whether user is following team user or not
  def set_followed_user_info
    followed_user_ids = current_user.followed_user_ids

    @users.each do |u|
      u['is_followed'] = followed_user_ids.include?(u.id)
      u['is_current_user'] = current_user.id == u.id
    end
  end

  USER_ERRORS = {
      not_in_team: 'User is not in the team.',
      sole_admin: 'The only leader of a team cannot be removed.',
      nil => 'An error occurred.'
  }
end

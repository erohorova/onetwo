class ErrorsController < ApplicationController

  skip_before_filter :authenticate_user!
  layout 'errors'

  def not_found
    render status: :not_found
  end

  def forbidden
    render status: :forbidden
  end

  def internal_error
    render status: :internal_server_error
  end

  def unauthorized
    render status: :unauthorized
  end

end

# DYNAMIC ATTRIBUTES
#   ...

# EXCLUDING ATTRIBUTES:
#   ...

# DEFAULT FIELDS:
# 1  :id
# 2  :name
# 3  :status
# 4  :isEnabled
# 5  :wfType
# 6  :wfInputSource
# 7  :wfActionableColumns
# 8  :wfRules
# 9  :wfAction
# 10 :wfOutputResult
# 11 :createdAt
# 12 :creator

class WorkflowBridge < Bridges
  def initialize(workflows, user: nil, fields: '')
    super(workflows, user: user, fields: fields)

    @creators = {}
  end

  def attributes
    details = []

    workflow_ids = @objects.map(&:id).compact

    creator_ids = @objects.map(&:user_id).compact

    if @fields.empty?
      creators    = @organization.users.where(id: creator_ids)
      @creators   = UserBridge.new(creators, user: @user, fields: 'id,handle,full_name').attributes
      @creators   = Hash[@creators.map { |author| [author[:id], author] }]
    end

    # Start generating array of hashes that corresponds to list of workflows.
    @objects.each do |workflow|
      workflow_hash = {}
      if @fields.empty?
        workflow_hash.merge!(self.default_fields(workflow))
      else
        @fields.each do |field|
          authorized?(field) && workflow_hash.merge!(field.to_camelcase_sym => self.send(field, workflow))
        end
      end
      details << workflow_hash
    end

    details
  end

  def default_fields(workflow)
    {
      id: workflow.id,
      name: workflow.name,
      status: workflow.status,
      isEnabled: workflow.is_enabled,
      wfType: workflow.wf_type,
      wfInputSource: workflow.wf_input_source,
      wfActionableColumns: workflow.wf_actionable_columns,
      wfRules: workflow.wf_rules,
      wfAction: workflow.wf_action,
      wfOutputResult: workflow.wf_output_result,
      createdAt: workflow.created_at,
      creator: @creators[workflow.user_id]
    }
  end

  # DESC:
  #   This is for security reasons. Ensures none of the keys unless listed here must be transmitted.
  #   We do not want all of the methods/attributes defined in this class should be available.
  #   This will prevent the attackers from exploiting the API.
  def whitelisted_keys
    [
      :id, :name, :status, :is_enabled, :wf_type, :wf_input_source, :wf_actionable_columns,
      :wf_rules, :wf_action, :wf_output_result, :created_at, :creator
    ]
  end

  #On-demand
  def id(workflow)
    workflow.id
  end

  def name(workflow)
    workflow.name
  end

  def status(workflow)
    workflow.status
  end

  def is_enabled(workflow)
    workflow.is_enabled
  end

  def wf_type(workflow)
    workflow.wf_type
  end

  def wf_input_source(workflow)
    workflow.wf_input_source
  end

  def wf_actionable_columns(workflow)
    workflow.wf_actionable_columns
  end

  def wf_rules(workflow)
    workflow.wf_rules
  end

  def wf_action(workflow)
    workflow.wf_action
  end

  def wf_output_result(workflow)
    workflow.wf_output_result
  end

  def created_at(workflow)
    workflow.created_at
  end

  def creator(workflow)
    @creators[workflow.user_id]
  end
end
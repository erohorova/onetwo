# DYNAMIC ATTRIBUTES

# DEFAULT FIELDS:
# 1. id
# 2. name

class TagBridge < Bridges
  def attributes
    details = []

    # Start generating array of hashes that corresponds to list of tags.
    @objects.each do |tag|
      tag_hash = {}
      if @fields.empty?
        tag_hash.merge!(self.default_fields(tag))
      else
        @fields.each do |field|
          authorized?(field) && tag_hash.merge!(field.to_camelcase_sym => self.send(field, tag))
        end
      end

      details << tag_hash
    end

    details
  end

  def default_fields(tag)
    {
      id: tag.id,
      name: tag.name
    }
  end

  # DESC:
  #   This is for security reasons. Ensures none of the keys unless listed here must be transmitted.
  #   We do not want all of the methods/attributes defined in this class should be available.
  #   This will prevent the attackers from exploiting the API.
  def whitelisted_keys
    [
      :id, :name
    ]
  end

  private
    #### DB queries: Starts here ####
    # ...
    #### DB queries: Ends here ####

    #### On-demand methods: Starts here ####
    def id(tag)
      tag.id
    end

    def name(tag)
      tag.name
    end
    #### On-demand methods: Ends here ####
end
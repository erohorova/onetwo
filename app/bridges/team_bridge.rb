# DYNAMIC ATTRIBUTES:
# N/A

# EXCLUDING ATTRIBUTES:
# 1. roles
# 2. organizationId
# 4. createdBy
# 5. pendingMembersCount
# 6. autoAssignContent

# DEFAULT FIELDS:
# 1. id
# 2. name
# 3. description
# 4. slug
# 5. imageUrls (small, medium, large)
# 6. membersCount
# 7. isMember
# 8. isTeamAdmin
# 9. isTeamSubAdmin
# 10. isPrivate
# 11. isPending
# 12. isEveryoneTeam
# 13. onlyAdminCanPost
# Refer method #default_fields.

class TeamBridge < Bridges
  def initialize(teams, user: nil, fields: '')
    super(teams, user: user, fields: fields)

    @all_active_users = {}
    @all_user_roles   = {}
    @all_pending_invites = []
    @following_channel_ids = {}
    @team_admin_names = {}
  end

  def attributes
    details = []

    team_ids  = @objects.map(&:id).compact

    if @fields.empty? || @fields.include?('members_count')
      _active_users = all_active_users(team_ids)
      @all_active_users = Hash[_active_users.collect { |team_users| [team_users['team_id'], team_users['total_active_users']] }]
    end

    if @fields.empty?
      _existing_roles = existing_roles(team_ids)
      @all_user_roles = Hash[_existing_roles.map { |team_id, team_users| [team_id, team_users.map(&:as_type)] }]
    end

    if @fields.empty?
      @all_pending_invites = all_pending_invitations(team_ids)
    end

    if @fields.include?('channel_ids')
      team_channel_follows = TeamsChannelsFollow.where(team_id: team_ids).select(:channel_id, :team_id).group_by(&:team_id)
      team_channel_follows.each do |team_id, team_channel_follows|
        @following_channel_ids[team_id] = team_channel_follows.map(&:channel_id)
      end
    end

    if @fields.empty? || @fields.include?('created_by')
      @team_admin_names = admin_user_names(team_ids)
    end

    # Start generating array of hashes that corresponds to list of teams.
    @objects.each do |team|
      team_hash = {}
      if @fields.empty?
        team_hash.merge!(self.default_fields(team))
      else
        @fields.each do |field|
          authorized?(field) && team_hash.merge!(field.to_camelcase_sym => self.send(field, team))
        end
      end

      details << team_hash
    end

    details
  end

  def default_fields(team)
    current_team_roles = @all_user_roles[team.id] || []

    {
      id: team.id,
      name: team.name,
      description: team.description,
      slug: team.slug,
      imageUrls: {
        small:  team.image.url(:small),
        medium: team.image.url(:medium),
        large:  team.image.url(:large)
      },
      isEveryoneTeam: team.is_everyone_team,
      membersCount: members_count(team),
      isPrivate: team.is_private,
      isMandatory: team.is_mandatory,
      isTeamAdmin: current_team_roles.include?('admin'),
      isTeamSubAdmin: current_team_roles.include?('sub_admin'),
      isMember: current_team_roles.include?('member'),
      isPending: @all_pending_invites.include?(team.id).try(:to_bool),
      onlyAdminCanPost: team.only_admin_can_post,
      createdBy: @team_admin_names.try(:[], team.id)
    }
  end

  # DESC:
  #   This is for security reasons. Ensures none of the keys unless listed here must be transmitted.
  #   We do not want all of the methods/attributes defined in this class should be available.
  #   This will prevent the attackers from exploiting the API.
  def whitelisted_keys
    [
      :channel_ids, :description, :id, :is_private,
      :is_dynamic, :label, :members_count, :name, :slug
    ]
  end

  private
    #### DB queries: Starts here ####
    # DESC:
    #   1. Retrieve users for given team IDs.
    #   2. Only non-suspended users.
    #   3. Presence of email address.
    #   4. unique users in all.
    def admin_user_names(team_ids)
      TeamsUser.joins(:user).where("team_id in (?) and as_type = ?", team_ids, 'admin').each do |team_user|
        @team_admin_names[team_user.team_id] = team_user.user.name
      end
      @team_admin_names
    end

    def all_active_users(team_ids)
      User
        .where(organization_id: @organization.id)
        .joins(:teams_users)
        .where("teams_users.team_id in (?)", team_ids)
        .visible_members
        .uniq
        .group("teams_users.team_id")
        .select("teams_users.team_id, COUNT(DISTINCT(users.id)) as total_active_users")
    end

    # DESC: Retrieve all pending invitations of a given user matching team IDs.
    def all_pending_invitations(team_ids)
      Invitation
        .where(invitable_id: team_ids, invitable_type: 'Team')
        .where(recipient_id: @user.id)
        .pending
        .pluck('invitations.invitable_id')
    end

    # DESC: Retrieve user roles for matching team IDs.
    def existing_roles(team_ids)
      @user
        .teams_users
        .where(team_id: team_ids)
        .group_by(&:team_id)
    end
    #### DB queries: Ends here ####

    #### On-demand methods: Starts here ####
    def channel_ids(team)
      @following_channel_ids[team.id].presence || []
    end

    def description(team)
      team.description
    end

    def id(team)
      team.id
    end

    def is_private(team)
      team.is_private
    end

    def is_dynamic(team)
      team.is_dynamic
    end

    def label(team)
      team.name
    end
    alias_method :name, :label

    def members_count(team)
      @all_active_users[team.id]
    end

    def slug(team)
      team.slug
    end
    #### On-demand methods: Ends here ####
end

# Journey bridge will only give info about journey card i.e. cover card and
# info about pathways in that journey. EXCLUDING PACK CARDS INFO.
class JourneyBridge < Bridges
  include Api::V2::CardsHelper

  attr_accessor :cards

  def initialize(cards, user: nil, fields: '', options: {})
    super(cards, user: user, fields: fields)
    @pathway_relations_data = []
    @pathway_start_dates = {}
  end

  def attributes
    details = []

    cover_id = @objects.map(&:id).first
    ordered_journey_pack_rel = JourneyPackRelation.journey_relations(cover_id: cover_id).reject { |jr_rel| jr_rel.from_id == cover_id }

    if ordered_journey_pack_rel.present?
      @pathway_start_dates = ordered_journey_pack_rel.map { |pathway| [pathway.from_id, pathway.start_date] }.to_h

      @pathway_relations_data = Card.where(id: @pathway_start_dates.keys, organization_id: @organization.id)
      
      unless (@user.is_org_admin? || @pathway_relations_data.length == 0)
        @visible_pathway_relations = @pathway_relations_data.visible_to_userV2(@user).pluck(:id) 
      end
      @pathway_relations_data = @pathway_relations_data.order("FIELD(cards.id, #{@pathway_start_dates.keys.join(',')})")
    end

    # Start generating array of hashes that corresponds to list of assignments.
    @objects.each do |card|
      card_hash = {}
      if @fields.empty?
        card_hash.merge!(self.default_fields(card))
      else
        @fields.each do |field|
          authorized?(field) && card_hash.merge!(field.to_camelcase_sym => self.send(field, card))
        end
      end

      details << card_hash
    end

    details
  end

  def default_fields(card)
    _card_id = id(card)

    _fields = {
      journeySection: journey_section_details(card)
    }
    _fields
  end

  # DESC:
  #   This is for security reasons. Ensures none of the keys unless listed here must be transmitted.
  #   We do not want all of the methods/attributes defined in this class should be available.
  #   This will prevent the attackers from exploiting the API.
  def whitelisted_keys
    [
      :id
    ]
  end

  private
    def journey_section_details(card)
      _options = []
      _all_card_pack_ids = CardPackRelation.where(cover_id: @pathway_relations_data.map(&:id)).not_deleted.pluck(:cover_id,:from_id).each_with_object({}){|a, h| (h[a.first]||=[] )<< a.last }
      @pathway_relations_data.each do |pathway|
        options = {}
        options = {
          block_message: pathway.message,
          block_title: pathway.title,
          id: pathway.id,
          completed_percentage: pathway.pathway_completed_percent(@user, true),
          hidden: pathway.hidden,
          start_date: @pathway_start_dates[pathway.id],
          state: pathway.state,
          card_pack_ids: _all_card_pack_ids[pathway.id],
          visible: (@user.is_org_admin? || @visible_pathway_relations.include?(pathway.id))
        }
        _options << options
      end
    _options
    end

    #### On-demand methods: Starts here ####
    def id(card)
      (card.id.nil? ? Card.card_ecl_id(card.ecl_id) : card.id.to_s)
    end
    #### On-demand methods: Ends here ####
end

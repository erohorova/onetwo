# DYNAMIC ATTRIBUTES:
# N/A

# EXCLUDING ATTRIBUTES:
# 1. organizationId
# 2. roles
# 3. senderId

# DEFAULT FIELDS:
# 1. id
# 2. name
# 3. createdAt
# 4. sendInviteEmail
# Refer method #default_fields.

class InvitationFileBridge < Bridges
  def initialize(imports, user: nil, fields: '')
    super(imports, user: user, fields: fields)
  end

  def attributes
    details = []

    @objects.each do |file|
      invitation_file_hash = {}
      if @fields.empty?
        invitation_file_hash.merge!(self.default_fields(file))
      else
        @fields.each do |field|
          authorized?(field) && invitation_file_hash.merge!(field.to_camelcase_sym => self.send(field, file))
        end
      end

      details << invitation_file_hash
    end

    details
  end

  def default_fields(file)

    {
      id: file.id,
      createdAt: created_at(file),
      sendInviteEmail: file.send_invite_email
    }
  end

  # DESC:
  #   This is for security reasons. Ensures none of the keys unless listed here must be transmitted.
  #   We do not want all of the methods/attributes defined in this class should be available.
  #   This will prevent the attackers from exploiting the API.
  def whitelisted_keys
    [
      :id, :created_at, :send_invite_email
    ]
  end

  private
    #### On-demand methods: Starts here ####
    def id(file)
      file.id
    end

    def created_at(file)
      file.created_at.strftime('%Y-%m-%d %H:%M:%S')
    end

    def send_invite_email(file)
      file.send_invite_email
    end
    #### On-demand methods: Ends here ####
end
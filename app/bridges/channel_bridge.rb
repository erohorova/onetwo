# DYNAMIC ATTRIBUTES:
#  ...

# EXCLUDING ATTRIBUTES:
# 1. updatedAt
# 2. keywords
# 3. onlyAuthorsCanPost
# 4. eclEnabled
# 5. curateOnly
# 6. curateUgc
# 7. isPromoted
# 8. isOpen
# 9. provider
# 10. providerImage
# 11. followersCount
# 12. isOwner
# 13. authorsCount
# 14. createdAt
# 15. authorName
# 16. curators
#     16.1 isFollowing
#     16.2 id
#     16.3 handle
#     16.4 fullName
#     16.5 followersCount
#     16.6 followingCount
# 17. carousels
# 18. videoStreamsCount
# 19. coursesCount
# 20. smartbitesCount
# 21. publishedPathwaysCount
# 22. publishedJourneysCount
# 23. isCurator
# 24. teams
#     24.1 id
#     24.2 name
# 25. creator
#     25.1 isFollowing
#     25.2 id
#     25.3 handle
#     25.4 fullName
#     25.5 followersCount
#     25.6 followingCount

# DEFAULT FIELDS:
# 1. id
# 2. label
# 3. isPrivate
# 4. allowFollow
# 5. bannerImageUrls
# 6. profileImageUrl
# 7. profileImageUrls
# 8. isFollowing
# 9. slug
# Refer method #default_fields.

# TODO:
#   Conditional cross-table lookups.
#   Right now, we just calculate associated details as it is w/o checking if they will be needed or not.

class ChannelBridge < Bridges
  def initialize(channels, user: nil, fields: '')
    super(channels, user: user, fields: fields)

    @user_followed_channels = []
    @cached_channels_followers_count = {}
    @channel_followers = {}
    @creators = {}
  end

  # DESC:
  #   Responsible to iterate over all channels and returns specific details. default OR on-demand values.
  def attributes
    details = []

    # Doing this only when edc-web asks for specific fields otherwise not:
    get_all_channel_followers if @fields.include?('followers_count')

    if @fields.include?('creator')
      _creators = get_all_channel_creators
      minified_users = UserBridge.new(_creators, user: @user, fields: 'id,name,handle,is_suspended').attributes
      @creators = Hash[minified_users.map { |_creator| [_creator[:id], _creator] }]
    end

    if @fields.empty? || @fields.include?('is_following')
      @user_followed_channels  = followed_channels
    end

    # Start generating array of hashes that corresponds to list of channels.
    @objects.each do |channel|
      channel_hash = {}
      if @fields.empty?
        channel_hash.merge!(self.default_fields(channel))
      else
        @fields.each do |field|
          authorized?(field) && channel_hash.merge!(field.to_camelcase_sym => self.send(field, channel))
        end
      end

      details << channel_hash
    end

    details
  end

  def default_fields(channel)
    {
      id:               channel.id,
      label:            channel.label,
      description:      channel.description,
      isPrivate:        channel.is_private,
      allowFollow:      channel.allow_follow,
      bannerImageUrls:  banner_image_urls(channel),
      profileImageUrl:  profile_image_url(channel),
      profileImageUrls: profile_image_urls(channel),
      isFollowing:      is_following(channel),
      updatedAt:        channel.updated_at,
      slug:             channel.slug
    }
  end

  # DESC:
  #   This is for security reasons. Ensures none of the keys unless listed here must be transmitted.
  #   We do not want all of the methods/attributes defined in this class should be available.
  #   This will prevent the attackers from exploiting the API.
  def whitelisted_keys
    [
      :allow_follow, :banner_image_urls, :creator, :description,
      :followers_count, :id, :image_url, :is_following,
      :is_private, :label, :profile_image_url, :profile_image_urls,
      :slug, :updated_at
    ]
  end

  private
    #### DB queries: Starts here ####

    # DESC:
    #   A channel can be followed by a group or a user.
    #   User following a channel via team has an indirect relation i.e. team_channels_follows.
    #   User following a channel has a direct relation i.e. follows.
    #   follows and team_channels_follows are mutually-exclusive in context of a user.
    #   So, we perform:
    #     Subquery 1:
    #       a. All teams that a user is a part of.
    #       b. Pick all channels that teams from #1 follows.
    #     Subquery 2:
    #       a. Pick channels that a user follows.
    #     Final UNION ALL:
     #      Club all channel IDs from Subquery 1 & 2
     #    Returns an array of followed channels.
    def followed_channels
      # Subquery 1:
      channel_ids  = @objects.map(&:id).compact
      teams_channels_sub_query = TeamsUser
        .joins('INNER JOIN teams_channels_follows on teams_users.team_id = teams_channels_follows.team_id')
        .where('teams_channels_follows.channel_id' => channel_ids, 'teams_users.user_id' => @user.id)
        .select('teams_channels_follows.channel_id')
        .to_sql

      # Subquery 2:
      follows_sub_query = Follow
        .where(followable_id: channel_ids, followable_type: 'Channel', user_id: @user.id)
        .select('follows.followable_id as channel_id')
        .to_sql

      # Final query: Subquery 1 & 2 will now yields all followd channels IDs as a UNION ALL query.
      query = [teams_channels_sub_query, follows_sub_query].join(' UNION ALL ')

      _followed_channels = Channel.find_by_sql(query)

      _followed_channels.map { |c| c['channel_id'] }
    end

    def get_all_channel_followers
      channel_ids = @objects.map(&:id).compact
      uncache_channels_ids = []
      channel_ids.each do |channel_id|
        cached_count = Rails.cache.read("channel_#{channel_id}_followers_count".to_sym)
        if cached_count.present?
          @cached_channels_followers_count[channel_id] = cached_count
        else
          uncache_channels_ids << channel_id
        end
      end

      if uncache_channels_ids.present?
        individual_users = Follow
          .where(followable_id: uncache_channels_ids, followable_type: 'Channel')
          .joins(:user).where('users.is_suspended' => false, 'users.organization_id' => @user.organization_id)
          .pluck(:followable_id, :user_id)

        #When a user is suspended, teams_user record is not destroyed.
        #Hence user status check condition
        users_via_teams = TeamsUser
          .joins("JOIN users on users.id = teams_users.user_id")
          .joins("JOIN teams_channels_follows as tcf on teams_users.team_id = tcf.team_id")
          .where({'users.is_suspended' => false, 'users.status' => User::ACTIVE_STATUS, 'tcf.channel_id' => uncache_channels_ids})
          .pluck(:'tcf.channel_id', :'teams_users.user_id')

        individual_users.each do |individual|
          (@channel_followers[individual[0]] ||= []) << individual[1]
        end

        users_via_teams.each do |team|
          (@channel_followers[team[0]] ||= []) << team[1]
        end
      end
    end

    def get_all_channel_creators
      creator_ids = @objects.map(&:user_id)
      @organization.users.where(id: creator_ids)
    end
    #### DB queries: Ends here ####

    #### On-demand methods: Starts here ####
    def allow_follow(channel)
      channel.allow_follow
    end

    def banner_image_urls(channel)
      channel.fetch_bannerimage_urls
    end

    def description(channel)
      channel.description
    end

    def id(channel)
      channel.id
    end

    def is_private(channel)
      channel.is_private
    end

    def label(channel)
      channel.label
    end

    def profile_image_url(channel)
      channel.mobile_photo(:medium)
    end

    def profile_image_urls(channel)
      channel.fetch_profile_image_urls
    end

    def is_following(channel)
      @user_followed_channels.include?(channel.id)
    end

    def followers_count(channel)
      if @cached_channels_followers_count[channel.id].present?
        @cached_channels_followers_count[channel.id]
      else
        Rails.cache.fetch("channel_#{channel.id}_followers_count".to_sym) do
          @channel_followers[channel.id]&.uniq&.compact&.length.to_i
        end
      end
    end

    def creator(channel)
      @creators[channel.user_id]
    end
    
    def image_url(channel)
      channel.mobile_photo || channel.photo
    end

    def slug(channel)
      channel.slug
    end

    def updated_at(channel)
      channel.updated_at
    end
    #### On-demand methods: Ends here ####
end

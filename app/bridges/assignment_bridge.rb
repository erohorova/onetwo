class AssignmentBridge < Bridges
  include Api::V2::CardsHelper

  attr_accessor :cards

  def initialize(assignments, user: nil, fields: '', options: {})
    super(assignments, user: user, fields: fields)

    @cards     = {}
    @assignors = {}
    @team_assignments = {}

    # Assignment deals with card's information and receives `card(id,title,message)` from Bridges.rb.
    # Now, we parse `@association_fields` to be `id,title,message`.
    @card_fields = dig_card
    @options = options
  end

  def attributes
    details = []

    # All assignment IDs and user IDs they are assigned to.
    assignment_ids = @objects.map(&:id)
    card_ids = @objects.map(&:assignable_id)

    # All assigned cards and their minimal response.
    assigned_cards = Card.where(id: card_ids, organization_id: @organization.id)
    cards = CardBridge.new(assigned_cards, user: @user, fields: @card_fields, options: @options).attributes

    # NOTE: assignments are always LXP card i.e card will always have a unique primary ID.
    @cards = Hash[cards.map { |card| [card[:id].to_i, card] }]

    # Fetch assignor's (users) information from each assignment.
    if @fields.include?('assignor')
      _all_team_assignments = all_team_assignments(assignment_ids)
      @team_assignments = Hash[_all_team_assignments.map{ |team_assignment| [team_assignment.assignment_id, team_assignment.assignor_id] }]
      _all_assignors = all_assignors(assignment_ids)
      minified_users = UserBridge.new(_all_assignors, user: @user, fields: 'id,name').attributes
      @assignors     = Hash[minified_users.map{ |user| [user[:id], user] }]
    end

    # Start generating array of hashes that corresponds to list of assignments.
    @objects.each do |assignment|
      assignment_hash = {}
      if @fields.empty?
        assignment_hash.merge!(self.default_fields(assignment))
      else
        @fields.each do |field|
          authorized?(field) && assignment_hash.merge!(field.to_camelcase_sym => self.send(field, assignment))
        end
      end

      details << assignment_hash
    end

    details
  end

  def default_fields(assignment)
    _fields = {
      id: assignment.id,
      assignable: assignable(assignment)
    }
  end

  # DESC:
  #   This is for security reasons. Ensures none of the keys unless listed here must be transmitted.
  #   We do not want all of the methods/attributes defined in this class should be available.
  #   This will prevent the attackers from exploiting the API.
  def whitelisted_keys
    [
      :assignable, :assignable_id, :assignor, :completed_at,
      :due_at, :id, :start_date, :started_at, :state, :created_at
    ]
  end

  private
    #### DB queries: Starts here ####
    def all_assignors(assignment_ids)
      assignor_ids = TeamAssignment.where(assignment_id: assignment_ids).select('assignor_id')
      User.where(id: assignor_ids, organization_id: @organization.id).select('id,first_name,last_name,email')
    end

    def all_team_assignments(assignment_ids)
      TeamAssignment.where(assignment_id: assignment_ids).select('assignment_id,assignor_id')
    end
    #### DB queries: Ends here ####

    #### On-demand methods: Starts here ####
    def assignable_id(assignment)
      assignment.assignable_id
    end

    def completed_at(assignment)
      assignment.completed_at
    end

    def due_at(assignment)
      assignment.due_at
    end

    def id(assignment)
      assignment.id
    end

    def assignable(assignment)
      @cards[assignment.assignable_id]
    end

    def assignor(assignment)
      _user_id = @team_assignments[assignment.id]
      return if _user_id.nil?

      @assignors[_user_id]
    end

    def started_at(assignment)
      assignment.started_at
    end

    def start_date(assignment)
      assignment.start_date
    end

    def state(assignment)
      assignment.state
    end

    def created_at(assignment)
      assignment.created_at
    end
    #### On-demand methods: Ends here ####
end

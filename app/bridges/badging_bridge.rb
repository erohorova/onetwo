# DYNAMIC ATTRIBUTES
#   ...

# EXCLUDING ATTRIBUTES:
#   ...

# DEFAULT FIELDS:
# 1. id
# 2. title
# 3. imageUrl
# 4. badgeId

class BadgingBridge < Bridges
  def initialize(badgings, user: nil, fields: '')
    super(badgings, user: user, fields: fields)

    @badges = {}
  end

  def attributes
    details = []

    badge_ids = @objects.map(&:badge_id).compact

    @badges = all_badges(badge_ids)

    # Start generating array of hashes that corresponds to list of badges.
    @objects.each do |badging|
      badging_hash = {}
      
      if @fields.empty?
        badging_hash.merge!(self.default_fields(badging))
      else
        @fields.each do |field|
          authorized?(field) && badging_hash.merge!(field.to_camelcase_sym => self.send(field, badging))
        end
      end
      details << badging_hash
    end

    details
  end

  def default_fields(badging)
    {
      id: badging.id,
      title: badging.title,
      imageUrl: image_url(badging),
      badgeId: badging.badge_id,
      badgeableId: badging.badgeable_id
    }
  end

  # DESC:
  #   This is for security reasons. Ensures none of the keys unless listed here must be transmitted.
  #   We do not want all of the methods/attributes defined in this class should be available.
  #   This will prevent the attackers from exploiting the API.
  def whitelisted_keys
    [
      :id, :image_url, :title
    ]
  end

  private
    #### DB queries: Starts here ####
    def all_badges(badge_ids)
      CardBadge
        .joins(:badgings)
        .where(id: badge_ids, organization_id: @organization.id)
        .select(:id, :image_file_name, :image_content_type, :image_file_size, :image_updated_at, 'badgings.id as badging_id')
        .index_by(&:badging_id)
    end
    #### DB queries: Ends here ####

    #### On-demand methods: Starts here ####
    def id(badging)
      badging.id
    end

    def image_url(badging)
      @badges[badging.id].image.url(:large)
    end

    def title(badging)
      badging.title
    end
    #### On-demand methods: Ends here ####
end
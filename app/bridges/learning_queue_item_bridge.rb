# DEFAULT FIELDS
# 1. id
# 2. displayType
# 3. deepLinkId
# 4. deepLinkType
# 5. source
# 6. updatedAt

class LearningQueueItemBridge < Bridges
  def initialize(learning_queue_items, user: nil, fields: '', options: {})
    super(learning_queue_items, user: user, fields: fields)

    @options = options
  end

  def attributes
    details = []

    queueable_ids = @objects.map(&:queueable_id)
    @queueables = @user.organization.cards.where(id: queueable_ids).visible_cards
    @queueables = CardBridge.new(@queueables, user: @user, options: @options).attributes

    # Create hash for queueables
    @queueables = @queueables.map { |e| [e[:id].to_i, e] }.to_h

    @assignments = @user.assignments.select(:id, :state, :start_date, :due_at, :assignable_id).where(assignable_id: queueable_ids)
    @assignments = AssignmentBridge.new(@assignments, user: @user, fields: 'id,state,start_date,due_at,assignable_id').attributes

    # Create hash for assignments
    @assignments = @assignments.map { |e| [e[:assignableId], e] }.to_h

    if @objects.map(&:source).uniq.include?('bookmarks')
      @bookmarkables = card_ids_grouped_by_bookmark_ids(queueable_ids)
    end

    # Start generating array of hashes that corresponds to list of learning_queue_items.
    @objects.each do |learning_queue_item|
      learning_queue_item_hash = {}
      if @fields.empty?
        learning_queue_item_hash.merge!(self.default_fields(learning_queue_item))
      else
        @fields.each do |field|
          authorized?(field) && learning_queue_item_hash.merge!(field.to_camelcase_sym => self.send(field, learning_queue_item))
        end
      end

      details << learning_queue_item_hash
    end

    details
  end

  def default_fields(learning_queue_item)
    _fields = {
      id: learning_queue_item.id,
      displayType: learning_queue_item.display_type,
      deepLinkId: learning_queue_item.deep_link_id,
      deepLinkType: learning_queue_item.deep_link_type,
      source: learning_queue_item.source,
      updatedAt: learning_queue_item.updated_at,
      snippet: learning_queue_item.snippet,
      queueable: queueable(learning_queue_item)
    }

    if learning_queue_item.source == 'bookmarks'
      _fields.merge!(bookmarkId: bookmarkable(learning_queue_item.queueable_id))
    end

    _fields
  end

  # DESC:
  #   This is for security reasons. Ensures none of the keys unless listed here must be transmitted.
  #   We do not want all of the methods/attributes defined in this class should be available.
  #   This will prevent the attackers from exploiting the API.
  def whitelisted_keys
    [
      :deep_link_id, :deep_link_type, :display_type,
      :id, :queueable, :snippet, :source, :updated_at
    ]
  end

  private

  #### DB queries: Starts here ####
  def card_ids_grouped_by_bookmark_ids(card_ids)
    @user.bookmarks.where(bookmarkable_id: card_ids)
      .select(:id, :bookmarkable_id).group_by(&:bookmarkable_id)
      .transform_values { |value| value.first&.id }
  end
  #### DB queries: Ends here ####

  #### On-demand methods: Starts here ####
  def bookmarkable(card_id)
    @bookmarkables[card_id.to_i]
  end

  def deep_link_id(learning_queue_item)
    learning_queue_item.deep_link_id
  end

  def deep_link_type(learning_queue_item)
    learning_queue_item.deep_link_type
  end

  def display_type(learning_queue_item)
    learning_queue_item.display_type
  end

  def id(learning_queue_item)
    learning_queue_item.id
  end

  def queueable(learning_queue_item)
    # queueable
    _queueable = @queueables[learning_queue_item.queueable_id]

    # queueable assignment
    assignment = @assignments[learning_queue_item.queueable_id]
    _queueable.merge!(assignment: assignment) if assignment

    _queueable
  end

  def snippet(learning_queue_item)
    learning_queue_item.snippet
  end

  def source(learning_queue_item)
    learning_queue_item.source
  end

  def updated_at(learning_queue_item)
    learning_queue_item.updated_at
  end
  #### On-demand methods: Ends here ####
end

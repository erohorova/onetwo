# DYNAMIC ATTRIBUTES:
#   ...

# EXCLUDING ATTRIBUTES:
#  1. description
#  2. isOpen
#  3. isRegistrable
#  4. bannerUrl
#  5. enableRoleBasedAuthorization
#  6. homePage
#  7. showSubBrandOnLogin
#  8. showOnboarding
#  9. favicons
#  10. customDomain
#  12. ssos
#      12.1 name
#      12.2 labelName
#      12.3 iconUrl
#      12.4 authenticationUrl
#      12.5 webAuthenticationUrl
#      12.6 ssoName
#  13. cobrands
#  14. configs
#      14.1 id
#      14.2 name
#      14.3 value
#  15. isOnboardingConfigurable
#  14. onboardingOptions
#  15. onboardingStepsNames
#  16. enableAnalyticsReporting
#  17. currentTodaysLearning
#  18. readableCardTypes
#  19. fromAddress
#  20. orgSubscriptions

# DEFAULT FIELDS:
#  1. id
#  2. hostName
#  3. name
#  4. coBrandingLogo
#  5. imageUrl
#  Refer method #default_fields.

class OrganizationBridge < Bridges
  def attributes
    details = []

    # Start generating array of hashes that corresponds to list of organizations.
    @objects.each do |organization|
      org_hash = {}
      if @fields.empty?
        org_hash.merge!(self.default_fields(organization))
      else
        @fields.each do |field|
          authorized?(field) && org_hash.merge!(field.to_camelcase_sym => self.send(field, organization))
        end
      end

      details << org_hash
    end

    details
  end

  def default_fields(organization)
    {
      id:        organization.id,
      hostName:  organization.host_name,
      name:      organization.name,
      coBrandingLogo: co_branding_logo(organization),
      imageUrl:   image_url(organization)
    }
  end

  # DESC:
  #   This is for security reasons. Ensures none of the keys unless listed here must be transmitted.
  #   We do not want all of the methods/attributes defined in this class should be available.
  #   This will prevent the attackers from exploiting the API.
  def whitelisted_keys
    [
      :co_branding_logo, :host_name, :id,
      :image_url, :name, :stock_images
    ]
  end

  private
    #### DB queries: Starts here ####
    # ...
    #### DB queries: Ends here ####

    #### On-demand methods: Starts here ####
    def co_branding_logo(organization)
      organization.co_branding_logo.present? ? organization.co_branding_logo.url : nil
    end

    def host_name(organization)
      organization.host_name
    end

    def id(organization)
      organization.id
    end

    def image_url(organization)
      organization.mobile_photo || organization.photo
    end

    def name(organization)
      organization.name
    end

    # DESC:
    #   Making stock images available as an organization info even though they are global + static values.
    def stock_images(organization = nil)
      CARD_STOCK_IMAGE
    end
    #### On-demand methods: Ends here ####
end

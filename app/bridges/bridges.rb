# INFO:
#   This is a base class that takes a collection of objects and a list of fields (comma-separated),
#     and only returns those fields from the object.
#     Basically, a presentation layer.
#     Resolve directs fields from the table as well as the selective fields from the associated tables.
#     Refer `FieldsResolver`.

# ARGUMENTS:
#   fields: `id,label,description`. Comma-separated. Simple for now.
#   user: Logged in user. Sets the context with whom.
#   objects: channels/users/cards/organizations. Majorly. More entities coming soon.

# RESPONSE:
#   Takes care of camelcasing automatically.
#   A convention to be adopted whenever new attributes are added.

class Bridges
  def initialize(objects, user: nil, fields: "")
    @objects      = objects
    if user
      @user         = user
      @organization = @user.organization
    end

    # Reset fields to be blank in case of NIL being configured.
    fields = "" if fields.nil?

    # Case 1: fields exists in the form of comma-separated string.
    # Empty string means no fields to be parsed and the bridge should return the default fields.
    if fields.is_a?(String)
      parser = Parser.parse(fields.strip)
      @resolved_fields = parser[:fields]
    else
      # Case 2: fields exists in the form of an array.
      # This means this class is now receiving the tree-structure of the attributes.
      @resolved_fields = fields
    end

    # All the simple fields will be extracted as simple strings. For say, ["id", "title", "message", ...]
    @fields = @resolved_fields.collect { |f| f[:name] if f[:type] == :simple }.compact

    # All the complex/association fields will still be represented as a tree.
    # [
    #   {
    #     type: :relationship,
    #     name: "channels",
    #     fields: [
    #       {
    #         type: :simple,
    #         name: "id"
    #       },
    #       {
    #         type: :simple,
    #         name: "label"
    #       },
    #       {
    #         type: :relationship,
    #         name: "author"
    #         fields: [
    #           {
    #             type: :simple,
    #             name: "first_name"
    #           }
    #         ]
    #       }
    #     ]
    #   }
    # ]
    @association_fields = @resolved_fields.select { |f| f[:type] == :relationship }.compact
  end

  # DESC:
  #   #whitelisted_keys are explicitly handled in each of the Bridge.
  #   We do not want other implementation details to be exploitable by any means.
  def authorized?(field_name)
    self.whitelisted_keys.include?(field_name.to_sym)
  end

  def dig_badging
    @association_fields.collect { |f| f[:fields] if f[:name] == "badging" }.compact.flatten
  end

  def dig_card
    @association_fields.collect { |f| f[:fields] if f[:name] == 'card' }.compact.flatten
  end

  def dig_channels
    @association_fields.collect { |f| f[:fields] if f[:name] == "channels" }.compact.flatten
  end

  def dig_pack_cards
    @association_fields.collect { |f| f[:fields] if f[:name] == "pack_cards" }.compact.flatten
  end

  def dig_profile
    @association_fields.collect { |f| f[:fields] if f[:name] == "profile" }.compact.flatten
  end

  def dig_teams
    @association_fields.collect { |f| f[:fields] if f[:name] == "teams" }.compact.flatten
  end

  def dig_tags
    @association_fields.collect { |f| f[:fields] if f[:name] == "tags" }.compact.flatten
  end

  def whitelisted_keys
    raise NotImplementedError, 'value method must be implemented in the child class'
  end
end

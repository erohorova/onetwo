# DYNAMIC ATTRIBUTES:
# ...

# EXCLUDING ATTRIBUTES:
# updatedAt
# mentions
# packCards
# journeySection
# reviewStarted
# resourceGroup
# userTaxonomyTopics

# DEFAULT FIELDS:
# 1. title
# 2. message
# 3. cardType
# 4. cardSubtype
# 5. slug
# 6. state
# 7. createdAt
# 8. provider
# 9. providerImage
# 10. readableCardType
# 11. shareUrl
# 12. averageRating
# 13. skillLevel
# 14. filestack
# 15. ecl_metadata (eclSourceDisplayName, eclSourceTypeName, eclSourceLogoUrl)
# 16. eclDurationMetadata (calculated_duration, calculated_duration_display)
# 17. author
# 18. isOfficial
# 19. isReported
# 20. isPaid
# 21. votesCount
# 22. commentsCount
# 23. publishedAt
# 24. prices (id, amount, currency, symbol)
# 25. isBookmarked
# 26. isAssigned
# 27. hidden
# 28. isPublic
# 29. markFeatureDisabledForSource
# 30. completionState
# 31. isUpvoted
# 32. allRatings
# 33. cardMetadatum (id, plan)
# 34. badging
# 35. rank
# 36. quiz (quizQuestionOptions, isAssessment, hasAttempted, attemptedOption, attemptCount, canBeReanswered)
# 37. resource (id, imageUrl, title, description, url, siteName, type, videoUrl, embedHtml)
# 38. videoStream (id, imageUrl, status, uuid, playbackUrl, embedPlaybackUrl, startTime, width, height, recording(id, hlsLocation, mp4Location), watchers)
# 39. projectId
# 40. isClone
# 41. isNew
# 42. completedPercentage
# 43. teamsPermitted
# 44. usersPermitted
# Refer method #default_fields.

# TODO:
#   Conditional cross-table lookups.
#   Right now, we just calculate associated details as it is w/o checking if they will be needed or not.

class CardBridge < Bridges
  include ActionView::Helpers::DateHelper, Api::V2::CardsHelper

  def initialize(cards, user: nil, fields: '', options: {})
    super(cards, user: user, fields: fields)

    @tags        = {}
    @teams       = {}
    @polls       = {}
    @prices      = []
    @votes       = {}
    @voters      = {}
    @authors     = {}
    @streams     = {}
    @reported    = []
    @projects    = {}
    @metadata    = {}
    @bookmarks   = []
    @resources   = {}
    @assignments = {}
    @card_badgings    = {}
    @cards_ratings    = {}
    @file_resources   = {}
    @watchers_count   = {}
    @source_details   = {}
    @curated_channels = {}
    @pack_cards_count = {}
    @journey_packs_count     = {}
    @users_with_access       = {}
    @card_subscriptions      = {}
    @curated_channel_ids     = {}
    @cards_rating_counts     = 0
    @teams_with_permission   = {}
    @users_with_permission   = {}
    @non_curated_channel_ids = {}
    @user_content_completion = {}
    @user_rating = {}

    @channel_fields = dig_channels
    @team_fields    = dig_teams
    @tag_fields     = dig_tags
    @card_badging_fields = dig_badging

    # ORGANIZATION CONFIGS
    @reporting_content_enabled = @organization.reporting_content_enabled?
    @auto_marked = @organization.auto_mark_as_complete_enabled?
    @duration_format = @organization.get_settings_for('duration_format').present?

    #directly_mergeable_attributes will not create key value pair of method and values
    #instead this will directly merge values at card level
    @directly_mergeable_attributes = %w(quiz ecl_metadata)

    if options
      @is_native_app = options[:is_native_app]
      @last_access_at = options[:last_access_at]
    end
  end

  # DESC:
  #   Responsible to iterate over all cards and returns specific details. Default OR on-demand values.
  def attributes
    details = []
    author_ids = @objects.map(&:author_id).compact
    card_ids   = @objects.map(&:id).compact
    # Authors
    if @fields.empty? || @fields.include?('author')
      authors    = @organization.users.where(id: author_ids)
      @authors   = UserBridge.new(authors, user: @user, fields: 'id,handle,avatarimages,full_name,profile').attributes
      @authors   = Hash[@authors.map { |author| [author[:id], author] }]
    end

    # UserContentCompletion to calculate the status of the progress.
    if @fields.empty? || @fields.include?('completion_state')
      user_content_completion = UserContentCompletion.select(:completable_id, :state).where(completable_type: 'Card', completable_id: card_ids, user_id: @user.id)
      @user_content_completion = user_content_completion.map { |e| [e.completable_id, e.state.try(:upcase)] }.to_h
    end

    # Assignments
    if @fields.empty?  || @fields.include?('assignment') || @fields.include?('is_assigned')
      assignments  = Assignment.where(assignable_id: card_ids, assignable_type: 'Card', user_id: @user.id).where.not(state: 'dismissed').select(:id, :start_date, :due_at, :state, :assignable_id)
      @assignments = assignments.index_by(&:assignable_id)
    end

    # Bookmarks
    if @fields.empty? || @fields.include?('is_bookmarked')
      @bookmarks = Bookmark.where(bookmarkable_id: card_ids, bookmarkable_type: 'Card', user_id: @user.id).pluck(:bookmarkable_id)
    end

    # Show restrict
    if @fields.include?('show_restrict')
      @card_bookmarks = Bookmark.where(bookmarkable_id: card_ids, bookmarkable_type: 'Card').index_by(&:bookmarkable_id)
      @card_content_completions = UserContentCompletion.where(completable_type: 'Card', completable_id: card_ids).index_by(&:completable_id)
      @card_assignments  = Assignment.where(assignable_id: card_ids, assignable_type: 'Card').index_by(&:assignable_id)
    end

    # Reported cards
    if @fields.empty? || @fields.include?('is_reported')
      @reported = CardReporting.where(card_id: card_ids, user_id: @user.id).pluck(:card_id) if @reporting_content_enabled
    end

    # Card metadata
    if @fields.empty? || (@fields & %w(skill_level card_metadatum paid_by_user average_rating)).any?
      metadata  = CardMetadatum.where(card_id: card_ids).select('id, plan, card_id, average_rating, level')
      @metadata = Hash[metadata.map { |_metadata| [_metadata.card_id, _metadata] }]
    end

    # Count of cards in pathways
    if @fields.include?('pack_cards_count')
      _pack_cards_data = all_pack_cards_data(card_ids)
      @pack_cards_count = Hash[_pack_cards_data.map { |_cards_data| [_cards_data['cover_id'], _cards_data['count']] }]
    end

    # Count of pathways in journey
    if @fields.include?('journey_packs_count')
      _journey_packs_data = all_journey_packs_data(card_ids)
      @journey_packs_count = Hash[_journey_packs_data.map { |_cards_data| [_cards_data['cover_id'], _cards_data['count']] }]
    end

    # NOTE: Lookup for prices will be successful only for physical cards and not for virtual/ECL cards.
    # Prices
    if @fields.empty? || @fields.include?('prices')
      @prices = Price.where(card_id: card_ids).select('id, amount, card_id, currency').group_by(&:card_id)
    end

    # Polls
    if @fields.empty? || @fields.include?('quiz')
      poll_attempts = QuizQuestionAttempt.where(quiz_question_id: card_ids, quiz_question_type: 'Card', user_id: @user.id)
      @polls        = Hash[poll_attempts.map { |poll_attempt| [poll_attempt.quiz_question_id, poll_attempt.current_selection] }]
    end

    # Resources
    if @fields.empty?
      resource_ids = @objects.map(&:resource_id).compact
      resources    = Resource.where(id: resource_ids)
      @resources   = Hash[resources.map { |resource| [resource.id, resource] }]
    end

    # Video streams
    if @fields.empty? || @fields.include?('video_stream')
      streams      = VideoStream.where(card_id: card_ids)
      stream_ids   = streams.ids
      @streams     = Hash[streams.map { |stream| [stream.card_id, stream] }]
      @recordings  = Recording.where(video_stream_id: stream_ids, transcoding_status: 'available', default: true).order(sequence_number: :asc).group_by(&:video_stream_id)
      @watchers_count = VideoStreamsUser.where(video_stream_id: stream_ids).group(:video_stream_id).count
    end

    # Card badging
    if @fields.empty? || @fields.include?('badging')
      card_badgings  = CardBadging.where(badgeable_id: card_ids)
      _card_badgings = BadgingBridge.new(card_badgings, user: @user, fields: @card_badging_fields).attributes
      @card_badgings = Hash[_card_badgings.map{|card_badging| [card_badging[:badgeableId], card_badging.except(:badgeableId)]}]
    end

    # User-rated cards
    if @fields.empty? || @fields.include?('skill_level')
      cards_ratings = CardsRating.where(card_id: card_ids, user_id: author_ids)
      @cards_ratings = Hash[cards_ratings.map { |cards_rating| [cards_rating.card_id, cards_rating.level]}]
    end

    # Total ratings
    if @fields.empty? || @fields.include?('all_ratings')
      @cards_rating_counts = CardsRating.where(card_id: card_ids).where.not(rating: nil).group(:card_id).count
    end

    # User ratings
    if @fields.include?('user_rating')
      @user_rating = CardsRating.where(card_id: card_ids, user_id: @user.id).where.not(rating: nil)
    end

    # Votes
    if @fields.empty? || @fields.include?('is_upvoted')
      @votes = Vote.where(votable_id: card_ids, votable_type: 'Card', user_id: @user.id).index_by(&:votable_id)
    end

    # Card subscriptions
    if @fields.include?('paid_by_user')
      @card_subscriptions = @user.active_card_subscriptions.where(card_id: card_ids).group_by(&:card_id)
    end

    if @fields.include?('due_at')
      @chosen_assignments = Assignment.where(user_id: @user.id, assignable_id: card_ids, assignable_type: 'Card')
        .select(:id, :due_at, :assignable_id)
        .group_by(&:assignable_id)
        .transform_values(&:first)
    end

    if @fields.include?('assigner')
      @chosen_team_assignments = TeamAssignment.joins(:assignment)
        .where(assignments: {user_id: @user.id, assignable_id: card_ids, assignable_type: 'Card'})
        .select('team_assignments.*, assignments.assignable_id as card_id')
        .group_by(&:card_id)
        .transform_values(&:first)
    end

    if @fields.include?('shared_by')
      @card_user_shares, @shared_by_users = card_share_data(card_ids)
    end

    if @fields.include?('subscription_end_date')
      @chosen_subscriptions = CardSubscription.where(user_id: @user.id, card_id: card_ids)
        .select(:card_id, :end_date)
        .group_by(&:card_id)
        .transform_values(&:first)
    end

    # File resources
    if @fields.include?('file_resources')
      @file_resources = FileResource
        .where(attachable_type: 'Card', attachable_id: card_ids)
        .select(:id, :attachment_file_name, :attachment_content_type, :attachment_file_size, :attachment_updated_at, :filestack, :attachable_id)
        .group_by(&:attachable_id)
    end

    if @fields.include?('payment_enabled')
      source_ids = @objects.map {|e| e.ecl_metadata&.dig('source_id')}.compact
      @source_details = SourcesCredential.where(source_id: source_ids, organization_id: @organization.id).index_by(&:source_id)
    end

    # Channels
    if (@fields & %w(channels channel_ids)).any?
      curated_channels = all_curated_channels(card_ids)
      @channel_fields = @channel_fields.empty? ? 'id,label,is_private' : @channel_fields

      _channels = ChannelBridge.new(curated_channels.values.flatten.uniq, user: @user, fields: @channel_fields).attributes
      _channels = _channels.group_by { |channel| channel[:id] }

      # channels
      curated_channels.each { |card_id, channels| @curated_channels[card_id] = _channels.slice(*channels.map(&:id)).values.flatten }

      # channel_ids
      @curated_channel_ids = curated_channels
      @curated_channel_ids.each { |k,v| v.map!(&:id) }
    end

    # Tags
    if @fields.include?('tags')
      tags = all_tags(card_ids)

      @tag_fields = @tag_fields.empty? ? 'id,name' : @tag_fields

      _tags = TagBridge.new(tags.values.flatten.uniq, user: @user, fields: @tag_fields).attributes
      _tags = _tags.group_by { |tag| tag[:id] }

      tags.each { |card_id, tags| @tags[card_id] = _tags.slice(*tags.map(&:id)).values.flatten }
    end

    # Permitted teams
    # should_fetch_card_permissions? will ensure that teams_permitted are not sent for:
      # user is org admin
      # no cards are private
      # the user is the author of the private card
    if (@fields.empty? || @fields.include?('teams_permitted')) && should_fetch_card_permissions?(card_ids)
      teams_with_permission = all_teams_permitted(card_ids)

      _teams_with_permission = TeamBridge.new(teams_with_permission.values.flatten.uniq, user: @user, fields: 'id,name,channel_ids').attributes
      _teams_with_permission = _teams_with_permission.group_by { |team| team[:id] }

      teams_with_permission.each do |card_id, teams|
       @teams_with_permission[card_id] = _teams_with_permission.slice(*teams.map(&:id)).values.flatten
      end
    end

    # Permitted users
    # should_fetch_card_permissions? will ensure that users_permitted are not sent for:
      # user is org admin
      # no cards are private
      # the user is the author of the private card
    if (@fields.empty? || @fields.include?('users_permitted')) && should_fetch_card_permissions?(card_ids)
      users_permitted  = all_users_permitted(card_ids)

      _users_permitted = UserBridge.new(users_permitted.values.flatten.uniq, user: @user, fields: 'id,name,channel_ids,team_ids').attributes
      _users_permitted = _users_permitted.group_by { |user| user[:id] }

      users_permitted.each do |card_id, users|
        @users_with_permission[card_id] = _users_permitted.slice(*users.map(&:id)).values.flatten
      end
    end

    # Teams
    if @fields.include?('teams')

      teams  = Team.joins(:shared_cards).where(teams_cards: {card_id: card_ids}).select(:id, :name, 'teams_cards.card_id', 'teams_cards.team_id').group_by(&:card_id)

      @team_fields = @team_fields.empty? ? 'id,name' : @team_fields

      _teams = TeamBridge.new(teams.values.flatten.uniq, user: @user, fields: @team_fields).attributes
      _teams = _teams.group_by { |team| team[:id] }

      teams.each { |card_id, teams| @teams[card_id] = _teams.slice(*teams.map(&:id)).values.flatten }
    end

    if @fields.include?('users_with_access')
      users_with_access = all_users_with_access(card_ids)

      _users_with_access = UserBridge.new(users_with_access.values.flatten.uniq, user: @user, fields: 'id,handle,email,full_name,avatarimages').attributes
      _users_with_access = _users_with_access.group_by { |user| user[:id] }

      users_with_access.each { |card_id, users| @users_with_access[card_id] = _users_with_access.slice(*users.map(&:id)).values.flatten }
    end

    if @fields.include?('non_curated_channel_ids')
      @non_curated_channel_ids = ChannelsCard.where(card_id: card_ids, state: ChannelsCard::STATE_NEW).group_by(&:card_id).each {|k,v| v.map!(&:channel_id)}
    end

    if @fields.include?('voters')
      card_voters = Vote.where(votable_id: card_ids, votable_type: 'Card').select(:votable_id, :user_id).group_by(&:votable_id)

      voters = @organization.users.where(id: card_voters.values.flatten.map(&:user_id))
      voters = UserBridge.new(voters, user: @user, fields: 'id,handle,name,avatarimages').attributes
      voters = voters.group_by { |voter| voter[:id] }

      card_voters.map do |votable_id, vote|
        @voters[votable_id] = voters.slice(*vote.map(&:user_id)).values.flatten
      end
    end

    if @fields.empty? || @fields.include?('project_id')
      @projects = Project.where(card_id: card_ids).pluck(:card_id, :id).to_h
    end

    # Leaps
    if @fields.include?('leaps')
      @pathway_leaps = Leap.where(pathway_id: card_ids).group_by(&:pathway_id)
      @standalone_leaps = Leap.where(card_id: card_ids, pathway_id: nil)
        .group_by(&:card_id)
    end

    if @fields.include?('is_new') && @last_access_at.present?
      _pack_cards_data = all_pack_cards_data(card_ids)
      @last_packs_updates = Hash[_pack_cards_data.map { |cards_data| [cards_data['cover_id'], cards_data['last_added'].to_i] }]
      _journey_updates_data = last_journey_updates_data(card_ids)
      @last_journey_updates = Hash[_journey_updates_data.map do |cards_data|
        [cards_data['cover_id'], [cards_data['last_section_added'].to_i, cards_data['last_card_added'].to_i].max]
      end
      ]
    end

    # Start generating array of hashes that corresponds to the list of cards.
    @objects.each do |card|
      card_hash = {}
      if @fields.empty?
        card_hash.merge!(self.default_fields(card))
      else
        @fields.each do |field|
          if @directly_mergeable_attributes.include?(field)
            authorized?(field) && card_hash.merge!(self.send(field, card))
          else
            authorized?(field) && card_hash.merge!(field.to_camelcase_sym => self.send(field, card))
          end
        end
      end
      details << card_hash
    end

    details
  end

  def assigner(card)
    user = @chosen_team_assignments[card.id]&.assignor
    fields = 'id,handle,full_name,first_name,last_name,avatarimages'
    UserBridge.new([user], user: @user, fields: fields)
      .attributes.first if user.present?
  end

  def due_at(card)
    @chosen_assignments[card.id]&.due_at
  end

  def subscription_end_date(card)
    @chosen_subscriptions[card.id]&.end_date
  end

  def default_fields(card)
    _card_id = id(card)

    author_details   = author(card)
    badging_details  = badging(card)

    resource_details = article(card)
    stream_details   = video_stream(card)

    _fields = {
      id:       _card_id,
      title:    (card.message != card.title) ? card.title.try(:clean) : '',
      message:  card.sanitize_message,
      cardType: card.card_type,
      cardSubtype: card.card_subtype,
      slug:     card.slug,
      state:    card.state,
      isOfficial: card.is_official,
      provider: card.provider,
      providerImage: provider_image(card),
      readableCardType: readable_card_type(card),
      shareUrl:         share_url(_card_id),
      averageRating:    average_rating(card),
      skillLevel:       skill_level(card),
      filestack:        filestack(card),
      votesCount:       card.votes_count,
      commentsCount:    card.comments_count,
      publishedAt:      card.published_at,
      createdAt:        card.created_at,
      prices:           prices(card),
      isAssigned:       is_assigned(card),
      isBookmarked:     is_bookmarked(card),
      hidden:           card.hidden,
      isPublic:         card.is_public,
      isPaid:           is_paid(card),
      markFeatureDisabledForSource: mark_feature_disabled_for_source(card),
      completionState:  completion_state(card),
      isUpvoted:        is_upvoted(card),
      allRatings:       all_ratings(card)
    }

    _fields.merge!(isReported: is_reported(card)) if @reporting_content_enabled
    _fields.merge!(cardMetadatum: card_metadatum(card)) if @metadata[card.id]
    _fields.merge!(ecl_metadata(card)) if card.ecl_metadata.present?
    _fields.merge!(eclDurationMetadata: ecl_duration_metadata(card)) if card.time_to_read
    _fields.merge!(author: author_details) if author_details.present?
    _fields.merge!(badging: badging_details) if badging_details.present?
    # to pass channel card rank only if the attr accessor is set
    # happens only when sort param = 'rank' passed for v2/cards
    _fields.merge!(rank: rank(card)) if card.channel_card_rank.present?

    # quiz / assessment
    _fields.merge!(quiz(card)) if card.card_type == 'poll'

    # article
    _fields.merge!(resource: resource_details) if resource_details.present?

    # video stream
    _fields.merge!(videoStream: stream_details) if stream_details.present?

    # project
    _fields.merge!(projectId: project_id(card)) if card.card_type == "project"

    # is_clone if shared channels is enabled
    _fields.merge!(isClone: card.super_card_id.present?)

    # completed_percentage for pathway/journey cards
    if %w[pack journey].include?(card.card_type)
      _fields.merge!(completedPercentage: completed_percentage(card))
    end

    # Should send these params for non admins and private cards
    # Should not send these params when
      # card is public
    unless card.is_public
      _fields.merge!(teamsPermitted: teams_permitted(card))
      _fields.merge!(usersPermitted: users_permitted(card))
    end

    # end
    # upload / scorm
    # ... they are filestack content

    # resource_group
    # ... added for Savannah courses imported as SmartCards. Only for Mobile. Skipping...

    _fields
  end

  # DESC:
  #   This is for security reasons. Ensures none of the keys unless listed here must be transmitted.
  #   We do not want all of the methods/attributes defined in this class should be available.
  #   This will prevent the attackers from exploiting the API.
  # Declared in Bridges#valid_method?
  # Implemented across all sublasses.
  def whitelisted_keys
    [
      :all_ratings, :article, :assignment, :author, :auto_complete, :average_rating,
      :badging, :card_metadatum, :card_subtype, :card_type, :channel_ids, :channels,
      :channels, :comments_count, :completed_percentage, :completion_state, :completion_state,
      :created_at, :duration, :ecl_duration_metadata, :ecl_id, :ecl_metadata, :file_resources,
      :filestack, :hidden, :id, :is_assigned, :is_bookmarked, :is_clone, :is_official, :is_paid,
      :is_paid, :is_public, :is_reported, :is_upvoted, :journey_packs_count, :language, :leaps,
      :locked, :mark_feature_disabled_for_source, :message, :non_curated_channel_ids, :pack_cards,
      :pack_cards_count, :paid_by_user, :payment_enabled, :prices, :project_id, :provider,
      :provider_image, :published_at, :quiz, :rank, :readable_card_type, :resource, :share_url,
      :show_restrict, :skill_level, :slug, :state, :tags, :tags, :teams, :teams_permitted, :title,
       :user_rating, :users_permitted, :users_with_access, :video_stream, :views_count, :voters,
       :votes_count, :due_at, :assigner, :subscription_end_date, :is_new, :shared_by
    ]
  end

  private
    #### DB queries: Starts here ####
    # DESC:
    #   Returns cover_id and count of inner cards as the resultset.
    def all_pack_cards_data(card_ids)
      CardPackRelation
        .joins('INNER JOIN cards on cards.id = card_pack_relations.cover_id and card_pack_relations.cover_id != card_pack_relations.from_id')
        .where('cover_id in (?)', card_ids)
        .group('card_pack_relations.cover_id')
        .select(:cover_id, 'max(card_pack_relations.created_at) as last_added, count(cover_id) as count')
    end

    def all_teams_permitted(card_ids)
      @organization
        .teams
        .joins(:card_team_permissions)
        .where(card_team_permissions: {card_id: card_ids})
        .select(:id, :name, 'card_team_permissions.card_id', 'card_team_permissions.team_id')
        .group_by(&:card_id)
    end

    def is_clone(card)
      card.super_card_id.present?
    end

    def all_users_permitted(card_ids)
      @organization
        .users
        .joins(:card_user_permissions)
        .where(card_user_permissions: {card_id: card_ids})
        .select(:id, :first_name, :last_name, :email, 'card_user_permissions.card_id')
        .group_by(&:card_id)
    end

    def all_users_with_access(card_ids)
      @organization
        .users
        .joins(:card_user_shares)
        .where(card_user_shares: {card_id: card_ids})
        .select(:id, :first_name, :last_name, :email, :handle, :picture_url, :avatar_file_name, :avatar_content_type, :avatar_file_size, :avatar_updated_at, 'card_user_shares.card_id', 'card_user_shares.user_id')
        .group_by(&:card_id)
    end

    def locked(card)
      child_card = @objects.find { |c| c.id == card.id }
      "#{child_card["locked"]}"&.to_bool
    end

    #### DB queries: Ends here ####

    #### On-demand methods: Starts here ####
    def all_curated_channels(card_ids)
      @organization
        .channels
        .joins(:curated_channels_cards)
        .where(channels_cards: { card_id: card_ids })
        .select('channels.id, channels.label, channels.is_private, channels_cards.card_id')
        .group_by(&:card_id)
    end

    # Returns journey id and count of pathways present in the journey
    def all_journey_packs_data(card_ids)
      JourneyPackRelation
        .joins('
          INNER JOIN cards on cards.id = journey_pack_relations.cover_id AND
          journey_pack_relations.cover_id != journey_pack_relations.from_id AND
          cards.deleted_at IS NULL
        ')
        .where('cover_id in (?)', card_ids)
        .group('journey_pack_relations.cover_id')
        .select(:cover_id, 'count(cover_id) as count')
    end

    def last_journey_updates_data(card_ids)
      JourneyPackRelation
        .joins('INNER JOIN card_pack_relations on card_pack_relations.cover_id = journey_pack_relations.from_id')
        .where('journey_pack_relations.cover_id in (?)', card_ids)
        .group('journey_pack_relations.cover_id')
        .select('journey_pack_relations.cover_id, max(journey_pack_relations.created_at) as last_section_added,
                  max(card_pack_relations.created_at) as last_card_added')
    end

    def all_tags(card_ids)
      Tag
        .joins(:taggings)
        .where(taggings: { taggable_id: card_ids, taggable_type: 'Card' })
        .select('tags.id, tags.name, taggings.taggable_id')
        .group_by(&:taggable_id)
    end

    def card_subtype(card)
      card.card_subtype
    end

    def card_type(card)
      card.card_type
    end

    def created_at(card)
      card.created_at
    end

    def title(card)
      (card.message != card.title) ? card.title.try(:clean) : ''
    end

    def id(card)
      (card.id.nil? ? Card.card_ecl_id(card.ecl_id) : card.id.to_s)
    end

    def author(card)
      @authors[card.author_id]
    end

    def badging(card)
      @card_badgings[card.id]
    end

    # TODO: Need to simplify completed_percent
    def completed_percentage(card)
      card.completed_percent(@user)
    end

    def card_metadatum(card)
      return nil if @metadata[card.id].blank?
      {
        id: @metadata[card.id].id,
        plan: @metadata[card.id].plan
      }
    end

    def ecl_metadata(card)
      return {} if card.ecl_metadata.blank?
      {
        eclSourceDisplayName: card.ecl_metadata['source_display_name'],
        eclSourceTypeName:    card.ecl_metadata['source_type_name'],
        eclSourceLogoUrl:     card.ecl_metadata['source_logo_url'],
        eclSourceCpeCredits:  card.ecl_metadata['cpe_credits'],
        eclSourceCpeSubArea:  card.ecl_metadata['cpe_subject']
      }
    end

    def ecl_duration_metadata(card)
      if card.time_to_read # this condition is requried for on demand request.
        {
          calculated_duration: card.time_to_read,
          calculated_duration_display: TimeToWord.convert(time_in_sec: card.time_to_read,duration_format: @duration_format)
        }
      end
    end

    def ecl_id(card)
      card.ecl_id
    end

    def filestack(card)
      card_filestack(card, @user.id, @organization, @is_native_app)
    end

    def provider_image(card)
      return nil if card.provider_image.blank?
      @is_native_app ? card.provider_image : secured_url(card.provider_image, @user.id, @organization)
    end

    # FORMAT:
    # {
    #   leaps: {
    #     inPathways: [
    #       {
    #         id: ...,
    #         cardId: ...,
    #         pathwayId: ...,
    #         wrongId: ...,
    #         correctId: ...
    #       },
    #       {
    #         ...
    #       }
    #     ]
    #   }
    # }
    def leaps(card)
      leap_details = {}

      standalone_leap = @standalone_leaps[card.id]&.first
      if standalone_leap.present?
        standalone_leap_hash ={
          id:        standalone_leap.id,
          cardId:    standalone_leap.card_id,
          pathwayId: standalone_leap.pathway_id,
          wrongId:   standalone_leap.wrong_id,
          correctId: standalone_leap.correct_id
        }
        leap_details.merge!(standalone: standalone_leap_hash)
      end

      # All card leaps within a pathway.
      pathway_leaps = @pathway_leaps[card.id]
      if pathway_leaps.present?
        leap_details.merge!(inPathways: [])
        pathway_leaps.each do |leap|
          in_pathways_leap = {
            id:        leap.id,
            cardId:    leap.card_id,
            pathwayId: leap.pathway_id,
            wrongId:   leap.wrong_id,
            correctId: leap.correct_id
          }
          leap_details[:inPathways] << in_pathways_leap
        end
      end

      leap_details
    end

    def is_new(card)
      return unless @last_access_at && card.published?
      case card.card_type
      when 'pack'
        @last_packs_updates && @last_packs_updates[card.id] && (@last_packs_updates[card.id] > @last_access_at)
      when 'journey'
        @last_journey_updates && @last_journey_updates[card.id] && (@last_journey_updates[card.id] > @last_access_at)
      else
        card.published_at.to_i > @last_access_at
      end
    end

    # NOTE:
    #   Returns a boolean value.
    #   Decides whether the cards in an organization can be marked as completed.
    #   1. Either checks for `mark_feature_disabled` for a source in ecl_metadata.
    #   OR
    #   2. If a card is a SCORM card.
    def mark_feature_disabled_for_source(card)
      # #1 for ECL source
      disabled_for_source = card.ecl_metadata&.dig('mark_feature_disabled').to_s.to_bool

      # #2 SCORM card
      # .. Checks if resource has a SCORM URL.
      has_scorm_resource = @resources[card.resource_id] && @resources[card.resource_id].is_scorm_resource?
      # .. Checks if organization has feature enabled and filestack has `scorm_course` = TRUE.
      disabled_for_scorm_card = @auto_marked && (card.has_scorm_filestack? || has_scorm_resource)

      disabled_for_source || disabled_for_scorm_card
    end

    def skill_level(card)
      (@metadata[card.id]&.level || @cards_ratings[card.id])
    end

    # should_fetch_card_permissions?
    # Will return
      # return false for admin user
      # return false if there are no private cards
      # returns true if none of the above evaluate to true
    def should_fetch_card_permissions?(card_ids)
      private_cards = Card.where(id: card_ids, organization_id: @user.organization_id, is_public: false)
      # return false if there are no private cards
      return false if private_cards.blank?

      # returns true if none of the above evaluate to true
      return true
    end

    # NOTE: Lookup for prices will be successful only for physical cards and not for virtual/ECL cards.
    # Physical cards has prices in `prices` table.
    # ECL card just builds the prices object. No physical entries in `prices` table.
    def prices(card)
      # @prices[card.id] => For physical cards.
      # card.prices => For ECL/virtual cards.
      _prices = @prices[card.id] || card.prices
      _prices.map do |price|
        _entry = {
          id: price.id,
          amount: price.skillcoin? ? price.amount.round : price.amount,
          currency: price.currency,
          symbol: CURRENCY_TO_SYMBOL[price.currency] || '$'
        }
      end
    end

    # DESC:
    #   Returns cover_id and count of inner cards as the resultset.
    def pack_cards_count(card)
      @pack_cards_count[card.id] || 0
    end

    def readable_card_type(card)
      card.card_type_display_name
    end
    # DESC:
    #   Returns cover_id and count of inner cards as the resultset.
    def journey_packs_count(card)
      @journey_packs_count[card.id] || 0
    end

    def channels(card)
      _channels = @curated_channels[card.id]
      return [] if _channels.nil?

      _channels
    end

    def tags(card)
      _tags = @tags[card.id]
      return [] if _tags.nil?

      _tags
    end

    #### List all card-type specific logic: Starts here ####
    # Card type 1: ARTICLE
    # DESC:
    #   Either select secure image from the resource
    #   OR
    #   Pick one from the Stock Images.

    # NOTE: Lookup for a resource will be successful only for physical cards and not for virtual/ECL cards.
    # Physical cards has a resource in `resources` table.
    # ECL card just builds the resource object. No physical entries in `resources` table.
    def article(card)
      _resource = @resources[card.resource_id] || card.resource
      return if _resource.nil?

      image_url = _resource.image_url_secure || card_resource(card)
      video_url = _resource.video_url

      {
        id: _resource.id,
        imageUrl: @is_native_app ? image_url : secured_url(image_url, @user.id, @organization),
        title: _resource.title,
        description: _resource.description,
        url: _resource.url_with_context(@user.try(:id)),
        siteName: _resource.site_name,
        type: _resource.type,
        videoUrl: @is_native_app ? video_url : secured_url(video_url, @user.id, @organization),
        embedHtml: _resource.embed_html
      }
    end
    alias_method :resource, :article

    # Card type 2: QUIZ or ASSESSMENT
    # TODO:
    #   N+1 query.
    def quiz(card)
      return {} if card.card_type != 'poll'
      _options = []
      polls = {}
      card.quiz_question_options.each do |qqo|
        file_resource = qqo.file_resource
        option = {
          id: qqo.id,
          label: qqo.label,
          isCorrect: qqo.is_correct, # this key is used in web only.
          is_correct: qqo.is_correct, # this key is used in moblie as well as in web.
          image_url: file_resource && file_resource.file_url,
          count: card.quiz_question_stats.try(:options_stats).try(:[], qqo['id'].to_s) || 0
        }
        file_resource && option.merge!(fileResource: {
          id: file_resource.id,
          filestack: file_resource.filestack,
        })
        _options << option
        polls = Hash[_options.map{|a| [a[:id], a]}]
      end
      {
        quizQuestionOptions: _options,
        isAssessment: card.is_a_poll_question?,
        hasAttempted: @polls[card.id].present?,
        attemptedOption: polls[@polls[card.id]],
        attemptCount: card.quiz_question_stats.try(:attempt_count),
        canBeReanswered: card.can_be_reanswered
      }
    end

    # Card type 3: VIDEO STREAM
    def video_stream(card)
      _stream = @streams[card.id]
      return if _stream.nil?

      _recordings = @recordings[_stream.id]

      video_stream_hash = {
        id: _stream.id,
        imageUrl: _stream.image,
        status: _stream.status,
        uuid: _stream.uuid,
        playbackUrl: _stream.playback_url,
        embedPlaybackUrl: _stream.embed_playback_url,
        startTime: _stream.start_time,
        width: _stream.width,
        height: _stream.height
      }

      _recording = _recordings&.first

      past_stream_available = _stream.status == 'past' && _recording && (!_recording.hls_location.nil? || !_recording.mp4_location.nil?)
      if past_stream_available
        recording =  {
          id: _recording.id,
          hlsLocation: @is_native_app ? "https://#{Settings.aws_cdn_host}/#{_recording.hls_location}" : secured_video_stream(_recording.hls_location, @user.id, @organization),
          mp4Location: @is_native_app ? "https://#{Settings.aws_cdn_host}/#{_recording.mp4_location}" : secured_video_stream(_recording.mp4_location, @user.id, @organization)
        }
        video_stream_hash.merge!(recording: recording, watchers: @watchers_count[_stream.id])
      end

      video_stream_hash
    end
    #### List all card-type specific logic: Ends here ####

    def assignment(card)
      @assignments[card.id]
    end

    def paid_by_user(card)
      is_paid(card) && !!(@card_subscriptions[card.id] && @card_subscriptions[card.id].length > 0)
    end

    def payment_enabled(card)
      @source_details[card.ecl_metadata&.dig('source_id')]&.auth_api_info.present?
    end

    def views_count(card)
      id = card.id
      return 0 if id.blank? || Card.is_ecl_id?(id.to_s)
      key = Analytics::CardViewMetricsRecorder.card_view_count_cache_key(card)
      (
        Rails.cache.fetch(key) ||
        Analytics::CardViewMetricsRecorder.set_initial_card_view_count(card, key)
      )
    end

    def file_resources(card)
      _file_resources = @file_resources[card.id]
      return [] unless _file_resources.present?

      _file_resources.map do |file_resource|
        {
          id: file_resource.id,
          file_url: file_resource.file_url,
          filestack: file_resource.filestack
        }
      end
    end

    def channels(card)
      _channels = @curated_channels[card.id]
      return [] if _channels.nil?

      _channels
    end

    def channel_ids(card)
      @curated_channel_ids[card.id].presence || []
    end

    def comments_count(card)
      card.comments_count
    end

    def all_ratings(card)
      @cards_rating_counts[card.id].to_i
    end

    def auto_complete(card)
      card.auto_complete
    end

    def average_rating(card)
      @metadata[card.id]&.average_rating
    end

    def duration(card)
      card.duration
    end

    def hidden(card)
      card.hidden
    end

    def is_official(card)
      card.is_official
    end

    def is_upvoted(card)
      !!@votes[card.id]
    end

    def is_bookmarked(card)
      @bookmarks.include?(card.id)
    end

    def is_public(card)
      card.is_public
    end

    def completion_state(card)
      @user_content_completion[card.id]
    end

    def is_paid(card)
      card.is_paid
    end

    def is_reported(card)
      @reported.include?(card.id)
    end

    def is_assigned(card)
      !!@assignments[card.id]
    end

    def language(card)
      card.language
    end

    def provider(card)
      card.provider
    end

    def share_url(card)
      card_id = card.is_a?(Card) ? card.id : card
      ClientOnlyRoutes.show_insight_url_with_org(@organization, id: card_id)
    end

    def card_share_data(card_ids)
      card_shares = CardUserShare.where(card_id: card_ids, user_id: @user.id).select("card_id, shared_by_id")
      card_user_shares = Hash[card_shares.map{ |card_share| [card_share.card_id, card_share.shared_by_id] }]

      users = User.where(id: card_shares.map(&:shared_by_id)).select('id,first_name,last_name,email,handle')
      shared_by_users = Hash[UserBridge.new(users, user: @user, fields: 'id,name, handle').attributes.map{ |user| [user[:id], user] }]
      [card_user_shares, shared_by_users]
    end

    def show_restrict(card)
      assignments = !!@card_assignments[card.id]
      bookmarks = !!@card_bookmarks[card.id]
      user_content_completions = !!@card_content_completions[card.id]

      !(assignments || bookmarks || user_content_completions)
    end

    def completion_state(card)
      @user_content_completion[card.id]
    end

    # Restricted to these teams
    def teams_permitted(card)
      @teams_with_permission[card.id].presence || []
    end

    # Restricted to these users
    def users_permitted(card)
      @users_with_permission[card.id].presence || []
    end

    # These are teams with whom a card has been shared
    def teams(card)
      @teams[card.id].presence || []
    end

    # These are users with whom a card has been shared
    def users_with_access(card)
      @users_with_access[card.id].presence || []
    end

    def is_paid(card)
      is_pathway_or_journey = card.pack_card? || card.journey_card?
      is_pathway_or_journey ? CardMetadatum::PAID_PLANS.include?(@metadata[card.id].try(:plan)) : card.is_paid
    end

    def message(card)
      card.sanitize_message
    end

    def non_curated_channel_ids(card)
      @non_curated_channel_ids[card.id].presence || []
    end

    def published_at(card)
      card.published_at
    end

    def voters(card)
      @voters[card.id].presence || []
    end

    def user_rating(card)
      @user_rating[card.id]
    end

    def shared_by(card)
      _user_id = @card_user_shares[card.id]
      return if _user_id.nil?

      @shared_by_users[_user_id]
    end

    def pack_cards(card)
      # this is to override the card.pack_cards
      #this will always empty, if a user needs a pack_cards data then will request pathway/journey apis
      []
    end

    def rank(card)
      card.channel_card_rank
    end

    def project_id(card)
      @projects[card.id]
    end

    def state(card)
      card.state
    end

    def slug(card)
      card.slug
    end

    def votes_count(card)
      card.votes_count
    end
    #### On-demand methods: Ends here ####
end

# DEFAULT FIELDS:
#  1. id
#  2. amount
#  3. startDate
#  4. currency
#  5. createdAt
#  6. userId
# Refer method #default_fields.

class UserSubscriptionBridge < Bridges
  def attributes
    details = []

    # Start generating array of hashes that corresponds to list of user_subscriptions.
    @objects.each do |subscription|
      user_subscription_hash = {}

      user_subscription_hash.merge!(self.default_fields(subscription))

      details << user_subscription_hash
    end

    details
  end

  def default_fields(user_subscription)
    {
      id: user_subscription.id,
      amount: user_subscription.amount,
      startDate: user_subscription.start_date,
      currency: user_subscription.currency,
      createdAt: user_subscription.created_at,
      userId: user_subscription.user_id
    }
  end

  # NOTE: Not yet implemented.
  # DESC:
  #   This is for security reasons. Ensures none of the keys unless listed here must be transmitted.
  #   We do not want all of the methods/attributes defined in this class should be available.
  #   This will prevent the attackers from exploiting the API.
  def whitelisted_keys
    [
      :id, :amount, :start_date, :currency, :created_at
    ]
  end

  private
    #### DB queries: Starts here ####
    # ...
    #### DB queries: Ends here ####

    #### On-demand methods: Starts here ####
    # ...
    #### On-demand methods: Ends here ####
end

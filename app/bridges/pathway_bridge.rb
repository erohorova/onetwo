class PathwayBridge < Bridges
  include Api::V2::CardsHelper

  attr_accessor :cards

  def initialize(cards, user: nil, fields: '', options: {})
    super(cards, user: user, fields: fields)
    @pack_cards  = {}
    @pack_cards_fields = dig_pack_cards
    # Removing @pack_cards_fields from @association_fields because pack_cards_fields also contains on-demand teams, channels,tags fields if we not remove this fields than it
    # gives duplicate @team_fields, @channel_fields, @tag_fields
    @association_fields = @association_fields.select { |f| f[:name] != "pack_cards" }

    @options = options
  end

  def attributes
    details = []
    card_ids   = @objects.map(&:id).compact

    if @fields.include?('pack_cards')
      pack_cards = all_pack_cards(card_ids)
      _pack_cards = CardBridge.new(pack_cards.values.flatten.uniq, user: @user, fields: @pack_cards_fields, options: @options).attributes
      _pack_cards = _pack_cards.group_by { |card| card[:id] }

      #filtering unauthorized cards
      selected_private_children_cards = @user.private_accessible_cards(_pack_cards.keys)
      is_admin_user = @user.is_admin_user?
      pack_cards.each do |key , values|
        cover_card = @objects.select{ |card| card.id == key}.first
        values.each do |c|
          if !selected_private_children_cards.map(&:id).include?(c.id) && !is_admin_user && cover_card.author_id != @user.id
            _pack_cards[c.id.to_s].first.clear[:message] = 'You are not authorized to access this card'
          end
        end
      end

      pack_cards.each { |cover_id, cards| 
        @pack_cards[cover_id] = _pack_cards.slice(*cards.map(&:id).map(&:to_s)).values.flatten 
      }
      @fields.delete('pack_cards')
    end

    cover_fields = @resolved_fields.select { |f| f[:type] == :simple }
    cover_fields += @association_fields
    cover_cards = CardBridge.new(@objects, user: @user, fields: cover_fields, options: @options).attributes

    # Start generating array of hashes that corresponds to list of pathways.
    cover_cards.each do |cc|
      card_hash = cc
      if pack_cards
        card_hash.merge!(packCards: @pack_cards[cc[:id].to_i])
      end
      details << card_hash
    end
    details
  end

  # NOTE: Not yet integrated.
  # DESC:
  #   This is for security reasons. Ensures none of the keys unless listed here must be transmitted.
  #   We do not want all of the methods/attributes defined in this class should be available.
  #   This will prevent the attackers from exploiting the API.
  def whitelisted_keys
    []
  end

  private
    def all_pack_cards(card_ids)
      cards_hash = Card
        .joins('INNER JOIN card_pack_relations on cards.id = card_pack_relations.from_id')
        .where('card_pack_relations.cover_id in (?)', card_ids)
        .select('cards.*, card_pack_relations.id as cpr_id,card_pack_relations.cover_id,card_pack_relations.from_type, card_pack_relations.from_id, card_pack_relations.to_id, card_pack_relations.locked')
        .group_by(&:cover_id)

      #ordering the pack cards
      cards_hash.each do |key, values|
        first_relation = values.find{|r| r.from_id == key && r.from_type == 'Card'}
        ordered = [values.delete(first_relation)]
        until values.empty? do
          last_size = values.size
          values.delete_if {|reln| ordered << reln if reln.cpr_id == ordered.last.to_id }
          return ordered unless values.size < last_size
        end
        cards_hash[key] = ordered.reject { |cp_rel| cp_rel.from_id == key }
      end
      return cards_hash  
    end
end

# DYNAMIC ATTRIBUTES:
#   ...

# EXCLUDING ATTRIBUTES:
#   ...

# DEFAULT FIELDS:
#  1. id
#  2. timeZone
#  3. language
#  4. expertTopics
#  5. learningTopics
# Refer method #default_fields.

class UserProfileBridge < Bridges
  def attributes
    details = []

    @org_default_language = @organization.org_default_language
    
    # Start generating array of hashes that corresponds to list of user_profiles.
    @objects.each do |profile|
      profile_hash = {}
      if @fields.empty?
        profile_hash.merge!(self.default_fields(profile))
      else
        @fields.each do |field|
          # TODO:
          #   No support added yet. for dynamic resolution.
          authorized?(field) && profile_hash.merge!(field.to_camelcase_sym => self.send(field, profile))
        end
      end

      details << profile_hash
    end

    details
  end

  def default_fields(profile)
    {
      id: profile.id,
      timeZone: profile.time_zone,
      language: profile.language || @org_default_language,
      expertTopics: profile.expert_topics,
      learningTopics: profile.learning_topics,
      jobTitle: profile.job_title
    }
  end

  # DESC:
  #   This is for security reasons. Ensures none of the keys unless listed here must be transmitted.
  #   We do not want all of the methods/attributes defined in this class should be available.
  #   This will prevent the attackers from exploiting the API.
  def whitelisted_keys
    [
      :dob, :expert_topics, :job_title, :language,
      :learning_topics, :tac_accepted, :tac_accepted_at,
      :time_zone
    ]
  end

  private
    #### DB queries: Starts here ####
    # ...
    #### DB queries: Ends here ####

    #### On-demand methods: Starts here ####
    def dob(profile)
      profile.dob
    end

    def expert_topics(profile)
      profile.expert_topics
    end

    def job_title(profile)
      profile.job_title
    end

    def language(profile)
      profile.language || @org_default_language 
    end

    def learning_topics(profile)
      profile.learning_topics
    end

    def tac_accepted(profile)
      profile.tac_accepted
    end

    def tac_accepted_at(profile)
      profile.tac_accepted_at
    end

    def time_zone(profile)
      profile.time_zone
    end
    #### On-demand methods: Ends here ####
end

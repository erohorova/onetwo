# EXCLUDING ATTRIBUTES:
# 1. createdAt
# 2. updatedAt
# 3. organization

# DEFAULT FIELDS:
# 1. id
# 2. code
# 3. context
# 4. enabled
# 5. parent
# 6. creator
class WidgetBridge < Bridges
  def initialize(widgets, user: nil, fields: '')
    super(widgets, user: user, fields: fields)
    @creators = {}
  end

  # DESC:
  #   Responsible to iterate over all widgets and returns specific details.
  #   Default OR on-demand values.
  def attributes
    details = []

    if @fields.empty? || @fields.include?('creator')
      _creators = get_all_widget_creators
      minified_users = UserBridge.new(_creators, user: @user, fields: 'id,name,handle,is_suspended').attributes
      @creators = Hash[minified_users.map { |_creator| [_creator[:id], _creator] }]
    end

    # Start generating array of hashes that corresponds to list of widgets.
    @objects.each do |widget|
      widget_hash = {}
      if @fields.empty?
        widget_hash.merge!(self.default_fields(widget))
      else
        @fields.each do |field|
          authorized?(field) && widget_hash.merge!(field.to_camelcase_sym => self.send(field, widget))
        end
      end

      details << widget_hash
    end

    details
  end

  def default_fields(widget)
    _fields = {
      id: widget.id,
      code: widget.code,
      context: widget.context,
      enabled: widget.enabled,
      parent_id: widget.parent_id,
      parent_type: widget.parent_type,
      creator: creator(widget)
    }
  end

  # DESC:
  #   This is for security reasons. Ensures none of the keys unless listed here must be transmitted.
  #   We do not want all of the methods/attributes defined in this class should be available.
  #   This will prevent the attackers from exploiting the API.
  def whitelisted_keys
    [:id, :code, :context, :enabled, :parent, :creator]
  end

  private
  #### DB queries: Starts here ####

  def get_all_widget_creators
    creator_ids = @objects.map(&:creator_id)
    @organization.users.where(id: creator_ids)
  end

  #### DB queries: Ends here ####

  #### On-demand methods: Starts here ####

  def id(widget)
    widget.id
  end

  def code(widget)
    widget.code
  end

  def context(widget)
    widget.context
  end

  def enabled(widget)
    widget.enabled
  end

  def parent_id(widget)
    widget.parent_id
  end

  def parent_type(widget)
    widget.parent_type
  end

  def creator(widget)
    @creators[widget.creator_id]
  end

  #### On-demand methods: Ends here ####
end

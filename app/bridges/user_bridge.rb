# DYNAMIC ATTRIBUTES:
# 1. Active Orgs
# 2. csrfToken
# 3. csrfParam
# 4. currentSessionProvider
# 5. jwtToken
# 6. onboardingOptions
# 7. onboardingStepsNames

# EXCLUDING ATTRIBUTES:
# 1. pubnubChannels
# 2. signInCount
# 3. ssoInfo
# 4. writableChannels
# 5. defaultTeamId
# 6. followingUsers
# 7. newsletterOptIn
# 8. organization
# 9. groups
# 10. followingChannels
# 11. isSuspended
# 12. passwordResetRequired
# 13. bio
# 14. onboardingOptions
# 15. onboardingStepsNames
# 16. currentSessionProvider

# DEFAULT FIELDS:
# 1. id
# 2. email
# 3. fullName
# 4. isAdmin
# 5. isSuperAdmin
# 6. name
# 7. orgAnnualSubscriptionPaid
# 8. picture
# 9. organization
# 10. permissions
# 12. profile
# Refer method #default_fields.

class UserBridge < Bridges
  def initialize(users, user: nil, fields: '')
    super(users, user: user, fields: fields)

    @minimal_organization = {}
    @observers            = {}
    @profiles             = {}
    @roles                = {}
    @subscriptions        = {}
    @teams_users_ids      = {}
    @directly_following_channel_ids = {}
    @onboarding_completed_dates = {}
    @channel_fields = dig_channels
    @profile_fields = dig_profile
  end

  def load_profiles?
    (@fields.empty? || @fields.include?('profile') ||
        @fields.include?('expert_skills') || @fields.include?('job_title') ||
        @fields.include?('dashboard_info') || @fields.include?('company'))
  end

  def attributes
    details = []

    user_ids  = @objects.map(&:id).compact
    @minimal_organization = organization_details

    if load_profiles?
      profiles  = UserProfile.where(user_id: user_ids)
      @profiles = Hash[profiles.collect { |item| [item['user_id'], item] }]
    end

    @fields.include?('roles') && @roles = all_roles(user_ids).group_by(&:user_id)

    if @fields.include?('is_following')
      observers = all_observers(user_ids)
      @observers = Hash[observers.collect { |item| [item['followable_id'], item['is_followed']] }]
    end

    if @fields.include?('user_subscriptions')
      _subscriptions = UserSubscription.where(organization_id: @organization.id, user_id: user_ids)
      minified_subscriptions = UserSubscriptionBridge.new(_subscriptions, user: @user).attributes
      @subscriptions = Hash[minified_subscriptions.map { |_metadata| [_metadata[:userId], _metadata] }]
    end

    if @fields.include?('onboarding_completed_date')
      _user_onboardings = UserOnboarding.where(user_id: user_ids)
      @onboarding_completed_dates = Hash[_user_onboardings.collect { |item| [item['user_id'], item.completed_at ] }]
    end

    # This gives channels ids of channels which are followed by this user.
    if @fields.include?('channel_ids')
      @directly_following_channel_ids = Follow
        .where(user_id: user_ids, followable_type: 'Channel')
        .select(:user_id, :followable_id)
        .group_by(&:user_id)
        .each { |user_id, dfc| dfc.map!(&:followable_id) }
    end

    if @fields.include?('team_ids')
      @teams_users_ids = TeamsUser
        .where(user_id: user_ids)
        .select(:team_id, :user_id)
        .group_by(&:user_id)
        .each { |user_id, tu| tu.map!(&:team_id) }
    end

    # Start generating array of hashes that corresponds to list of users.
    @objects.each do |user|
      user_hash = {}
      if @fields.empty?
        user_hash.merge!(self.default_fields(user))
      else
        @fields.each do |field|
          authorized?(field) && user_hash.merge!(field.to_camelcase_sym => self.send(field, user))
        end
      end

      details << user_hash
    end

    details
  end

  def default_fields(user)
    {
      id:           user.id,
      email:        user.email,
      fullName:     user.full_name,
      isAdmin:      user.is_admin_user?,
      isSuperAdmin: user.is_admin?,
      handle:         user.handle,
      orgAnnualSubscriptionPaid: subscribed_to_org?(user),
      picture:      picture(user),
      organization: @minimal_organization,
      permissions:  user.get_all_user_permissions,
      # TODO: This is somehow mandatory. Figure this out.
      profile:      profile(user)
    }
  end

  # DESC:
  #   This is for security reasons. Ensures none of the keys unless listed here must be transmitted.
  #   We do not want all of the methods/attributes defined in this class should be available.
  #   This will prevent the attackers from exploiting the API.
  def whitelisted_keys
    [
      :avatarimages, :bio, :channel_ids, :coverimages, :created_at,
      :current_sign_in_at, :default_team_id, :email, :expert_skills,
      :first_name, :followers_count, :following_channels, :following_count,
      :full_name, :handle, :hide_from_leaderboard, :id, :is_complete,
      :is_following, :is_suspended, :last_name, :name, :picture, :profile,
      :roles, :roles_default_names, :status, :subscribed_to_org, :team_ids,
      :user_subscriptions, :writable_channels, :onboarding_completed_date, :job_title,
      :dashboard_info, :company
    ]
  end

  private
    #### DB queries: Starts here ####
    def all_observers(user_ids)
      Follow
        .joins("INNER JOIN users on users.id = follows.followable_id and follows.followable_type = 'User'")
        .where('follows.user_id = ? and follows.followable_id in (?) and follows.deleted_at is NULL', @user, user_ids)
        .select("follows.followable_id as followable_id, (CASE WHEN follows.id THEN TRUE ELSE FALSE END) as is_followed")
    end

    # DESC:
    #   A user has roles via user_roles.
    #   So, an INNER JOIN to pluck only name of roles.
    def all_roles(user_ids)
      Role
        .joins('INNER JOIN user_roles on roles.id = user_roles.role_id')
        .where('user_roles.user_id in (?) and roles.organization_id = ?', user_ids, @organization.id)
        .select('user_roles.user_id, roles.name, roles.default_name')
    end

    def organization_details
      _details = OrganizationBridge.new([@organization], fields: 'host_name,image_url').attributes[0]

      # TODO:
      #   Most of the config keys in the organisation are still underscored instead of camelcase.
      #   Dependency still remains with the case now. Keeping as is and marking as `To Be Refactored`.
      _details.merge!('web_session_timeout' => @organization.web_session_timeout)
      _details.merge!('inactive_web_session_timeout' => @organization.inactive_web_session_timeout)
    end
    #### DB queries: Ends here ####

    #### On-demand methods: Starts here ####
    def avatarimages(user)
      user.fetch_avatar_urls_v2
    end

    def onboarding_completed_date(user)
      @onboarding_completed_dates[user.id]
    end

    def bio(user)
      user.bio
    end

    def coverimages(user)
      user.fetch_coverimage_urls
    end

    def created_at(user)
      user.created_at
    end

    def current_sign_in_at(user)
      user.current_sign_in_at
    end

    def default_team_id(user)
      user.default_team_id
    end

    def email(user)
      user.email
    end

    def expert_skills(user)
      _profile = @profiles[user.id]
      _profile&.expert_topics
    end

    def dashboard_info(user)
      _profile = @profiles[user.id]
      _profile&.dashboard_info
    end

    def company(user)
      _profile = @profiles[user.id]
      _profile&.company
    end

    def job_title(user)
      _profile = @profiles[user.id]
      _profile&.job_title
    end

    def first_name(user)
      user.first_name
    end

    # TODO:
    #   N+1 query
    def followers_count(user)
      user.followers_count
    end

    # TODO:
    #   N+1 query
    def following_channels(user)
      _channels = user.followed_channels

      ChannelBridge.new(_channels, user: @user, fields: 'id').attributes
    end

    # TODO:
    #   N+1 query
    def following_count(user)
      user.following_count
    end

    def full_name(user)
      user.name
    end
    alias_method :name, :full_name

    def handle(user)
      user.handle
    end

    def hide_from_leaderboard(user)
      user.hide_from_leaderboard
    end

    def id(user)
      user.id
    end

    def is_complete(user)
      user.is_complete
    end

    def is_following(user)
      @observers[user.id].to_s.to_bool
    end

    def is_suspended(user)
      user.suspended?
    end

    def last_name(user)
      user.last_name
    end

    def picture(user)
      user.photo(:medium)
    end

    def profile(user)
      _profile = @profiles[user.id]

      _profile ? UserProfileBridge.new([_profile], user: user, fields: @profile_fields).attributes[0] : nil
    end

    # DESC:
    #   Though the information of roles are captured in `roles` and `user_roles`.
    #   We still consider `organization#organization_role` as a valid one.
    def roles(user)
      # .map to prevent extra db calls with .pluck.
      _user_roles = (@roles[user.id] || []).map(&:name)
    end

    def roles_default_names(user)
      (@roles[user.id] || []).map(&:default_name)
    end

    def status(user)
      user.status
    end

    # TODO:
    #   N+1 query.
    def subscribed_to_org?(user)
      UserSubscription.paid_annual_org_subscription?(org_id: @organization.id, user_id: user.id)
    end

    # DESC:
    #   Subscriptions for each user.
    def user_subscriptions(user)
      @subscriptions[user.id]
    end

    # TODO:
    #   N+1 query.
    def writable_channels(user)
      _channels = user.writable_channels
      fields = @channel_fields.present? ? @channel_fields : 'id,slug,label'
      ChannelBridge.new(_channels, user: @user, fields: fields).attributes
    end

    # This method return channel ids which are directly followed by the user
    def channel_ids(user)
      @directly_following_channel_ids[user.id].presence || []
    end

    # Team ids to which user belongs
    def team_ids(user)
      @teams_users_ids[user.id].presence || []
    end
    #### On-demand methods: Ends here ####
end
